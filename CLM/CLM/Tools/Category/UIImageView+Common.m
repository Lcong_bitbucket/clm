//
//  UIImageView+Common.m
//  xcwl
//
//  Created by 聪 on 16/4/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "UIImageView+Common.h"

@implementation UIImageView (Common)
- (void)setImageURLStr:(NSString *)urlString placeholder:(UIImage *)image{
    if(urlString.length<= 5){
        urlString = @"";
    }
    [self sd_setImageWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:image];
}

- (void)setImageWithURLStr:(NSString *)urlString placeholderImage:(UIImage *)placeholder completed:(UIImageViewCompletionBlock)completedBlock{
    [self sd_setImageWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(completedBlock){
            completedBlock(image,error);
        }
    }];
}
@end
