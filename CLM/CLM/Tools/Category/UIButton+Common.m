//
//  UIButton+Common.m
//  xcwl
//
//  Created by 聪 on 16/4/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "UIButton+Common.h"

@implementation UIButton (Common)
- (void)setImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder{
    [self sd_setImageWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forState:state placeholderImage:placeholder];
}
- (void)setBackgroundImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder {
    [self sd_setBackgroundImageWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forState:state placeholderImage:placeholder];
}

- (void)setImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder completed:(UIbuttonCompletionBlock)completedBlock{
    [self sd_setImageWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forState:state placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(completedBlock){
            completedBlock(image,error);
        }
    }];
}

- (void)setBackgroundImageWithURLString:(NSString *)urlString forState:(UIControlState)state placeholderImage:(UIImage *)placeholder completed:(UIbuttonCompletionBlock)completedBlock{
    [self sd_setBackgroundImageWithURL:[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forState:state placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(completedBlock){
            completedBlock(image,error);
        }
    }];
}
@end
