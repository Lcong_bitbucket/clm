//
//  NSString+Common.h
//  CLM
//
//  Created by cong on 16/12/8.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Common)
+(NSString *)mainBundlePath:(NSString *)name;
///返回根目录路径 "document"
+ (NSString*)getDocumentPath;
///返回 "document/dir/" 文件夹路径
+ (NSString*)getDirectoryForDocuments:(NSString*)dir;
///返回 "document/filename" 路径
+ (NSString*)getPathForDocuments:(NSString*)filename;
///返回 "document/dir/filename" 路径
+ (NSString*)getPathForDocuments:(NSString*)filename inDir:(NSString*)dir;
///文件是否存在
+ (BOOL)isFileExists:(NSString*)filepath;
///删除文件
+ (BOOL)deleteWithFilepath:(NSString*)filepath;
///返回该文件目录下 所有文件名
+ (NSArray*)getFilenamesWithDir:(NSString*)dir;

@end
