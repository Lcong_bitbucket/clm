//
//  CommonUtility.h
//  SandBayCinema
//
//  Created by Rayco on 12-11-1.
//  Copyright (c) 2012年 Apps123. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import <SDWebImage/UIImage+GIF.h>
#import "XCSupplyViewController.h"
#import "XCSupplysDetailViewController.h"
#import "XCDemandViewController.h"
#import "XCDemandsDetailViewController.h"
#import "XCNewsViewController.h"
#import "XCNewsDetailViewController.h"
#import "XCPublishSupplyViewController.h"
#import "XCPublishDemandViewController.h"
#import "XCCommonUrlViewController.h"
#import "XCZixunViewController.h"
#import "XCMessageViewController.h"
#import "XCMessageDetailViewController.h"
#import "XCUserManager.h"
#import "XCZhuanjiaViewController.h"
#import "XCZhuanJiaDetailViewController.h"
#import "XCZhuanjiaApplyViewController.h"
#import "XCPublishJiGouViewController.h"
#import "XCJiGouViewController.h"
#import "XCJiGouDetailViewController.h"
#import "ChatViewController.h"
#import "XCApplyOrderViewController.h"
#import "XCLoginViewController.h"
#import "XCMeetingViewController.h"
#import "XCMeetingDetailViewController.h"
#import "EnroolMeetingViewController.h"
#import "XCPublishCardViewController.h"
#import "XCPublishDeviceViewController.h"

#import "XCQuestionsViewController.h"
#import "XCQuestionDetailViewController.h"
#import "XCPublishQuestionViewController.h"

#import "XCDevicesViewController.h"
#import "XCDeviceDetailViewController.h"

CG_INLINE void setViewFrameSizeWidth(UIView *v ,float w) {
    CGRect frame = v.frame;
    frame.size.width = w;
    v.frame = frame;
}

CG_INLINE AppDelegate* app_delegate() {
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

CG_INLINE UIColor *getHexColor(NSString *hexColor) {
    if(!hexColor || [hexColor isEqualToString:@""] || [hexColor length] < 7){
        if (hexColor.length != 4) {
            return [UIColor whiteColor];
        }
    }
    
    if (hexColor.length == 4) {
        hexColor = [NSString stringWithFormat:@"#%c%c%c%c%c%c",[hexColor characterAtIndex:1],[hexColor characterAtIndex:1],[hexColor characterAtIndex:2],[hexColor characterAtIndex:2],[hexColor characterAtIndex:3],[hexColor characterAtIndex:3]];
    }
    
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 1;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
    
    range.location = 3;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
    
    range.location = 5;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}
CG_INLINE CGSize GetStringSize(NSString *str,float fontSize,NSString *fontName,CGSize formatSize) {
    UIFont *font;
    if (fontName && fontName.length > 1) {
        font = [UIFont fontWithName:fontName size:fontSize];
    }
    else {
        font = [UIFont systemFontOfSize:fontSize];
    }
    
    CGSize size = formatSize;
    CGSize labelsize = CGSizeZero;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        NSDictionary *attributes = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paragraphStyle.copy};
        labelsize = [str boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
    }
    else {
        labelsize = [str sizeWithFont:font
                    constrainedToSize:size
                        lineBreakMode:NSLineBreakByWordWrapping];
    }
    
    return labelsize;
}

CG_INLINE BOOL isValidString(id obj) {
    if (!obj) {
        return NO;
    }
    if ([obj isKindOfClass:NSClassFromString(@"NSString")]) {
        if ([obj length] > 0) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - MBHUD
CG_INLINE void showHint(NSString *hint){
    if(hint.length == 0 || !hint){
        return;
    }
    UIView *view = [[UIApplication sharedApplication].delegate window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.userInteractionEnabled = NO;
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = hint;
    hud.margin = 10.f;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    //    hud.yOffset = iPhone5()?200.f:150.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3];
}

CG_INLINE void showHUD(NSString *hint,UIView *view){
    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:view];
    HUD.labelText = hint;
    [view addSubview:HUD];
    HUD.bezelView.backgroundColor = [UIColor blackColor];
    HUD.contentColor = [UIColor whiteColor];
    [HUD show:YES];
}

CG_INLINE void showHUDCustomView(UIView *view,NSString *hint,NSString *imgName,NSInteger delay){
    // 显示到主窗口中
    UIImage  *image=[UIImage sd_animatedGIFNamed:imgName];
    UIImageView  *gifview=[[UIImageView alloc]initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
    gifview.image=image;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.color=[UIColor clearColor];//默认颜色太深了
    hud.mode = MBProgressHUDModeCustomView;
    hud.labelText = hint;
    hud.customView=gifview;
    if(delay)
        [hud hide:YES afterDelay:delay];
}



CG_INLINE void showHUDDefaultCustomView(UIView *view,NSString *hint){
    showHUDCustomView(view,hint,@"loading1",0);
}

CG_INLINE void hidHUDCustomView(UIView *view){
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

CG_INLINE void openImageWithZLPhotoLib(NSInteger index,NSMutableArray *imageArray){
    
    NSMutableArray *_photos = [NSMutableArray new];
    for (int i = 0; i < imageArray.count;i++) {
        ZLPhotoPickerBrowserPhoto *photo = [[ZLPhotoPickerBrowserPhoto alloc] init];
        if([imageArray[i] isKindOfClass:[UIImage class]]){
            photo.photoImage = imageArray[i];
        } else {
            photo.photoURL = [NSURL URLWithString:ImgUrl(imageArray[i])];
        }
        [_photos addObject:photo];
    }
    
    ZLPhotoPickerBrowserViewController *pickerBrowser = [[ZLPhotoPickerBrowserViewController alloc] init];
    // 淡入淡出效果
    // pickerBrowser.status = UIViewAnimationAnimationStatusFade;
    // 数据源/delegate
    pickerBrowser.photos = _photos;
    // 当前选中的值
    pickerBrowser.currentIndex = index;
    // 展示控制器
    [pickerBrowser showPickerVc:app_delegate().window.rootViewController];
}


#pragma mark - webview
CG_INLINE NSMutableArray * setDefaultJSEvent(id webView){
    //添加长按
    //    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    //    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    
    
    if([webView isKindOfClass:[WKWebView class]]){
        //获取图片数组
        static  NSString * const jsGetImages = @"function getImages(){\
        var objs = document.getElementsByTagName(\"img\");\
        var imgScr = '';\
        for(var i=0;i<objs.length;i++){\
        imgScr = imgScr + objs[i].src + '+';\
        };\
        return imgScr;\
        };";
        [webView evaluateJavaScript:jsGetImages completionHandler:^(id result, NSError * _Nullable error) {
            
        }];
        __block NSMutableArray *imageArr = [NSMutableArray new];
        [webView evaluateJavaScript:@"getImages()" completionHandler:^(id urlResurlt, NSError * _Nullable error) {
            [imageArr addObjectsFromArray:[urlResurlt componentsSeparatedByString:@"+"]];
            if (imageArr.count >= 2) {
                [imageArr removeLastObject];
            }
        }];
        
        [webView evaluateJavaScript:@"function registerImageClickAction(){\
         var imgs=document.getElementsByTagName('img');\
         var length=imgs.length;\
         for(var i=0;i<length;i++){\
         img=imgs[i];\
         var p = img.parentNode;\
         var tag = p.tagName;\
         if(tag .toUpperCase() != 'A'){\
         img.onclick=function(){\
         window.location.href='xcshowimg:'+this.src}\
         }\
         }\
         }" completionHandler:^(id result, NSError * _Nullable error) {
             
         }];
        
        [webView evaluateJavaScript:@"registerImageClickAction();" completionHandler:^(id result, NSError * _Nullable error) {
            
        }];
        return imageArr;
        
    } else {
        //获取图片数组
        static  NSString * const jsGetImages = @"function getImages(){\
        var objs = document.getElementsByTagName(\"img\");\
        var imgScr = '';\
        for(var i=0;i<objs.length;i++){\
        imgScr = imgScr + objs[i].src + '+';\
        };\
        return imgScr;\
        };";
        [webView stringByEvaluatingJavaScriptFromString:jsGetImages];//注入js方法
        NSString *urlResurlt = [webView stringByEvaluatingJavaScriptFromString:@"getImages()"];
        NSMutableArray* imageArray = [NSMutableArray arrayWithArray:[urlResurlt componentsSeparatedByString:@"+"]];
        if (imageArray.count >= 2) {
            [imageArray removeLastObject];
        }
        
        
        //添加图片可点击js
        [webView stringByEvaluatingJavaScriptFromString:@"function registerImageClickAction(){\
         var imgs=document.getElementsByTagName('img');\
         var length=imgs.length;\
         for(var i=0;i<length;i++){\
         img=imgs[i];\
         var p = img.parentNode;\
         var tag = p.tagName;\
         if(tag .toUpperCase() != 'A'){\
         img.onclick=function(){\
         window.location.href='xcshowimg:'+this.src}\
         }\
         }\
         }"];
        [webView stringByEvaluatingJavaScriptFromString:@"registerImageClickAction();"];
        
        return imageArray;
    }
}
CG_INLINE void openTel(NSString *urlStr){
    
    NSString *tel = urlStr;
    NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",tel];
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:str]])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    
}

CG_INLINE void openAppURL(NSString *urlStr,BOOL isPrompt){
    NSString *str = ImgUrl(urlStr);
  
    if(isPrompt){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"是否打开网址" message:urlStr delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
                if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:str]]){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                }
            }
        }];
        [alert show];
    } else {
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:str]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
    }
    
    
}


//0不跳转，1跳site_url，2跳content，3项目，4需求， 5资讯，6发布项目，7发布需求 8我要咨询（项目或需求）　9我要咨询机构　10预约专家　11我的消息　12专家　13机构 14发布专家　　15发布机构

CG_INLINE void goADPage(NSDictionary *dic,UIViewController *pushvc){
    

    UIViewController *rootVC = [[[[UIApplication sharedApplication]delegate]window]rootViewController];
    
    if([dic[@"jumptype"] integerValue] == 0) {
        
    } else if([dic[@"jumptype"] integerValue] == 1) {
        XCCommonUrlViewController *vc = [[XCCommonUrlViewController alloc] init];
        vc.link_url = dic[@"site_url"];
        vc.title = dic[@"title"];
        vc.hidesBottomBarWhenPushed=YES;
        
        if(pushvc == nil){
               UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }

    } else if([dic[@"jumptype"] integerValue] == 2) {
        XCCommonUrlViewController *vc = [[XCCommonUrlViewController alloc] init];
        vc.link_url = dic[@"content"];
        vc.title = dic[@"title"];
        vc.hidesBottomBarWhenPushed=YES;

        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }

    } else if([dic[@"jumptype"] integerValue] == 3) {
        if([dic[@"article_id"] integerValue] == 0) {
            XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            XCSupplysDetailViewController * vc=[[XCSupplysDetailViewController alloc]init];
            vc.hidesBottomBarWhenPushed=YES;
             vc.id = [dic[@"article_id"] integerValue];
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
    } else if([dic[@"jumptype"] integerValue] == 4) {
        if([dic[@"article_id"] integerValue] == 0) {
            XCDemandViewController *vc = [[XCDemandViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            XCDemandsDetailViewController * vc=[[XCDemandsDetailViewController alloc]init];
            vc.hidesBottomBarWhenPushed=YES;
            vc.id = [dic[@"article_id"] integerValue];
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
    } else if([dic[@"jumptype"] integerValue] == 5) {
        if([dic[@"article_id"] integerValue] == 0) {
            XCNewsViewController *vc = [[XCNewsViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            XCNewsDetailViewController * vc=[[XCNewsDetailViewController alloc]init];
            vc.hidesBottomBarWhenPushed=YES;

            vc.id = [dic[@"article_id"] integerValue];
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
    } else if([dic[@"jumptype"] integerValue] == 6) {
        XCPublishSupplyViewController *vc = [[XCPublishSupplyViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;

        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    }else if([dic[@"jumptype"] integerValue] == 7) {
        XCPublishDemandViewController *vc = [[XCPublishDemandViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;

        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    } else if([dic[@"jumptype"] integerValue] == 8){
        XCZixunViewController *vc = [[XCZixunViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        
        vc.zixunTitle = dic[@"title"];
        vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    }else if([dic[@"jumptype"] integerValue] == 9){
        XCZixunViewController *vc = [[XCZixunViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;

        vc.zixunTitle = dic[@"title"];
        vc.type = 1;
        vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    } else if([dic[@"jumptype"] integerValue] == 10){
        XCZixunViewController *vc = [[XCZixunViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;

        vc.zixunTitle = dic[@"title"];
        vc.type = 2;
        vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    }else if([dic[@"jumptype"] integerValue] == 11){
      
        if([dic[@"article_id"] integerValue] == 0) {
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:nil success:^(BOOL isSuccess) {
                XCMessageViewController *vc = [[XCMessageViewController alloc] init];
                vc.hidesBottomBarWhenPushed=YES;

                if(pushvc == nil){
                    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                    [rootVC presentViewController:nav animated:YES completion:nil];
                } else {
                    [pushvc.navigationController pushViewController:vc animated:YES];
                }
            }];
        }
        else {
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:nil success:^(BOOL isSuccess) {
                XCMessageDetailViewController  *vc = [[XCMessageDetailViewController alloc] init];
                vc.hidesBottomBarWhenPushed=YES;

                vc.id = [dic[@"article_id"] integerValue];
                if(pushvc == nil){
                    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                    [rootVC presentViewController:nav animated:YES completion:nil];
                } else {
                    [pushvc.navigationController pushViewController:vc animated:YES];
                }
            }];
        }
    }else if([dic[@"jumptype"] integerValue] == 12){
        
        if([dic[@"article_id"] integerValue] == 0) {
            XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            XCZhuanJiaDetailViewController *vc = [[XCZhuanJiaDetailViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            vc.id = [dic[@"article_id"] integerValue];
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
    }else if([dic[@"jumptype"] integerValue] == 13){

        if([dic[@"article_id"] integerValue] == 0) {
            XCJiGouViewController *vc = [[XCJiGouViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            XCJiGouDetailViewController *vc = [[XCJiGouDetailViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;

            vc.id = [dic[@"article_id"] integerValue];
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
    }else if([dic[@"jumptype"] integerValue] == 14){
        XCZhuanjiaApplyViewController *vc = [[XCZhuanjiaApplyViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;

        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
      
    } else if([dic[@"jumptype"] integerValue] == 15){
        
        XCPublishJiGouViewController *vc = [[XCPublishJiGouViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;

        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    } else if([dic[@"jumptype"] integerValue] == 16){
       [[XCUserManager sharedInstance] presentloginVcIfNotSign:pushvc success:^(BOOL isSuccess) {
            
           ChatViewController* vc = [[ChatViewController alloc] initWithConversationChatter:[NSString stringWithFormat:@"%ld",[dic[@"article_id"] integerValue]] conversationType:EMConversationTypeChat];
           vc.promptInfo = dic[@"content"];
           vc.hidesBottomBarWhenPushed=YES;

           if(pushvc == nil){
               UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
               [rootVC presentViewController:nav animated:YES completion:nil];
           } else {
               [pushvc.navigationController pushViewController:vc animated:YES];
           }
           
       }];
      
    }else if([dic[@"jumptype"] integerValue] == 17){
        
            XCLoginViewController *vc = [[XCLoginViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;
            
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        
    }else if([dic[@"jumptype"] integerValue] == 18){
        XCApplyOrderViewController *vc = [[XCApplyOrderViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        vc.zixunTitle = dic[@"title"];
        vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
        
    }else if([dic[@"jumptype"] integerValue] == 19){
        if([dic[@"article_id"] integerValue] == 0) {
            XCMeetingViewController *vc = [[XCMeetingViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;
             if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            XCMeetingDetailViewController *vc = [[XCMeetingDetailViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;
            
            vc.id = [dic[@"article_id"] integerValue];
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
    }else if([dic[@"jumptype"] integerValue] == 20){
       
        EnroolMeetingViewController *vc = [[EnroolMeetingViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        vc.imgurl = dic[@"img_url"];
        vc.ID = dic[@"article_id"];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
    }else if([dic[@"jumptype"] integerValue] == 21){
        XCPublishCardViewController *vc = [[XCPublishCardViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
         vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
        
    }else if([dic[@"jumptype"] integerValue] == 18){
        XCPublishDeviceViewController *vc = [[XCPublishDeviceViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
         vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
        
    }else if([dic[@"jumptype"] integerValue] == 23){
        XCApplyOrderViewController *vc = [[XCApplyOrderViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
        vc.type = 1;
        vc.zixunTitle = dic[@"title"];
        vc.id = [dic[@"article_id"] integerValue];
        if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
        
    }else if([dic[@"jumptype"] integerValue] == 24){
        if([dic[@"article_id"] integerValue] == 0) {
            XCQuestionsViewController *vc = [[XCQuestionsViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        } else {
            XCQuestionDetailViewController *vc = [[XCQuestionDetailViewController alloc] init];
            vc.id = [dic[@"article_id"] integerValue];
            vc.hidesBottomBarWhenPushed=YES;
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        
    }else if([dic[@"jumptype"] integerValue] == 25){
        XCPublishQuestionViewController *vc = [[XCPublishQuestionViewController alloc] init];
        vc.hidesBottomBarWhenPushed=YES;
         if(pushvc == nil){
            UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
            [rootVC presentViewController:nav animated:YES completion:nil];
        } else {
            [pushvc.navigationController pushViewController:vc animated:YES];
        }
        
    }else if([dic[@"jumptype"] integerValue] == 26){
        if([dic[@"article_id"] integerValue] == 0) {
            XCDevicesViewController *vc = [[XCDevicesViewController alloc] init];
            vc.hidesBottomBarWhenPushed=YES;
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        } else {
            XCDeviceDetailViewController *vc = [[XCDeviceDetailViewController alloc] init];
            vc.id = [dic[@"article_id"] integerValue];
            vc.hidesBottomBarWhenPushed=YES;
            if(pushvc == nil){
                UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
                [rootVC presentViewController:nav animated:YES completion:nil];
            } else {
                [pushvc.navigationController pushViewController:vc animated:YES];
            }
        }
        
    } else if([dic[@"jumptype"] integerValue] == 100){
        if([dic[@"article_id"] integerValue] == 0){
            NSURL *url = [NSURL URLWithString:@"newmaterial2501://"];
             if ([[UIApplication sharedApplication] canOpenURL:url]) {
                 [[UIApplication sharedApplication] openURL:url];
            } else {
                openAppURL(dic[@"site_url"], NO);
            }
        } else if([dic[@"article_id"] integerValue] == 1){
            NSURL *url = [NSURL URLWithString:@"xcwl2501://"];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                openAppURL(dic[@"site_url"], NO);
            }
        } else if([dic[@"article_id"] integerValue] == 2){
            NSURL *url = [NSURL URLWithString:@"clm2501://"];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                openAppURL(dic[@"site_url"], NO);
            }
        }
        
    }
    
 
   
}
