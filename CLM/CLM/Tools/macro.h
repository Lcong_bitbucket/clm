//
//  static.h
//  VKTemplateForiPhone
//
//  Created by Vescky on 14-9-4.
//  Copyright (c) 2014年 Vescky. All rights reserved.
//

#ifndef VKTemplateForiPhone_static_h
#define VKTemplateForiPhone_static_h

#define KEY_WINDOW  [[UIApplication sharedApplication]keyWindow]
#define Height [UIScreen mainScreen].bounds.size.height
#define Width [UIScreen mainScreen].bounds.size.width
#define Screen_Height MAX(Height, Width)
#define Screen_Width MIN(Height, Width)

#define UTF8(url) [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]

//支付宝配置
#define Alipay_Partner_ID @"2088911440689655"
#define Alipay_Saller_ID @"85693899@qq.com"
#define Alipay_Private_Key @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALl5KQwqd+R10K+9mycjBvDnANh0xu0y32WKKf4BCAAu66ON3FZd+bWFXIeQVNSxb7okYrS34pgs755tLp56Qi0LE0FBiHIfXC4iKmgcMAm8RmZxIcEIjNSM8bn3p+J0hinGro3Ci+0qChpx1PqYtFToqXfdGv6xsZwOLB0Bs1ZLAgMBAAECgYAom8OF+2Jmrfj0EXJ//iZN5nYD7oWDd6j75WVOTNGAoMVM+QluC4NZ4AojUFGShArvWXoXEU/We7g3GrajUidcr4WsLQP/gEllx/1Q+a4CzupSOYhgJLk86Ep0IOvNbv51MeWYjHT+QWjONH5/NP3mhQyfD06ykvFKcwHzRdwEAQJBAO79PpUqd/Evyu1IuLfHrbs9y/CLluo37Hh2xu9mQfx5pd5BTPzYTbMQehXQ5cEBf+Ci5nqgnkBgbMq72zUSwgECQQDGrMWgiRJirmAmG4aycbwrC/kUcRjGedgMpELz/YDylSNUFTe+abHw2HwZ66UGR4cIE6Aj8yTzDxzPgDOsNIBLAkEA2FjpDqAjdHOPgL8UeUqKO7b1qAy10dJOf78G2DH6ClUVkt4Kk9o8fJ2t55H0a4Sv/ut5OgmtORoRYnYtOx6AAQJAao9ahItgmBstKONUsLs+ENygIS2z+yh5D/a+jn1mvzXst/mVZ4TrUuHaVlDEt4R/0X4tQ3rzU4bEjisWDAQw1wJAaI1IeN7RiZbDOe7aH0u71jCArokYPJjcjXY63hFX4cZ4jywjliNO4UiEMl2LsxifQPaYFNqc58bFM5Ffn7zsRg=="

//推送
#define JPushAppKey @"3e4d95c2cf49515e7b07fa5f"
#define JPushChannel @"appStore"

//通知中心
#define ReloadUserModelNotification @"ReloadUserModelNotification"
//环信
#define kSDKUsername   @"username"

#pragma mark - some keys

//友盟分享
#define UMSHARE_KEY @"583ce00e45297d6def000cfe"

#define SHARE_WECHAT_APPID @"wxa6d77dfb3d4e373a"
#define SHARE_WECHAT_SECRECT @"d4624c36b6795d1d99dcf0547af5443d"

#define BAIDU_MAP_KEY @"qEXeIrR5D4lLKGcPnr8jGvNX"
#define EASEMOB_KEY @"openmaterials#celeme"

#define SHARE_QQ_APPID @"1105820215"
#define SHARE_QQ_KEY @"1DlU9GyAwhuTeVog"


#define SHARE_SINA_KEY @"2905341063"
#define SHARE_SINA_SECRECT @"754f0632218b67f85009aaebdc454066"

//友盟统计
#define UMENG_APPKEY @"583ce00e45297d6def000cfe"


#ifdef DEBUG
#define EASEMOB_CERTNAME @"push_dev"// 有dev是开发环境，没有是生产环境
#else
#define EASEMOB_CERTNAME @"push_dis"
#endif



#pragma mark - Urls

#pragma mark - NSUserdefault 字段
////版本更新
//#define LatestAppVersion @"LatestAppVersion"

//是否打开引导页
#define isOpenIntroduce @"isOpenIntroduce"

// 定义静态常量字符串
#define uxy_staticConstString(__string)               static const char *__string = #__string;

//服务器地址配置
#ifdef DEBUG
#define XCHTTPClientLog 1
#define AFSERVER_DATA [[NSUserDefaults standardUserDefaults] objectForKey:@"AFSERVER_DATA"]

#else
#define XCHTTPClientLog 0
#define AFSERVER_DATA @"http://m.cele.me"

#endif

#define Social_Share_Link @"http://www.materials.cn/"//分享链接
#define ImgUrl(x) ([x hasPrefix:@"http:"]) ? x:[NSString stringWithFormat:@"%@%@",AFSERVER_DATA,x]
 
#pragma mark - Resources
#define Default_Loading_Image_1 [[UIImage imageNamed:@"placeholder"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch] //长方形
#define Default_Loading_Image_2 [[UIImage imageNamed:@"placeholder"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch]
#define Default_Avatar_Image_1 [UIImage imageNamed:@"user-profile"] //未登录头像
#define Default_Avatar_Image_2 [UIImage imageNamed:@"AppIcon"]// APP图标 可以作为分享默认

#define imageNamed(image) [UIImage imageNamed:image]

#define WEAKSELF typeof(self) __weak weakSelf = self

//颜色
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define colorRGBName(R,G,B,alph) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:alph]

#define mobileToken [[UIDevice currentDevice].identifierForVendor UUIDString]


#ifdef DEBUG
#define XCLog(format, ...) printf("\n[%s] %s [第%d行] %s\n", __TIME__, __FUNCTION__, __LINE__, [[NSString stringWithFormat:format, ## __VA_ARGS__] UTF8String]);
#else
#define XCLog(format, ...)
#endif

//当前版本
#define curVersion [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]


#endif
