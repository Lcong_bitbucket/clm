//
//  XCBaseRequest.h
//  xcwl
//
//  Created by 聪 on 16/6/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"
#import <YYCache/YYCache.h>

typedef NS_ENUM(NSUInteger, XCReueqstType){
    XCRequestTypeGET = 0,
    XCRequestTypePOST,
};

typedef NS_ENUM(NSUInteger, XCHTTPClientRequestCachePolicy){
    XCHTTPClientReturnCacheDataThenLoad = 0,///< 有缓存就先返回缓存，同步请求数据
    XCHTTPClientReloadIgnoringLocalCacheData, ///< 忽略缓存，重新请求
    XCHTTPClientReturnCacheDataElseLoad,///< 有缓存就用缓存，没有缓存就重新请求(用于数据不变时)
    XCHTTPClientReturnCacheDataDontLoad,///< 有缓存就用缓存，没有缓存就不发请求，当做请求出错处理（用于离线模式）
};

@interface XCBaseCommand : XCBaseModel

@property (nonatomic,strong) NSString *baseUrl; //基地址
@property (nonatomic,strong) NSString *api; // 请求api
@property (nonatomic,strong) NSMutableDictionary *params; // 请求参数
@property (nonatomic,strong) NSMutableDictionary *encryParams;//加密参数
@property (nonatomic,assign) NSInteger timeoutInterval;
@property (nonatomic,strong) NSString *HUDStr;//HUD 加载提示语
@property (nonatomic,strong) NSString *HUDIMGStr;//HUD 加载GIF名
//@property (nonatomic,strong) id curVC;//当前请求VC 或者 view
@property (nonatomic,strong) UIView *curView; //请求界面 用于显示HUD 如果存在即显示加载动画
@property (nonatomic,assign) XCReueqstType requestType;//请求方法 post or get

//请求回调
@property (nonatomic,strong) NSURLSessionDataTask * _Nonnull task;
@property (nonatomic,copy) void (^responseBlock)(NSDictionary *responseObject,NSError *error);
//上传进度回调
@property (nonatomic,copy) void (^uploadBlock)(NSProgress * _Nonnull uploadProgress);

@property (nonatomic,strong) NSArray *imgs;//需要上传图片数组


@property (nonatomic,assign) XCHTTPClientRequestCachePolicy cachePolicy;//缓存策略
@property (nonatomic,readonly) NSString *url;//请求网址 也是缓存key值
@property (nonatomic,strong) YYCache *cache;//缓存
@property (nonatomic,strong) NSString *cacheKey;//缓存

@property (nonatomic,assign) BOOL openLog;//是否打印请求日志
@property (nonatomic,assign) BOOL isAddHead;//是否添加公共参数
@property (nonatomic,assign) BOOL isEncry;//是否加密
@property (nonatomic,assign) BOOL isRespJson;//是否返回json换行
@end
