//
//  XCHttpClient.m
//  xcwl
//
//  Created by 聪 on 16/6/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCHttpClient.h"
#import "Encryption.h"
#import <AdSupport/AdSupport.h>

static NSString * const XCHTTPClientRequestCache = @"XCHTTPClientRequestCache";

@implementation XCHttpClient
sharedInstanceM

- (instancetype)init
{
    self = [super init];
    if (self) {
        _httpSessionManager = [AFHTTPSessionManager manager];
        _httpSessionManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _httpSessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _httpSessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];//如果报接受类型不一致请替换一致text/html或别的
        [_httpSessionManager.requestSerializer setValue:curVersion forHTTPHeaderField:@"version"];
        NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        [_httpSessionManager.requestSerializer setValue:advertisingId forHTTPHeaderField:@"UUID"];
 
    }
    return self;
}

//合适的时机加载持久化后Cookie 一般都是app刚刚启动的时候
- (void)loadSavedCookies{
    NSArray *cookies = [[XCUserManager sharedInstance] cookies];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie *cookie in cookies){
        NSLog(@"cookie,name:= %@,valuie = %@",cookie.name,cookie.value);
        [cookieStorage setCookie:cookie];
    }
}


- (void)request:(XCBaseCommand *)command{
    XCLog(@"params = %@",command.params);
    //设置cookie
    XCUserManager *manager = [XCUserManager sharedInstance];
    XCUserModel *model = [manager userModel];
    if(manager.isLogin){
        [_httpSessionManager.requestSerializer setValue:[NSString stringWithFormat:@"%ld",(long)model.id] forHTTPHeaderField:@"user_id"];
        [self loadSavedCookies];
    }
    
    if([[XCNetworkMonitoring sharedInstance] getNetworkStatus] == 0){
        command.cachePolicy = XCHTTPClientReturnCacheDataDontLoad;
        showHint([[XCNetworkMonitoring sharedInstance] getNetworkStatusDescription]);
    }
    //缓存
    id object = [command.cache objectForKey:command.cacheKey];
    if(command.openLog && command.cachePolicy != XCHTTPClientReloadIgnoringLocalCacheData)
        XCLog(@"******************* 加载了缓存 %@ **********\n= %@",command.url,object);
    switch (command.cachePolicy) {
        case XCHTTPClientReturnCacheDataThenLoad: {//先返回缓存，同时请求
            if (object) {
                if(command.responseBlock){
                    command.responseBlock(object,nil);
                }
            }
            break;
        }
        case XCHTTPClientReloadIgnoringLocalCacheData: {//忽略本地缓存直接请求
            
            break;
        }
        case XCHTTPClientReturnCacheDataElseLoad: {//有缓存就返回缓存，没有就请求
            if (object) {//有缓存
                if(command.responseBlock){
                    command.responseBlock(object,nil);
                }
                return ;
            }
            break;
        }
        case XCHTTPClientReturnCacheDataDontLoad: {//有缓存就返回缓存,从不请求（用于没有网络）
            if (object) {//有缓存
                if(command.responseBlock){
                    command.responseBlock(object,nil);
                }
            }
            return ;
        }
        default: {
            break;
        }
    }
    
    //开始请求
    if(command.requestType == XCRequestTypeGET){
        [self GETRequest:command];
    } else if(command.requestType == XCRequestTypePOST){
        [self POSTRequest:command];
    }
}

- (void)POSTRequest:(XCBaseCommand *)command{
    
    if(command.curView){
        showHUDCustomView(command.curView,command.HUDStr,command.HUDIMGStr,0);
    }
    
    if(command.timeoutInterval!=0){
        _httpSessionManager.requestSerializer.timeoutInterval = command.timeoutInterval;
    }
   
    [_httpSessionManager POST:[NSString stringWithFormat:@"%@%@",command.baseUrl,command.api] parameters:command.isEncry?command.encryParams:command.params progress:^(NSProgress * _Nonnull uploadProgress) {
        if(command.openLog)
            XCLog(@"%@ 下载进度为:%lf",command.url,1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //       NSDictionary * dict = responseObject;
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        str = [str stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
        str = [str stringByReplacingOccurrencesOfString:@"\t" withString:@"\\t"];
        str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
        str = [str stringByReplacingOccurrencesOfString:@"'" withString:@"\\\""];
        NSError *error;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            XCLog(@"#### Error--json->toValue:%@",error);
        }
        
        //        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if(command.openLog)
            XCLog(@"---*******POST Success ******---\n%@ \nparams = %@\nrespose = %@ ",command.url,command.params,dict);
        
        if(command.curView){
            hidHUDCustomView(command.curView);
        }
        [command.cache setObject:dict forKey:command.cacheKey];
        if(command.responseBlock){
            command.task = task;
            command.responseBlock(dict,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(command.openLog)
            XCLog(@"---*******POST Failure ******---\n%@ \nparams = %@ \n%@",command.url,command.params,error);
        if (command.curView){
            hidHUDCustomView(command.curView);
        }
        
        NSString *statuts = @"未知网络错误,请检查您的网络!";
        NSInteger statusCode = error.code;
        if (statusCode == 0) {
            statuts = @"未能连接到服务器!";
        } else if (statusCode == 200) {
            // 请求成功
        } else if (statusCode == 404) {
            statuts = @"请求错误!";
        } else if (statusCode >= 500) {
            statuts = @"服务器异常!";
        } else if (statusCode == -1001) {
            statuts = @"网络连接超时，请重试!";
        } else if (statusCode == -1004) {
            statuts = @"未能连接到服务器!";
        }
        showHint(statuts);
        
        if(command.responseBlock){
            command.responseBlock(nil,error);
        }
    }];
}

- (void)GETRequest:(XCBaseCommand *)command{
    
    if(command.curView){
        showHUDCustomView(command.curView,command.HUDStr,command.HUDIMGStr,0);
    }
    
    if(command.timeoutInterval!=0){
        _httpSessionManager.requestSerializer.timeoutInterval = command.timeoutInterval;
    }
    
    if(command.isRespJson){
        _httpSessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    } else {
        _httpSessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    NSDictionary *params = command.isEncry?command.encryParams:command.params;
    
    [_httpSessionManager GET:[NSString stringWithFormat:@"%@%@",command.baseUrl,command.api] parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        if(command.openLog)
            XCLog(@"%@下载进度为:%lf",command.url, 1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //         NSDictionary * dict = responseObject;
        
        NSString *str = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        str = [str stringByReplacingOccurrencesOfString:@"\r" withString:@"\\r"];
        str = [str stringByReplacingOccurrencesOfString:@"\t" withString:@"\\t"];
        str = [str stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
        str = [str stringByReplacingOccurrencesOfString:@"'" withString:@"\\\""];
        NSError *error;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&error];
        if (error) {
            XCLog(@"#### Error--json->toValue:%@",error);
        }
        
        //        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if(command.openLog)
            XCLog(@"---*******GET Success ******---\n%@ \nparams = %@ \nrespose = %@ ",command.url,command.params,dict);
        if(command.curView){
            hidHUDCustomView(command.curView);
        }
        [command.cache setObject:dict forKey:command.cacheKey];
        if(command.responseBlock){
            command.task = task;
            command.responseBlock(dict,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(command.openLog)
            XCLog(@"---*******GET Failure ******---\n%@ \nparams = %@ \n%@",command.url,command.params,error);
        if (command.curView){
            hidHUDCustomView(command.curView);
        }
        
        NSString *statuts = @"未知请求错误";
        NSInteger statusCode = error.code;
        if (statusCode == 0) {
            statuts = @"未能连接到服务器";
        }else if (statusCode == 200) {
            // 请求成功
        } else if (statusCode == 404) {
            statuts = @"请求错误";
        } else if (statusCode >= 500) {
            statuts = @"服务器异常";
        }else if (statusCode == -1001) {
            statuts = @"网络连接超时，请重试";
        }else if (statusCode == -1004) {
            statuts = @"未能连接到服务器";
        }
        showHint(statuts);
        
        if(command.responseBlock){
            command.responseBlock(nil,error);
        }
    }];
}

- (void)upload:(XCBaseCommand *)command{
    XCUserManager *manager = [XCUserManager sharedInstance];
    XCUserModel *model = [manager userModel];
    if(manager.isLogin){
        [_httpSessionManager.requestSerializer setValue:[NSString stringWithFormat:@"%ld",(long)model.id] forHTTPHeaderField:@"user_id"];
        [self loadSavedCookies];
    }
    
    if(command.imgs.count == 0){
        return ;
    }
    [_httpSessionManager POST:[NSString stringWithFormat:@"%@%@",command.baseUrl,@"/tools/upload_ajax.ashx?action=UpLoadFile"] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for(int i = 0 ; i <command.imgs.count ; i ++)
        {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(command.imgs[i], 0.8) name:@"Filedata" fileName:[NSString stringWithFormat:@"file%d.jpg",i+1] mimeType:@"image/jpeg"];
        }
        
        //        for (int i = 0; i < command.imgs.count; i++) {
        //
        //            UIImage *image = command.imgs[i];
        //            NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
        //
        //            // 在网络开发中，上传文件时，是文件不允许被覆盖，文件重名
        //            // 要解决此问题，
        //            // 可以在上传时使用当前的系统事件作为文件名
        //            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        //            // 设置时间格式
        //            [formatter setDateFormat:@"yyyyMMddHHmmss"];
        //            NSString *dateString = [formatter stringFromDate:[NSDate date]];
        //            NSString *fileName = [NSString  stringWithFormat:@"%@.jpg", dateString];
        //            /*
        //             *该方法的参数
        //             1. appendPartWithFileData：要上传的照片[二进制流]
        //             2. name：对应网站上[upload.php中]处理文件的字段（比如upload）
        //             3. fileName：要保存在服务器上的文件名
        //             4. mimeType：上传的文件的类型
        //             */
        //            [formData appendPartWithFileData:imageData name:@"upload" fileName:fileName mimeType:@"image/jpeg"]; //
        //        }
        
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        XCLog(@"%@上传进度为:%lf",command.url, 1.0 * uploadProgress.completedUnitCount / uploadProgress.totalUnitCount);
        if(command.uploadBlock){
            command.uploadBlock(uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        XCLog(@"---*******Upload Success ******---\n%@\n%@",command.url,dict);
        if(command.curView){
            hidHUDCustomView(command.curView);
        }
        if(command.responseBlock){
            command.responseBlock(dict,nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        XCLog(@"---*******Upload Failure ******---\n%@\n%@",command.url,error);
        if (command.curView){
            hidHUDCustomView(command.curView);
        }
        
        NSString *statuts = @"未知请求错误";
        NSInteger statusCode = error.code;
        if (statusCode == 0) {
            statuts = @"未能连接到服务器";
        }else if (statusCode == 200) {
            // 请求成功
        } else if (statusCode == 404) {
            statuts = @"请求错误";
        } else if (statusCode >= 500) {
            statuts = @"服务器异常";
        }else if (statusCode == -1001) {
            statuts = @"网络连接超时，请重试";
        }else if (statusCode == -1004) {
            statuts = @"未能连接到服务器";
        }
        showHint(statuts);
        
        if(command.responseBlock){
            command.responseBlock(nil,error);
        }
        
    }];
}
@end
