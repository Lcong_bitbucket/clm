//
//  XCHttpClient.h
//  xcwl
//
//  Created by 聪 on 16/6/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LSSingleton.h"
#import "XCNetworkMonitoring.h"
#import "XCBaseCommand.h"


@interface XCHttpClient : NSObject{
    AFHTTPSessionManager *_httpSessionManager;
}
sharedInstanceH


- (void)request:(XCBaseCommand*)command;

- (void)upload:(XCBaseCommand *)command;

@end
