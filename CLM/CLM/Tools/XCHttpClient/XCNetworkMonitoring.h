//
//  XCNetworkMonitoring.h
//  xcwl
//
//  Created by 聪 on 16/6/14.
//  Copyright © 2016年 聪. All rights reserved.
//




#import <Foundation/Foundation.h>
#import "LSSingleton.h"
typedef NS_ENUM(NSInteger, CSNetworkStatus)  {
    /// 无网络
    CS_NETWORK_STATUS_NONE = 0,
    /// WiFi 网络
    CS_NETWORK_STATUS_WiFi = 1,
    /// 手机网
    CS_NETWORK_STATUS_WWAN = 2,
    //未知
    CS_NETWORK_STATUS_UnKnow = 3,
};


@interface XCNetworkMonitoring : NSObject
{
    int _count;
}
@property (readwrite, nonatomic, strong) AFNetworkReachabilityManager *reachabilityManager;

sharedInstanceH
/**
 *  启动网络监听
 */
- (void)startMonitoring;

/**
 *  获取网络的状态
 *
 *  @return 返回网络的状态
 */
- (CSNetworkStatus)getNetworkStatus;

/**
 *  获取网络的状态描述
 *
 *  @return 返回网络的状态描述
 */
- (nullable NSString *)getNetworkStatusDescription;


@end
