//
//  XCNetworkMonitoring.m
//  xcwl
//
//  Created by 聪 on 16/6/14.
//  Copyright © 2016年 聪. All rights reserved.
//
NSString * const _kCSNetworkStringWWAN                =       @"手机网络";
NSString * const _kCSNetworkStringWiFi                =       @"WiFi网络";
NSString * const _kCSNoneNetworkString                =       @"无网络连接";
NSString * const _kCSUnKnowNetworkString              =       @"未知网络";

#import "XCNetworkMonitoring.h"

@implementation XCNetworkMonitoring
sharedInstanceM

- (AFNetworkReachabilityManager *)reachabilityManager {
    if (_reachabilityManager == nil) {
        _reachabilityManager = [AFNetworkReachabilityManager sharedManager];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:AFNetworkingReachabilityDidChangeNotification
                                                   object:nil];
    }
    return _reachabilityManager;
}

// 开始监听网络状态
- (void)startMonitoring
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self.reachabilityManager startMonitoring];
    });
}

- (CSNetworkStatus)getNetworkStatus
{
    CSNetworkStatus networkStatu;
    AFNetworkReachabilityStatus status;
    status = self.reachabilityManager.networkReachabilityStatus;
    
    if (status == AFNetworkReachabilityStatusNotReachable) {
        networkStatu = CS_NETWORK_STATUS_NONE;
    }else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
        networkStatu = CS_NETWORK_STATUS_WWAN;
    }else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
        networkStatu = CS_NETWORK_STATUS_WiFi;
    } else if(status == AFNetworkReachabilityStatusUnknown){
        networkStatu = CS_NETWORK_STATUS_UnKnow;
    }
    return networkStatu;
}

- (NSString *)getNetworkStatusDescription
{
    NSString *des = @"";
    AFNetworkReachabilityStatus status;
    status = self.reachabilityManager.networkReachabilityStatus;
    
    if (status == AFNetworkReachabilityStatusNotReachable) {
        des = _kCSNoneNetworkString;
    }else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
        des = _kCSNetworkStringWWAN;
    }else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
        des = _kCSNetworkStringWiFi;
    } else if(status == AFNetworkReachabilityStatusUnknown){
        des = _kCSUnKnowNetworkString;
    }
    return des;
}

- (void)reachabilityChanged1:(AFNetworkReachabilityStatus)notification
{
    CSNetworkStatus networkStatu;
    NSString *statusString  = nil;
    AFNetworkReachabilityStatus status = notification;
    
    if (status == AFNetworkReachabilityStatusNotReachable) {
        networkStatu = CS_NETWORK_STATUS_NONE;
        statusString = _kCSNoneNetworkString;
    }else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
        networkStatu = CS_NETWORK_STATUS_WWAN;
        statusString = _kCSNetworkStringWWAN;
    }else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
        networkStatu = CS_NETWORK_STATUS_WiFi;
        statusString = _kCSNetworkStringWiFi;
    } else if(status == AFNetworkReachabilityStatusUnknown){
        networkStatu = CS_NETWORK_STATUS_UnKnow;
        statusString = _kCSUnKnowNetworkString;
    }
    
    if (_count > 1) {
        [self showNetworkStatus:status];
    }
    _count++;
    
}

- (void)reachabilityChanged:(NSNotification *)notification
{
    CSNetworkStatus networkStatu;
    NSString *statusString  = nil;
    AFNetworkReachabilityStatus status;
    
    NSDictionary *statusDictionary = notification.userInfo;
    status = [statusDictionary[@"AFNetworkingReachabilityNotificationStatusItem"] integerValue];
    
    if (status == AFNetworkReachabilityStatusNotReachable) {
        networkStatu = CS_NETWORK_STATUS_NONE;
        statusString = _kCSNoneNetworkString;
    }else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
        networkStatu = CS_NETWORK_STATUS_WWAN;
        statusString = _kCSNetworkStringWWAN;
    }else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
        networkStatu = CS_NETWORK_STATUS_WiFi;
        statusString = _kCSNetworkStringWiFi;
    } else if(status == AFNetworkReachabilityStatusUnknown){
        networkStatu = CS_NETWORK_STATUS_UnKnow;
        statusString = _kCSUnKnowNetworkString;
    }
    
    if (_count > 1) {
        [self showNetworkStatus:status];
    }
    _count++;
    
}

- (void)showNetworkStatus:(AFNetworkReachabilityStatus)status {
    if (status == AFNetworkReachabilityStatusNotReachable) {
        showHint(@"无网络连接");
    } else if (status == AFNetworkReachabilityStatusReachableViaWWAN) {
        showHint(@"已切换手机网络");
    } else if (status == AFNetworkReachabilityStatusReachableViaWiFi){
        showHint(@"已切换WiFi网络");
    } else if (status == AFNetworkReachabilityStatusUnknown){
        showHint(@"已切换未知网络");
    }
}

@end
