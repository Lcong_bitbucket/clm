//
//  XCBaseRequest.m
//  xcwl
//
//  Created by 聪 on 16/6/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseCommand.h"
#import "Encryption.h"

@implementation XCBaseCommand

- (instancetype)init
{
    self = [super init];
    if (self) {
        //默认配置
        self.baseUrl = AFSERVER_DATA;
        self.api = @"";
        self.timeoutInterval = 20;
        self.requestType = XCRequestTypePOST;
        self.HUDStr = @"";
        self.HUDIMGStr = @"loading1";
        self.openLog = XCHTTPClientLog;
        self.cachePolicy = XCHTTPClientReloadIgnoringLocalCacheData;//忽略缓存，重新请求
        self.isAddHead = NO;
        self.isEncry = NO;
    }
    return self;
}

- (NSString *)url{
    NSMutableString *requestUrl = [[NSMutableString alloc] init];
    [requestUrl appendFormat:@"%@%@",self.baseUrl,self.api];
    
    NSString *url = nil;
    
    NSMutableDictionary *params = self.isEncry?self.encryParams:self.params;
    if([[params allKeys] count] > 0){
        [requestUrl appendString:@"?"];
        for(NSString *key in [params allKeys]){
            [requestUrl appendFormat:@"%@=%@&",key,params[key]];
        }
        url = [requestUrl substringToIndex:requestUrl.length - 1];
    } else {
        url = requestUrl;
    }
    //    XCLog(@"params = %@",_params);
    //    XCLog(@"url = %@",url);
    return url;
}

- (NSString *)cacheKey{
    NSMutableString *requestUrl = [[NSMutableString alloc] init];
    [requestUrl appendFormat:@"%@%@",self.baseUrl,self.api];
    
    NSString *url = nil;
    
    NSMutableDictionary *params = self.params;
    if([[params allKeys] count] > 0){
        [requestUrl appendString:@"?"];
        for(NSString *key in [params allKeys]){
            if(![key isEqualToString:@"sessionid"]){
                [requestUrl appendFormat:@"%@=%@&",key,params[key]];
            }
        }
        url = [requestUrl substringToIndex:requestUrl.length - 1];
    } else {
        url = requestUrl;
    }
    return url;
}

- (YYCache *)cache{
    if(!_cache){
        _cache = [[YYCache alloc] initWithName:@"XCHTTPClientRequestCache"];
        _cache.memoryCache.shouldRemoveAllObjectsOnMemoryWarning = YES;
        _cache.memoryCache.shouldRemoveAllObjectsWhenEnteringBackground = YES;
    }
    return _cache;
}


- (NSMutableDictionary *)params{
    if(!_params){
        _params = [NSMutableDictionary new];
    }
    //添加公共参数
    if(self.isAddHead){
        [_params setValue:@"iOS" forKey:@"os"];
        [_params setValue:curVersion forKey:@"appVersion"];
         //        [_params setValue:[[UserSessionCenter shareSession] getUserId] forKey:@"user_id"];
    }
    
    return _params;
}

- (NSMutableDictionary *)encryParams{
    if(!_encryParams){
        _encryParams = [NSMutableDictionary new];
    }
    //加密
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMdd"];
    NSString *nowDate = [dateFormatter stringFromDate:[NSDate date]];
    
    NSString *aesKey = [NSString stringWithFormat:@"xcwl.com%@",nowDate];
    [_encryParams setValue:[Encryption AES128Encrypt:[self.params mj_JSONString] key:aesKey] forKey:@"params"];
    //    XCLog(@"encryParams = %@",_encryParams);
    return _encryParams;
}

@end
