//
//  ShareManager.m
//  GolfFriend
//
//  Created by Vescky on 14-5-18.
//  Copyright (c) 2014年 vescky.org. All rights reserved.
//

#import "ShareManager.h"


@implementation ShareManager

+ (id)shareManeger {
    static ShareManager *shareManagerInstance = nil;
    static dispatch_once_t shareManagerPredicate;
    dispatch_once(&shareManagerPredicate, ^{
        shareManagerInstance = [[self alloc] init];
    });
    return shareManagerInstance;
}

- (void)shareDic:(NSDictionary *)dic vc:(UIViewController *)vc{
    if(!self.shareIV){
        self.shareIV = [[UIImageView alloc] init];
    }
    NSDictionary *d = [dic copy];
    [self.shareIV setImageWithURLStr:[d objectForKey:@"img"] placeholderImage:nil completed:^(UIImage *image, NSError *error) {
        
        [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),
                                                   @(UMSocialPlatformType_WechatTimeLine),
                                                   @(UMSocialPlatformType_QQ),
                                                   ]];//                                                   @(UMSocialPlatformType_Sina),

        
        [UMSocialShareUIConfig shareInstance].sharePageGroupViewConfig.sharePageGroupViewPostionType = UMSocialSharePageGroupViewPositionType_Bottom;
        [UMSocialShareUIConfig shareInstance].sharePageScrollViewConfig.shareScrollViewPageItemStyleType = UMSocialPlatformItemViewBackgroudType_IconAndBGRadius;
        [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
            [[ShareManager shareManeger] shareToSocialToPlatformType:platformType webUrl:ImgUrl([d objectForKey:@"msg"]) title:[d objectForKey:@"title"] text:[d objectForKey:@"zhaiyao"] image:image parrentViewController:vc];
        }];
    }];
}

- (void)sharePlatformName:(UMSocialPlatformType)platformName Dic:(NSDictionary *)dic{
    if(!self.shareIV){
        self.shareIV = [[UIImageView alloc] init];
    }
    NSDictionary *d = [dic copy];
    [self.shareIV setImageWithURLStr:ImgUrl([d objectForKey:@"msg"]) placeholderImage:nil completed:^(UIImage *image, NSError *error) {
            [[ShareManager shareManeger] shareToSocialToPlatformType:platformName webUrl:[d objectForKey:@"msg"] title:[d objectForKey:@"title"] text:[d objectForKey:@"zhaiyao"] image:image parrentViewController:nil];
    }];

    
}

- (void)shareToSocialToPlatformType:(UMSocialPlatformType)platformType webUrl:(NSString*)_url title:(NSString*)_stitle text:(NSString*)_text image:(UIImage*)_image parrentViewController:(UIViewController*)parrentVc{
    NSString *shareTitle = _stitle?_stitle:@"测了么";
    NSString *shareText = _text?_text:@"来自测了么的分享";
    UIImage *shareImage =_image?_image:Default_Avatar_Image_2;
    
    //创建分享消息对象
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    //创建网页内容对象
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:shareTitle descr:shareText thumImage:shareImage];
    //设置网页地址
    shareObject.webpageUrl = [_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    //分享消息对象设置分享内容对象
    messageObject.shareObject = shareObject;
    
    //调用分享接口
    [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
        if (error) {
            UMSocialLogInfo(@"************Share fail with error %@*********",error);
        }else{
            if ([data isKindOfClass:[UMSocialShareResponse class]]) {
                UMSocialShareResponse *resp = data;
                //分享结果消息
                UMSocialLogInfo(@"response message is %@",resp.message);
                //第三方原始返回的数据
                UMSocialLogInfo(@"response originalResponse data is %@",resp.originalResponse);
            }else{
                UMSocialLogInfo(@"response data is %@",data);
            }
        }
        [self alertWithError:error];
    }];
}

- (void)alertWithError:(NSError *)error
{
    NSString *result = nil;
    if (!error) {
        result = [NSString stringWithFormat:@"分享成功"];
    }
    else{
        NSMutableString *str = [NSMutableString string];
        if (error.userInfo) {
            for (NSString *key in error.userInfo) {
                [str appendFormat:@"%@ = %@\n", key, error.userInfo[key]];
            }
        }
        if (error) {
            result = @"分享失败";
        }
        else{
            result = [NSString stringWithFormat:@"分享失败"];
        }
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享结果"
                                                    message:result
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"sure", @"确定")
                                          otherButtonTitles:nil];
    [alert show];
}


@end
