//
//  MYCustomPanel.h
//  MYBlurIntroductionView-Example
//
//  Created by Matthew York on 10/17/13.
//  Copyright (c) 2013 Matthew York. All rights reserved.
//

#import "MYIntroductionPanel.h"

@interface MYCustomPanel : MYIntroductionPanel <UITextViewDelegate> {
    
}

@property (weak, nonatomic) IBOutlet UIImageView *bgIV;
@property (weak, nonatomic) IBOutlet UIButton *goMainBtn;


- (IBAction)didPressEnable:(id)sender;

@end
