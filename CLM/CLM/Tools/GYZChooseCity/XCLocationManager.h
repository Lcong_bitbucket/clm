//
//  XCLocationManager.h
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@interface XCLocationManager : NSObject<CLLocationManagerDelegate>{
}
sharedInstanceH
-(void)locationStart;
@property(nonatomic,retain)CLLocationManager *locationManager;
@property (nonatomic,copy) void (^didUpdateLocatin)(NSDictionary *dic);
@end
