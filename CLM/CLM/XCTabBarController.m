//
//  XCTabBarController.m
//  xcwl
//
//  Created by 聪 on 16/4/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTabBarController.h"
#import "XCZixunMainViewController.h"
#import "XCNewsViewController.h"
#import "XCMineViewController.h"
#import "XCHomeViewController.h"
#import "MYCustomPanel.h"
#import "XCSocailMainViewController.h"
#import "XCCircleViewController.h"
#import "XCDevicesViewController.h"
  
@interface XCTabBarController ()<UITabBarControllerDelegate>
{
    NSInteger flashTime;
    NSTimer *flashTimer;
}
@property (strong, nonatomic) UIView *launchView;

@end

@implementation XCTabBarController
static BOOL isLoad;
- (void)viewDidLoad{
    [super viewDidLoad];
    [self initTabBar];
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:isOpenIntroduce]){
        [self buildIntro];
    } else {
        //加载界面 广告
        if(!isLoad){
            [self lanuchScreen];
            isLoad = YES;
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:ReloadUserModelNotification object:nil];
    [self setupUnreadMessageCount];
    
   
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[XCUserManager sharedInstance] reloadInfoSuccess:^{
        
    } failure:^(NSError *error) {
        
    }];
}



- (void)lanuchScreen{
    
    self.launchView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.launchView];
    
    UIImageView *bgIV = [[UIImageView alloc] initWithFrame:self.launchView.bounds];
    bgIV.image= imageNamed(@"qidong");
    [self.launchView addSubview:bgIV];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height)];
    imageV.userInteractionEnabled = YES;
    typeof(UIImageView *) __weak imageSelf = imageV;
    NSString *imgUrl =  [NSString stringWithFormat:@"%@%@",AFSERVER_DATA,@"/upload/appscreen/ad.jpg"];
    flashTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(jishi) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:flashTimer forMode: NSRunLoopCommonModes];
    
    
    [imageV setImageWithURLStr:imgUrl placeholderImage:imageNamed(@"qidong") completed:^(UIImage *image, NSError *error) {
        
        if(!error){
            imageSelf.image = image;
            
            UIButton *skipBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - 60, 30, 40, 40)];
            skipBtn.tag = 111;
            skipBtn.layer.masksToBounds = YES;
            skipBtn.layer.borderWidth = 1;
            skipBtn.layer.borderColor = getHexColor(@"#393939").CGColor;
            skipBtn.layer.cornerRadius = 20;
            skipBtn.backgroundColor = [UIColor whiteColor];
            [skipBtn setTitleColor:getHexColor(@"#393939") forState:UIControlStateNormal];
            skipBtn.titleLabel.font = [UIFont systemFontOfSize:10];
            skipBtn.titleLabel.numberOfLines = 2;
            skipBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [skipBtn setTitle:@"跳过\n3S" forState:UIControlStateNormal];
            [skipBtn addTarget:self action:@selector(removeLun) forControlEvents:UIControlEventTouchUpInside];
            [imageSelf addSubview:skipBtn];
        } else {
            [self removeLun];
        }
 
    }];
    
    [self.launchView addSubview:imageV];
    
}

- (void)jishi{
    flashTime ++;
    UIButton *skipBtn = [self.launchView viewWithTag:111];
    [skipBtn setTitle:[NSString stringWithFormat:@"跳过\n%ldS",4 - flashTime] forState:UIControlStateNormal];
    if(flashTime > 3){
        [flashTimer invalidate];
        [self removeLun];
    }
}

-(void)removeLun {
    [flashTimer invalidate];
    flashTime = 0;
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:1.5 animations:^{
            
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
//            self.launchView.transform=CGAffineTransformMakeScale(2.f, 2.f);
            self.launchView.alpha = 0;
            
        } completion:^(BOOL finished) {
            [app_delegate() checkVersion];
            [self.launchView removeFromSuperview];
        }];
     });
}
 
-(void)initTabBar
{
    
    XCHomeViewController *vc1 = [[XCHomeViewController alloc] init];
    UINavigationController *nav1 = [[UINavigationController alloc]
                                                   initWithRootViewController:vc1];
    
    XCZixunMainViewController *vc2 = [[XCZixunMainViewController alloc] init];
    UINavigationController *nav2 = [[UINavigationController alloc]
                                                    initWithRootViewController:vc2];
    
    XCCircleViewController *vc3 = [[XCCircleViewController alloc] init];
    UINavigationController *nav3 = [[UINavigationController alloc]
                                                   initWithRootViewController:vc3];
    
    XCDevicesViewController *vc4 = [[XCDevicesViewController alloc] init];
    UINavigationController *nav4 = [[UINavigationController alloc]
                                    initWithRootViewController:vc4];
    
    XCMineViewController *vc5 = [[XCMineViewController alloc] init];
    UINavigationController *nav5 = [[UINavigationController alloc]
                                                    initWithRootViewController:vc5];
    
    self.viewControllers = @[nav1,nav2,nav3,nav4,nav5];
    self.delegate = self;
    
    
    UITabBarItem *tabBarItem1 = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [self.tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [self.tabBar.items objectAtIndex:4];


    tabBarItem1.title = @"预约";
    tabBarItem2.title = @"咨询";
    tabBarItem3.title = @"测试圈";
    tabBarItem4.title = @"测试+";
    tabBarItem5.title = @"我的";
    
    tabBarItem1.image = [[UIImage imageNamed:@"icon_foot1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem1.selectedImage = [[UIImage imageNamed:@"icon_foot1a"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem2.image = [[UIImage imageNamed:@"icon_foot2"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem2.selectedImage = [[UIImage imageNamed:@"icon_foot2a"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
     
    tabBarItem3.image = [[UIImage imageNamed:@"icon_foot3"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem3.selectedImage = [[UIImage imageNamed:@"icon_foot3a"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    tabBarItem4.image = [[UIImage imageNamed:@"icon_foot4"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem4.selectedImage = [[UIImage imageNamed:@"icon_foot4a"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    
    tabBarItem5.image = [[UIImage imageNamed:@"icon_foot5"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tabBarItem5.selectedImage = [[UIImage imageNamed:@"icon_foot5a"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:getHexColor(@"#707070"), NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:getHexColor(@"#1566bf"), NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13], NSFontAttributeName, nil] forState:UIControlStateNormal];

}


#pragma mark UITabBarControllerDelegate
- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    [[XCUserManager sharedInstance] reloadInfoSuccess:^{
        
    } failure:^(NSError *error) {
        
    }];
    XCLog(@"--tabbaritem.title--%@",viewController.tabBarItem.title);
    return YES;
}

//引导页
-(void)buildIntro{
    
    //    MYCustomPanel *panel0 = [[MYCustomPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"MYCustomPanel"];
    //    panel0.bgIV.image = imageNamed(@"guide00");
    //    panel0.goMainBtn.hidden = YES;
    
    MYCustomPanel *panel1 = [[MYCustomPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"MYCustomPanel"];
    panel1.bgIV.image = imageNamed(@"Guide1");
    panel1.goMainBtn.hidden = YES;
    
    MYCustomPanel *panel2 = [[MYCustomPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"MYCustomPanel"];
    panel2.bgIV.image = imageNamed(@"Guide2");
    panel2.goMainBtn.hidden = YES;
    
    //Create custom panel with events
    MYCustomPanel *panel3 = [[MYCustomPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"MYCustomPanel"];
    panel3.bgIV.image = imageNamed(@"Guide3");
    panel3.goMainBtn.hidden = NO;
    
//    MYCustomPanel *panel4 = [[MYCustomPanel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) nibNamed:@"MYCustomPanel"];
//    panel4.bgIV.image = imageNamed(@"guide04");
//    panel4.goMainBtn.hidden = NO;
    
    //Add panels to an array
    NSArray *panels = @[panel1, panel2, panel3];
    
    //Create the introduction view and set its delegate
    MYBlurIntroductionView *introductionView = [[MYBlurIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    introductionView.delegate = self;
    introductionView.PageControl.pageIndicatorTintColor = getHexColor(@"#e2e2e2");
    introductionView.PageControl.currentPageIndicatorTintColor = getHexColor(@"#fb5756");
    introductionView.PageControl.frame = CGRectMake((Screen_Width - 148)/2, Screen_Height - 120, 148, 37);
    [introductionView.RightSkipButton setTitleColor:getHexColor(@"#fb5756") forState:UIControlStateNormal];
    introductionView.RightSkipButton.hidden = YES;
    [introductionView setBackgroundColor:[UIColor colorWithRed:90.0f/255.0f green:175.0f/255.0f blue:113.0f/255.0f alpha:0.65]];
    //introductionView.LanguageDirection = MYLanguageDirectionRightToLeft;
    
    //Build the introduction with desired panels
    [introductionView buildIntroductionWithPanels:panels];
    
    //Add the introduction to your view
    [self.view addSubview:introductionView];
}
#pragma mark - MYIntroduction Delegate
-(void)introduction:(MYBlurIntroductionView *)introductionView didChangeToPanel:(MYIntroductionPanel *)panel withIndex:(NSInteger)panelIndex{
    NSLog(@"Introduction did change to panel %ld", (long)panelIndex);
//    introductionView.RightSkipButton.hidden = YES;

//    if(panelIndex == 2){
//        introductionView.RightSkipButton.hidden = YES;
//    } else {
//        introductionView.RightSkipButton.hidden = NO;
//    }
}

-(void)introduction:(MYBlurIntroductionView *)introductionView didFinishWithType:(MYFinishType)finishType {
    NSLog(@"Introduction did finish");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:isOpenIntroduce];
}

#pragma mark - 环信
// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    XCUserManager *um = [XCUserManager sharedInstance];
    XCUserModel *user = um.userModel;
    unreadCount += user.new_message;
   
    UIViewController *vc = self.viewControllers[4];
    if (vc) {
        if (unreadCount > 0) {
            vc.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",(int)unreadCount];
        }else{
            vc.tabBarItem.badgeValue = nil;
        }
    }

    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
}

@end
