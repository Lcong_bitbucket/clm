//
//  XCCommonUrlViewController.m
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCCommonUrlViewController.h"

@interface XCCommonUrlViewController ()

@end

@implementation XCCommonUrlViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    if(!self.title)
        self.title = @"详情";
    if(self.link_url){
        NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",AFSERVER_DATA,self.link_url]]];
        [self.webView loadRequest:req];
    }
    if(self.htmlStr){
        [self.webView loadHTMLString:self.htmlStr baseURL:nil];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
