//
//  XCPopViewTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCPopViewTableViewCell.h"

@implementation XCPopViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    _title.text = _dic[@"title"];
    if(_dic[@"img_url"] && [_dic[@"img_url"] length] > 0){
        _titleL.constant = 30;
        [_icon setImageURLStr:ImgUrl(_dic[@"img_url"]) placeholder:nil];
        _icon.hidden = NO;
    } else {
        _titleL.constant = 8;
        _icon.hidden = YES;
    }
}
@end
