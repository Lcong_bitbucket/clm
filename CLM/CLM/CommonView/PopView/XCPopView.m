//
//  XCPopView.m
//  CLM
//
//  Created by cong on 16/12/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCPopView.h"
#import "XCPopViewTableViewCell.h"

@implementation XCPopView
sharedInstanceM


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initView];
       
    }
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hidden];
}

- (void)initView{
    _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [UIView new];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.layer.masksToBounds = YES;
    _tableView.layer.cornerRadius = 5;
     [self addSubview:_tableView];
}
- (void)setDataArray:(NSArray *)dataArray{
    
    _dataArray = dataArray;
    [_tableView reloadData];
}

- (void)showInView:(UIView *)v withRect:(CGRect)tbRect{
     [v addSubview:self];
    self.frame = v.bounds;
    
    _tableView.frame = CGRectMake(tbRect.origin.x, tbRect.origin.y,tbRect.size.width, 0);
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:15 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        
        _tableView.frame = CGRectMake(_tableView.mj_x, _tableView.mj_y,_tableView.mj_w, _dataArray.count > 8 ? 44*8:_dataArray.count * 44);
//        [_tableView selectRowAtIndexPath:[NSIndexPath indexPathWithIndex:self.curSelectedIndex] animated:YES scrollPosition:UITableViewScrollPositionMiddle];

    } completion:^(BOOL finished) {
    }];
}


#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XCPopViewTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"XCPopViewTableViewCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"XCPopViewTableViewCell" bundle:nil] forCellReuseIdentifier:@"XCPopViewTableViewCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"XCPopViewTableViewCell"];
    }
    cell.dic = _dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self hide:indexPath.row];
    
}

- (void)hidden{
    [self hide:-1];
}

- (void)hide:(NSInteger)index{
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:1 initialSpringVelocity:15 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        _tableView.frame = CGRectMake(_tableView.mj_x,_tableView.mj_y, _tableView.mj_w,0);
    } completion:^(BOOL finished) {
        if(self.didClickBlock ){
            if(index >= 0)
                self.curSelectedIndex = index;
            self.didClickBlock(index,_dataArray);
        }
        [self removeFromSuperview];
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
