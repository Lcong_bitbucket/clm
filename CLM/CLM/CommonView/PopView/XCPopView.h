//
//  XCPopView.h
//  CLM
//
//  Created by cong on 16/12/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCPopView : UIView<UITableViewDelegate,UITableViewDataSource>{
    UITableView *_tableView;
}
sharedInstanceH
@property (nonatomic,assign) NSInteger curSelectedIndex;

- (void)showInView:(UIView *)v withRect:(CGRect)tbRect;
- (void)hidden;
@property (nonatomic,strong) NSArray *dataArray;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger selectedIndex,NSArray *dataArray);
@end
