//
//  UpdateAlertTableViewCell.m
//  xcwl
//
//  Created by 聪 on 16/7/22.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "UpdateAlertTableViewCell.h"

@implementation UpdateAlertTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _prompt.textInsets = UIEdgeInsetsMake(2, 8, 2, 8);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)setUpdateDic:(NSDictionary *)updateDic{
    _updateDic = updateDic;
    _prompt.text = [NSString stringWithFormat:@"版本号:%@",_updateDic[@"version"]];
    _verson.text = @"更新内容";
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;// 字体的行间距
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:15],
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    self.msg.attributedText = [[NSAttributedString alloc] initWithString:_updateDic[@"update_remark"]?_updateDic[@"update_remark"]:@""  attributes:attributes];
}

- (IBAction)close:(id)sender {
    if(self.updateBlock){
        self.updateBlock(0);
    }
}

- (IBAction)update:(id)sender {
    if(self.updateBlock){
        self.updateBlock(1);
    }
}
@end
