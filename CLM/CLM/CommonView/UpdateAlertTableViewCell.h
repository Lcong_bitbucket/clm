//
//  UpdateAlertTableViewCell.h
//  xcwl
//
//  Created by 聪 on 16/7/22.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateAlertTableViewCell : UITableViewCell
- (IBAction)close:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;
@property (weak, nonatomic) IBOutlet UILabel *verson;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *prompt;
- (IBAction)update:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *msg;
@property (nonatomic,strong) NSDictionary *updateDic;
@property (nonatomic,copy) void (^updateBlock)(NSInteger type);
@end
