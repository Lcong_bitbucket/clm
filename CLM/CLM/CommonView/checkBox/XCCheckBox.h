//
//  XCCheckBox.h
//  xcwl
//
//  Created by cong on 17/2/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCCheckBox : UIButton<UITableViewDelegate ,UITableViewDataSource>

@property (nonatomic, strong)UITableView *tableView;

/** 1.内部视图 */
@property (nonatomic, strong) UIView *contentView;
/** 2.边线,选择器和上方tool之间的边线 */
@property (nonatomic, strong)UIView *lineView;
@property (nonatomic, strong)UIButton *buttonLeft;
/** 5.右边的按钮 */
@property (nonatomic, strong)UIButton *buttonRight;
/** 6.标题label */
@property (nonatomic, strong)UILabel *labelTitle;
/** 7.下边线,在显示模式是STPickerContentModeCenter的时候显示 */
@property (nonatomic, strong)UIView *lineViewDown;

/** 1.标题，default is nil */
@property(nullable, nonatomic,copy) NSString          *title;
/** 2.字体，default is nil (system font 17 plain) */
@property(null_resettable, nonatomic,strong) UIFont   *font;
/** 3.字体颜色，default is nil (text draws black) */
@property(null_resettable, nonatomic,strong) UIColor  *titleColor;
/** 4.按钮边框颜色颜色，default is RGB(205, 205, 205) */
@property(null_resettable, nonatomic,strong) UIColor  *borderButtonColor;
/** 5.选择器的高度，default is 240 */
@property (nonatomic, assign)CGFloat heightPicker;
@property (nonatomic, assign)BOOL isSingle;//是否单选

/**
 *  7.显示
 */
- (void)show;

/**
 *  8.移除
 */
- (void)remove;

@property (nonatomic, strong)NSMutableArray *arrayData;
@property (nonatomic,copy) void (^didClickBlock)(NSMutableArray *arrayData);
@end

@interface XCCheckBoxModel : XCBaseModel
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *item;
@property (nonatomic,copy) NSString *value;
@property (nonatomic,assign) NSInteger id;

 @property (nonatomic,assign) BOOL isSelected;
@end
