//
//  XCCheckBoxTableViewCell.h
//  xcwl
//
//  Created by cong on 17/2/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCCheckBox.h"
@interface XCCheckBoxTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (nonatomic,strong) XCCheckBoxModel *model;
@end
