//
//  XCCheckBox.m
//  xcwl
//
//  Created by cong on 17/2/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCCheckBox.h"
#import "XCCheckBoxTableViewCell.h"

@implementation XCCheckBox

#pragma mark - --- init 视图初始化 ---
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setupDefault];
        [self setupUI];
    }
    return self;
}
- (void)setupDefault
{
    // 1.设置数据的默认值
    _title             = nil;
    _font              = [UIFont systemFontOfSize:15];
    _titleColor        = [UIColor blackColor];
    _borderButtonColor = colorRGBName(205, 205, 205, 1);
     _heightPicker      = 240;
    
    // 2.设置自身的属性
    self.bounds = [UIScreen mainScreen].bounds;
    self.backgroundColor = colorRGBName(0, 0, 0, 102.0/255);
    self.layer.opacity = 0.0;
    [self addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchUpInside];
    
    // 3.添加子视图
    [self addSubview:self.contentView];
    [self.contentView addSubview:self.lineView];
    [self.contentView addSubview:self.tableView];
    [self.contentView addSubview:self.buttonLeft];
    [self.contentView addSubview:self.buttonRight];
    [self.contentView addSubview:self.labelTitle];
    [self.contentView addSubview:self.lineViewDown];
}

- (void)setupUI
{}


#pragma mark - --- delegate 视图委托 ---

#pragma mark - --- event response 事件相应 ---

- (void)selectedOk:(UIButton *)btn
{
    btn.selected = !btn.isSelected;
    for(XCCheckBoxModel *model in _arrayData){
        model.isSelected = btn.isSelected;
    }
    if(self.didClickBlock){
        self.didClickBlock(_arrayData);
    }
    [self.tableView reloadData];
}

- (void)selectedCancel
{
    [self remove];
}

#pragma mark - --- private methods 私有方法 ---


- (void)show
{
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self setCenter:[UIApplication sharedApplication].keyWindow.center];
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:self];
    
        CGRect frameContent =  self.contentView.frame;
        frameContent.origin.y -= self.contentView.height;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.layer setOpacity:1.0];
            self.contentView.frame = frameContent;
        } completion:^(BOOL finished) {
        }];
    
}

- (void)remove
{
         CGRect frameContent =  self.contentView.frame;
        frameContent.origin.y += self.contentView.height;
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [self.layer setOpacity:0.0];
            self.contentView.frame = frameContent;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
}

#pragma mark - --- setters 属性 ---

- (void)setTitle:(NSString *)title
{
    _title = title;
    [self.labelTitle setText:title];
}

- (void)setFont:(UIFont *)font
{
    _font = font;
    [self.buttonLeft.titleLabel setFont:font];
    [self.buttonRight.titleLabel setFont:font];
    [self.labelTitle setFont:font];
}

- (void)setTitleColor:(UIColor *)titleColor
{
    _titleColor = titleColor;
    [self.labelTitle setTextColor:titleColor];
    [self.buttonLeft setTitleColor:titleColor forState:UIControlStateNormal];
    [self.buttonRight setTitleColor:titleColor forState:UIControlStateNormal];
}

- (void)setBorderButtonColor:(UIColor *)borderButtonColor
{
    _borderButtonColor = borderButtonColor;
    
    [self.buttonLeft.layer setBorderColor:borderButtonColor.CGColor];
    [self.buttonLeft.layer setBorderWidth:0.5];
    [self.buttonLeft.layer setCornerRadius:4];
    
    [self.buttonRight.layer setBorderColor:borderButtonColor.CGColor];
    [self.buttonRight.layer setBorderWidth:0.5];
    [self.buttonRight.layer setCornerRadius:4];
}

- (void)setHeightPicker:(CGFloat)heightPicker
{
    _heightPicker = heightPicker;
    self.contentView.mj_h = heightPicker;
}

#pragma mark - --- getters 属性 ---
- (UIView *)contentView
{
    if (!_contentView) {
        CGFloat contentX = 0;
        CGFloat contentY = Screen_Height;
        CGFloat contentW = Screen_Width;
        CGFloat contentH = self.heightPicker;
        _contentView = [[UIView alloc]initWithFrame:CGRectMake(contentX, contentY, contentW, contentH)];
        [_contentView setBackgroundColor:[UIColor whiteColor]];
    }
    return _contentView;
}

- (UIView *)lineView
{
    if (!_lineView) {
        CGFloat lineX = 0;
        CGFloat lineY = 44;
        CGFloat lineW = self.contentView.width;
        CGFloat lineH = 0.5;
        _lineView = [[UIView alloc]initWithFrame:CGRectMake(lineX, lineY, lineW, lineH)];
        [_lineView setBackgroundColor:self.borderButtonColor];
    }
    return _lineView;
}

- (UITableView *)tableView
{
    if (!_tableView) {
        CGFloat pickerW = self.contentView.width;
        CGFloat pickerH = self.contentView.height - self.lineView.bottom;
        CGFloat pickerX = 0;
        CGFloat pickerY = self.lineView.bottom;
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(pickerX, pickerY, pickerW, pickerH)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
         [_tableView setBackgroundColor:[UIColor whiteColor]];
    }
    return _tableView;
}

- (UIButton *)buttonLeft
{
    if (!_buttonLeft) {
        CGFloat leftW = 44;
        CGFloat leftH = self.lineView.top - 10;
        CGFloat leftX = 16;
        CGFloat leftY = (self.lineView.top - leftH) / 2;
        _buttonLeft = [[UIButton alloc]initWithFrame:CGRectMake(leftX, leftY, leftW, leftH)];
        [_buttonLeft setTitle:@"取消" forState:UIControlStateNormal];
        [_buttonLeft setTitleColor:self.titleColor forState:UIControlStateNormal];
        [_buttonLeft.layer setBorderColor:self.borderButtonColor.CGColor];
        [_buttonLeft.layer setBorderWidth:0.5];
        [_buttonLeft.layer setCornerRadius:4];
        [_buttonLeft.titleLabel setFont:self.font];
        [_buttonLeft addTarget:self action:@selector(selectedCancel) forControlEvents:UIControlEventTouchUpInside];
    }
    return _buttonLeft;
}

- (UIButton *)buttonRight
{
    if (!_buttonRight) {
        CGFloat rightW = self.buttonLeft.width;
        CGFloat rightH = self.buttonLeft.height;
        CGFloat rightX = self.contentView.width - rightW - self.buttonLeft.mj_x;
        CGFloat rightY = self.buttonLeft.mj_y;
        _buttonRight = [[UIButton alloc]initWithFrame:CGRectMake(rightX, rightY, rightW, rightH)];
        [_buttonRight setTitle:@"确定" forState:UIControlStateNormal];
 
        [_buttonRight setTitleColor:self.titleColor forState:UIControlStateNormal];
        [_buttonRight setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        [_buttonRight.layer setBorderColor:self.borderButtonColor.CGColor];
        [_buttonRight.layer setBorderWidth:0.5];
        [_buttonRight.layer setCornerRadius:4];
        [_buttonRight.titleLabel setFont:self.font];
        [_buttonRight addTarget:self action:@selector(remove) forControlEvents:UIControlEventTouchUpInside];
    }
    return _buttonRight;
}

- (UILabel *)labelTitle
{
    if (!_labelTitle) {
        CGFloat titleX = self.buttonLeft.right + 5;
        CGFloat titleY = 0;
        CGFloat titleW = self.contentView.width - titleX * 2;
        CGFloat titleH = self.lineView.top;
        _labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(titleX, titleY, titleW, titleH)];
        [_labelTitle setTextAlignment:NSTextAlignmentCenter];
        [_labelTitle setTextColor:self.titleColor];
        [_labelTitle setFont:self.font];
        _labelTitle.adjustsFontSizeToFitWidth = YES;
    }
    return _labelTitle;
}

- (UIView *)lineViewDown
{
    if (!_lineViewDown) {
        CGFloat lineX = 0;
        CGFloat lineY = self.tableView.bottom;
        CGFloat lineW = self.contentView.width;
        CGFloat lineH = 0.5;
        _lineViewDown = [[UIView alloc]initWithFrame:CGRectMake(lineX, lineY, lineW, lineH)];
        [_lineViewDown setBackgroundColor:self.borderButtonColor];
    }
    return _lineViewDown;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
    NSString *identifier = @"XCCheckBoxTableViewCell";
    XCCheckBoxTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    cell.model = _arrayData[indexPath.row];
   
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(_isSingle){
        for(XCCheckBoxModel *model in _arrayData){
            model.isSelected = NO;
        }
    }
   
    XCCheckBoxModel *model = _arrayData[indexPath.row];
    model.isSelected = !model.isSelected;
    if(self.didClickBlock){
        self.didClickBlock(self.arrayData);
     }
    [self checkArray];
    if(_isSingle){
        [self remove];
    }
    [self.tableView reloadData];
    
   
}

- (void)checkArray{
    BOOL isAllSelected = YES;
    for(XCCheckBoxModel *model in _arrayData){
        if(model.isSelected == NO){
            isAllSelected = NO;
            break;
        }
    }
    _buttonRight.selected = isAllSelected;
}

- (void)setArrayData:(NSMutableArray *)arrayData{
    _arrayData = arrayData;
    [self checkArray];
    [self.tableView reloadData];
}

@end

@implementation XCCheckBoxModel

@end
