//
//  XCCheckBoxTableViewCell.m
//  xcwl
//
//  Created by cong on 17/2/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCCheckBoxTableViewCell.h"

@implementation XCCheckBoxTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCCheckBoxModel *)model{
    _model = model;
    if(_model.isSelected){
        [self setAccessoryType:UITableViewCellAccessoryCheckmark];
        _title.textColor = UIColorFromRGB(0x5080D7);
    } else {
        [self setAccessoryType:UITableViewCellAccessoryNone];
        _title.textColor = [UIColor blackColor];
    }
    _title.text = _model.item;
    if(_model.item.length == 0){
        _title.text = _model.title;
    }
}

@end
