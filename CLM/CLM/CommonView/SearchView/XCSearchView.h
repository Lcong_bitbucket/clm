//
//  XCSearchView.h
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCSearchView : UIView<UITextFieldDelegate>{
}
@property (nonatomic,strong) UITextField *searchTF;
@property (nonatomic,strong) UIButton *searchBtn;
@property (nonatomic,copy) void (^didSearchBlock)(NSString *text);

@end
