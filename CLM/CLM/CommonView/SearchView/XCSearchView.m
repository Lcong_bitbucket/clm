//
//  XCSearchView.m
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCSearchView.h"


@implementation XCSearchView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self UI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self UI];
    }
    return self;
}

- (void)UI{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.height/2;
    self.backgroundColor = UIColorFromRGB(0xf0f0f0);
    
    _searchTF = [[UITextField alloc] init];
    _searchTF.font = [UIFont systemFontOfSize:15];
//    _searchTF.textColor = [UIColor whiteColor];
    _searchTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    _searchTF.placeholder = @"查询您感兴趣的关键字...";
    _searchTF.returnKeyType = UIReturnKeySearch;
    _searchTF.delegate = self;
    [_searchTF setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self addSubview:_searchTF];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(textFieldTextDidChange:)
     name:UITextFieldTextDidChangeNotification
     object:_searchTF];

    _searchBtn = [[UIButton alloc] init];
    [_searchBtn setImage:imageNamed(@"icon_search") forState:UIControlStateNormal];
    [_searchBtn addTarget:self action:@selector(search:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_searchBtn];
    
     [_searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_searchBtn.mas_right).offset(8);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(-8);
        make.bottom.mas_equalTo(0);
    }];
    
    [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(5);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(self.mas_height);
    }];
    
}

#pragma mark - textfiled
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    [_searchTF resignFirstResponder];
    return YES;
}


- (void)textFieldTextDidChange:(NSNotification *)n{
    if(self.didSearchBlock){
        self.didSearchBlock(_searchTF.text);
    }
}


- (void)search:(UIButton *)btn{
    if(self.didSearchBlock){
        [_searchTF resignFirstResponder];
        self.didSearchBlock(_searchTF.text);
    }
}

 
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
