//
//  XCPublishView.h
//  CLM
//
//  Created by cong on 16/12/1.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCPublishView : UIView
sharedInstanceH
- (void)showInView:(UIView *)v;
- (void)hide;
@property (nonatomic,copy) void (^hideBlock)(NSInteger type);
@property (nonatomic,strong) NSArray *titles;
@end
