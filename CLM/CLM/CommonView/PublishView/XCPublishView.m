//
//  XCPublishView.m
//  CLM
//
//  Created by cong on 16/12/1.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCPublishView.h"
#import "JMWhenTapped.h"

@implementation XCPublishView

sharedInstanceM

- (void)setTitles:(NSArray *)titles{
    _titles = titles;
    [self whenTapped:^{
        [self hiddView:-1];
    }];
    [self initView];
}

- (void)hide{
    [self hiddView:-1];
}

- (void)initView{
    CGFloat h = 50;
    CGFloat bottom = Screen_Width/5;
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, Screen_Height - h *self.titles.count-bottom, Screen_Width, h*self.titles.count+bottom)];
    bgView.backgroundColor = UIColorFromRGB(0x000000);
    bgView.alpha = 0.9;
    [self addSubview:bgView];
    
    for(int i = 0; i < self.titles.count; i++){
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, Screen_Height - (self.titles.count - i)*h - bottom, Screen_Width,50)];
        [btn addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = i;
        [btn setTitle:self.titles[i] forState:UIControlStateNormal];
        [self addSubview:btn];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)showInView:(UIView *)v{
 
    [v addSubview:self];
    self.frame = CGRectMake(0, v.mj_h, v.mj_w, v.mj_h);
    
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:15 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        self.frame = CGRectMake(0, 0, v.mj_w, v.mj_h);
    } completion:^(BOOL finished) {
    }];
}

- (void)hiddView:(NSInteger)type{
    UIView *superView = self.superview;
    [UIView animateWithDuration:0 delay:0 usingSpringWithDamping:1 initialSpringVelocity:15 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
        self.frame = CGRectMake(0,superView.mj_h, superView.mj_w, superView.mj_h);
    } completion:^(BOOL finished) {
        if(self.hideBlock){
            self.hideBlock(type);
        }
        [self removeFromSuperview];
    }];
}

- (void)choose:(UIButton*)btn{
    [self hiddView:btn.tag];
}

@end
