//
//  AppDelegate.m
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "AppDelegate.h"
#import <UMSocialCore/UMSocialCore.h>
#import "UMMobClick/MobClick.h"
#import "UpdateAlertTableViewCell.h"
#import "FPSDisplay.h"

//极光推送
#import "JPUSHService.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
// 如果需要使用idfa功能所需要引入的头文件（可选）
#import <AdSupport/AdSupport.h>


#import "ChatDemoHelper.h"
#import "AppDelegate+Parse.h"
#import "AppDelegate+EaseMob.h"

@interface AppDelegate ()<JPUSHRegisterDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
  
//基地址配置
#ifdef DEBUG
     if(![[NSUserDefaults standardUserDefaults] objectForKey:@"AFSERVER_DATA"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"http://m.cele.me" forKey:@"AFSERVER_DATA"];
     }
#endif
    [[XCNetworkMonitoring sharedInstance] startMonitoring];

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
//    //版本检测
//    [self checkVersion];
    
    // 初始化环信SDK，详细内容在AppDelegate+EaseMob.m 文件中
    _connectionState = EMConnectionConnected;

//    [self parseApplication:application didFinishLaunchingWithOptions:launchOptions];
    
    NSString *apnsCertName = nil;
    
    [self easemobApplication:application
didFinishLaunchingWithOptions:launchOptions
                      appkey:EASEMOB_KEY
                apnsCertName:EASEMOB_CERTNAME
                 otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
    

    
    [self defaultConfig];//虚线设置
    //初始化界面
    [self initTabBar];
    
    //配置友盟分享
    [self configUmengShare];
    
    //友盟统计
    [self umengTrack];
    //配置键盘
    [self keyBoardManager];
    
    //极光推送配置
    [self JPushConfig:launchOptions];
    
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
#ifdef DEBUG
    [FPSDisplay shareFPSDisplay];
#endif
    return YES;
}

- (void)defaultConfig{
    IPDashedLineView *appearance = [IPDashedLineView appearance];
    [appearance setLineColor:UIColorFromRGB(0xcdcfd4)];
    [appearance setLengthPattern:@[@3, @2]];
}

- (void)initTabBar{
    
    self.tabBarVC = [[XCTabBarController alloc] init];
    [ChatDemoHelper shareHelper].mainVC = self.tabBarVC;

    [self.window setRootViewController:self.tabBarVC ];
 }
#pragma mark - 配置键盘
//配置键盘
-(void)keyBoardManager{
    IQKeyboardManager.sharedManager.enable = true;
    
    //Enabling keyboard manager
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
    manager.keyboardDistanceFromTextField = 0;
    manager.enableAutoToolbar = YES;//这个是它自带键盘工具条开关
    manager.toolbarManageBehaviour = IQAutoToolbarByPosition;
    manager.shouldToolbarUsesTextFieldTintColor = YES;
    manager.shouldResignOnTouchOutside = YES;//这个是点击空白区域键盘收缩的开关
}

#pragma mark - 配置友盟分享
//配置友盟分享
- (void)configUmengShare{
    [[UMSocialManager defaultManager] setUmSocialAppkey:UMSHARE_KEY];

#ifdef DEBUG
    [[UMSocialManager defaultManager] openLog:YES];
#endif
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:SHARE_WECHAT_APPID appSecret:SHARE_WECHAT_SECRECT redirectURL:Social_Share_Link];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:SHARE_QQ_APPID appSecret:SHARE_WECHAT_SECRECT redirectURL:Social_Share_Link];
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:SHARE_SINA_KEY  appSecret:SHARE_SINA_SECRECT redirectURL:Social_Share_Link];
   
}

#pragma mark - 配置友盟统计
//配置友盟统计
- (void)umengTrack {
    UMConfigInstance.appKey = UMENG_APPKEY;
    UMConfigInstance.channelId = @"App Store";
#ifdef DEBUG
    [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    [MobClick startWithConfigure:UMConfigInstance];
#else
    [MobClick startWithConfigure:UMConfigInstance];
#endif
}

#pragma mark - 检查版本
//检查版本
- (void)checkVersion
{
     
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@"GetIOSAppInfo" forKey:@"action"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx" ;
    command.params = params;
    
    WEAKSELF;
    [command setResponseBlock:^(NSDictionary *responseObject, NSError *error) {
        if(error){
            
        } else {
           
                   NSDictionary *_updateDic = responseObject;
            NSInteger curV = [[curVersion stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
            NSInteger updateV = [[_updateDic[@"version"] stringByReplacingOccurrencesOfString:@"." withString:@""] integerValue];
                    if(updateV > curV){
                         UpdateAlertTableViewCell *updateAlertViewCell =  [[[NSBundle mainBundle] loadNibNamed:@"UpdateAlertTableViewCell" owner:self options:nil] lastObject];
                            updateAlertViewCell.updateDic = _updateDic;
//                            updateAlertViewCell.closeBtn.hidden = YES;
                            [updateAlertViewCell setUpdateBlock:^(NSInteger type) {
                                if(type == 0){
                                    [UIView animateWithDuration:1.5 animations:^{
                                        [updateAlertViewCell.contentView removeFromSuperview];
                                    }];
                                } else {
                                    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:_updateDic[@"apk_url"]]])
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_updateDic[@"apk_url"]]];
                                }
                            }];
                            updateAlertViewCell.contentView.frame = self.window.bounds;
                            [self.window addSubview:updateAlertViewCell.contentView];
                            
                        }
                      }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}


#pragma mark - 极光推送设置
- (void)JPushConfig:(NSDictionary *)launchOptions{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
        entity.types = UNAuthorizationOptionAlert|UNAuthorizationOptionBadge|UNAuthorizationOptionSound;
        [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    }
    else if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
                                              categories:nil];
    }
    else {
        //categories 必须为nil
        [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                          UIRemoteNotificationTypeSound |
                                                          UIRemoteNotificationTypeAlert)
                                              categories:nil];
    }
    
    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
#ifdef DEBUG
    [JPUSHService setupWithOption:launchOptions appKey:JPushAppKey
                          channel:JPushChannel
                 apsForProduction:NO
            advertisingIdentifier:advertisingId];
#else
    [JPUSHService setupWithOption:launchOptions appKey:JPushAppKey
                          channel:JPushChannel
                 apsForProduction:YES
            advertisingIdentifier:advertisingId];
#endif
}
 
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    if([url.scheme isEqualToString:@"newmaterial2501"] || [url.scheme isEqualToString:@"xcwl2501"] || [url.scheme isEqualToString:@"clm2501"]){
        return YES;
    }
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if([url.scheme isEqualToString:@"newmaterial2501"] || [url.scheme isEqualToString:@"xcwl2501"] || [url.scheme isEqualToString:@"clm2501"]){
        return YES;
    }
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - 推送相关
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [JPUSHService registerDeviceToken:deviceToken];
}
#pragma mark- JPUSHRegisterDelegate

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        [self handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionSound); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
        [self handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error{
     NSLog(@"注册通知错误 -- %@",error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [self handleRemoteNotification:userInfo];
    XCLog(@"收到通知:%@", [self logDic:userInfo]);
    //JPush Required
    [JPUSHService handleRemoteNotification:userInfo];
}

///custom methor
- (void)handleRemoteNotification:(NSDictionary*)notificationInfo {
    [[XCUserManager sharedInstance] reloadInfoSuccess:^{
        
    } failure:^(NSError *error) {
        
    }];
   
    if([notificationInfo[@"type"] integerValue]){
        if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"通知" message:notificationInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"忽略" otherButtonTitles:@"查看", nil];
            [alertView uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
                if(btnIndex == 1){
                    goADPage(notificationInfo, nil);
                }
            }];
            [alertView show];
        } else {
            goADPage(notificationInfo, nil);
        }
    } else {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"通知" message:notificationInfo[@"aps"][@"alert"] delegate:self cancelButtonTitle:@"忽略" otherButtonTitles:nil];
        [alertView uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            
        }];
        [alertView show];
    }
}

// 极光日志打印
- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}

@end
