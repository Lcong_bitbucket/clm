//
//  XCCommonUrlViewController.h
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCWKWebViewController.h"

@interface XCCommonUrlViewController : XCWKWebViewController
@property (nonatomic,strong) NSString *link_url;
@property (nonatomic,strong) NSString *htmlStr;
@end
