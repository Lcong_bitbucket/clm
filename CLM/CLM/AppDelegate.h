//
//  AppDelegate.h
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCTabBarController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    EMConnectionState _connectionState;

}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) XCTabBarController *tabBarVC;
- (void)checkVersion;
-(void)loginEaseMob :(NSDictionary*)dic success:(void(^)(EMError *error))success;
- (void)initTabBar;
@end

