//
//  XCSocailMainViewController.m
//  CLM
//
//  Created by cong on 17/3/24.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCSocailMainViewController.h"
#import "XCScrollTableViewCell.h"
#import "CycleTableViewCell.h"
#import "ItemTableViewCell.h"
#import "XCPageMenuView.h"
 
@interface XCSocailMainViewController ()<UIScrollViewDelegate>{
    XCPageMenuView *_menuView;
    NSInteger _curVCIndex;
}
@property (nonatomic,strong) NSMutableArray *ad1Array;
@property (nonatomic,strong) NSMutableArray *func1Array;

@end

@implementation XCSocailMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _ad1Array = [NSMutableArray new];
    _func1Array = [NSMutableArray new];
    NSArray *tempfunc1 = @[
                           @{@"title":@"发现需求",@"img_url":@"icon_fxxu"},
                           @{@"title":@"我要测试",@"img_url":@"icon_wycs"},
                           @{@"title":@"提供测试",@"img_url":@"icon_tgcs"},
                           @{@"title":@"预约测试",@"img_url":@"icon_yycs"}
                           ];
    [_func1Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:tempfunc1]];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:NO tableView:self.tableView];
    // Do any additional setup after loading the view from its nib.
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGFloat offsetY = scrollView.contentOffset.y;
//    XCLog(@"offy = %lf %lf",offsetY,139 * Screen_Width / 320 + 105 );
//   __block XCScrollTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
//    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        CGFloat h = 139 * Screen_Width / 320 + 105;
//
//        for(int i = 0; i < cell.tbArray.count; i ++){
//            UITableView *tb = cell.tbArray[i];
//            if(offsetY >= h){
//                tb.scrollEnabled = YES;
//            } else {
//                tb.scrollEnabled = NO;
//            }
//        }
//    });
//    
//}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        CGFloat h = 139 * Screen_Width / 320;
         return h;
    } else if(indexPath.section == 1){
        return 105;
    } else if(indexPath.section == 2){
        CGFloat h = Screen_Height - 64 - 49-44;
        
        return h;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
 
        NSString *identifier = @"CycleTableViewCell";
        CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        if(self.ad1Array.count > 0){
            cell.heightC.constant = 139 * Screen_Width / 320;
            cell.contentView.hidden = NO;
        } else {
            cell.contentView.hidden = YES;
            cell.heightC.constant = 0;
        }
        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic,self);
        }];
        cell.cycles = _ad1Array;
        return cell;
        
    } else if(indexPath.section == 1){
        NSString *identifier = @"ItemTableViewCell";
        ItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^(XCMarkModel *model) {
            if([model.title isEqualToString:@"发现需求"]){
                XCDemandViewController *vc = [[XCDemandViewController alloc] init];
                [vc customBackButton];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            } else if([model.title isEqualToString:@"提供测试"]){
                XCPublishSupplyViewController *vc = [[XCPublishSupplyViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            } else if([model.title isEqualToString:@"预约测试"]){
                XCPublishDemandViewController *vc = [[XCPublishDemandViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                
            } else if([model.title isEqualToString:@"我要测试"]){
                XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }];
        cell.col = 4;
        cell.array = self.func1Array;
        return cell;
    } else if(indexPath.section == 2){
        NSString *identifier = @"XCScrollTableViewCell";
        XCScrollTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
             cell = [[XCScrollTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.index = _curVCIndex;
        [cell setDidScrollBlock:^(BOOL isShowTop) {
           
                if(isShowTop){
                    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                } else {
                    [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                }
          }];
        cell.dataArray = @[@"资讯",@"活动",@"帖子"];
        return cell;
    }
    return nil;
}

#pragma mark - 重写----设置标题和标注的高度
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 2){
       return 44;
    }
    return 0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    if(section == 2){
        UIView *v = [[UIView alloc] init];
        v.backgroundColor = UIColorFromRGB(0xffffff);
        if(!_menuView){
            _menuView = [[XCPageMenuView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width-80, 44)];
             _menuView.titleArray = @[@"资讯",@"活动",@"帖子"];
            [_menuView setDidClickBlock:^(NSInteger index) {
                _curVCIndex = index;
                [tableView reloadData];
            }];
        }
        [v addSubview:_menuView];
        
        UIButton *moreBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, Screen_Width - 80, 80, 44)];
        [moreBtn setTitle:@"更多" forState:UIControlStateNormal];
        
        return v;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
