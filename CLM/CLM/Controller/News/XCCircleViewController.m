//
//  XCCircleViewController.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCCircleViewController.h"
#import "WMPanGestureRecognizer.h"
#import "XCNewsViewController.h"
#import "XCMeetingViewController.h"
#import "XCCardsViewController.h"
#import "CardMainViewController.h"

@interface XCCircleViewController () <UIGestureRecognizerDelegate>{
    CGFloat kWMHeaderViewHeight ;
    CGFloat kNavigationBarHeight ;
}
@property (nonatomic, strong) NSArray *musicCategories;
@property (nonatomic, strong) WMPanGestureRecognizer *panGesture;
@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic, assign) CGFloat viewTop;
@property (nonatomic,strong) NSMutableArray *adArray;

@end

@implementation XCCircleViewController

- (NSArray *)musicCategories {
    if (!_musicCategories) {
        _musicCategories = @[@"资讯", @"活动", @"帖子"];
    }
    return _musicCategories;
}

- (instancetype)init {
    if (self = [super init]) {
        kWMHeaderViewHeight = 125 * Screen_Width / 320 + 90;
        kNavigationBarHeight = 64;
   
        self.menuViewContentMargin = 15;
        self.titleSizeNormal = 15;
        self.titleSizeSelected = 15;
        self.menuViewStyle = WMMenuViewStyleFlood;
        self.menuItemWidth = 60;
        self.menuViewLayoutMode = WMMenuViewLayoutModeLeft;
        self.progressViewCornerRadius = 5;
        self.progressHeight = 30;
        self.menuHeight = 44;
        self.viewTop = kWMHeaderViewHeight + kNavigationBarHeight;
        self.titleColorSelected = UIColorFromRGB(0xffffff);
        self.progressColor = UIColorFromRGB(0x1f59c4);
        self.titleColorNormal = UIColorFromRGB(0x1c1f26);
        self.menuBGColor = UIColorFromRGB(0xffffff);
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"测试圈";

    _adArray = [NSMutableArray new];
    
    self.panGesture = [[WMPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnView:)];
    [self.view addGestureRecognizer:self.panGesture];
    
    for(NSInteger i = 0; i < 3; i++){
        UIView *v = [self.view viewWithTag:i+10];
        [v whenTapped:^{
            switch (i) {
                case 0:
                {
                    XCNewsViewController *vc = [[XCNewsViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [vc customBackButton];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                case 1:
                {
                    XCMeetingViewController *vc = [[XCMeetingViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [vc customBackButton];
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
                    break;
                case 2:
                {
                    CardMainViewController *vc = [[CardMainViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [vc customBackButton];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                    break;
                    
                default:
                    break;
            }
        }];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestAD];
}

- (void)panOnView:(WMPanGestureRecognizer *)recognizer {
    NSLog(@"pannnnnning received..");
    
    CGPoint currentPoint = [recognizer locationInView:self.view];
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        self.lastPoint = currentPoint;
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint velocity = [recognizer velocityInView:self.view];
        CGFloat targetPoint = velocity.y < 0 ? kNavigationBarHeight : kNavigationBarHeight + kWMHeaderViewHeight;
        NSTimeInterval duration = fabs((targetPoint - self.viewTop) / velocity.y);
        
        if (fabs(velocity.y) * 1.0 > fabs(targetPoint - self.viewTop)) {
            NSLog(@"velocity: %lf", velocity.y);
            [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.viewTop = targetPoint;
            } completion:nil];
            
            return;
        }
        
    }
    CGFloat yChange = currentPoint.y - self.lastPoint.y;
    self.viewTop += yChange;
    self.lastPoint = currentPoint;
}

// MARK: ChangeViewFrame (Animatable)
- (void)setViewTop:(CGFloat)viewTop {
    
    _viewTop = viewTop;
    
    if (_viewTop <= kNavigationBarHeight) {
        _viewTop = kNavigationBarHeight;
    }
    
    if (_viewTop > kWMHeaderViewHeight + kNavigationBarHeight) {
        _viewTop = kWMHeaderViewHeight + kNavigationBarHeight;
    }
    
    self.viewFrame = CGRectMake(0, _viewTop-64, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - _viewTop-49);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)requestAD{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"7" forKey:@"place"];
    [params setValue:@"GetAD" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_adArray removeAllObjects];
                [_adArray addObjectsFromArray:jsonResponse[@"ds"]];
                if (_adArray.count>0) {
                    self.cycleScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
                    self.cycleScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
                    self.cycleScrollView.placeholderImage = Default_Loading_Image_1;
                    self.cycleScrollView.delegate = self;
                    self.cycleScrollView.autoScrollTimeInterval = 3;
                    
                    if(_adArray.count <= 1){
                        self.cycleScrollView.infiniteLoop = NO;
                        self.cycleScrollView.autoScroll = NO;
                    } else {
                        self.cycleScrollView.infiniteLoop = YES;
                        self.cycleScrollView.autoScroll = YES;
                    }
                    
                    NSMutableArray *titles = [NSMutableArray new];
                    NSMutableArray *imgs = [NSMutableArray new];
                    for ( int i = 0; i < _adArray.count ; i++){
                        [titles addObject:_adArray[i][@"title"]];
                        [imgs addObject:ImgUrl(_adArray[i][@"img_url"])];
                    }
                    self.cycleScrollView.imageURLStringsGroup = imgs;
                 }

            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    goADPage(_adArray[index], self);
}

#pragma mark - Datasource & Delegate
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.musicCategories.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if(index == 0){
        XCNewsViewController *vc = [[XCNewsViewController alloc] init];
        vc.vcType = 1;
        vc.hidesBottomBarWhenPushed = YES;
        return vc;
    } else if(index == 1){
        XCMeetingViewController *vc = [[XCMeetingViewController alloc] init];
        vc.vcType = 1;
        vc.hidesBottomBarWhenPushed = YES;
        return vc;
    } else{
        XCCardsViewController *vc = [[XCCardsViewController alloc] init];
        vc.vcType = 2;
        vc.hidesBottomBarWhenPushed = YES;
        return vc;
    }
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    return self.musicCategories[index];
}

 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
