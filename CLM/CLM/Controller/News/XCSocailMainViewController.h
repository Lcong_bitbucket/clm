//
//  XCSocailMainViewController.h
//  CLM
//
//  Created by cong on 17/3/24.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCSocailMainViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
