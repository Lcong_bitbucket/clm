//
//  XCCommentModel.h
//  CLM
//
//  Created by cong on 16/12/20.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCCommentModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,assign) NSInteger user_id;
@property (nonatomic,copy) NSString *user_name;
@property (nonatomic,assign) NSInteger article_id;
@end
