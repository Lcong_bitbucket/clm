//
//  XCNewsModel.h
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCNewsModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *zhaiyao;
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,assign) NSInteger click;
@property (nonatomic,copy) NSString *tag;
 
@end
