//
//  XCCommentTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/20.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCCommentTableViewCell.h"
#import "NSString+Emoji.h"

@implementation XCCommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCCommentModel *)model{
    _model = model;
    
    [_avatar setImageURLStr:ImgUrl(_model.avatar) placeholder:Default_Avatar_Image_1];
    _time.text = _model.add_time;
    _content.attributedText = [NSString attributedTextWithText:_model.content];
    _name.text = _model.user_name;
}

@end
