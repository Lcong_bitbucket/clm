//
//  XCSegmentView.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMenuSegmentView.h"

@implementation XCMenuSegmentView
 
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.menuView = [[WMMenuView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, 44)];
        self.menuView.backgroundColor = UIColorFromRGB(0xffffff);
        self.menuView.delegate = self;
        self.menuView.dataSource = self;
        self.menuView.style = WMMenuViewStyleFlood;
        self.menuView.layoutMode = WMMenuViewLayoutModeLeft;
        self.menuView.progressHeight = 30;
        self.menuView.contentMargin = 10;
        self.menuView.progressViewBottomSpace = 5;
        self.menuView.progressViewCornerRadius = 5;
        self.menuView.lineColor = UIColorFromRGB(0x1f59c4);
         
        CALayer *bottomLine = [CALayer layer];
        bottomLine.backgroundColor = UIColorFromRGB(0xe2e2e2).CGColor;
        bottomLine.frame = CGRectMake(0, self.height - 0.5, self.width, 0.5);
        [self.menuView.layer addSublayer:bottomLine];
  
        [self addSubview:self.menuView];

    }
    return self;
}

- (void)layoutSubviews{
    self.menuView.frame = CGRectMake(0, 0, Screen_Width, 44);
    [self.menuView resetFrames];

}

- (void)setTitleArray:(NSArray *)titleArray{
    _titleArray = titleArray;
}

#pragma mark - WMMenuView Delegate
- (void)menuView:(WMMenuView *)menu didSelesctedIndex:(NSInteger)index currentIndex:(NSInteger)currentIndex {
   
    _selectIndex = currentIndex;
    if(self.didClickBlock){
        self.didClickBlock(_selectIndex);
    }
   
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    return [self wm_calculateItemWithAtIndex:index];
 
}

- (CGFloat)menuView:(WMMenuView *)menu itemMarginAtIndex:(NSInteger)index {
  
    return 0;
}

- (CGFloat)menuView:(WMMenuView *)menu titleSizeForState:(WMMenuItemState)state atIndex:(NSInteger)index {
    switch (state) {
        case WMMenuItemStateSelected: {
            return self.titleSizeSelected;
            break;
        }
        case WMMenuItemStateNormal: {
            return self.titleSizeNormal;
            break;
        }
    }
}

- (UIColor *)menuView:(WMMenuView *)menu titleColorForState:(WMMenuItemState)state atIndex:(NSInteger)index {
    switch (state) {
        case WMMenuItemStateSelected: {
            return self.titleColorSelected;
            break;
        }
        case WMMenuItemStateNormal: {
            return self.titleColorNormal;
            break;
        }
    }
}

#pragma mark - WMMenuViewDataSource
- (NSInteger)numbersOfTitlesInMenuView:(WMMenuView *)menu {
    return self.titleArray.count;
}

- (NSString *)menuView:(WMMenuView *)menu titleAtIndex:(NSInteger)index {
    return self.titleArray[index];
}

- (CGFloat)wm_calculateItemWithAtIndex:(NSInteger)index {
    NSString *title = self.titleArray[index];
    UIFont *titleFont = [UIFont systemFontOfSize:self.titleSizeSelected];
    NSDictionary *attrs = @{NSFontAttributeName: titleFont};
    CGFloat itemWidth = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attrs context:nil].size.width;
    return ceil(itemWidth);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
