//
//  XCCommentTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/20.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCCommentModel.h"

@interface XCCommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *content;


@property (nonatomic,strong) XCCommentModel *model;
@end
