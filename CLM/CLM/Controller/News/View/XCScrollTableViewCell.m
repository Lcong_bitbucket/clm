//
//  XCScrollTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/24.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCScrollTableViewCell.h"
#import "XCNewsTableViewCell.h"

@implementation XCScrollTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
 
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self UI];
    }
    return self ;
}

- (void)UI{
    
    _tbArray = [NSMutableArray new];
    
    _scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,Screen_Width, Screen_Height - 64 - 49)];
    _scroll.pagingEnabled = YES;
    [self.contentView addSubview:_scroll];
    
   
}

- (void)setIndex:(NSInteger)index{
    _index = index;
    [self.scroll setContentOffset:CGPointMake(Screen_Width * index, 0) animated:YES];
    if(_dataArray.count > _index){
        
    }
    
}

//- (void)

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    for(int i = 0; i < _dataArray.count; i++){
        UITableView *tb = [[UITableView alloc] initWithFrame:CGRectMake(i*Screen_Width, 0,Screen_Width, Screen_Height - 64 - 49) style:UITableViewStylePlain];
        tb.tag = 10+i;
        tb.separatorStyle = UITableViewCellSeparatorStyleNone;
        tb.dataSource = self;
        tb.delegate = self;
        [_tbArray addObject:tb];
        [self.scroll addSubview:tb];
    }
    self.scroll.contentSize = CGSizeMake(Screen_Width * _dataArray.count,Screen_Height - 64 - 49);
}
 
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if(offsetY > 10){
        if(self.didScrollBlock){
            self.didScrollBlock(NO);
        }
    } else {
        if(self.didScrollBlock){
            self.didScrollBlock(YES);
        }
    }
}


#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"XCNewsTableViewCell";
    XCNewsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
//    cell.model = _dataArray[indexPath.row];
    return cell;
    
}
 


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
