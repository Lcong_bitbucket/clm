//
//  XCNewsTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCNewsTableViewCell.h"

@implementation XCNewsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _title.lineSpacing = 5;
    _mark.textInsets = UIEdgeInsetsMake(2, 4, 2, 4);
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCNewsModel *)model{
    _model = model;
    _title.text = _model.title;
    _des.text = [NSString stringWithFormat:@"%ld浏览 %@",(long)_model.click,_model.add_time];
    _mark.text = _model.tag;
    if(_model.img_url.length > 0){
        [_icon setImageURLStr:ImgUrl(_model.img_url) placeholder:Default_Loading_Image_1];
        _icon.hidden = NO;
        _iconW.constant = 90;
        _iconR.constant = 8;
    } else {
        _icon.hidden = YES;
        _iconW.constant = 0;
        _iconR.constant = 0;
    }
}

@end
