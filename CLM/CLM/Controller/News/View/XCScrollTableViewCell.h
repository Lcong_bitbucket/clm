//
//  XCScrollTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/24.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCScrollTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UIScrollView *scroll;
@property (nonatomic,strong) NSMutableArray *tbArray;

@property (nonatomic, assign) CGPoint lastPoint;

@property (nonatomic,assign) NSInteger index;
@property (nonatomic,strong) NSMutableArray *dataArray;

@property (nonatomic,copy) void (^didScrollBlock)(BOOL isShowTop);
@end
