//
//  XCNewsTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCNewsModel.h"


@interface XCNewsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *title;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconR;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *mark;
@property (nonatomic,strong) XCNewsModel *model;
@end
