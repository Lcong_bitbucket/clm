//
//  XCSegmentView.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMMenuView.h"

@interface XCMenuSegmentView : UIView<WMMenuViewDelegate,WMMenuViewDataSource>
@property (nonatomic, strong) WMMenuView *menuView;
@property (nonatomic,assign) NSInteger selectIndex;
/**
 *  选中时的标题尺寸
 *  The title size when selected (animatable)
 */
@property (nonatomic, assign) CGFloat titleSizeSelected;

/**
 *  非选中时的标题尺寸
 *  The normal title size (animatable)
 */
@property (nonatomic, assign) CGFloat titleSizeNormal;

/**
 *  标题选中时的颜色, 颜色是可动画的.
 *  The title color when selected, the color is animatable.
 */
@property (nonatomic, strong) UIColor *titleColorSelected;

/**
 *  标题非选择时的颜色, 颜色是可动画的.
 *  The title's normal color, the color is animatable.
 */
@property (nonatomic, strong) UIColor *titleColorNormal;

@property (nonatomic,strong) NSArray * titleArray;

@property (nonatomic,copy) void (^didClickBlock)(NSInteger index);

@end
