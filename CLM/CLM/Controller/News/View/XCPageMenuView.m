//
//  XCPageMenuView.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCPageMenuView.h"

@implementation XCPageMenuView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(15, 0, frame.size.width - 30, frame.size.height)];
        [self addSubview:_scroll];
        
        CALayer *bottomLine = [CALayer layer];
        bottomLine.backgroundColor = UIColorFromRGB(0xe2e2e2).CGColor;
        bottomLine.frame = CGRectMake(0, self.height - 0.5, self.width, 0.5);
        [self.layer addSublayer:bottomLine];
        
        _btnArray = [NSMutableArray new];

    }
    return self;
}

- (void)setTitleArray:(NSArray *)titleArray{
    _titleArray = titleArray;
    CGFloat lastW = 0;
    for(NSInteger i = 0; i < _titleArray.count ; i++){
        CGFloat btnw = [self wm_calculateItemWithAtIndex:_titleArray[i]];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(lastW, 5,btnw, self.height-10)];
        [btn setTitle:_titleArray[i] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitleColor:UIColorFromRGB(0x1c1f26) forState:UIControlStateNormal];
        [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
        [btn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0xffffff)] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageWithColor:UIColorFromRGB(0x1f59c4)] forState:UIControlStateSelected];
        btn.tag = 100 + i;
        [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 5;
        [_scroll addSubview:btn];
        
        [_btnArray addObject:btn];
        lastW += btnw + 10;
    }
    self.selectIndex =0;
    _scroll.contentSize = CGSizeMake(lastW, self.height);
}

- (void)btnClick:(UIButton *)btn{
    self.selectIndex = btn.tag - 100;
    
}

- (void)setSelectIndex:(NSInteger)selectIndex{
    _selectIndex = selectIndex;
    for(int i = 0; i < _btnArray.count; i ++){
        if(i == _selectIndex){
            [_btnArray[i] setSelected:YES];
            if(self.didClickBlock){
                self.didClickBlock(i);
            }
        } else {
            [_btnArray[i] setSelected:NO];
        }
    }
}

- (CGFloat)wm_calculateItemWithAtIndex:(NSString*)title {
    
    UIFont *titleFont = [UIFont systemFontOfSize:14];
    NSDictionary *attrs = @{NSFontAttributeName: titleFont};
    CGFloat itemWidth = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:attrs context:nil].size.width;
    return ceil(itemWidth+20);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
