//
//  XCPageMenuView.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCPageMenuView : UIView{
    UIScrollView *_scroll;
}
@property (nonatomic,strong) NSMutableArray * btnArray;
 @property (nonatomic,strong) NSArray * titleArray;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger index);
@property (nonatomic,assign) NSInteger selectIndex;

@end
