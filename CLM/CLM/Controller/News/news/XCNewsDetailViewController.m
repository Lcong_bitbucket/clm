//
//  XCNewsDetailViewController.m
//  CLM
//
//  Created by cong on 16/12/27.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCNewsDetailViewController.h"

#import "JHInputToolBar.h"
#import "UIView+Extension.h"
#import "JHEmotionKeyboard.h"
#import "JHEmotion.h"
#import "JHEmotionTextView.h"
#import "JHTextPart.h"
#import "RegexKitLite.h"
#import "JHEmotionTool.h"

@interface XCNewsDetailViewController ()<JHInputToolBarDelegate>{
    NSInteger *answer_id;
}

@property (nonatomic, strong) JHInputToolBar *toolbar;//自定义的toolBar
@property (nonatomic,assign) CGFloat keyboardH;//系统键盘的高度
@property (nonatomic,assign) BOOL switchingKeybaord;//是否切换键盘
@property (nonatomic, strong) JHEmotionKeyboard *emotionKeyboard;//自定义表情键盘

@end

@implementation XCNewsDetailViewController
#pragma mark - keyboard
- (JHEmotionKeyboard *)emotionKeyboard
{
    if (_emotionKeyboard == nil) {
        _emotionKeyboard = [[JHEmotionKeyboard alloc]init];
        _emotionKeyboard.width = Screen_Width;
        _emotionKeyboard.height = self.keyboardH;
    }
    return _emotionKeyboard;
}

/**
 *  处理键盘的方法
 *
 *  @param notification
 */
- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    if (self.switchingKeybaord) return;
    
    NSDictionary *userInfo = notification.userInfo;
    // 动画的持续时间
    double duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // 键盘的frame
    CGRect keyboardF = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardH = keyboardF.size.height;
    
    // 执行动画
    [UIView animateWithDuration:duration animations:^{
        // 工具条的Y值 == 键盘的Y值 - 工具条的高度
        if (keyboardF.origin.y > self.view.height) { // 键盘的Y值已经远远超过了控制器view的高度
            self.toolbar.y = self.view.height - self.toolbar.height;
        } else {
            self.toolbar.y = keyboardF.origin.y - self.toolbar.height-64;
        }
    }];
}

/**
 *  创建工具条
 */
- (void)toolBarCommon
{
    self.toolbar = [[JHInputToolBar alloc]init];
    self.toolbar.frame = CGRectMake(0, Screen_Height-64-60, Screen_Width, 60);
    self.toolbar.delegate = self;
    [self.view addSubview:self.toolbar];
}
/**
 *  点击工具条的代理方法
 *
 *  @param toolBar toolBar
 *  @param tag     点击工具条上的按钮的tag
 */
- (void)toolBar:(JHInputToolBar *)toolBar ButtonTag:(NSUInteger)tag andTextView:(JHEmotionTextView *)textView {
    switch (tag) {
        case 0: // 表情
            [self switchKeyboard:textView];
            break;
        case 1: // 相册
            break;
        case 2: // 发送评论
            [self sendComment:textView.fullText];
            break;
    }
    
}
/**
 *  切换 表情／键盘
 */
- (void)switchKeyboard:(JHEmotionTextView *)textView
{
    if(!textView.isFirstResponder)
        [textView becomeFirstResponder];
    
    if (textView.inputView == nil) {//切换成表情键盘
        
        textView.inputView = self.emotionKeyboard;
        
        self.toolbar.showKeyboardButton = YES;//显示成键盘按钮
        
    }else//切换成系统键盘
    {
        textView.inputView = nil;
        self.toolbar.showKeyboardButton = NO;//显示成表情按钮
    }
    
    // 开始切换键盘
    self.switchingKeybaord = YES;
    
    // 退出键盘
    [textView endEditing:YES];
    //    [self.view endEditing:YES];
    //    [self.view.window endEditing:YES];
    //    [self.textView resignFirstResponder];
    // 结束切换键盘
    self.switchingKeybaord = NO;//这行代码要放在下面的  弹出键盘  的前面，不然会出现bug
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 弹出键盘
        [textView becomeFirstResponder];
    });
    
}

- (void)sendComment:(NSString *)text{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"comment_add" forKey:@"action"];
    [params setValue:text forKey:@"txtContent"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self.webView reload];
                //                            page = 1;
                //                            [self requestComment:page];
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customShareButton];
    self.title = @"资讯详情";
    self.webView.frame = CGRectMake(0, 0, Screen_Width, Screen_Height - 64 - 60);
    
    //处理键盘
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    //创建toolBar
    [self toolBarCommon];
    
    if(self.id){
        NSString *urlString = nil;
        if([[XCUserManager sharedInstance] isLogin]){
            urlString = [NSString stringWithFormat:@"%@/app_news_detail.aspx?id=%ld&uid=%ld&version=%@",AFSERVER_DATA,(long)self.id,(long)[XCUserManager sharedInstance].userModel.id,curVersion];
        } else {
            urlString = [NSString stringWithFormat:@"%@/app_news_detail.aspx?id=%ld&version=%@",AFSERVER_DATA,(long)self.id,curVersion];
        }
        NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
      
        [self.webView loadRequest:req];
    }
    
    [self.bridge registerHandler:@"handleClick" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSLog(@"response: %@", data);
        if([data[@"jumptype"] integerValue] == 17){
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                NSString *urlString = self.webView.URL.absoluteString;
                
                NSString *urlstr = [NSString stringWithFormat:@"%@&uid=%ld",urlString,(long)[XCUserManager sharedInstance].userModel.id];
                
                NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlstr]];
                [self.webView loadRequest:req];
                
            }];
        } else {
            goADPage(data, self);
        }
        responseCallback(@"成功");
    }];

    [self getAttach:self.id];

    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    if(self.type == 0){
       [params setValue:@(5) forKey:@"type"];
    } else {
         [params setValue:@(11) forKey:@"type"];
    }

    [params setValue:@(self.id) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
