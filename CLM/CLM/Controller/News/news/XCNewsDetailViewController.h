//
//  XCNewsDetailViewController.h
//  CLM
//
//  Created by cong on 16/12/27.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCWKWebViewController.h"

@interface XCNewsDetailViewController : XCWKWebViewController
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,assign) NSInteger type;//0资讯 1 帖子
@end
