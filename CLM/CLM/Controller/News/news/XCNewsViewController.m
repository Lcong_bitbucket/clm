//
//  XCNewsViewController.m
//  CLM
//
//  Created by cong on 16/12/1.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCNewsViewController.h"
#import "XCSearchView.h"

#import "XCPopView.h"
#import "CycleTableViewCell.h"
#import "XCNewsTableViewCell.h"
#import "XCNewsDetailViewController.h"

@interface XCNewsViewController ()
{
    NSString *_keyword;
 }
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *adArray;

@end

@implementation XCNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray new];
    _adArray = [NSMutableArray new];
    [self customBackButton];
    XCSearchView *searchView = [[XCSearchView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 80, 30)];
    searchView.searchTF.placeholder = @"输入您要查找的资讯...";
    [searchView setDidSearchBlock:^(NSString *text) {
        _keyword = text;
        self.curPage = 1;
        [self refresh];
    }];
 
    self.navigationItem.titleView = searchView;
     
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    
    [self beginRefreshing];
    [self customShareButton];

    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    [params setValue:@(5) forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

#pragma mark - 数据请求

- (void)refresh{
    if(self.curPage == 1&&self.vcType== 0)
        [self requestAD];
    [self request:self.curPage];
}

- (void)request:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:_keyword forKey:@"keyword"];
    [params setValue:@"GetNewsList" forKey:@"action"];
    if(self.vcType == 0){
        [params setValue:@(0) forKey:@"type"];

    } else if(self.vcType == 1){
        [params setValue:@(1) forKey:@"type"];

    }

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];

        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1){
                    [_dataArray removeAllObjects];
                }
                [_dataArray addObjectsFromArray:[XCNewsModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                [self stopRefresh:NO];
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestAD{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"1" forKey:@"place"];
    [params setValue:@"GetAD" forKey:@"action"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_adArray removeAllObjects];
                [_adArray addObjectsFromArray:jsonResponse[@"ds"]];
                [_tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if(self.adArray.count > 0)
            return 1;
        return 0;
    } else {
        return _dataArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        
        NSString *identifier = @"CycleTableViewCell";
        CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        if(self.adArray.count > 0){
            cell.heightC.constant = 139 * Screen_Width / 320;
            cell.contentView.hidden = NO;
        } else {
            cell.contentView.hidden = YES;
            cell.heightC.constant = 0;
        }
        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic,self);
        }];
        cell.cycles = _adArray;
        return cell;
        
    } else if(indexPath.section == 1){
        NSString *identifier = @"XCNewsTableViewCell";
        XCNewsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _dataArray[indexPath.row];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XCNewsDetailViewController *vc = [[XCNewsDetailViewController alloc] init];
 
//    XCNewDetailViewController *vc = [[XCNewDetailViewController alloc] init];
    vc.id = [_dataArray[indexPath.row] id];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
