//
//  XCCircleViewController.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "WMPageController.h"
#import "SDCycleScrollView.h"

@interface XCCircleViewController : WMPageController<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet SDCycleScrollView *cycleScrollView;

 

@end
