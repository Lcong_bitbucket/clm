//
//  AllPartViewController.h
//  VKTemplateForiPhone
//
//  Created by  Ivan on 15/12/30.
//  Copyright © 2015年 Vescky. All rights reserved.
//

#import "XCTableViewController.h"

@interface AllPartViewController : XCTableViewController
{
    IBOutlet UITableView *tbView,*tbView2;
}
@property(nonatomic,assign)BOOL isSelected;//是否为选择界面
@property (nonatomic,copy) void (^didClickBlock)(NSDictionary *dic);
@end
