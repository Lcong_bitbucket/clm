//
//  AllPartViewController.m
//  VKTemplateForiPhone
//
//  Created by  Ivan on 15/12/30.
//  Copyright © 2015年 Vescky. All rights reserved.
//

#import "AllPartViewController.h"
#import "XCPartMenuFirstTableViewCell.h"
#import "XCPartMenuSecTableViewCell.h"
#import "XCCardsViewController.h"

@interface AllPartViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_dataArray;
    NSInteger _firstSelected;
}
@end

@implementation AllPartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray new];
    [self defaultConfig:tbView];
    [self defaultConfig:tbView2];
    
    [self customBackButton];
    self.title = @"所有版块";

    [self requestPart];
}
-(void)didselect:(NSInteger)index
{
    _firstSelected = index;
    [tbView selectRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    [tbView2 reloadData];
}

#pragma mark loadData
- (void)requestPart{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShequCategory" forKey:@"action"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if ([jsonResponse[@"status"] integerValue] == 1) {
                NSArray *datalist=[jsonResponse objectForKey:@"ds"];
                [_dataArray addObjectsFromArray:datalist];
                [tbView reloadData];
                [self didselect:0];
                
            }else{
                showHint(jsonResponse[@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}


#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:tbView]) {
        return _dataArray.count;
    }
    if(_dataArray.count > _firstSelected){
        return [_dataArray[_firstSelected][@"ds"] count];
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tbView]) {
        NSString *identifier = @"XCPartMenuFirstTableViewCell";
        XCPartMenuFirstTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.title.text = _dataArray[indexPath.row][@"title"];
        cell.backgroundColor=tableView.backgroundColor;
        cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame] ;
        cell.selectedBackgroundView.backgroundColor =[UIColor whiteColor];
        
        return cell;
    }
    else
    {
        NSString *identifier = @"XCPartMenuSecTableViewCell";
        XCPartMenuSecTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        NSArray *array = _dataArray[_firstSelected][@"ds"];
        NSDictionary *dic = array[indexPath.row];
        cell.title.text = dic[@"title"];
        [cell.icon setImageURLStr:ImgUrl(dic[@"img_url"]) placeholder:Default_Loading_Image_1];
        cell.content.text = dic[@"content"];
        return cell;
    }
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:tbView]) {
        [self didselect:indexPath.row];
    } else {
         NSArray *array = _dataArray[_firstSelected][@"ds"];
         NSDictionary *dic = array[indexPath.row];
        
        [tbView2 deselectRowAtIndexPath:indexPath animated:YES];

        if(self.isSelected){
            if(self.didClickBlock){
                self.didClickBlock(dic);
            }
            [self goBack];
        } else {
            XCCardsViewController *vc = [[XCCardsViewController alloc] init];
            vc.category = dic;
            [vc customBackButton];
            [self.navigationController pushViewController:vc animated:YES];
            
        }
        
    }
}


@end
