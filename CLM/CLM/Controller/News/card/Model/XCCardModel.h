//
//  XCNewsModel.h
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCCardModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *zhaiyao;
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,assign) NSInteger click;
@property (nonatomic,copy) NSString *tag;
@property (nonatomic,copy) NSString *nick_name;
@property (nonatomic,copy) NSString *category;
@property (nonatomic,assign) NSInteger category_id;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,assign) NSInteger comment_count;

@end
