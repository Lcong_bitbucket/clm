//
//  XCPublishCardViewController.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"
@interface XCPublishCardViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UIView *categoryTop;
 
@property (weak, nonatomic) IBOutlet UITextField *categoryTF;
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *desTX;
@property (weak, nonatomic) IBOutlet UIScrollView *picScroll;

- (IBAction)fabuClick:(id)sender;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) void (^didPublishBlock)();

@end
