//
//  XCNewsViewController.h
//  CLM
//
//  Created by cong on 16/12/1.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCCardsViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign) NSInteger vcType; //0 普通列表 1 我发布的 2热门

@property (nonatomic,strong) NSDictionary *category;
@end
