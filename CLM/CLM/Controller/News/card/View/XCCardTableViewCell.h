//
//  XCNewsTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCCardModel.h"


@interface XCCardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *title;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UILabel *click;
@property (weak, nonatomic) IBOutlet UILabel *comment;

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconR;
@property (nonatomic,strong) XCCardModel *model;
@end
