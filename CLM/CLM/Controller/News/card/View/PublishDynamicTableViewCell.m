//
//  PublishDynamicTableViewCell.m
//  xcwl
//
//  Created by cong on 16/10/26.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "PublishDynamicTableViewCell.h"

@implementation PublishDynamicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)click:(id)sender {
    if(self.didClickBlock){
        self.didClickBlock();
    }
}
@end
