//
//  XCPartMenuFirstTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCPartMenuFirstTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
