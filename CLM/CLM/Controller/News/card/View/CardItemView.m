//
//  GDItemView.m
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "CardItemView.h"

@implementation CardItemView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        _icon = [[UIImageView alloc] init];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:13];
        _title.textColor = getHexColor(@"#999999");
        _title.textAlignment = NSTextAlignmentCenter;
        
        _rLine = [[UIView alloc] init];
        _rLine.backgroundColor = getHexColor(@"#eaeaea");
        
        _bLine = [[UIView alloc] init];
        _bLine.backgroundColor = getHexColor(@"#eaeaea");
        
        [self addSubview:_icon];
        [self addSubview:_title];
        [self addSubview:_rLine];
        [self addSubview:_bLine];
    }
    return self;
}
- (void)setModel:(NSDictionary *)model{
    _model = model;
 
    WEAKSELF;
     [_icon setImageWithURLStr:_model[@"img_url"] placeholderImage:Default_Loading_Image_2 completed:^(UIImage *image, NSError *error) {
        if(!error){
            weakSelf.icon.image = image;
            CGFloat h = 50;//image.size.height * 90 / image.size.width;
            CGFloat w = 50;
            weakSelf.icon.frame = CGRectMake((self.width - w)/2, 10+(50-h)/2, w, h);

//            _icon.frame = CGRectMake((self.width - 90)/2, 10+(50-h)/2, 90, h);
        } else {
            weakSelf.icon.frame = CGRectMake((self.width - 50)/2, 10, 50, 50);
        }
    }];
     
    
    _title.text = _model[@"title"];
    _title.frame = CGRectMake(0, self.height - 25, self.width, 20);
    
    _rLine.frame = CGRectMake(self.width - 0.5, 0, 0.5, self.height);
    _bLine.frame = CGRectMake(0, self.height - 0.5, self.width, 0.5);
}


@end
