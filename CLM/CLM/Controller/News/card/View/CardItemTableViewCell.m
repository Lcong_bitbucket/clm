//
 //  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "CardItemTableViewCell.h"
#import "CardItemView.h"
@implementation CardItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setArray:(NSMutableArray *)array{
    _array = array;
    [self.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat w = ceilf(Screen_Width/4.0);
    CGFloat h = 90;

    for(int i = 0; i < _array.count; i++){
        CardItemView *itemView = [[CardItemView alloc] init];
        itemView.frame = CGRectMake((i%4)*w, h*(i/4), w, h);
        itemView.model = _array[i];
        [self.contentView addSubview:itemView];
        [itemView whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(_array[i]);
            }
        }];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
