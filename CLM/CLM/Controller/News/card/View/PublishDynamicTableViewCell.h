//
//  PublishDynamicTableViewCell.h
//  xcwl
//
//  Created by cong on 16/10/26.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublishDynamicTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *publishBtn;
- (IBAction)click:(id)sender;
@property (nonatomic,copy) void (^didClickBlock)();
@end
