//
//  GDItemView.h
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
 @interface CardItemView : UIView

@property (strong, nonatomic) UIImageView *icon;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UIView *rLine;
@property (strong, nonatomic) UIView *bLine;
@property (nonatomic,strong) NSDictionary *model;
@end
