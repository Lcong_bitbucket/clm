//
 //  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface CardItemTableViewCell : UITableViewCell
@property (nonatomic,strong) NSMutableArray *array;
@property (nonatomic,copy) void (^didClickBlock)(NSDictionary *model);

@end
