//
//  CardMainViewController.m
//  xcwl
//
//  Created by cong on 16/11/2.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "CardMainViewController.h"
#import "CycleTableViewCell.h"

#import "CardItemTableViewCell.h"
#import "HotSectionView.h"
#import "XCPageMenuView.h"
#import "XCNewsDetailViewController.h"
#import "XCCardTableViewCell.h"
#import "XCCardsViewController.h"
#import "PublishDynamicTableViewCell.h"
#import "AllPartViewController.h"
#import "XCPublishCardViewController.h"
 
@interface CardMainViewController (){
    NSString *_keywords;
    XCPageMenuView *_menuView;
}
@property (nonatomic,strong) NSMutableArray *ad1Array;
@property (nonatomic,strong) NSMutableArray *func1Array;
@property (nonatomic,strong) NSMutableArray *hotCardArray;
@end

@implementation CardMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    [self customNavigationBarItemWithImageName:@"icon_search" title:nil isLeft:NO];
    self.title = @"帖子";
    
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    CGRect frame=CGRectMake(0, 0, 0, CGFLOAT_MIN);
    self.tableView.tableHeaderView=[[UIView alloc]initWithFrame:frame];

    self.ad1Array = [NSMutableArray new];
    self.func1Array = [NSMutableArray new];
    self.hotCardArray = [NSMutableArray new];
    
    _menuView = [[XCPageMenuView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 80, 44)];
    _menuView.backgroundColor = [UIColor whiteColor];
    [_menuView setDidClickBlock:^(NSInteger type) {
        [self beginRefreshing];
    }];
    _menuView.titleArray = @[@"最新",@"最热"];
    
    [self beginRefreshing];
    // Do any additional setup after loading the view from its nib.
}


- (void)rightBarButtonAction:(UIButton *)btn{
    XCCardsViewController *vc = [[XCCardsViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)refresh{
    [self requestAD];
    [self requestPart];
    [self requestHotTiezi:self.curPage];
}

#pragma mark - Request
- (void)requestAD{
     NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"8" forKey:@"place"];
    [params setValue:@"GetAD" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.cachePolicy = XCHTTPClientReturnCacheDataThenLoad;
    [command setResponseBlock:^(NSDictionary *responseObject, NSError *error) {
        if(error){
        } else {
            if ([responseObject[@"status"] integerValue] == 1){
                [self.ad1Array removeAllObjects];
                [self.ad1Array addObjectsFromArray:responseObject[@"ds"]];
                
                [self.tableView reloadData];
           
            }else {
                showHint(responseObject[@"msg"]);
            }
        }
    }];
    
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestPart{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
     [params setValue:@"GetHotShequCategory" forKey:@"action"];
    XCBaseCommand *command = [XCBaseCommand new];
        command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([jsonResponse[@"status"] integerValue] == 1){
                [self.func1Array removeAllObjects];
                [self.func1Array addObjectsFromArray:jsonResponse[@"ds"]];
                [_tableView reloadData];
            } else {
                showHint(jsonResponse[@"msg"]);
            }
         }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

-(void)requestHotTiezi:(NSInteger )page
{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];

    [params setValue:@(_menuView.selectIndex) forKey:@"type"];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
     [params setValue:@(10) forKey:@"rows"];
    [params setValue:@(self.curPage) forKey:@"page"];
    [params setValue:@"GetShequArticles" forKey:@"action"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.cachePolicy = XCHTTPClientReturnCacheDataThenLoad;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
        } else {
            if ([jsonResponse[@"status"] integerValue] == 1) {
                if(self.curPage == 1)
                    [self.hotCardArray removeAllObjects];
                [self.hotCardArray addObjectsFromArray:[XCCardModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else
                    [self stopRefresh:NO];
                [_tableView reloadData];

             }
            else {
                showHint(jsonResponse[@"desc"]);
                [self stopRefresh:NO];
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
 
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 1;
    } else if(section == 1){
        return 1;
    } else if(section == 2) {
        return self.hotCardArray.count;
    } else if(section == 3){
        return 1;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        return  UITableViewAutomaticDimension;
    } else if(indexPath.section == 1){
         return ceilf(self.func1Array.count/4.0)*90;
    } else if(indexPath.section == 2){
         return  UITableViewAutomaticDimension;
    } else if(indexPath.section == 3){
        return UITableViewAutomaticDimension;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
         NSString *identifier = @"CycleTableViewCell";
        CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.heightC.constant = 120 * Screen_Width / 375;

        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic, self);
        }];
        cell.cycles = self.ad1Array;
        return cell;
    }else if(indexPath.section == 1){
 
        NSString *identifier = @"CardItemTableViewCell";
        CardItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^(NSDictionary *dic) {
            XCCardsViewController *vc = [[XCCardsViewController alloc] init];
            vc.category = dic;
            [vc customBackButton];
             [self.navigationController pushViewController:vc animated:YES];
        }];
        cell.array = self.func1Array;
        return cell;
        
    }else if(indexPath.section == 2){
        NSString *identifier = @"XCCardTableViewCell";
        XCCardTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _hotCardArray[indexPath.row];
        return cell;
    } else if(indexPath.section == 3){
        NSString *identifier = @"PublishDynamicTableViewCell";
        PublishDynamicTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }

        [cell.publishBtn setTitle:@"我要发帖子" forState:UIControlStateNormal];
        [cell.publishBtn setImage:imageNamed(@"icon_write") forState:UIControlStateNormal];
        [cell setDidClickBlock:^{
            XCLog(@"发布帖子");
            XCPublishCardViewController *vc = [[XCPublishCardViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        return cell;
        
    }
    return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 2){
        XCNewsDetailViewController *vc = [[XCNewsDetailViewController alloc] init];
        vc.type = 1;
        vc.id = [_hotCardArray[indexPath.row] id];
        vc.hidesBottomBarWhenPushed =YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - 重写----设置标题和标注的高度
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat h = CGFLOAT_MIN;
    switch (section) {
        case 1:
            h = 40;
            break;
        case 2:
            h = 44;
            break;
         default:
            h = CGFLOAT_MIN;
            break;
    }
    return h;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    CGFloat h = CGFLOAT_MIN;
    switch (section) {
        case 1:
            return 10;
            break;
            default:
            h = CGFLOAT_MIN;
            break;
    }
    return h;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = nil;
    switch (section) {
        case 1:
        {
            header = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil] lastObject];
            ((HotSectionView *)header).title.text = @"热门版块";
            [header whenTouchedUp:^{
                AllPartViewController *vc = [[AllPartViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
             }];
        }
            break;
        case 2:
        {
            header = [[UIView alloc] init];
            header.backgroundColor = [UIColor whiteColor];
            [header addSubview:_menuView];
            
            UIButton *moreBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - 80, 0, 80, 44)];
            [moreBtn setTitle:@"更多" forState:UIControlStateNormal];
            [moreBtn setTitleColor:UIColorFromRGB(0x999999) forState:UIControlStateNormal];
            moreBtn.titleLabel.font = [UIFont systemFontOfSize:14];
            [moreBtn addTarget:self action:@selector(moreClick:) forControlEvents:UIControlEventTouchUpInside];
            [header addSubview:moreBtn];
        }
            break;
         default:
            header = nil;
            break;
    }
    return header;
}

- (void)moreClick:(UIButton *)btn{
    XCCardsViewController *vc = [[XCCardsViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footView;
    switch (section) {
        case 1:{
            footView = [[UIView alloc] init];
            footView.backgroundColor = UIColorFromRGB(0xEDF1F4);
        }
            break;
            
        default:
            footView = nil;
            break;
    }
    
    return footView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
