//
//  XCPublishCardViewController.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCPublishCardViewController.h"
#import "XCCardModel.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "AllPartViewController.h"

@interface XCPublishCardViewController ()<TZImagePickerControllerDelegate>{
    XCCardModel *_model;
    NSInteger category_id;
}
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableArray *photos;
@property (nonatomic , strong) NSMutableArray *imgUrls;
@end

@implementation XCPublishCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发布帖子";
    [self customBackButton];
    
    _assets = [NSMutableArray new];
    _photos = [NSMutableArray new];
    _imgUrls = [NSMutableArray new];
    
    [_categoryTop whenTapped:^{
        AllPartViewController *vc = [[AllPartViewController alloc] init];
        vc.isSelected = YES;
        [vc setDidClickBlock:^(NSDictionary *dic) {
            category_id = [dic[@"id"] integerValue];
            _categoryTF.text = dic[@"title"];
        }];
        [self.navigationController pushViewController:vc animated:YES];
        
    }];
    
    _desTX.placeholder = @"请输入您想跟大家分享的内容";
    [self reloadScrollView];
    if(self.id){
        [self requestDetail];
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"GetNewsDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _model = [XCCardModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    [self refreshUI];
                }
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)refreshUI{
    if(_model){
        _categoryTF.text = _model.category;
        category_id = _model.category_id;
        _titleTF.text = _model.title;
        _desTX.text = _model.content;
        if(_model.img_url.length > 0)
            [_imgUrls addObjectsFromArray:[_model.img_url componentsSeparatedByString:@","]];
        [self reloadScrollView];
    }
}

- (void)reloadScrollView{
    // 先移除，后添加
    [[self.picScroll subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // 加一是为了有个添加button
    NSUInteger assetCount = self.imgUrls.count + 1;
    CGFloat width = 56;
    
    for (NSInteger i = 0; i < assetCount; i++) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.imageView.contentMode = UIViewContentModeScaleAspectFill;
        btn.frame = CGRectMake(8 + (width+10) * i, 5, width, width);
        [btn addTarget:self action:@selector(pushImagePickerController) forControlEvents:UIControlEventTouchUpInside];
        
        // UIButton
        if (i == self.imgUrls.count){
            // 最后一个Button
            [btn setImage:[UIImage imageNamed:@"icon_file"] forState:UIControlStateNormal];
        }else{
            // 如果是本地ZLPhotoAssets就从本地取，否则从网络取
            [btn setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:ImgUrl(self.imgUrls[i])] placeholderImage:Default_Loading_Image_1];
        }
        
        [self.picScroll addSubview:btn];
    }
    
    self.picScroll.contentSize = CGSizeMake((self.assets.count + 1) * (width + 10) + 16, width);
    
    //    if(_imgUrls.count > 0){
    //        [_picBtn setImageWithURLString:ImgUrl(_imgUrls[0]) forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"icon_file"]];
    //    } else {
    //        [_picBtn setImage:[UIImage imageNamed:@"icon_file"] forState:UIControlStateNormal];
    //    }
}

- (void)photoSelect:(UIButton *)btn{
    [self pushImagePickerController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)pushImagePickerController {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    
    
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.isSelectOriginalPhoto = YES;
    
    imagePickerVc.selectedAssets = self.assets; // 目前已经选中的图片数组
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
    // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
    // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];
    
    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingGif = NO;
    
    // 4. 照片排列按修改时间升序
    imagePickerVc.sortAscendingByModificationDate = YES;
    
    // imagePickerVc.minImagesCount = 3;
    // imagePickerVc.alwaysEnableDoneBtn = YES;
    
    // imagePickerVc.minPhotoWidthSelectable = 3000;
    // imagePickerVc.minPhotoHeightSelectable = 2000;
    
    /// 5. Single selection mode, valid when maxImagesCount = 1
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = YES;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.needCircleCrop = NO;
    imagePickerVc.circleCropRadius = Screen_Width/2;
    /*
     [imagePickerVc setCropViewSettingBlock:^(UIView *cropView) {
     cropView.layer.borderColor = [UIColor redColor].CGColor;
     cropView.layer.borderWidth = 2.0;
     }];*/
    
    //imagePickerVc.allowPreview = NO;
#pragma mark - 到这里为止
    
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}
#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    self.assets = assets.mutableCopy;
    self.photos = photos.mutableCopy;
    if(self.assets.count > 0){
        [_imgUrls removeAllObjects];
        for(int i = 0; i < self.assets.count ; i ++){
            [_imgUrls addObject:@""];
        }
        for(int i = 0; i < self.assets.count ; i++){
            XCBaseCommand *command = [XCBaseCommand new];
            command.curView = self.view;
            
            command.imgs = @[self.photos[i]];
            [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
                if(error){
                    
                } else {
                    if([[jsonResponse objectForKey:@"status"] intValue]==1)
                    {
                        NSString *logPath=[jsonResponse objectForKey:@"path"];
                        [_imgUrls replaceObjectAtIndex:i withObject:logPath];
                        showHint(jsonResponse[@"desc"]);
                        [self reloadScrollView];
                    }  else {
                        showHint(jsonResponse[@"desc"]);
                    }
                }
            }];
            [[XCHttpClient sharedInstance] upload:command];
        }
    } else {
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fabuClick:(id)sender {
    if(_categoryTF.text.length == 0){
        showHint(@"请选择版块分类");
        [XCTools shakeAnimationForView:_categoryTF];
        
    }else if(_titleTF.text.length == 0){
        showHint(@"请输入标题");
        [XCTools shakeAnimationForView:_titleTF];
        
    } else if(_desTX.text.length == 0){
        showHint(@"请输入内容");
        [XCTools shakeAnimationForView:_desTX];
        
    }else {
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                [self request];
            }
        }];
    }
}

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(category_id) forKey:@"category_id"];
    [params setValue:_titleTF.text forKey:@"title"];
    [params setValue:_desTX.text forKey:@"content"];

    NSMutableString *img_url = [[NSMutableString alloc] init];
    for(int i = 0; i < _imgUrls.count ; i ++){
        if(i == _imgUrls.count -1 ){
            [img_url appendString:[NSString stringWithFormat:@"%@",_imgUrls[i]]];
        } else {
            [img_url appendString:[NSString stringWithFormat:@"%@,",_imgUrls[i]]];
        }
    }
    
    if(self.id){
        [params setValue:@(self.id) forKey:@"id"];
    }
    
   [params setValue:img_url forKey:@"img_url"];
    
    [params setValue:@"EditShequArticle" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1) {
                showHint(jsonResponse[@"msg"]);
                if(self.didPublishBlock){
                    self.didPublishBlock();
                }
                 [self goBack];
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

@end
