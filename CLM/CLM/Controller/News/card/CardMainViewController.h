//
//  CardMainViewController.h
//  xcwl
//
//  Created by cong on 16/11/2.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface CardMainViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
