//
//  MeetingListTableViewCell.h
//  VKTemplateForiPhone
//
//  Created by licong on 16/2/26.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MeetingtListModel.h"
@interface MeetingListTableViewCell : UITableViewCell
{
    CALayer* _bottomLine;
}
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *prompt;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *title;
 @property (weak, nonatomic) IBOutlet TTTAttributedLabel *time;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *address;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *scan;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *price;

@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,strong) MeetingtListModel *model ;
@end
