//
//  MeetingListTableViewCell.m
//  VKTemplateForiPhone
//
//  Created by licong on 16/2/26.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "MeetingListTableViewCell.h"
#import "NSString+Emoji.h"


@implementation MeetingListTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _prompt.textInsets = UIEdgeInsetsMake(2, 5, 2, 5);
    _title.numberOfLines = 2;
    _title.lineSpacing = 5;
    
    _bottomLine = [CALayer layer];
    _bottomLine.backgroundColor = UIColorFromRGB(0xcccccc).CGColor;
    [self.layer addSublayer:_bottomLine];
    
}

- (void)layoutSubviews{
    _bottomLine.frame = CGRectMake(0, self.height - 1 , self.width, 1/[UIScreen mainScreen].scale);

}

- (void)setModel:(MeetingtListModel *)model{
    _model = model;

    self.title.text=model.title;
    
    [self.time setText:[NSString stringWithFormat:@"时间：%@",_model.time] afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:14];
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        NSRange range1 = NSMakeRange(0, 3);
        NSRange range2 = NSMakeRange(3, mutableAttributedString.length - 3);
        
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName
                                            value:(__bridge id)font
                                            range:range1];
            CFRelease(font);
        }
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[getHexColor(@"#303f49") CGColor] range:range1];
    

        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[getHexColor(@"#4f4d4d") CGColor] range:range2];
        return mutableAttributedString;
    }];
    
    [self.address setText:[NSString stringWithFormat:@"地点：%@",model.address] afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:14];
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        NSRange range1 = NSMakeRange(0, 3);
        NSRange range2 = NSMakeRange(3, mutableAttributedString.length - 3);
        
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName
                                            value:(__bridge id)font
                                            range:range1];
            CFRelease(font);
        }
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[getHexColor(@"#303f49") CGColor] range:range1];
        
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[getHexColor(@"#4f4d4d") CGColor] range:range2];
        return mutableAttributedString;
    }];
    
    [self.scan setText:[NSString stringWithFormat:@"浏览:%ld",(long)model.click] afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        
        UIFont *boldSystemFont = [UIFont boldSystemFontOfSize:14];
        CTFontRef font = CTFontCreateWithName((__bridge CFStringRef)boldSystemFont.fontName, boldSystemFont.pointSize, NULL);
        NSRange range1 = NSMakeRange(0, 3);
        NSRange range2 = NSMakeRange(3, mutableAttributedString.length - 3);
        
        if (font) {
            [mutableAttributedString addAttribute:(NSString *)kCTFontAttributeName
                                            value:(__bridge id)font
                                            range:range1];
            CFRelease(font);
        }
        
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[getHexColor(@"#303f49") CGColor] range:range1];
         
        [mutableAttributedString addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)[getHexColor(@"#4f4d4d") CGColor] range:range2];
        return mutableAttributedString;
    }];
    
    if(_model.price <= 0){
        self.price.text = @"免费";
    } else {
        self.price.text = [NSString stringWithFormat:@"￥%.lf",_model.price];
    }
    
    _title.text = _model.title;
    //转换成缩略图地址
     
    [_img setImageURLStr:ImgUrl(model.img_url) placeholder:Default_Loading_Image_1];
 
 
        if(_model.bm_status == 1){
            _prompt.text = @"报名中";
            _prompt.backgroundColor = getHexColor(@"#1D4FC0");
            
        } else {
            _prompt.text = @"已结束";
            _prompt.backgroundColor = getHexColor(@"#DD9113");
         }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
