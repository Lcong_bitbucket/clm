//
//  EnroolResultViewController.h
//  xcwl
//
//  Created by cong on 17/2/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface EnroolResultViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *serial_number;
@property (weak, nonatomic) IBOutlet UILabel *titleL;
@property (weak, nonatomic) IBOutlet UILabel *user_name;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *didian;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIButton *qqBtn;
@property (weak, nonatomic) IBOutlet UIButton *wxBtn;
@property (weak, nonatomic) IBOutlet UIButton *pyqBtn;
@property (weak, nonatomic) IBOutlet UIButton *wbBtn;
- (IBAction)shareClick:(id)sender;
@property (nonatomic,assign) NSInteger meetingID ;
@property (nonatomic,strong) NSDictionary *info;//报名结果
@end
