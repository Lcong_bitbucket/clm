//
//  EnroolResultViewController.m
//  xcwl
//
//  Created by cong on 17/2/10.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "EnroolResultViewController.h"
#import "UIButton+ImageTitleSpacing.h"


@interface EnroolResultViewController ()
{
     BOOL isHaveShowShare;
}
@end

@implementation EnroolResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"会议报名";
    [self customBackButton];
      
    _serial_number.text = self.info[@"serial_number"];
    _titleL.text = self.info[@"title"];
    _user_name.text = self.info[@"name"];
    _time.text = self.info[@"time"];
    _didian.text = self.info[@"address"];
    if([self.info[@"canhui_price"] integerValue] == 0){
        _price.text = @"免费";
    } else {
        _price.text = [NSString stringWithFormat:@"￥%@",self.info[@"canhui_price"]];
    }
    // Do any additional setup after loading the view from its nib.
    
    
    [_qqBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
    [_wxBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
    [_pyqBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
    [_wbBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
  
}
 
-(void)GetShareLinkPlatform:(NSString *)name{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    [params setValue:@(10) forKey:@"type"];
    [params setValue:@(self.meetingID) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)shareClick:(id)sender {
     UIButton *btn = (UIButton *)sender;
    NSArray *platArr = @[UMSPlatformNameQQ,UMSPlatformNameWechatSession,UMSPlatformNameWechatTimeline,UMSPlatformNameSina];
     
    [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
        if(isSuccess){
             [self GetShareLinkPlatform:platArr[btn.tag-10]];
        }
    }];
}

//
//- (void)shareType:(NSString *)platformName{
//    if(!STST.is_Login)
//    {
//        [[NSNotificationCenter defaultCenter]postNotificationName:@"SHOWLOGIN" object:nil];
//        return;
//    }
//    
//    NSString * urlstring=[NSString stringWithFormat:@"%@/service/userApi.ashx?action=GetShareLink&article_id=%d",baseurl,self.meetingID];
//    
//    urlstring = [urlstring stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    
//    AFHTTPRequestOperationManager  *   HttpManager=[AFHTTPRequestOperationManager manager];
//    [HttpManager GET:urlstring parameters:nil success:^(AFHTTPRequestOperation * operation,id responseobject)
//     {
//         LOG(@"sharelink =%@",responseobject);
//         if([Format(responseobject[@"status"]) isEqualToString:@"1"])
//         {
//             [[ShareManager shareManeger] sharePlatformName:platformName Dic:responseobject vc:self];
//         }
//         
//     }failure:^(AFHTTPRequestOperation * operation,NSError * error)
//     {
//         
//     }];
// }


@end
