//
//  EnroolMeetingViewController.m
//  VKTemplateForiPhone
//
//  Created by licong on 16/2/26.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "EnroolMeetingViewController.h"
#import "XCCheckBox.h"
#import "EnroolResultViewController.h"

@interface EnroolMeetingViewController ()< UITableViewDataSource,UITableViewDelegate>

@end

@implementation EnroolMeetingViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    NSArray *zanzhuArray = @[@"会场报告",@"会刊广告",@"大屏幕广告",@"参会证",@"资料入袋",@"展示台",@"易拉宝",@"礼品"];
    NSMutableArray *arrayData = [NSMutableArray new];
    for(int i = 0; i < zanzhuArray.count; i ++){
        XCCheckBoxModel *model = [[XCCheckBoxModel alloc] init];
        model.item = zanzhuArray[i];
        model.title = zanzhuArray[i];

        [arrayData addObject:model];
    }
    [_zanzhuBG whenTapped:^{
        [self.view endEditing:YES];
        XCCheckBox *checkBox = [[XCCheckBox alloc]init];
        [checkBox setArrayData:arrayData];
        [checkBox setTitle:@"请选择赞助类型"];
        [checkBox show];
        
        [checkBox setDidClickBlock:^(NSMutableArray *arr) {
            NSMutableString *zanzhuStr = [NSMutableString new];
            for(int i = 0;i < arr.count ;i ++){
                if([arr[i] isSelected]){
                    [zanzhuStr appendString:[arr[i] title]];
                    [zanzhuStr appendString:@","];
                 }
            }
             _zanzhuLabel.text =zanzhuStr.length > 0 ? [zanzhuStr substringToIndex:zanzhuStr.length - 1] :zanzhuStr;
        }];
    }];

    _huiyiBottomBG.hidden = YES;
    _huiyiBottomBGH.constant = 0;
    if(self.type == 0){
         _huiyiBottomBGH.constant = 195;
        _huiyiBottomBG.hidden = NO;
        self.title = @"会议报名";

    }
    // Do any additional setup after loading the view.
    _canhuiyuanyin.placeholder = @"请填写参会原因";
    _mark.placeholder = @"请填写备注";
    _mark.layer.masksToBounds = YES;
    _mark.layer.cornerRadius = 2;
    _mark.layer.borderWidth = 0.5;
    _mark.layer.borderColor = getHexColor(@"#f2f2f2").CGColor;
    
    if([[XCUserManager sharedInstance] isLogin])
    {
        XCUserModel *userModel = [XCUserManager sharedInstance].userModel;
        
        _userNameTF.text = userModel.nick_name;// [userInfo objectForKey:@"nick_name"];
        _telTF.text = userModel.mobile;// Format([userInfo objectForKey:@"mobile"]) ;
        _emailTF.text = userModel.email;// [userInfo objectForKey:@"email"];
        _companyTF.text = userModel.company_name;// [userInfo objectForKey:@"company_name"];
        _jobTF.text = userModel.zhiwei;// [userInfo objectForKey:@"zhiwei"];
     }
    [self.headIMG sd_setImageWithURL:[NSURL URLWithString:ImgUrl(_imgurl)]];
     if(_imgurl.length == 0 ){
        self.headIMG.hidden = YES;
        _imgAspect.constant = CGFLOAT_MAX;
    }

}



#pragma mark - 请求数据
- (void)requestTel{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetKF" forKey:@"action"];
    [params setValue:@"4" forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                openTel(jsonResponse[@"tel"]);
                
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - 提交报名
- (IBAction)enroolEvent:(id)sender {
    if (_userNameTF.text.length == 0){
        showHint(@"请输入您的姓名");
    } else if (_telTF.text.length == 0) {
        showHint(@"请输入手机号");
    } else if (_companyTF.text.length == 0){
        showHint(@"请输入您的公司名称");
    } else if (_jobTF.text.length==0){
        showHint(@"请输入您的职位");
    } else{
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                 [self requestEnroll];
        }];
    }
}

- (IBAction)zixun:(id)sender {
    [self requestTel];
}

 /*!brief 
    报名请求
 */

- (void)requestEnroll{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    
    if(self.type==0)
    {
        XCUserModel *userModel = [XCUserManager sharedInstance].userModel;
        
        [params setValue:@"BaomingActivity" forKey:@"action"];
        [params setValue:self.ID forKey:@"id"];
        [params setValue:@(userModel.id) forKey:@"user_id"];
        [params setValue:_userNameTF.text forKey:@"contact"];
        [params setValue:_telTF.text forKey:@"mobile"];
        [params setValue:_emailTF.text forKey:@"email"];
        [params setValue:_companyTF.text forKey:@"company_name"];
        [params setValue:_jobTF.text forKey:@"zhiwei"];
        [params setValue:_mark.text forKey:@"product"];
        [params setValue:_canhuiyuanyin.text forKey:@"advise"];
        if(_zanzhuLabel.text.length > 0){
            [params setValue:@"1" forKey:@"is_zanzhu"];
            [params setValue:_zanzhuLabel.text forKey:@"zanzhu_content"];
        }
        [params setValue:@(_openInfoSW.on?1:0) forKey:@"is_public"];
    }
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                EnroolResultViewController *vc = [[EnroolResultViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.meetingID = [self.ID integerValue];
                vc.info = jsonResponse;
                [self.navigationController pushViewController:vc animated:YES];
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

@end
