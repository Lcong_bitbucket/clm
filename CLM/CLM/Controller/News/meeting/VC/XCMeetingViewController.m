//
//  XCActivityViewController.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMeetingViewController.h"
#import "MeetingListTableViewCell.h"
#import "XCMeetingDetailViewController.h"

@interface XCMeetingViewController ()
@property (nonatomic,strong) NSMutableArray *dataArray;

@end

@implementation XCMeetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"活动";
    [self customBackButton];
    [self customShareButton];

    _dataArray = [NSMutableArray new];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    [self beginRefreshing];
    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    [params setValue:@(10) forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

#pragma mark - 数据请求
- (void)refresh {
    [self request:self.curPage];
}

- (void)request:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
     [params setValue:@"GetActivityList" forKey:@"action"];
    if(self.vcType == 1){
        [params setValue:@(1) forKey:@"type"];
    }

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1){
                    [_dataArray removeAllObjects];
                }
                [_dataArray addObjectsFromArray:[MeetingtListModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                [self stopRefresh:NO];
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
        NSString *identifier = @"MeetingListTableViewCell";
        MeetingListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _dataArray[indexPath.row];
        return cell;
 }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XCMeetingDetailViewController *vc = [[XCMeetingDetailViewController alloc] init];
    vc.id = [_dataArray[indexPath.row] id];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
