//
//  EnroolMeetingViewController.h
//  VKTemplateForiPhone
//
//  Created by licong on 16/2/26.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"


@interface EnroolMeetingViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *headIMG;
@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
@property (weak, nonatomic) IBOutlet UITextField *telTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *jobTF;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *mark;
@property (weak, nonatomic) IBOutlet UIButton *enroolBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgAspect;
 
@property (weak, nonatomic) IBOutlet UIView *zanzhuBG;
@property (weak, nonatomic) IBOutlet UILabel *zanzhuLabel;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *canhuiyuanyin;
@property (weak, nonatomic) IBOutlet UISwitch *openInfoSW;
@property (weak, nonatomic) IBOutlet UIView *huiyiBottomBG;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *huiyiBottomBGH;

- (IBAction)enroolEvent:(id)sender;
- (IBAction)zixun:(id)sender;

@property(assign,nonatomic)NSInteger type;
@property(strong,nonatomic)NSString * meetingTitle;
@property(strong,nonatomic)NSString * imgurl;
@property(strong,nonatomic)NSString * ID;

@end
