//
//  meetListModel.h
//  VKTemplateForiPhone
//
//  Created by licong on 16/2/26.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "XCBaseModel.h"

@interface MeetingtListModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *img_url;
 @property (nonatomic,copy) NSString *time;
@property (nonatomic,assign) NSInteger bm_status;//0 没有直播 1 即将开播 2 直播中 3直播结束
@property (nonatomic,assign) NSInteger click;//浏览次数
@property (nonatomic,copy) NSString *address;//浏览次数
@property (nonatomic,assign) CGFloat price;

@end



