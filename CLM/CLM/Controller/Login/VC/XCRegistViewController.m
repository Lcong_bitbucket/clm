//
//  XCRegistViewController.m
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCRegistViewController.h"
#import "UIImageView+AFNetworking.h"
#import "XCMyInfoViewController.h"
#define maxTime 60

@interface XCRegistViewController ()
{
    NSInteger _curTime;
    NSTimer *timer;
}
@end

@implementation XCRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"注册新用户";
    
    [self getImgCode];

    [_imgCode whenTouchedUp:^{
        [self getImgCode];
    }];
}

- (void)getImgCode{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[dat timeIntervalSince1970]*1000;
    NSString *timeString = [NSString stringWithFormat:@"%f", a];
    NSString *imgUrl = [NSString stringWithFormat:@"%@/tools/verify_code.ashx?time=%@",AFSERVER_DATA,timeString];
    
    NSURL *urlStr = [NSURL URLWithString:imgUrl];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:urlStr];
    
    [_imgCode setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        [_imgCode setImage:image];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        /**< @param error : NSError对象 加载错误的原因代号(例如 : 403) */
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)getCode:(id)sender {
    if(_phoneTF.text.length == 0)
    {
        [XCTools shakeAnimationForView:_phoneTF];
        showHint(@"请输入您的手机号码");
        return;
    }
    else if(_imgCodeTF.text.length == 0)
    {
        [XCTools shakeAnimationForView:_imgCodeTF];
        showHint(@"请输入图形验证码");
        return;
    } else {
        [self requestCode];
    }
}

- (void)requestCode{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"1" forKey:@"type"];
    [params setValue:_imgCodeTF.text forKey:@"imgCode"];
    [params setValue:_phoneTF.text forKey:@"mobile"];
    [params setValue:@"user_register_smscode" forKey:@"action"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                _curTime = maxTime;
                if(timer && [timer isValid])
                    [timer invalidate];
                timer  = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changetitle) userInfo:nil repeats:YES];
                [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
                _codeBtn.enabled = NO;
                [_codeBtn setBackgroundColor:UIColorFromRGB(0xa4abb1)];
                [_codeBtn  setTitle:[NSString stringWithFormat:@"重新获取验证码(%ld)",(long)_curTime] forState:UIControlStateDisabled];

            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)changetitle
{
    _curTime --;
    if(_curTime != 0)
    {
         [_codeBtn  setTitle:[NSString stringWithFormat:@"重新获取验证码(%ld)",(long)_curTime] forState:UIControlStateDisabled];
    }
    else
    {
        if(timer && [timer isValid])
            [timer invalidate];
         [_codeBtn setTitle:[NSString stringWithFormat:@"获取验证码"] forState:UIControlStateNormal];
        _codeBtn.backgroundColor=UIColorFromRGB(0x036EB8);
        _codeBtn.enabled = YES;

    }
}

- (IBAction)xieyi:(id)sender {
    
}

- (IBAction)nextStep:(id)sender {
    if(_phoneTF.text.length == 0)
    {
        [XCTools shakeAnimationForView:_phoneTF];
        showHint(@"请输入您的手机号码");
        return;
    }
    else if(_codeTF.text.length == 0)
    {
        [XCTools shakeAnimationForView:_codeTF];
         showHint(@"请输入图形验证码");
        return;
    } else if(_passwordTF.text.length == 0){
        [XCTools shakeAnimationForView:_passwordTF];
        showHint(@"请输入密码");
        return;
    } else if(![_passwordTF.text isEqualToString:_secPasswordTF.text]){
        showHint(@"两次输入密码不一致");
        [XCTools shakeAnimationForView:_passwordTF];
        [XCTools shakeAnimationForView:_secPasswordTF];

        return;
    } else {
        [self regist];
    }
}

- (void)regist{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_codeTF.text forKey:@"txtCode"];
    [params setValue:_phoneTF.text forKey:@"txtMobile"];
    [params setValue:_passwordTF.text forKey:@"txtPassword"];
    [params setValue:@"user_register" forKey:@"action"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
               showHint(jsonResponse[@"msg"]);
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    if(self.didRegistBlock){
//                        self.didRegistBlock(_phoneTF.text,_passwordTF.text);
//                    }
//                    [self goBack];
                    [self loginUserName:_phoneTF.text password:_passwordTF.text];
                });
               
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];

}

- (void)loginUserName:(NSString *)userName password:(NSString *)password{
    [[XCUserManager sharedInstance] loginUsername:userName password:password success:^{
        showHint(@"登录成功");
        [[XCUserManager sharedInstance] reloadInfoSuccess:^{
            XCMyInfoViewController *vc = [[XCMyInfoViewController alloc] init];
            vc.isRegist = YES;
            [self.navigationController pushViewController:vc animated:YES];
        } failure:^(NSError *error) {
            
        }];
    } failure:^(NSError *error) {
        
    }];
 }

@end
