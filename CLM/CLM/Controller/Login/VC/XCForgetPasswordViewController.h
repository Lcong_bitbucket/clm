//
//  XCForgetPasswordViewController.h
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface XCForgetPasswordViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *imgCodeTF;
@property (weak, nonatomic) IBOutlet UIImageView *imgCode;
@property (weak, nonatomic) IBOutlet UITextField *codeTF;
@property (weak, nonatomic) IBOutlet UIButton *codeBtn;
- (IBAction)getCode:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *secPasswordTF;
- (IBAction)xieyi:(id)sender;

- (IBAction)nextStep:(id)sender;
@property (nonatomic,copy) void (^didRegistBlock)(NSString *phone,NSString *password);
@end
