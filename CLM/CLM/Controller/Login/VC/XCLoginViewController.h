//
//  XCLoginViewController.h
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface XCLoginViewController : XCBaseViewController
- (IBAction)close:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *prompt;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UITextField *accountTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;

- (IBAction)login:(id)sender;
- (IBAction)regist:(id)sender;
- (IBAction)forgetPasswrod:(id)sender;

@property (nonatomic,copy) void (^success)(BOOL isSuccess);

@end
