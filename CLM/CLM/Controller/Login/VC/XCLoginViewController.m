//
//  XCLoginViewController.m
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCLoginViewController.h"
#import "XCRegistViewController.h"
#import "XCForgetPasswordViewController.h"


@interface XCLoginViewController ()<UITextFieldDelegate>

@end

@implementation XCLoginViewController

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
    
    _passwordTF.delegate = self;
    
    _loginBtn.layer.cornerRadius = _loginBtn.frame.size.height/2;
    _loginBtn.layer.masksToBounds = YES;
    _loginBtn.layer.shadowColor = [UIColor redColor].CGColor;
    _loginBtn.layer.shadowOffset = CGSizeMake(5.0, 5.0);
    _loginBtn.layer.shadowOpacity = YES;
    
    NSString *promptStr = @"一站式全流程测试服务平台";
    self.prompt.text = promptStr;
    
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self submitLogin];
    return YES;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   }


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)close:(id)sender {
    [self goBack];
}
- (IBAction)login:(id)sender {
     [self submitLogin];
}

- (IBAction)regist:(id)sender {
    XCRegistViewController *vc = [[XCRegistViewController alloc]  init];
    [vc setDidRegistBlock:^(NSString *phone, NSString *password) {
        _accountTF.text = phone;
        _passwordTF.text = password;
//        [self submitLogin];
        
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)forgetPasswrod:(id)sender {
    XCForgetPasswordViewController *vc = [[XCForgetPasswordViewController alloc]  init];
    [vc setDidRegistBlock:^(NSString *phone, NSString *password) {
        _accountTF.text = phone;
        _passwordTF.text = password;
        //        [self submitLogin];
    }];
    [self.navigationController pushViewController:vc animated:YES];

}

- (void)submitLogin{
    [self.view endEditing:YES];
    if(!isValidString(_accountTF.text)){
        showHint(@"请填输入手机号");
        [XCTools shakeAnimationForView:_accountTF];
        return;
    } else if(!isValidString(_accountTF.text)){
        showHint(@"请输入密码");
        [XCTools shakeAnimationForView:_accountTF];
        return;
    }
    [[XCUserManager sharedInstance] loginUsername:_accountTF.text password:_passwordTF.text success:^{
        showHint(@"登录成功");
        [[XCUserManager sharedInstance] reloadInfoSuccess:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
                if(self.success){
                    self.success(YES);
                }
            }];
        } failure:^(NSError *error) {
            
        }];
     } failure:^(NSError *error) {
 
    }];
//    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
//    [params setValue:_accountTF.text forKey:@"user_login_name"];
//    [params setValue:_passwordTF.text forKey:@"user_login_pwd"];
//    [params setValue:@"user_login" forKey:@"action"];
//
//    XCBaseCommand *command = [XCBaseCommand new];
//    command.api = @"/tools/submit_ajax.ashx";
//    command.curView = self.view;
//    command.params = params;
//    
//    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
//        if(error){
//            
//        } else {
//            if([[jsonResponse objectForKey:@"status"] intValue]==1)
//            {
//                showHint(jsonResponse[@"msg"]);
//                
//                XCUserManager *manager = [XCUserManager sharedInstance];
//                manager.userModel.id = [jsonResponse[@"user_id"] integerValue];
//                manager.cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
//                [manager save];
//                [self requestUserInfo];
//                
//             }else {
//                showHint(jsonResponse[@"msg"]);
//            }
//        }
//    }];
//    [[XCHttpClient sharedInstance] request:command];
 
}


#pragma mark - 请求数据
- (void)requestUserInfo{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetUserInfo" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                XCUserManager *manager =[XCUserManager sharedInstance];
                manager.userModel = [XCUserModel mj_objectWithKeyValues:jsonResponse];
                [manager save];
                [self.navigationController dismissViewControllerAnimated:YES completion:^{
                    if(self.success){
                        self.success(YES);
                    }
                }];
             } else {
                 showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

@end
