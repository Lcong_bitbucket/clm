//
//  XCZhuanjiaModel.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCZhuanjiaModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *img_url;
@property (nonatomic,strong) NSString *company;
@property (nonatomic,strong) NSString *zhiwei;
@property (nonatomic,strong) NSString *lingyu;
@property (nonatomic,strong) NSString *img_banner;
@property (nonatomic,strong) NSString *add_time;
@property (nonatomic,assign) NSInteger is_top;
@property (nonatomic,strong) NSString *province;
@property (nonatomic,strong) NSString *diqu;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *anli;
@property (nonatomic,assign) NSInteger jigou_id;
@property (nonatomic,assign) NSInteger user_id;

@end
