//
//  XCZhuanjiaTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCZhuanjiaTableViewCell.h"

@implementation XCZhuanjiaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(XCZhuanjiaModel *)model{
    _model = model;
    [_avatar setImageURLStr:ImgUrl(_model.img_url) placeholder:Default_Avatar_Image_1];
    _name.text = _model.name;
    _zhiwei.text = _model.zhiwei;
    _company.text = _model.company;
    _content.text = [NSString stringWithFormat:@"领域:%@",_model.lingyu];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
