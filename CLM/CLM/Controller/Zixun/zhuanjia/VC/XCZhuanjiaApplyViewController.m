//
//  XCZhuanjiaApplyViewController.m
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCZhuanjiaApplyViewController.h"
#import "STPickerArea.h"
#import "STPickerDate.h"
#import "STPickerSingle.h"
#import "XCMarkModel.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "XCJiGouModel.h"
#import "XCZhuanjiaModel.h"
#import "XCJiGouViewController.h"
@interface XCZhuanjiaApplyViewController ()<STPickerAreaDelegate, STPickerSingleDelegate, STPickerDateDelegate,TZImagePickerControllerDelegate>
{
    XCZhuanjiaModel *_curModel;
    XCJiGouModel *_curJiGouModel;
    
    NSMutableArray *_menu2Array;
    NSMutableArray *_menu2Value;

    NSMutableArray *_jigouArray;
    NSMutableArray *_jigouValue;
    
    NSString *_province;
    NSString *_diqu;

}
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableArray *photos;
@property (nonatomic , strong) NSMutableArray *imgUrls;
@end

@implementation XCZhuanjiaApplyViewController
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area
{
    NSString *text = [NSString stringWithFormat:@"%@ %@ %@", province, city, area];
    _province = province;
    _diqu = [NSString stringWithFormat:@"%@ %@",city, area];
    self.addressL.text = text;

}

- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle selectedIndex:(NSInteger)row
{
    if(pickerSingle.tag == 111){
        NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
        self.lingyuL.text = text;
    } else if(pickerSingle.tag == 112){
        NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
        if(row == 0){
            self.jigouL.text = @"";
            _curJiGouModel = nil;
        } else {
            self.jigouL.text = text;
            _curJiGouModel = _jigouArray[row-1];
        }
    }
}


- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSString *text = [NSString stringWithFormat:@"%zd年%zd月%zd日", year, month, day];
//    self.timeL.text = text;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    _assets = [NSMutableArray new];
    _photos = [NSMutableArray new];
    _imgUrls = [NSMutableArray new];
    
    _menu2Array = [NSMutableArray new];
    _menu2Value = [NSMutableArray new];

    _jigouArray = [NSMutableArray new];
    _jigouValue = [NSMutableArray new];
    
    self.title = @"申请成为专家";
    [_addressBG whenTapped:^{
        [self.view endEditing:YES];

        STPickerArea *pickerArea = [[STPickerArea alloc]init];
        [pickerArea setDelegate:self];
        [pickerArea setContentMode:STPickerContentModeBottom];
        [pickerArea show];
        
    }];
    
//    [_timeBG whenTapped:^{
//        STPickerDate *pickerDate = [[STPickerDate alloc]init];
//        [pickerDate setDelegate:self];
//        [pickerDate show];
//    }];
    
    [_lingyuBG whenTapped:^{
        [self.view endEditing:YES];

        if(_menu2Value.count > 0){
            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
            pickerSingle.widthPickerComponent = 200;
            pickerSingle.tag = 111;
            [pickerSingle setArrayData:_menu2Value];
            [pickerSingle setTitle:@"请选择擅长领域"];
            [pickerSingle setTitleUnit:@""];
            [pickerSingle setContentMode:STPickerContentModeBottom];
            [pickerSingle setDelegate:self];
            [pickerSingle show];
        } else {
            [self requestMark:5];
        }
        
    }];
    
    [_jigouBG whenTapped:^{
        [self.view endEditing:YES];

        if(_jigouArray.count > 0){
            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
            pickerSingle.tag = 112;
            pickerSingle.widthPickerComponent = 300;
            [pickerSingle setArrayData:_jigouValue];
            [pickerSingle setTitle:@"请选择所属机构"];
            [pickerSingle setTitleUnit:@""];
            [pickerSingle setContentMode:STPickerContentModeBottom];
            [pickerSingle setDelegate:self];
            [pickerSingle show];
        } else {
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                if(isSuccess){
                    [self requestJiGou];
                }
            }];
        }
    }];
    
    [self requestJiGou];
    [self requestMark:5];
    
//    XCUserModel *userModel = [[XCUserManager sharedInstance] userModel];
//    _contact.text = userModel.nick_name;
//    _phone.text = userModel.mobile;
//    _zhiwei.text = userModel.zhiwei;
//    _company.text = userModel.company_name;
//    _email.text = userModel.email;
    
    if(self.id){
        [self requestDetail];
    }
    
    [self customNavigationBarItemWithImageName:@"icon_fbsb" title:nil isLeft:NO];
    // Do any additional setup after loading the view from its nib.
}
#pragma mark - 请求数据
- (void)requestTel{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetKF" forKey:@"action"];
    [params setValue:@"1" forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                openTel(jsonResponse[@"tel"]);
                
            } else {
                
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)refreshUI{
    
    if(_curModel){
        _titleTF.text = _curModel.name;
        _companyTF.text = _curModel.company;
        _zhiweiTF.text = _curModel.zhiwei;
        _content.text = _curModel.content;
        _anli.text = _curModel.anli;
        _lingyuL.text = _curModel.lingyu;
        _province = _curModel.province;
        _diqu = _curModel.diqu;
        _addressL.text = [NSString stringWithFormat:@"%@%@",_curModel.province,_curModel.diqu];
        [_imgUrls addObject:_curModel.img_url];
        [self reloadScrollView];

        for(XCJiGouModel *m in _jigouArray){
            if(m.id == _curModel.jigou_id){
                _curJiGouModel = m;
                _jigouL.text = _curJiGouModel.name;
            }
        }
    }
}

- (void)requestJiGou{
    if(![[XCUserManager sharedInstance] isLogin])
        return;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetJigouList" forKey:@"action"];
    [params setValue:@([[[XCUserManager sharedInstance] userModel] id]) forKey:@"user_id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_jigouArray removeAllObjects];
                [_jigouValue removeAllObjects];
                [_jigouArray addObjectsFromArray:[XCJiGouModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [_jigouValue addObject:@"无"];
                for(int i = 0; i < _jigouArray.count; i++){
                    [_jigouValue addObject:[_jigouArray[i] name]];
                }
                [self refreshUI];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}


- (void)requestMark:(NSInteger)type{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(type) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(type == 5){
                    [_menu2Array removeAllObjects];
                    [_menu2Value removeAllObjects];
                    [_menu2Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
          
                    for(int i = 0; i < _menu2Array.count; i++){
                        [_menu2Value addObject:[_menu2Array[i] title]];
                    }
                    [self refreshUI];
                }
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}


- (void)rightBarButtonAction:(UIButton *)btn{
    [self requestTel];

  
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"GetZhuanjiaDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _curModel = [XCZhuanjiaModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    [self refreshUI];
                    
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fabuClick:(id)sender {
    if(_titleTF.text.length == 0){
        showHint(@"请输入您的姓名");
        [XCTools shakeAnimationForView:_titleTF];
        
    }  else if(_companyTF.text.length == 0){
        showHint(@"请输入单位名称");
        [XCTools shakeAnimationForView:_companyTF];
        
    }  else if(_zhiweiTF.text.length == 0){
        showHint(@"请输入职位");
        [XCTools shakeAnimationForView:_zhiweiTF];
        
    } else if(_lingyuL.text.length == 0){
        showHint(@"请选择擅长领域");
        [XCTools shakeAnimationForView:_lingyuBG];
        
    } else if(_content.text.length == 0){
        showHint(@"请输入简介");
        [XCTools shakeAnimationForView:_content];
        
    } else if(_addressL.text.length == 0){
        showHint(@"请输入地理位置");
        [XCTools shakeAnimationForView:_addressBG];
        
    } else if(_anli.text.length == 0){
        showHint(@"请输入案例");
        [XCTools shakeAnimationForView:_anli];
        
    }else if(_imgUrls.count <= 0){
        showHint(@"请上传除个人图片");
        [XCTools shakeAnimationForView:_picBtn];
        
    } else {
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                [self request];
            }
        }];
    }
}

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_titleTF.text forKey:@"name"];
    [params setValue:_companyTF.text forKey:@"company"];
    [params setValue:_zhiweiTF.text forKey:@"zhiwei"];
    [params setValue:_lingyuL.text forKey:@"lingyu"];
    [params setValue:_province forKey:@"province"];
    [params setValue:_diqu forKey:@"diqu"];
    [params setValue:_content.text forKey:@"content"];
    [params setValue:_anli.text forKey:@"anli"];
    if(_imgUrls.count > 0){
        [params setValue:_imgUrls[0] forKey:@"img_url"];
    }
    if(_curJiGouModel){
        [params setValue:@(_curJiGouModel.id) forKey:@"jigou_id"];
    }
    if(self.id){
        [params setValue:@(self.id) forKey:@"id"];
    }
//    [params setValue:_contact.text forKey:@"contact"];
//    [params setValue:_phone.text forKey:@"mobile"];
//    [params setValue:_email.text forKey:@"email"];
//    [params setValue:_zhiwei.text forKey:@"zhiwei"];
//    [params setValue:_company.text forKey:@"company_name"];
    
    [params setValue:@"EditZhuanjia" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                if(self.didPublishBlock){
                    self.didPublishBlock();
                }
                [self goBack];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (IBAction)picClick:(id)sender {
    [self pushImagePickerController];
}


- (void)pushImagePickerController {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    
    
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.isSelectOriginalPhoto = YES;
    
    //    imagePickerVc.selectedAssets = self.assets; // 目前已经选中的图片数组
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
    // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
    // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];
    
    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingGif = NO;
    
    // 4. 照片排列按修改时间升序
    imagePickerVc.sortAscendingByModificationDate = YES;
    
    // imagePickerVc.minImagesCount = 3;
    // imagePickerVc.alwaysEnableDoneBtn = YES;
    
    // imagePickerVc.minPhotoWidthSelectable = 3000;
    // imagePickerVc.minPhotoHeightSelectable = 2000;
    
    /// 5. Single selection mode, valid when maxImagesCount = 1
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.needCircleCrop = NO;
    imagePickerVc.circleCropRadius = Screen_Width/2;
    /*
     [imagePickerVc setCropViewSettingBlock:^(UIView *cropView) {
     cropView.layer.borderColor = [UIColor redColor].CGColor;
     cropView.layer.borderWidth = 2.0;
     }];*/
    
    //imagePickerVc.allowPreview = NO;
#pragma mark - 到这里为止
    
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}
#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    self.assets = assets.mutableCopy;
    self.photos = photos.mutableCopy;
    if(self.assets.count > 0){
        [_imgUrls removeAllObjects];
        for(int i = 0; i < self.assets.count ; i ++){
            [_imgUrls addObject:@""];
        }
        for(int i = 0; i < self.assets.count ; i++){
            XCBaseCommand *command = [XCBaseCommand new];
            command.curView = self.view;
            
            command.imgs = @[self.photos[i]];
            [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
                if(error){
                    
                } else {
                    if([[jsonResponse objectForKey:@"status"] intValue]==1)
                    {
                        NSString *logPath=[jsonResponse objectForKey:@"path"];
                         [_imgUrls replaceObjectAtIndex:i withObject:logPath];
                        showHint(jsonResponse[@"desc"]);
                        [self reloadScrollView];
                    }  else {
                        showHint(jsonResponse[@"desc"]);
                    }
                }
            }];
            [[XCHttpClient sharedInstance] upload:command];
        }
    } else {
        
    }
    
}

//#pragma mark - 选择图片
//- (void)photoSelectet{
//    ZLPhotoPickerViewController *pickerVc = [[ZLPhotoPickerViewController alloc] init];
//    // MaxCount, Default = 9
//    pickerVc.maxCount = 1;
//    // Jump AssetsVc
//    pickerVc.status = PickerViewShowStatusCameraRoll;
//    // Filter: PickerPhotoStatusAllVideoAndPhotos, PickerPhotoStatusVideos, PickerPhotoStatusPhotos.
//    pickerVc.photoStatus = PickerPhotoStatusPhotos;
//    // Recoder Select Assets
//    pickerVc.selectPickers = self.assets;
//    // Desc Show Photos, And Suppor Camera
//    pickerVc.topShowPhotoPicker = YES;
//    pickerVc.isShowCamera = YES;
//    // CallBack
//    pickerVc.callBack = ^(NSArray<ZLPhotoAssets *> *status){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.assets = status.mutableCopy;
//            if(self.assets.count > 0){
//                [_imgUrls removeAllObjects];
//                for(int i = 0; i < self.assets.count ; i ++){
//                    [_imgUrls addObject:@""];
//                }
//
//                for(int i = 0; i < self.assets.count ; i++){
//                    
//                    XCBaseCommand *command = [XCBaseCommand new];
//                    command.curView = self.view;
//                    command.imgs = [@[[self.assets[i] thumbImage]] mutableCopy];
//                    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
//                        if(error){
//                            
//                        } else {
//                            if([[jsonResponse objectForKey:@"status"] intValue]==1)
//                            {
//                                NSString *logPath=[jsonResponse objectForKey:@"path"];
//                                [_imgUrls replaceObjectAtIndex:i withObject:logPath];
//                                [self reloadScrollView];
//                            }  else {
//                                showHint(jsonResponse[@"desc"]);
//                            }
//                        }
//                    }];
//                    [[XCHttpClient sharedInstance] upload:command];
//                }
//            } else {
//                
//            }
//            
//        });
//    };
//    [pickerVc showPickerVc:self];
//}

- (void)reloadScrollView{
    if(_imgUrls.count > 0){
        [_picBtn setImageWithURLString:ImgUrl(_imgUrls[0]) forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"icon_file"]];
    } else {
        [_picBtn setImage:[UIImage imageNamed:@"icon_file"] forState:UIControlStateNormal];
    }
}
@end
