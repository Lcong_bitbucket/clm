//
//  XCZhuanjiaApplyViewController.h
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface XCZhuanjiaApplyViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *jigouL;
@property (weak, nonatomic) IBOutlet UIView *jigouBG;
@property (weak, nonatomic) IBOutlet UIView *lingyuBG;
@property (weak, nonatomic) IBOutlet UILabel *lingyuL;
@property (weak, nonatomic) IBOutlet UIView *addressBG;
@property (weak, nonatomic) IBOutlet UILabel *addressL;
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *zhiweiTF;

@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *anli;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *content;
@property (weak, nonatomic) IBOutlet UIButton *picBtn;
- (IBAction)picClick:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *contact;
@property (weak, nonatomic) IBOutlet UITextField *zhiwei;
@property (weak, nonatomic) IBOutlet UITextField *company;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIButton *fabu;
- (IBAction)fabuClick:(id)sender;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) void (^didPublishBlock)();


@end
