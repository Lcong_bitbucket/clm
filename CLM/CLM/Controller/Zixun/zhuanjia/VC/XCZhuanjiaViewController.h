//
//  XCZhuanjiaViewController.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCZhuanjiaViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbTop;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,copy)NSString *lingyu;

@property (nonatomic,assign) NSInteger vcType; //0 普通列表 1 我发布的

@end
