//
//  XCQuestionAnswerTableViewCell.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCQuestionAnswerModel.h"
#import "XCQuestionCommentModel.h"
#import "XCQuestionModel.h"

@interface XCQuestionAnswerTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *replyPrompt;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewH;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *zhiwei;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (nonatomic,strong) XCQuestionAnswerModel *model;
@property (nonatomic,strong) XCQuestionModel *qModel;
@property (nonatomic,copy) void (^didReplyBlock)(XCQuestionAnswerModel *answerModel,XCQuestionCommentModel *commentModel);

@property (nonatomic,copy) void (^didLongTouchBlock)(XCQuestionAnswerModel *answerModel,XCQuestionCommentModel *commentModel);

@end
