//
//  XCAnswerTableViewCell.m
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCAnswerTableViewCell.h"

@implementation XCAnswerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCQuestionCommentModel *)model{
    _model = model;
    XCLog(@"content = %@",_model.content);
    _content.attributedText = [NSString attributedTextWithText:_model.content];
    XCLog(@"content = %@",[NSString attributedTextWithText:_model.content]);

}
@end
