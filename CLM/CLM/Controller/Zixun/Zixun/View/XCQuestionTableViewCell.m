//
//  XCQuestionTableViewCell.m
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCQuestionTableViewCell.h"
#import "WZLBadgeImport.h"

@implementation XCQuestionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCQuestionModel *)model{
    _model = model;
    [_avatar setImageURLStr:ImgUrl(_model.avatar) placeholder:Default_Avatar_Image_2];
    _name.text = _model.nick_name;
    _title.text = _model.content;
    _company.text = _model.company_name;
    _zhiwei.text = _model.zhiwei;
    _lingyu.text = @"";
 
    _answerCount.text = [NSString stringWithFormat:@"当前%d人回答",_model.answer_count];
    NSArray *imgArray;
    
    if(_model.img_url.length > 0)
        imgArray = [_model.img_url componentsSeparatedByString:@","];
    CGFloat w = self.imgsBGView.width / 3.0;
        for(int i = 0; i < 9 ; i++){
             UIImageView *iv = [self viewWithTag:10+i];
            NSArray* constrains = iv.constraints;
            
            if(i<imgArray.count){
                iv.hidden = NO;
                [iv whenTapped:^{
                    openImageWithZLPhotoLib(i,[imgArray copy]);
                }];
                [iv setImageURLStr:ImgUrl(imgArray[i]) placeholder:Default_Loading_Image_2];
            } else {
                iv.hidden = YES;
                
            }

            for (NSLayoutConstraint* constraint in constrains) {
                if (constraint.firstAttribute == NSLayoutAttributeHeight) {
                    if( i%3==0 && i < imgArray.count){
                        constraint.constant = w;
                    } else {
                        constraint.constant = 0;
                    }
                 }
            }
         }
 
}
@end
