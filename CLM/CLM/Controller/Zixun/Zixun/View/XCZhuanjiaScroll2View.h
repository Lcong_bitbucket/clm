//
//  XCZhuanjiaScroll2View.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCZhuanjiaModel.h"

@interface XCZhuanjiaScroll2View : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *zhiwei;
@property (nonatomic,strong) XCZhuanjiaModel *model;
@end
