//
//  XCZhuanjiaScroll2TableViewCell.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCZhuanjiaModel.h"

@interface XCZhuanjiaScroll2TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (nonatomic,strong) NSMutableArray *array;
@property (nonatomic,copy) void (^didClickBlock)(XCZhuanjiaModel *model);
@end
