//
//  XCZhuanjiaScroll2TableViewCell.m
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCZhuanjiaScroll2TableViewCell.h"
#import "XCZhuanjiaScroll2View.h"

@implementation XCZhuanjiaScroll2TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setArray:(NSMutableArray *)array{
    _array = array;
    [self.scroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat w =self.width/2;
    CGFloat h = self.height;
    
    for(int i = 0; i < _array.count; i++){
        XCZhuanjiaScroll2View *iv = [[[NSBundle mainBundle] loadNibNamed:@"XCZhuanjiaScroll2View" owner:self options:nil] lastObject];
        iv.frame = CGRectMake(i*w, 0, w, h);
        iv.model = _array[i];
        [iv whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(_array[i]);
            }
        }];
        [self.scroll addSubview:iv];
    }
    self.scroll.contentSize = CGSizeMake(_array.count * w, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
