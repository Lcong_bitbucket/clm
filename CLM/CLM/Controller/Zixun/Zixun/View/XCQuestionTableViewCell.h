//
//  XCQuestionTableViewCell.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCQuestionModel.h"

@interface XCQuestionTableViewCell : UITableViewCell{
  
}
@property (weak, nonatomic) IBOutlet UIView *imgsBGView;

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *zhiwei;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UILabel *answerCount;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lingyu;
@property (nonatomic,strong) XCQuestionModel *model;
@end
