//
//  XCFunc3TableViewCell.h
//  CLM
//
//  Created by cong on 17/1/12.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCFunc3TableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *firstBG;
@property (weak, nonatomic) IBOutlet UIView *secBG;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger type);
@end
