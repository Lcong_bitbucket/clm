//
//  XCFunc3TableViewCell.m
//  CLM
//
//  Created by cong on 17/1/12.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCFunc3TableViewCell.h"

@implementation XCFunc3TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_firstBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(0);
        }
    }];
    
    [_secBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(1);
        }
    }];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
