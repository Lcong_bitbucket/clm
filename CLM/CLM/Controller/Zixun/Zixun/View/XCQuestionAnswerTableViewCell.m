//
//  XCQuestionAnswerTableViewCell.m
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCQuestionAnswerTableViewCell.h"
#import "XCAnswerTableViewCell.h"

@implementation XCQuestionAnswerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _tableView.estimatedRowHeight = 44.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
 
    _tableView.delegate = self;
    _tableView.dataSource = self;
 
    [self whenTouchedUp:^{
        if(self.didReplyBlock&&_model.answer_list.count >0){
            self.didReplyBlock(_model,_model.answer_list[0]);
        }
     }];
    
//    UILongPressGestureRecognizer * longPressGesture =[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(viewWasLongTouchTapped:)];
//    longPressGesture.minimumPressDuration=1.5f;//设置长按 时间
//    [self addGestureRecognizer:longPressGesture];
//    
    [self whenLongTouchedUp:^{
        if(self.didLongTouchBlock&&_model.answer_list.count >0){
            self.didLongTouchBlock(_model,_model.answer_list[0]);
        }
    }];
    // Initialization code
}

//- (void)viewWasLongTouchTapped:(UILongPressGestureRecognizer*)longRecognizer{
//    if (longRecognizer.state==UIGestureRecognizerStateBegan) {
// 
//    
//    }
//}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_model.answer_list.count > 1)
        return _model.answer_list.count - 1;
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"XCAnswerTableViewCell";
    XCAnswerTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    XCQuestionCommentModel *answerModel = _model.answer_list[indexPath.row + 1];
    cell.model = answerModel;
    if(answerModel.from_id == _model.user.id){
        cell.prompt.text = @"追答";
        cell.prompt.textColor = UIColorFromRGB(0xff8421);
        cell.prompt.layer.borderColor = UIColorFromRGB(0xff8421).CGColor;
       
    } else {
        cell.prompt.text = @"追问";
        cell.prompt.textColor = UIColorFromRGB(0x1e59c5);
        cell.prompt.layer.borderColor = UIColorFromRGB(0x1e59c5).CGColor;
    }
    
    if(indexPath.row == 0){
        cell.topLine.hidden = YES;
    } else {
        cell.topLine.hidden = NO;
    }
    
    if(indexPath.row + 1 == _model.answer_list.count - 1){
        cell.bottomLine.hidden = YES;
     } else {
        cell.bottomLine.hidden = NO;
    }
    return cell;
}
 
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.didReplyBlock){
       self.didReplyBlock(_model,_model.answer_list[indexPath.row + 1]);
    }
}

- (void)setModel:(XCQuestionAnswerModel *)model{
    _model = model;
    _time.text = [NSString stringWithFormat:@"回答于%@",_model.add_time];
    if(_model.answer_list.count > 0){
         
        _content.attributedText = [NSString attributedTextWithText:[(XCQuestionCommentModel *)_model.answer_list[0] content]];

    }
    if(_model.answer_list.count == 1){
     
         _replyPrompt.hidden = NO;
       
    } else {
        _replyPrompt.hidden = YES;
    }
    [_avatar setImageURLStr:_model.user.avatar placeholder:Default_Avatar_Image_1];
    _name.text = _model.user.nick_name;
    _zhiwei.text = _model.user.zhiwei;
    _company.text = _model.user.company_name;
    [_tableView reloadData];
    
    CGFloat commentH = 0;
    if(_model.answer_list.count > 0){
        for(int i = 1 ; i < _model.answer_list.count ; i++){
            XCQuestionCommentModel *m = _model.answer_list[i];
            CGFloat h = GetStringSize(m.content, 14, nil, CGSizeMake(Screen_Width - 63 - 8, CGFLOAT_MAX)).height;
            if(h < 18)
                h = 18;
            commentH += h + 16 ;
        }
    }
   
    _tableViewH.constant = commentH;

}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
