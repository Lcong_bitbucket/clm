//
//  XCZhuanjiaScroll2View.m
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCZhuanjiaScroll2View.h"

@implementation XCZhuanjiaScroll2View

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setModel:(XCZhuanjiaModel *)model{
    _model = model;
     [_avatar setImageURLStr:ImgUrl(_model.img_url) placeholder:Default_Avatar_Image_2];
    _name.text = _model.name;
    _company.text = _model.company;
    _zhiwei.text = _model.zhiwei;
    
}
@end
