//
//  XCQuestionModel.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"
#import "XCQuestionAnswerModel.h"

@interface XCQuestionModel : XCBaseModel
 
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,assign) NSInteger user_id;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *nick_name;
@property (nonatomic,copy) NSString *company_name;
@property (nonatomic,copy) NSString *zhiwei;
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,assign) NSInteger accept_id;
@property (nonatomic,assign) NSInteger is_new;
@property (nonatomic,assign) NSInteger answer_count;

@property (nonatomic,strong) XCQuestionAnswerModel *accept_answer;
@property (nonatomic,strong) NSMutableArray *all_answer_list;

@end
