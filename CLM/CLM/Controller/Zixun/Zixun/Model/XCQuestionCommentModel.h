//
//  XCQuestionCommentModel.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCQuestionCommentModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,assign) NSInteger from_id;
@property (nonatomic,assign) NSInteger to_id;
@end
