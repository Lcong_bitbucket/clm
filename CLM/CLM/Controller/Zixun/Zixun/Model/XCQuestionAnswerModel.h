//
//  XCQuestionAnswerModel.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"
#import "XCUserModel.h"

@interface XCQuestionAnswerModel : XCBaseModel
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,strong) XCUserModel *user;
@property (nonatomic,strong) NSMutableArray *answer_list;
@end
