//
//  XCPublishQuestionViewController.h
//  CLM
//
//  Created by cong on 17/1/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"


@interface XCPublishQuestionViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *content;
@property (weak, nonatomic) IBOutlet UIScrollView *picScroll;

@property (weak, nonatomic) IBOutlet UITextField *contact;
@property (weak, nonatomic) IBOutlet UITextField *zhiwei;
@property (weak, nonatomic) IBOutlet UITextField *company;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;

- (IBAction)fabuClick:(id)sender;

@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) void (^didPublishBlock)();

@end
