//
//  XCQuestionDetailViewController.m
//  CLM
//
//  Created by cong on 17/1/12.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCQuestionDetailViewController.h"
#import "XCQuestionAnswerTableViewCell.h"
#import "XCQuestionModel.h"
#import "XCQuestionDetailHeaderTableViewCell.h"
#import "XCQuestionSectionView.h"

#import "JHInputToolBar.h"
#import "UIView+Extension.h"
#import "JHEmotionKeyboard.h"
#import "JHEmotion.h"
#import "JHEmotionTextView.h"
#import "JHTextPart.h"
#import "RegexKitLite.h"
#import "JHEmotionTool.h"

@interface XCQuestionDetailViewController ()<JHInputToolBarDelegate>
{
 
}
@property (nonatomic,strong) XCQuestionCommentModel *toReplyModel;
@property (nonatomic,strong) XCQuestionModel *curModel;

@property (nonatomic, strong) JHInputToolBar *toolbar;//自定义的toolBar
@property (nonatomic,assign) CGFloat keyboardH;//系统键盘的高度
@property (nonatomic,assign) BOOL switchingKeybaord;//是否切换键盘
@property (nonatomic, strong) JHEmotionKeyboard *emotionKeyboard;//自定义表情键盘
@end

@implementation XCQuestionDetailViewController
#pragma mark - keyboard
- (JHEmotionKeyboard *)emotionKeyboard
{
    if (_emotionKeyboard == nil) {
        _emotionKeyboard = [[JHEmotionKeyboard alloc]init];
        _emotionKeyboard.width = Screen_Width;
        _emotionKeyboard.height = self.keyboardH;
    }
    return _emotionKeyboard;
}

/**
 *  处理键盘的方法
 *
 *  @param notification
 */
- (void)keyboardWillChangeFrame:(NSNotification *)notification
{
    if (self.switchingKeybaord) return;
    
    NSDictionary *userInfo = notification.userInfo;
    // 动画的持续时间
    double duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // 键盘的frame
    CGRect keyboardF = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardH = keyboardF.size.height;
    
    // 执行动画
    [UIView animateWithDuration:duration animations:^{
        // 工具条的Y值 == 键盘的Y值 - 工具条的高度
        if (keyboardF.origin.y > self.view.height) { // 键盘的Y值已经远远超过了控制器view的高度
            self.toolbar.y = self.view.height;
        } else {
            self.toolbar.y = keyboardF.origin.y - self.toolbar.height-64;
        }
    }];
}

/**
 *  创建工具条
 */
- (void)toolBarCommon
{
    self.toolbar = [[JHInputToolBar alloc]init];
    self.toolbar.frame = CGRectMake(0, Screen_Height-64, Screen_Width, 50);
    self.toolbar.delegate = self;
    [self.view addSubview:self.toolbar];
}
/**
 *  点击工具条的代理方法
 *
 *  @param toolBar toolBar
 *  @param tag     点击工具条上的按钮的tag
 */
- (void)toolBar:(JHInputToolBar *)toolBar ButtonTag:(NSUInteger)tag andTextView:(JHEmotionTextView *)textView {
    switch (tag) {
        case 0: // 表情
            [self switchKeyboard:textView];
            break;
        case 1: // 相册
            break;
        case 2: // 发送评论
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                if(isSuccess){
                    [self sendComment:textView.fullText];
                }
            }];
            break;
    }
    
}
/**
 *  切换 表情／键盘
 */
- (void)switchKeyboard:(JHEmotionTextView *)textView
{
    if(!textView.isFirstResponder)
        [textView becomeFirstResponder];
    
    if (textView.inputView == nil) {//切换成表情键盘
        
        textView.inputView = self.emotionKeyboard;
        
        self.toolbar.showKeyboardButton = YES;//显示成键盘按钮
        
    }else//切换成系统键盘
    {
        textView.inputView = nil;
        self.toolbar.showKeyboardButton = NO;//显示成表情按钮
    }
    
    // 开始切换键盘
    self.switchingKeybaord = YES;
    
    // 退出键盘
    [textView endEditing:YES];
    //    [self.view endEditing:YES];
    //    [self.view.window endEditing:YES];
    //    [self.textView resignFirstResponder];
    // 结束切换键盘
    self.switchingKeybaord = NO;//这行代码要放在下面的  弹出键盘  的前面，不然会出现bug
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 弹出键盘
        [textView becomeFirstResponder];
    });
    
}

- (void)sendComment:(NSString *)text{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"question_id"];
    [params setValue:@"Answer" forKey:@"action"];
    [params setValue:text forKey:@"content"];
    [params setValue:@(_toReplyModel.id) forKey:@"answer_id"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self refresh];
             }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self showBottomBtn];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = NO;
}

- (void)showBottomBtn{
    if(_curModel.user_id == [XCUserManager sharedInstance].userModel.id){
        _replyBtnH.constant = 0;
    } else {
        _replyBtnH.constant = 45;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    IQKeyboardManager *manager = [IQKeyboardManager sharedManager];
    manager.enable = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    [self customShareButton];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:NO tableView:self.tableView];
    self.title = @"问题详情";
    [self refresh];
    
    //处理键盘
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [self toolBarCommon];
    
    // Do any additional setup after loading the view from its nib.
}

//获取当前界面数据
- (void)getList{
    NSData *data = [NSData dataWithContentsOfFile:[NSString mainBundlePath:@"QuestionList.json"]];
    
    NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    _curModel = [XCQuestionModel mj_objectWithKeyValues:jsonResponse];
    [self.tableView reloadData];
    [self stopRefresh:YES];
}

- (void)refresh{
//    [self getList];
    [self requestDetail];
}

- (void)rightBarButtonAction:(UIButton *)btn{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    [params setValue:@(9) forKey:@"type"];
    [params setValue:@(self.id) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"GetQuestionDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:YES];

        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                 _curModel = [XCQuestionModel mj_objectWithKeyValues:jsonResponse];
                 [self showBottomBtn];
                 [self.tableView reloadData];
 
            } else {
                showHint(jsonResponse[@"msg"]);
            }
            [self stopRefresh:YES];
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 1;
    } else if(section == 1){
        if(_curModel.accept_answer.answer_list > 0){
            return 1;
        } else {
            return 0;
        }
    } else {
        return _curModel.all_answer_list.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        
        NSString *identifier = @"XCQuestionDetailHeaderTableViewCell";
        XCQuestionDetailHeaderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _curModel;
        return cell;
    } else {
        NSString *identifier = @"XCQuestionAnswerTableViewCell";
        XCQuestionAnswerTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        WEAKSELF;
        [cell setDidReplyBlock:^(XCQuestionAnswerModel *aModel,XCQuestionCommentModel *cModel) {
            [weakSelf checkCommentQuestionModel:aModel commentModel:cModel];
        }];
        [cell setDidLongTouchBlock:^(XCQuestionAnswerModel *aModel, XCQuestionCommentModel *cModel) {
            [weakSelf checkAcceptQuestionModel:aModel commentModel:cModel];
        }];
        if(indexPath.section == 1){
            cell.model = _curModel.accept_answer;
        }else {
            cell.model = _curModel.all_answer_list[indexPath.row];
        }
        
        return cell;
    }
    
    
}

- (void)checkAcceptQuestionModel:(XCQuestionAnswerModel*)aModel commentModel:(XCQuestionCommentModel *)cModel{
    XCUserManager *manager = [XCUserManager sharedInstance];
    [manager presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
        if(isSuccess){
            XCUserModel *userModel = manager.userModel;
            if(userModel.id == _curModel.user_id){
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"采纳为最佳回答" delegate:nil cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
                [actionSheet uxy_handlerClickedButton:^(UIActionSheet *actionSheet, NSInteger btnIndex) {
                    if(btnIndex == 0){
                        [self acceptComentAnswerModel:aModel commentModel:cModel];
                    }
                }];
                [actionSheet showInView:self.view];
            }
            
        }
    }];
    
}

- (void)checkCommentQuestionModel:(XCQuestionAnswerModel*)aModel commentModel:(XCQuestionCommentModel *)cModel{
    XCUserManager *manager = [XCUserManager sharedInstance];
    [manager presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
        if(isSuccess){
            XCUserModel *userModel = manager.userModel;
            if(userModel.id == cModel.from_id){
                UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"删除追问/追答" delegate:nil cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"删除", nil];
                [actionSheet uxy_handlerClickedButton:^(UIActionSheet *actionSheet, NSInteger btnIndex) {
                    if(btnIndex == 0){
                        [self deleteComentAnswerModel:aModel commentModel:cModel];
                    }
                }];
                [actionSheet showInView:self.view];
            } else {
                if(_curModel.user_id == userModel.id || aModel.user.id == userModel.id){
                    if(aModel.user.id == cModel.from_id){
                        self.toReplyModel = cModel;
                        self.toolbar.textView.placeholder = [NSString stringWithFormat:@"追问%@:",aModel.user.nick_name];
                        [self.toolbar.textView becomeFirstResponder];
                    } else {
                        self.toReplyModel = cModel;
                        self.toolbar.textView.placeholder = [NSString stringWithFormat:@"追答%@:",aModel.user.nick_name];
                        [self.toolbar.textView becomeFirstResponder];
                    }
                } else {
                    showHint(@"不能评论其他人的答案");
                }
            }
        }
    }];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 10;
    }
    if(section == 1){
        if(_curModel.accept_answer.answer_list.count >0)
            return 50;
    }
    if(section == 2){
        if(_curModel.all_answer_list.count > 0)
         return 50;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        UIView *v = [[UIView alloc] init];
        v.backgroundColor = UIColorFromRGB(0xE9EEF2);
        return v;
    }
    if(section == 1){
        if(_curModel.accept_answer.answer_list.count >0) {
        XCQuestionSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"XCQuestionSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @" 最佳回答";
        sectionView.time.text = @"";
        sectionView.title.textColor = UIColorFromRGB(0xff8421);
        return sectionView;
       }
    }
    if(section == 2){
        if(_curModel.all_answer_list.count > 0){

        XCQuestionSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"XCQuestionSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @" 其他回答";
        sectionView.time.text = @"";
        sectionView.iconW.constant = 0;
        sectionView.title.textColor = UIColorFromRGB(0x1e59c5);
        return sectionView;
        }
    }
    return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    if(indexPath.section != 0){
//        XCQuestionAnswerModel * model = nil;
//        if(indexPath.section == 1){
//            model = _curModel.accept_answer;
//        }else {
//            model = _curModel.all_answer_list[indexPath.row];
//        }
//    }
 
}

- (void)deleteComentAnswerModel:(XCQuestionAnswerModel *)aModel commentModel:(XCQuestionCommentModel*)model{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"DelAnswer" forKey:@"action"];
    [params setValue:@(model.id) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self refresh];

            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)acceptComentAnswerModel:(XCQuestionAnswerModel *)aModel commentModel:(XCQuestionCommentModel*)cModel{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"AcceptAnswer" forKey:@"action"];
    [params setValue:@(_curModel.id) forKey:@"id"];
    [params setValue:@(cModel.id) forKey:@"answer_id"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self refresh];
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)replyClick:(id)sender {
    _toReplyModel = nil;
     self.toolbar.textView.placeholder = @"说点什么吧...";
    [self.toolbar.textView becomeFirstResponder];
    
    
}
@end
