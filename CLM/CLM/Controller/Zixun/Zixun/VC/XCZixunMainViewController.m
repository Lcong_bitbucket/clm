//
//  XCZixunMainViewController.m
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCZixunMainViewController.h"
#import "ItemTableViewCell.h"
#import "XCZhuanjiaTableViewCell.h"
#import "XCSearchView.h"
#import "HotSectionView.h"
#import "XCZhuanjiaViewController.h"
#import "XCZhuanJiaDetailViewController.h"
#import "CycleTableViewCell.h"
#import "XCFunc3TableViewCell.h"
#import "AD1TableViewCell.h"
#import "XCZhuanjiaScroll2TableViewCell.h"
#import "XCQuestionTableViewCell.h"
#import "XCQuestionsViewController.h"
#import "XCQuestionDetailViewController.h"
#import "XCZixunTiwenTableViewCell.h"
#import "XCPublishQuestionViewController.h"
#import "XCZhuanjiaViewController.h"


@interface XCZixunMainViewController ()
@property (nonatomic,strong) NSString *keyword;
@property (nonatomic,strong) NSMutableArray *zhuanjiaArray;
@property (nonatomic,strong) NSMutableArray *func1Array;
@property (nonatomic,strong) NSMutableArray *ad1Array;
@property (nonatomic,strong) NSMutableArray *ad2Array;
@property (nonatomic,strong) NSMutableArray *questionArray;

@end

@implementation XCZixunMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isShowBackBtn){
        [self customBackButton];
    }
    
    CGRect frame=CGRectMake(0, 0, 0, CGFLOAT_MIN);
    self.tableView.tableHeaderView = [[UIView alloc]initWithFrame:frame];
    
    _zhuanjiaArray = [NSMutableArray new];
    _func1Array = [NSMutableArray new];
    _ad1Array = [NSMutableArray new];
    _ad2Array = [NSMutableArray new];
    _questionArray = [NSMutableArray new];

    XCSearchView *searchView = [[XCSearchView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 80, 30)];
    [searchView setDidSearchBlock:^(NSString *text) {
        _keyword = text;
         [self refresh];
    }];
    self.navigationItem.titleView = searchView;

    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:NO tableView:self.tableView];
    [self beginRefreshing];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)refresh{
    [self requestAD:5];
    [self requestAD:6];

    [self requestFucn1];
    [self requestZhuanjia];
    [self requestQuestion];

}

- (void)requestFucn1{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(5) forKey:@"type"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_func1Array removeAllObjects];
                [_func1Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}
- (void)requestAD:(NSInteger)place{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(place) forKey:@"place"];
    [params setValue:@"GetAD" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(place == 5){
                    [_ad1Array removeAllObjects];
                    [_ad1Array addObjectsFromArray:jsonResponse[@"ds"]];
                }else if(place == 6){
                    [_ad2Array removeAllObjects];
                    [_ad2Array addObjectsFromArray:jsonResponse[@"ds"]];
                }
                [_tableView reloadData];
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestZhuanjia{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetZhuanjiaList" forKey:@"action"];
    [params setValue:@"1" forKey:@"is_latest"];
    [params setValue:_keyword forKey:@"keyword"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:YES];
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_zhuanjiaArray removeAllObjects];
                [_zhuanjiaArray addObjectsFromArray:[XCZhuanjiaModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
            [self stopRefresh:YES];

        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestQuestion{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetQuestionList" forKey:@"action"];
    [params setValue:@"1" forKey:@"is_latest"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:YES];
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_questionArray removeAllObjects];
                [_questionArray addObjectsFromArray:[XCQuestionModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
            [self stopRefresh:YES];
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if(_ad1Array.count > 0)
            return 1;
    } else if(section == 1){
        return 1;
    } else if(section == 2){
        return 1;
    } else if(section == 3){
        if(_ad1Array.count > 0)
            return 1;
    } else if(section == 4){
        return 1;
    } else if(section == 5){
        return _questionArray.count;
    } else if(section == 6){
        return 1;
    }
    return 0;
}
 
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        if(indexPath.section == 0){
             NSString *identifier = @"CycleTableViewCell";
            CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            if(self.ad1Array.count > 0){
                cell.heightC.constant = 120 * Screen_Width / 375;
                cell.contentView.hidden = NO;
                cell.cycles = _ad1Array;

            } else {
                cell.contentView.hidden = YES;
                cell.heightC.constant = 0;
            }
            [cell setCycleDetailBlock:^(NSDictionary *dic) {
                goADPage(dic,self);
            }];
            return cell;
            
        }
    } else if(indexPath.section == 1){
        NSString *identifier = @"XCFunc3TableViewCell";
        XCFunc3TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^(NSInteger type) {
            if(type == 0){
                //我要提问
                XCPublishQuestionViewController *vc = [[XCPublishQuestionViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                 [self.navigationController pushViewController:vc animated:YES];
            } else if(type == 1){
                //免费咨询
                XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }
        }];
         return cell;

    } else if(indexPath.section == 2){
            NSString *identifier = @"ItemTableViewCell";
            ItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            [cell setDidClickBlock:^(XCMarkModel *model) {
                
                XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
                vc.lingyu = model.title;
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            cell.col = 4;
            cell.array = self.func1Array;
            return cell;
       
    }  else if(indexPath.section == 3){
        NSString *identifier = @"AD1TableViewCell";
        AD1TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        if(_ad2Array.count > 0){
            cell.galleryH.constant = 25 + (Screen_Width - 25)*100/350 ;
            cell.contentView.hidden = NO;
            cell.cycles = _ad2Array;
         } else {
            cell.galleryH.constant = 0 ;
            cell.contentView.hidden = YES;
        }
        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic,self);
        }];
        return cell;
    } else if(indexPath.section == 4){
        NSString *identifier = @"XCZhuanjiaScroll2TableViewCell";
        XCZhuanjiaScroll2TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^(XCZhuanjiaModel *model) {
            XCZhuanJiaDetailViewController *vc = [[XCZhuanJiaDetailViewController alloc] init];
            vc.id = [model id];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        cell.array = _zhuanjiaArray;
        return cell;
    } else if(indexPath.section == 5){
        NSString *identifier = @"XCQuestionTableViewCell";
        XCQuestionTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _questionArray[indexPath.row];
        return cell;
    }else if(indexPath.section == 6){
        
        NSString *identifier = @"XCZixunTiwenTableViewCell";
        XCZixunTiwenTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
         return cell;

    }

    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    if(indexPath.section == 5){
        XCQuestionDetailViewController *vc = [[XCQuestionDetailViewController alloc] init];
        vc.id = [_questionArray[indexPath.row] id];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if(indexPath.section == 6){
        XCPublishQuestionViewController *vc = [[XCPublishQuestionViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [vc setDidPublishBlock:^{
            [self beginRefreshing];
        }];
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 2){
        return 50;
    }else if(section == 3){
        return 50;
    }else if(section == 5){
        return 50;
    }
  
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 2){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"按擅长领域找专家";
        sectionView.more.text = @"";
        
        [sectionView whenTouchedUp:^{
            
        }];
        sectionView.top.constant = 2;
        return sectionView;
    } else if(section == 3){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"新晋专家";
        sectionView.more.text = @"更多";
        [sectionView whenTouchedUp:^{
            [self goZhuanjia];
        }];
        return sectionView;
    } else if(section == 5){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"最新问题";
        sectionView.more.text = @"更多";
        [sectionView whenTouchedUp:^{
            [self goQuestion];
        }];
        return sectionView;
    }
    return nil;
}


-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
    
    return CGFLOAT_MIN;
    
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section {

    return nil;
}

- (void)goZhuanjia{
    XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)goQuestion{
    XCQuestionsViewController *vc = [[XCQuestionsViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
