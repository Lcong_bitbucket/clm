//
//  XCQuestionsViewController.h
//  CLM
//
//  Created by cong on 17/1/12.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCQuestionsViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *tiwenBtn;
@property (nonatomic,assign) NSInteger vcType;//0正常 1我发布的 2 我回答的 3 我被采纳的
- (IBAction)tiwenClick:(id)sender;

@end
