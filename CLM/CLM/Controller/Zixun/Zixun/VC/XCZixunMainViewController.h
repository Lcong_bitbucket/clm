//
//  XCZixunMainViewController.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCZixunMainViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign) BOOL isShowBackBtn;
@end
