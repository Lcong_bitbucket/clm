//
//  XCQuestionDetailViewController.h
//  CLM
//
//  Created by cong on 17/1/12.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCQuestionDetailViewController : XCTableViewController
- (IBAction)replyClick:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replyBtnH;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign) NSInteger id;
@end
