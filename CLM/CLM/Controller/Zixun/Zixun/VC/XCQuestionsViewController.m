//
//  XCQuestionsViewController.m
//  CLM
//
//  Created by cong on 17/1/12.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCQuestionsViewController.h"
#import "XCSearchView.h"
#import "XCQuestionTableViewCell.h"
#import "XCQuestionDetailViewController.h"
#import "XCPublishQuestionViewController.h"
#import "WZLBadgeImport.h"

@interface XCQuestionsViewController (){

}
@property (nonatomic,copy) NSString *keyword;
@property (nonatomic,strong) NSMutableArray *questionArray;
@end

@implementation XCQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    
    _tiwenBtn.titleLabel.numberOfLines = 2;
    
    _questionArray = [NSMutableArray new];
    if(self.vcType == 0){
        XCSearchView *searchView = [[XCSearchView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 80, 30)];
        [searchView setDidSearchBlock:^(NSString *text) {
            _keyword = text;
            [self refresh];
        }];
        self.navigationItem.titleView = searchView;
    } else if(self.vcType == 1){
        self.title = @"我发布过的问题";
    }else if(self.vcType == 2){
        self.title = @"我回答过的问题";
    }else if(self.vcType == 3){
        self.title = @"我被采纳的回答";
    }

    
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    [self beginRefreshing];

    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    XCPublishQuestionViewController *vc = [[XCPublishQuestionViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)refresh{
    [self requestQuestion:self.curPage];
}

- (void)requestQuestion:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetQuestionList" forKey:@"action"];
     [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:_keyword forKey:@"keyword"];
    
    if(self.vcType == 1){
        [params setValue:@([XCUserManager sharedInstance].userModel.id) forKey:@"user_id"];
        [params setValue:@"0" forKey:@"type"];
     }else if(self.vcType == 2){
        [params setValue:@([XCUserManager sharedInstance].userModel.id) forKey:@"user_id"];
         [params setValue:@"1" forKey:@"type"];
    }else if(self.vcType == 3){
        [params setValue:@([XCUserManager sharedInstance].userModel.id) forKey:@"user_id"];
         [params setValue:@"2" forKey:@"type"];
    }
 
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1)
                    [_questionArray removeAllObjects];
                [_questionArray addObjectsFromArray:[XCQuestionModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
               
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
                [self stopRefresh:NO];
             }
         }
    }];
    [[XCHttpClient sharedInstance] request:command];
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _questionArray.count;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
           NSString *identifier = @"XCQuestionTableViewCell";
        XCQuestionTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
   
     cell.model = _questionArray[indexPath.row];
    if(self.vcType > 0){
        if(cell.model.is_new > 0){
            cell.title.badgeCenterOffset = CGPointMake(-cell.title.width,0);
            [cell.title showBadgeWithStyle:WBadgeStyleRedDot value:0 animationType:WBadgeAnimTypeBreathe];
        } else {
            [cell.title clearBadge];
        }
    }
      return cell;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 添加一个删除按钮
    if(self.vcType == 1){
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewRowAction *deleteRoWAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {//title可自已定义
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"删除后不可恢复,确定删除？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
                [self delete:indexPath];
            }
        }];
        [alert show];
        
    }];//此处是iOS8.0以后苹果最新推出的api，UITableViewRowAction，Style是划出的标签颜色等状态的定义，这里也可自行定义
    
    UITableViewRowAction *editRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self modify:indexPath];
    }];
    editRowAction.backgroundColor = [UIColor colorWithRed:0 green:124/255.0 blue:223/255.0 alpha:1];//可以定义RowAction的颜色
    return @[deleteRoWAction, editRowAction];//最后返回这俩个RowAction 的数组
    
}

- (void)delete:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"DelQuestion" forKey:@"action"];
    [params setValue:@([_questionArray[indexPath.row] id]) forKey:@"id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_questionArray removeObjectAtIndex:indexPath.row];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)modify:(NSIndexPath *)indexPath{
    XCPublishQuestionViewController *vc = [[XCPublishQuestionViewController alloc] init];
    vc.id = [_questionArray[indexPath.row] id];
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    XCQuestionDetailViewController *vc = [[XCQuestionDetailViewController alloc] init];
    vc.id = [_questionArray[indexPath.row] id];
    vc.hidesBottomBarWhenPushed = YES;
    XCQuestionModel *model = _questionArray[indexPath.row];
    model.is_new= 0;
    [self.tableView reloadData];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tiwenClick:(id)sender {
    XCPublishQuestionViewController *vc = [[XCPublishQuestionViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];

}
@end
