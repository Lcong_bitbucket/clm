//
//  XCDemandModel.m
//  CLM
//
//  Created by cong on 16/12/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCDemandModel.h"

@implementation XCDemandModel
- (NSString*)ce_time{
    if(_ce_time.length == 0){
        return @"面议";
    }
    return _ce_time;
}

- (NSString *)price{
    if(_price.length == 0){
        return @"面议";
    }
    return _price;
}

@end
