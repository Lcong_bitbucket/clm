//
//  XCPublishDemandViewController.m
//  CLM
//
//  Created by cong on 16/12/21.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCPublishDemandViewController.h"
//#import "GYZChooseCityController.h"
#import "XCDemandModel.h"
#import "STPickerArea.h"
#import "STPickerDate.h"
#import "STPickerSingle.h"
#import "XCMarkModel.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "XCJiGouModel.h"
#import "XCPublishResultViewController.h"
#import "XCMyOrderHelpViewController.h"

@interface XCPublishDemandViewController ()<STPickerAreaDelegate, STPickerSingleDelegate, STPickerDateDelegate>
{
    XCDemandModel *_curModel;
    NSMutableArray *_menu2Array;//资质
//    NSMutableArray *_menu2Value;

    NSMutableArray *_menu1Array;//分类
    NSMutableArray *_menu1Value;
    
    __block XCJiGouModel *_curJiGouModel;
    NSMutableArray *_jigouArray;
    NSMutableArray *_jigouValue;
    
//    NSInteger _isRenzheng;
}

@end

@implementation XCPublishDemandViewController
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area
{
    NSString *text = [NSString stringWithFormat:@"%@ %@ %@", province, city, area];
    self.addressTF.text = text;
}

- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle selectedIndex:(NSInteger)row
{
    if(pickerSingle.tag == 111){
        NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
        self.fenleiTF.text = text;
    }
//    else if(pickerSingle.tag == 112){
//        _isRenzheng = [_menu2Array[row][@"id"] integerValue];
//        self.renzhengL.text = selectedTitle;
//        NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
//        if(row == 0){
//            self.jigouL.text = @"";
//            _curJiGouModel = nil;
//        } else {
//            self.jigouL.text = text;
//            _curJiGouModel = _jigouArray[row-1];
//        }
//    }
}

- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSString *text = [NSString stringWithFormat:@"%zd年%zd月%zd日", year, month, day];
    self.timeTF.text = text;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    
    _menu1Array = [NSMutableArray new];
    _menu1Value = [NSMutableArray new];
    
    _menu2Array = [NSMutableArray new];
 
    _jigouArray = [NSMutableArray new];
    _jigouValue = [NSMutableArray new];
    
    self.title = @"发布测试需求";
    
    UIView *addressCover = [[UIView alloc] init];
    [addressCover whenTouchedUp:^{
        [self.view endEditing:YES];
        
        STPickerArea *pickerArea = [[STPickerArea alloc]init];
        [pickerArea setDelegate:self];
        [pickerArea setContentMode:STPickerContentModeBottom];
        [pickerArea show];
    }];
    [self.addressBG addSubview:addressCover];
    [addressCover mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.addressBG.mas_left);
        make.top.mas_equalTo(self.addressBG.mas_top);
        make.bottom.mas_equalTo(self.addressBG.mas_bottom);
        make.right.mas_equalTo(self.addressBG.mas_right);
    }];

    
    
    UIView *fenLeiCover = [[UIView alloc] init];
    [fenLeiCover whenTouchedUp:^{
        [self.view endEditing:YES];
        
        if(_menu1Value.count > 0){
            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
            pickerSingle.widthPickerComponent = 200;
            pickerSingle.tag = 111;
            [pickerSingle setArrayData:_menu1Value];
            [pickerSingle setTitle:@"请选择测试类别"];
            [pickerSingle setTitleUnit:@""];
            [pickerSingle setContentMode:STPickerContentModeBottom];
            [pickerSingle setDelegate:self];
            [pickerSingle show];
        } else {
            [self requestCategory];
        }
    }];
    [self.fenleiBG addSubview:fenLeiCover];
    [fenLeiCover mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.fenleiBG.mas_left);
        make.top.mas_equalTo(self.fenleiBG.mas_top);
        make.bottom.mas_equalTo(self.fenleiBG.mas_bottom);
        make.right.mas_equalTo(self.fenleiBG.mas_right);
    }];

    
    UIView *timeCover = [[UIView alloc] init];
    [timeCover whenTouchedUp:^{
        [self.view endEditing:YES];
        
        STPickerDate *pickerDate = [[STPickerDate alloc]init];
        [pickerDate setDelegate:self];
        [pickerDate show];
    }];
    [self.timeBG addSubview:timeCover];
    [timeCover mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.timeBG.mas_left);
        make.top.mas_equalTo(self.timeBG.mas_top);
        make.bottom.mas_equalTo(self.timeBG.mas_bottom);
        make.right.mas_equalTo(self.timeBG.mas_right);
    }];
 
    
//    [_renzhengBG whenTapped:^{
//        [self.view endEditing:YES];
//        
//        if(_menu2Value.count > 0){
//            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
//            pickerSingle.widthPickerComponent = 200;
//            pickerSingle.tag = 112;
//            [pickerSingle setArrayData:_menu2Value];
//            [pickerSingle setTitle:@"请选择认证资质"];
//            [pickerSingle setTitleUnit:@""];
//            [pickerSingle setContentMode:STPickerContentModeBottom];
//            [pickerSingle setDelegate:self];
//            [pickerSingle show];
//        } else {
//            [self requestMark:6];
//        }
//
//    }];
    
//    [_jigouBG whenTapped:^{
//        [self.view endEditing:YES];
//        if(_jigouValue.count > 0){
//            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
//            pickerSingle.tag = 112;
//            pickerSingle.widthPickerComponent = 300;
//            [pickerSingle setArrayData:_jigouValue];
//            [pickerSingle setTitle:@"请选择所属机构"];
//            [pickerSingle setTitleUnit:@""];
//            [pickerSingle setContentMode:STPickerContentModeBottom];
//            [pickerSingle setDelegate:self];
//            [pickerSingle show];
//        } else {
//            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
//                if(isSuccess){
//                    [self requestJiGou];
//                }
//            }];
//        }
//    }];
    
//    [self requestJiGou];
    [self requestCategory];
    [self requestMark:2];
   
    if(self.id)
        [self requestDetail];
    
    [self customNavigationBarItemWithImageName:nil title:@"帮助" isLeft:NO];
    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    XCMyOrderHelpViewController *vc = [[XCMyOrderHelpViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)refreshUI{
    
    if(_curModel){
        _titleTF.text = _curModel.title;
        _timeTF.text = _curModel.ce_time;
        _priceTF.text = _curModel.price;
        _addressTF.text = _curModel.ce_address;
        _special_demand.text = _curModel.special_demand;
        _fenleiTF.text = _curModel.category;
     }
    
    //是否需要认证资质
    CGFloat w = 36,h=36;
    for(int i = 0; i < _menu2Array.count; i++){
        XCMarkModel *model = _menu2Array[i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [btn setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:ImgUrl([model img_url])] placeholderImage:Default_Loading_Image_1];
        btn.tag = 100+i;
        [btn setImage:imageNamed(@"tick") forState:UIControlStateSelected];
        NSArray *renzhengList = nil;
        if(_curModel.renzheng.length>0){
            renzhengList = [_curModel.renzheng componentsSeparatedByString:@","];
        }
        for(NSString*str in renzhengList){
            if([str integerValue] == model.id){
                model.isSelected = YES;
            }
        }
        btn.selected = model.isSelected;
        [btn whenTapped:^{
            btn.selected = !model.isSelected;
            model.isSelected = !model.isSelected;
        }];
        [self.renzhengScroll addSubview:btn];
    }
    self.renzhengScroll.contentSize = CGSizeMake(_menu2Array.count * (w +10) +10, 0);

}

//- (void)requestJiGou{
//    if(![[XCUserManager sharedInstance] isLogin])
//        return;
//    NSMutableDictionary *params = [NSMutableDictionary new];
//    [params setValue:@"GetJigouList" forKey:@"action"];
//    [params setValue:@([[[XCUserManager sharedInstance] userModel] id]) forKey:@"user_id"];
//    XCBaseCommand *command = [XCBaseCommand new];
//    command.api = @"/tools/submit_ajax.ashx";
//    command.params = params;
//    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
//        if(error){
//            
//        } else {
//            if([[jsonResponse objectForKey:@"status"] intValue]==1)
//            {
//                [_jigouArray removeAllObjects];
//                [_jigouValue removeAllObjects];
//                [_jigouArray addObjectsFromArray:[XCJiGouModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
//                [_jigouValue addObject:@"无"];
//                for(int i = 0; i < _jigouArray.count; i++){
//                    [_jigouValue addObject:[_jigouArray[i] name]];
//                }
//                [self refreshUI];
//            }else {
//                showHint(jsonResponse[@"msg"]);
//            }
//        }
//    }];
//    [[XCHttpClient sharedInstance] request:command];
//}


- (void)requestCategory{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetCategory" forKey:@"action"];
    [params setValue:@"15" forKey:@"parent"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_menu1Array removeAllObjects];
                [_menu1Value removeAllObjects];
                [_menu1Array addObjectsFromArray:jsonResponse[@"ds"]];
                for(int i = 0; i < _menu1Array.count; i++){
                    [_menu1Value addObject:_menu1Array[i][@"title"]];
                }
                [self refreshUI];

            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestMark:(NSInteger)type{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(type) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_menu2Array removeAllObjects];
                [_menu2Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self refreshUI];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"GetDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _curModel = [XCDemandModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    [self refreshUI];
                    
                 }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fabu:(id)sender {
    
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                [self request];
             }
        }];
 }

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_titleTF.text forKey:@"title"];
    [params setValue:_fenleiTF.text forKey:@"category"];
    
    NSMutableString *renzheng = [NSMutableString new];
    for(NSInteger i = 0; i < _menu2Array.count;i ++){
        XCMarkModel *model = _menu2Array[i];
        if(model.isSelected){
            if(i == _menu2Array.count - 1){
                [renzheng appendFormat:@"%ld",(long)model.id];
            } else {
                [renzheng appendFormat:@"%ld,",(long)model.id];
            }
        }
    }
    if(renzheng.length > 0 ){
        [params setValue:renzheng forKey:@"renzheng"];
    }
    
    [params setValue:_priceTF.text forKey:@"price"];
    [params setValue:_timeTF.text forKey:@"ce_time"];
    [params setValue:_addressTF.text forKey:@"ce_address"];
    [params setValue:_special_demand.text forKey:@"special_demand"];
 
    [params setValue:@(1) forKey:@"type"];
    [params setValue:@"EditDevice" forKey:@"action"];
    if(self.id){
        [params setValue:@(self.id) forKey:@"id"];
    }

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                XCPublishResultViewController *vc = [[XCPublishResultViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
             }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}



@end
