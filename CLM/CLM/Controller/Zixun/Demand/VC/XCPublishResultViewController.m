//
//  XCPublishResultViewController.m
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCPublishResultViewController.h"
#import "UIButton+ImageTitleSpacing.h"
#import "XCMyOrderPageViewController.h"


@interface XCPublishResultViewController ()

@end

@implementation XCPublishResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customBackButton];
    self.title = @"提交需求";
    [self customNavigationBarItemWithImageName:@"icon_kf" title:nil isLeft:NO];
    
    NSMutableAttributedString *hintString=[[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"请在我的需求中查看状态"]];
    [hintString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, hintString.length)];
    _checkLabel.attributedText = hintString;
    // Do any additional setup after loading the view from its nib.
    
    [_bottomBtn1 layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
    
    [_bottomBtn2 layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
    
    [_bottomBtn3 layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleTop imageTitleSpace:8];
    
    _bottomBtn1.selected = YES;
    
    [_checkLabel whenTapped:^{
        XCMyOrderPageViewController *vc = [[XCMyOrderPageViewController alloc] init];
        vc.curSelected = 1;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];

}

- (void)goBack{
    [self.navigationController popToViewController:self.navigationController.viewControllers[1] animated:YES];
}

- (void)rightBarButtonAction:(UIButton *)btn{
    [self requestTel];
}

#pragma mark - 请求数据
- (void)requestTel{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetKF" forKey:@"action"];
    [params setValue:@"4" forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                openTel(jsonResponse[@"tel"]);
            } else {
                
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
