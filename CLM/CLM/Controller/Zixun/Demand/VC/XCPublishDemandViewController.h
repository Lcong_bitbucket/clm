//
//  XCPublishDemandViewController.h
//  CLM
//
//  Created by cong on 16/12/21.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"

@interface XCPublishDemandViewController : XCBaseViewController

@property (weak, nonatomic) IBOutlet UIView *addressBG;
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UIScrollView *renzhengScroll;
@property (weak, nonatomic) IBOutlet UIView *renzhengBG;
@property (weak, nonatomic) IBOutlet UILabel *renzhengL;

@property (weak, nonatomic) IBOutlet UIView *fenleiBG;
@property (weak, nonatomic) IBOutlet UITextField *fenleiTF;
 
@property (weak, nonatomic) IBOutlet UITextField *priceTF;
@property (weak, nonatomic) IBOutlet UIView *timeBG;
@property (weak, nonatomic) IBOutlet UITextField *timeTF;

@property (weak, nonatomic) IBOutlet UITextField *addressTF;

@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *special_demand;

- (IBAction)fabu:(id)sender;
@property (nonatomic,assign) NSInteger id;//有ID 即为编辑修改
 
@end
