//
//  XCDemandViewController.h
//  CLM
//
//  Created by cong on 16/12/1.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCDemandViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbTop;

@property (nonatomic,copy) NSString *category;
@property (nonatomic,assign) NSInteger renzheng;
@property (nonatomic,copy) NSString *keyword;

@property (nonatomic,assign) NSInteger vcType; //0 普通列表 1 我发布的

@end
