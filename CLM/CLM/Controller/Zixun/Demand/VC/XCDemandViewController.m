//
//  XCDemandViewController.m
//  CLM
//
//  Created by cong on 16/12/1.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCDemandViewController.h"
#import "XCSearchView.h"
#import "XCDemandTableViewCell.h"
#import "XCDemandModel.h"
#import "XCPopView.h"
#import "CycleTableViewCell.h"
#import "XCDemandsDetailViewController.h"
#import "XCPublishDemandViewController.h"
//#import "JSDropDownMenu.h"
#import "DOPDropDownMenu.h"

@interface XCDemandViewController ()<DOPDropDownMenuDataSource,DOPDropDownMenuDelegate>
{
    
    NSMutableArray *_menu1Array;
    NSMutableArray *_menu1Value;
    NSInteger _currentMenu1Index;
    
    NSMutableArray *_menu2Array;
    NSMutableArray *_menu2Value;
    NSInteger _currentMenu2Index;
    
    NSInteger _isRenzheng;
    
}
@property (nonatomic, strong) DOPDropDownMenu *menu;

@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *adArray;

@end

@implementation XCDemandViewController
- (void)initMenu{
    // 添加下拉菜单
    _menu = [[DOPDropDownMenu alloc] initWithOrigin:CGPointMake(0, 0) andHeight:44];
    _menu.indicatorColor = [UIColor colorWithRed:175.0f/255.0f green:175.0f/255.0f blue:175.0f/255.0f alpha:1.0];
    _menu.separatorColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0];
    _menu.textColor = [UIColor colorWithRed:83.f/255.0f green:83.f/255.0f blue:83.f/255.0f alpha:1.0f];
    _menu.dataSource = self;
    _menu.delegate = self;
    
    [self.view addSubview:_menu];
    [_menu selectIndexPath:[DOPIndexPath indexPathWithCol:0 row:_currentMenu1Index]];
    
}
- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu
{
    return 2;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column
{
    if (column == 0) {
        return _menu1Value.count;
    }else if (column == 1){
        return _menu2Value.count;
    }
    return 0;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if (indexPath.column == 0) {
        return _menu1Value[indexPath.row];
    } else if (indexPath.column == 1){
        return _menu2Value[indexPath.row];
    }
    return nil;
}

// new datasource

- (NSString *)menu:(DOPDropDownMenu *)menu imageNameForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if (indexPath.column == 1) {
        return _menu2Array[indexPath.row][@"img_url"];
    }
    return nil;
}

- (NSString *)menu:(DOPDropDownMenu *)menu imageNameForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
    return nil;
}

// new datasource

- (NSString *)menu:(DOPDropDownMenu *)menu detailTextForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
    return nil;
}

- (NSString *)menu:(DOPDropDownMenu *)menu detailTextForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    return nil;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfItemsInRow:(NSInteger)row column:(NSInteger)column
{
    
    return 0;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
    return nil;
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath
{
    //    if (indexPath.item >= 0) {
    //        NSLog(@"点击了 %ld - %ld - %ld 项目",indexPath.column,indexPath.row,indexPath.item);
    //    }else {
    //        NSLog(@"点击了 %ld - %ld 项目",indexPath.column,indexPath.row);
    //    }
    
    if(indexPath.column == 0){
        _currentMenu1Index = indexPath.row;
        _category = _menu1Value[_currentMenu1Index];
        if(indexPath.row == 0){
            _category = @"";
        }
    } else{
        _currentMenu2Index = indexPath.row;
//        _renzheng = [_menu2Array[_currentMenu2Index][@"id"] integerValue];
        _isRenzheng = [_menu2Array[_currentMenu2Index][@"id"] integerValue];
    }
    [self beginRefreshing];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray new];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
     [self customBackButton];

    if(self.vcType == 0){
        _adArray = [NSMutableArray new];
        
        _menu1Array = [NSMutableArray new];
        _menu2Array = [NSMutableArray new];
        _menu1Value = [NSMutableArray new];
        _menu2Value = [NSMutableArray new];
        
        XCSearchView *searchView = [[XCSearchView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 20, 30)];
        searchView.searchTF.text = self.keyword;
        [searchView setDidSearchBlock:^(NSString *text) {
            _keyword = text;
            self.curPage = 1;
            [self refresh];
        }];
        self.navigationItem.titleView = searchView;
        
        if(_menu1Array.count == 0){
            [self requestCategory];
        }
        if(_menu2Array.count == 0){
            [self requestMark:6];
        }
    
    } else {
        self.title = @"我发布的需求";
        self.tbTop.constant = 0;
        [self beginRefreshing];
    }
    
    UIButton *fabuBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - 65, Screen_Height - 200, 55, 55)];
    if(_vcType == 0){
        [fabuBtn setTitle:@"发布\n设备" forState:UIControlStateNormal];
        
    } else if(_vcType == 1){
        [fabuBtn setTitle:@"发布\n需求" forState:UIControlStateNormal];

    }
    fabuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    fabuBtn.titleLabel.numberOfLines = 2;

    [fabuBtn setBackgroundImage:[UIImage imageNamed:@"bt_circular"] forState:UIControlStateNormal];
    [fabuBtn addTarget:self action:@selector(fabu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fabuBtn];
    // Do any additional setup after loading the view from its nib.
}

- (void)fabu{
    if(_vcType == 0){
        XCPublishSupplyViewController *vc = [[XCPublishSupplyViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
      
        [self.navigationController pushViewController:vc animated:YES];
    
    } else {
        
        XCPublishDemandViewController *vc = [[XCPublishDemandViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.curPage = 1;
    [self refresh];
}

#pragma mark - 数据请求
- (void)refresh{
    if(self.curPage)
        [self requestAD];
    [self request:self.curPage];
}

- (void)request:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:@(1) forKey:@"type"];
    [params setValue:_category forKey:@"category"];
    [params setValue:@(_isRenzheng) forKey:@"is_renzheng"];
    [params setValue:_keyword forKey:@"keyword"];
    [params setValue:@"GetDeviceList" forKey:@"action"];
    
    if(self.vcType == 1){
        [params setValue:@([[[XCUserManager sharedInstance] userModel] id]) forKey:@"user_id"];
    }
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1){
                    [_dataArray removeAllObjects];
                }
                [_dataArray addObjectsFromArray:[XCDemandModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
                [self stopRefresh:NO];
             }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestCategory{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetCategory" forKey:@"action"];
    [params setValue:@"15" forKey:@"parent"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_menu1Array removeAllObjects];
                [_menu1Value removeAllObjects];
                [_menu1Array addObject:@{@"title":@"测试类别"}];
                [_menu1Array addObjectsFromArray:jsonResponse[@"ds"]];
                for(int i = 0; i < _menu1Array.count; i++){
                    [_menu1Value addObject:_menu1Array[i][@"title"]];
                }
                if(_menu2Value.count > 0 && _menu1Value.count > 0){
                    [self initMenu];
                }
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestMark:(NSInteger)type{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(type) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(type == 6){
                    [_menu2Array removeAllObjects];
                    [_menu2Value removeAllObjects];
                    [_menu2Array addObject:@{@"title":@"认证资质"}];
                    [_menu2Array addObjectsFromArray:jsonResponse[@"ds"]];
                    
                    for(int i = 0; i < _menu2Array.count; i++){
                        [_menu2Value addObject:_menu2Array[i][@"title"]];
//                        if(self.renzheng == [_menu2Array[i][@"id"] integerValue]){
//                            _currentMenu2Index = i;
//                        }
                    }
                }
                if(_menu2Value.count > 0 && _menu1Value.count > 0){
                    [self initMenu];
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestAD{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetAD" forKey:@"action"];
    [params setValue:@"2" forKey:@"place"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_adArray removeAllObjects];
                [_adArray addObjectsFromArray:jsonResponse[@"ds"]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if(self.vcType == 0){
            if(self.adArray.count > 0)
                return 1;
        }
        return 0;
    } else {
        return _dataArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        
        NSString *identifier = @"CycleTableViewCell";
        CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        if(self.vcType == 0){
            if(self.adArray.count > 0){
                cell.heightC.constant = 120 * Screen_Width / 320;
                cell.contentView.hidden = NO;
            } else {
                cell.heightC.constant = 0;
                cell.contentView.hidden = YES;
            }

        } else {
            cell.heightC.constant = 0;
            cell.contentView.hidden = YES;
        }
        if(self.vcType == 0)
            cell.cycles = _adArray;
        return cell;
        
    } else if(indexPath.section == 1){
        NSString *identifier = @"XCDemandTableViewCell";
        XCDemandTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _dataArray[indexPath.row];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XCDemandsDetailViewController *vc = [[XCDemandsDetailViewController alloc] init];
    vc.id = [_dataArray[indexPath.row] id];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 添加一个删除按钮
    if(self.vcType == 1){
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewRowAction *deleteRoWAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {//title可自已定义
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"删除后不可恢复,确定删除？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
                [self delete:indexPath];
            }
        }];
        [alert show];
        
    }];//此处是iOS8.0以后苹果最新推出的api，UITableViewRowAction，Style是划出的标签颜色等状态的定义，这里也可自行定义
    
    UITableViewRowAction *editRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self modify:indexPath];
    }];
    editRowAction.backgroundColor = [UIColor colorWithRed:0 green:124/255.0 blue:223/255.0 alpha:1];//可以定义RowAction的颜色
    return @[deleteRoWAction, editRowAction];//最后返回这俩个RowAction 的数组
    
}

- (void)delete:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"delete_demand" forKey:@"action"];
    [params setValue:@([_dataArray[indexPath.row] id]) forKey:@"id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_dataArray removeObjectAtIndex:indexPath.row];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)modify:(NSIndexPath *)indexPath{
    XCPublishDemandViewController *vc = [[XCPublishDemandViewController alloc] init];
    vc.id = [_dataArray[indexPath.row] id];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
