//
//  XCPublishResultViewController.h
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface XCPublishResultViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *checkLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn1;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn2;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn3;

@end
