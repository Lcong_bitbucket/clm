//
//  XCDemandDetailTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCDemandDetailTableViewCell.h"

@implementation XCDemandDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
 
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCDemandModel *)model{
    _model = model;
    [_icon setImageURLStr:ImgUrl(_model.img_url) placeholder:Default_Loading_Image_1];
    _title.text = _model.title;
    _ce_time.text = [NSString stringWithFormat:@"测试周期:%@",_model.ce_time];
    _ce_price.text = [NSString stringWithFormat:@"测试价格:%@",_model.price];
    _remark.text = _model.remark;
    _address.text = _model.ce_address;
    _need.text = _model.special_demand;
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[_model.anli dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    _anli.attributedText = attrStr;
    _anli.font = [UIFont systemFontOfSize:14];
}

@end
