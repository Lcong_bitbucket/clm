//
//  XCDemandTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCDemandModel.h"

@interface XCDemandTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *handle_status;
@property (weak, nonatomic) IBOutlet UIScrollView *renzhengScroll;
@property (weak, nonatomic) IBOutlet UILabel *getOrderNum;
@property (nonatomic,copy) void (^didClickBlock)(NSDictionary *dic);
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rzH;
@property (weak, nonatomic) IBOutlet UILabel *price;

@property (nonatomic,strong) XCDemandModel *model;
@end
