//
//  XCDemandTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCDemandTableViewCell.h"

@implementation XCDemandTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
//    _handle_status.textColor = [UIColor whiteColor];
//    _handle_status.textInsets = UIEdgeInsetsMake(2, 4, 2, 4);
    // Initialization code
}

- (void)setModel:(XCDemandModel *)model{
    _model = model;

    _title.text = _model.title;
    _time.text = [NSString stringWithFormat:@"最后期限:%@",_model.ce_time] ;//[[NSDate dateFromTimeString:_model.add_time] uxy_timeAgo];
    _content.text = _model.remark;
    _address.text = _model.ce_address;
    _price.text = _model.price;
    if(_model.order_count == 0){
        _getOrderNum.hidden = YES;
    } else {
        _getOrderNum.hidden = NO;
        _getOrderNum.text = [NSString stringWithFormat:@"%ld人接单",(long)_model.order_count];
    }
    
    
    switch (_model.demand_status) {
        case -1:{
            _handle_status.textColor = UIColorFromRGB(0x3D4154);
            _handle_status.text = @"交易关闭";
         }
            break;
        case 0:{
            _handle_status.textColor = UIColorFromRGB(0xff0000);
            _handle_status.text = @"对接中";
        }
            break;
        case 1:{
             _handle_status.text = @"对接中";
             _handle_status.textColor = UIColorFromRGB(0xff0000);
        }
            break;
        case 2:
        {
            _handle_status.text = @"对接中";
            _handle_status.textColor = UIColorFromRGB(0xff0000);
        }
            break;
        case 3:
        {
            _handle_status.text = @"对接中";
            _handle_status.textColor = UIColorFromRGB(0xff0000);
        }
            break;
        case 4:
        {
            _handle_status.text = @"对接中";
            _handle_status.textColor = UIColorFromRGB(0xff0000);
        }
            break;
        case 5:{
            _handle_status.text = @"已完成";
            _handle_status.textColor = UIColorFromRGB(0x00ff00);
        }
             break;
        case 6:
        {
            _handle_status.text = @"已完成";
            _handle_status.textColor = UIColorFromRGB(0x00ff00);
        }
            break;
            
        default:
            break;
    }

    
//    if(_model.handle_status == 0){
//        _handle_status.text = @"对接中";
//        _handle_status.backgroundColor = UIColorFromRGB(0xff8422);
//        _handle_status.textColor = UIColorFromRGB(0xe5e5e5);
//
//    } else if(_model.handle_status == 1){
//        _handle_status.text = @"成功对接";
//        _handle_status.textColor = [UIColor redColor];
//        _handle_status.backgroundColor = UIColorFromRGB(0xa5a9b2);
//    }
    
    
    
    [self.renzhengScroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat w = 26;
    CGFloat h = 26;
    NSMutableArray *list = _model.renzheng_list;
    if(list.count == 0){
        _rzH.constant = 0;
    } else {
        _rzH.constant = 26;
    }
    for(int i = 0; i < list.count; i++){
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [iv setImageURLStr:ImgUrl(list[i][@"img_url"]) placeholder:Default_Loading_Image_1];
        [iv whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(list[i]);
            }
        }];
        [self.renzhengScroll addSubview:iv];
    }
    self.renzhengScroll.contentSize = CGSizeMake(list.count * (w +10) +10, 0);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
