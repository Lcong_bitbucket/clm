 //
//  AttachListViewController.m
//  xcwl
//
//  Created by 聪 on 16/8/4.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "AttachListViewController.h"
#import "AttachTableViewCell.h"
#import "myViewController.h"

@interface AttachListViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
}
@end

@implementation AttachListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"附件列表";
    [self customBackButton];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView registerNib:[UINib nibWithNibName:@"AttachTableViewCell" bundle:nil] forCellReuseIdentifier:@"AttachTableViewCell"];

    [self.view addSubview:_tableView];
 
    [self defaultConfig:_tableView];
    // Do any additional setup after loading the view.
}

- (NSMutableArray *)dataArray{
    if(!_dataArray){
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AttachTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AttachTableViewCell" forIndexPath:indexPath];
    cell.dic = _dataArray[indexPath.row];
    [cell setOpenFileBlock:^(NSString *url) {
        MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:url];
        myViewController *vc = [[myViewController alloc] init];
        vc.url = [NSURL fileURLWithPath:info.file];
        [self.navigationController pushViewController:vc animated:YES];
    }];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:UTF8(ImgUrl(_dataArray[indexPath.row][@"file_path"]))];
    if(info.state == MJDownloadStateCompleted){
        myViewController *vc = [[myViewController alloc] init];
        vc.url = [NSURL fileURLWithPath:info.file];
        [self.navigationController pushViewController:vc animated:YES];
    }
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
