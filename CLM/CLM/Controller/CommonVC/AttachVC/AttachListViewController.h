//
//  AttachListViewController.h
//  xcwl
//
//  Created by 聪 on 16/8/4.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface AttachListViewController : XCTableViewController
@property (nonatomic,strong) NSMutableArray *dataArray;
@end
