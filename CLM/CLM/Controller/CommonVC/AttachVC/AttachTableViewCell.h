//
//  AttachTableViewCell.h
//  NewMaterials
//
//  Created by 聪 on 16/7/27.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSProcessView.h"
#import "TTTAttributedLabel.h"

@interface AttachTableViewCell : UITableViewCell{
    
}
@property(strong,nonatomic)CSProcessView *processView;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *file_ext;
@property (weak, nonatomic) IBOutlet UILabel *file_name;
@property (weak, nonatomic) IBOutlet UILabel *file_des;
@property (weak, nonatomic) IBOutlet UIView *downloadBG;
@property (nonatomic,strong) NSDictionary *dic;
@property (nonatomic,copy) void (^openFileBlock)(NSString *url);
@end
