//
//  myViewController.h
//  ads
//
//  Created by Rsaif_Mac_Mini_NO2 on 15/9/21.
//  Copyright (c) 2015年 Rsaif_Mac_Mini_NO2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
@interface myViewController : QLPreviewController<QLPreviewControllerDataSource,QLPreviewControllerDelegate>
@property (nonatomic, copy) NSURL *url;
@end
