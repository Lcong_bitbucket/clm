//
//  AttachTableViewCell.m
//  NewMaterials
//
//  Created by 聪 on 16/7/27.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "AttachTableViewCell.h"

@implementation AttachTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.processView = [[CSProcessView alloc]initWithFrame:CGRectMake(0, 0, self.downloadBG.width, self.downloadBG.height)];
    self.processView.processRadius =4.0f;
    [self.downloadBG addSubview:self.processView];
    WEAKSELF;
    [self.processView setOpenFileBlock:^(NSString *url) {
        if(weakSelf.openFileBlock){
            weakSelf.openFileBlock(url);
        }
    }];
    // Initialization code
}

- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
    self.processView.url = UTF8(_dic[@"file_path"]);
    _file_ext.text = _dic[@"file_ext"];
    _file_ext.textInsets = UIEdgeInsetsMake(2, 3, 2, 3);
    _file_name.text = _dic[@"file_name"];
    _file_des.text = [NSString stringWithFormat:@"大小：%.2fM",[_dic[@"file_size"] integerValue]/1024/1024.00];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
