//
//  CSProcessView.h
//  CSTool_IPad
//
//  Created by 9377 on 15/11/5.
//  Copyright © 2015年 9377. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJDownload.h"

@interface CSProcessView : UIView


@property (nonatomic, strong) UIColor *processBackColor;
@property (nonatomic, strong) UIColor *processBoardColor;
@property (nonatomic, assign) float processBoardWidth;
@property (nonatomic, assign) float processRadius;
@property (nonatomic, assign) UIRectCorner processCorner;
@property (nonatomic,strong) UIColor *titleColor;
@property (nonatomic,strong) NSString *url;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, assign) UIRectCorner fillCorner;
@property (nonatomic, assign) float fillRadius;
@property (nonatomic, assign) NSString * type;
@property (nonatomic,copy) void (^openFileBlock)(NSString *url);

@property (nonatomic, assign) float process;//进度
@end
