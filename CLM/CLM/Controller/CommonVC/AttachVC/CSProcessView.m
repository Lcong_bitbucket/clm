//
//  CSProcessView.m
//  CSTool_IPad
//
//  Created by 9377 on 15/11/5.
//  Copyright © 2015年 9377. All rights reserved.
//
#import "CSProcessView.h"

@implementation CSProcessView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self defaultConfig];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self defaultConfig];
    }
    return self;
}

- (void)awakeFromNib{
    [self defaultConfig];
}

- (void)defaultConfig
{
    
    [self whenTapped:^{
        NSLog(@"ddddd");
        [self onClick];
    }];
    _processBoardWidth = 0.0f;
    _processRadius = CGRectGetHeight(self.frame)/2;
    _processBackColor = getHexColor(@"#FD9111");
    _processBoardColor = [UIColor colorWithRed:196/255.0f green:201/255.0f blue:209/255.0f alpha:1.0f];
    _processCorner = UIRectCornerAllCorners;
    _titleColor = [UIColor whiteColor];
    _fillRadius = _processRadius;
    _fillColor = getHexColor(@"#FD9111");
    _fillCorner = UIRectCornerTopLeft | UIRectCornerBottomLeft;
    
    _process = 0;
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self drawCanvas1WithFrame:rect];
}

- (void)drawCanvas1WithFrame: (CGRect)frame
{
    MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:ImgUrl(self.url)];
    NSLog(@"status = %d",info.state);
    NSString* textContent ;
    _process = 1.0 * info.totalBytesWritten / info.totalBytesExpectedToWrite;
    if (isnan(_process))
    {
        _process = 0;
    }
    if (_process > 1)
    {
        _process = 1;
    }
    else if (_process < 0 )
    {
        _process = 0;
    }
    if (info.state == MJDownloadStateCompleted) {
        textContent = @"打开";
        _process = 1.0;
    }else if (info.state == MJDownloadStateWillResume) {
        textContent = @"等待中";
    }else if (info.state == MJDownloadStateSuspened) {
        textContent = @"暂停";
    } else if (info.state == MJDownloadStateNone ) {
        textContent = @"下载";
        _process = 0;
    } else if (info.state == MJDownloadStateResumed ){
        textContent = [NSString stringWithFormat:@"%0.0f%%",_process *100];
    }
    
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = _processRadius;
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), CGRectGetHeight(frame)) byRoundingCorners: _processCorner cornerRadii: CGSizeMake(_processRadius, _processRadius)];
    
    [_processBoardColor setFill];
    [rectanglePath fill];
    
    //// Rectangle 2 Drawing
    UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRoundedRect: CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), (CGRectGetWidth(frame) ) * _process, CGRectGetHeight(frame)) byRoundingCorners: _processCorner cornerRadii: CGSizeMake(_fillRadius, _fillRadius)];
    [_fillColor setFill];
    [rectangle2Path fill];
    
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    
    //// Rectangle Drawing
    CGRect rectangleRect = frame;
    
    {
        
        
        
        NSMutableParagraphStyle* rectangleStyle = NSMutableParagraphStyle.defaultParagraphStyle.mutableCopy;
        rectangleStyle.alignment = NSTextAlignmentCenter;
        
        NSDictionary* rectangleFontAttributes = @{NSFontAttributeName: [UIFont systemFontOfSize:12], NSForegroundColorAttributeName:_processBoardColor, NSParagraphStyleAttributeName: rectangleStyle};
        NSDictionary* text = @{NSFontAttributeName: [UIFont systemFontOfSize:12], NSForegroundColorAttributeName:_titleColor, NSParagraphStyleAttributeName: rectangleStyle};
        
        CGFloat rectangleTextHeight = [textContent boundingRectWithSize: CGSizeMake(rectangleRect.size.width, INFINITY)  options: NSStringDrawingUsesLineFragmentOrigin attributes: text context: nil].size.height;
        CGContextSaveGState(context);
        CGContextClipToRect(context, rectangleRect);
        [textContent drawInRect: CGRectMake(CGRectGetMinX(rectangleRect), CGRectGetMinY(rectangleRect) + (CGRectGetHeight(rectangleRect) - rectangleTextHeight) / 2, CGRectGetWidth(rectangleRect), rectangleTextHeight) withAttributes: text];
        CGContextRestoreGState(context);
    }
    
}

- (void)setProcessBackColor:(UIColor *)processBackColor{
    _processBackColor = processBackColor;
    [self setNeedsDisplay];
}

- (void)setProcessBoardWidth:(float)processBoardWidth{
    _processBoardWidth = processBoardWidth;
    [self setNeedsDisplay];
}

- (void)setProcessCorner:(UIRectCorner)processCorner{
    _processCorner = processCorner;
    [self setNeedsDisplay];
}

- (void)setProcessRadius:(float)processRadius{
    _processRadius = processRadius;
    _fillRadius = _processRadius - _processBoardWidth/2;
    [self setNeedsDisplay];
}

- (void)setFillColor:(UIColor *)fillColor{
    _fillColor = fillColor;
    [self setNeedsDisplay];
}

- (void)setFillCorner:(UIRectCorner)fillCorner{
    _fillCorner = fillCorner;
    [self setNeedsDisplay];
}

- (void)setFillRadius:(float)fillRadius{
    _fillRadius = fillRadius;
    [self setNeedsDisplay];
}

- (void)setProcess:(float)process
{
    _process = process;
    if (isnan(_process))
    {
        _process = 0;
    }
    if (process > 1)
    {
        _process = 1;
    }
    else if (process < 0 )
    {
        _process = 0;
    }
    [self setNeedsDisplay];
}


- (void)onClick{
     MJDownloadInfo *info = [[MJDownloadManager defaultManager] downloadInfoForURL:ImgUrl(self.url)];
    
    if(info.state == MJDownloadStateSuspened || info.state == MJDownloadStateNone){
        NSLog(@"下载");
        [[MJDownloadManager defaultManager] download:ImgUrl(self.url) progress:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"下载进度 %lf",1.0 * info.totalBytesWritten / info.totalBytesExpectedToWrite);
            });
        } state:^(MJDownloadState state, NSString *file, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"下载进度 %lf",1.0 * info.totalBytesWritten / info.totalBytesExpectedToWrite);
            });
        }];
    }else if (info.state == MJDownloadStateCompleted){
        NSLog(@"打开");
        if(self.openFileBlock){
            self.openFileBlock(ImgUrl(self.url));
        }
    }else if (info.state == MJDownloadStateResumed || info.state == MJDownloadStateWillResume) {
        [[MJDownloadManager defaultManager] suspend:info.url];
        NSLog(@"暂停");
    }
    
}

- (void)setUrl:(NSString *)url{
    _url = url;
    [self setNeedsDisplay];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update:) name:MJDownloadProgressDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update:) name:MJDownloadStateDidChangeNotification object:nil];
    
}

- (void)update:(NSNotification *)n{
    NSLog(@"更新界面");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setNeedsDisplay];
    });
    
}


@end
