//
//  XCMessageDetailViewController.m
//  CLM
//
//  Created by cong on 16/12/22.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMessageDetailViewController.h"
#import "XCMessageModel.h"

@interface XCMessageDetailViewController ()
@property (nonatomic,strong) XCMessageModel *curMessageModel;
@end

@implementation XCMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"消息详情";
    [self request];
    // Do any additional setup after loading the view from its nib.
}

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
     [params setValue:@"GetMyMessage" forKey:@"action"];
    [params setValue:@(self.id) forKey:@"id"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _curMessageModel = [XCMessageModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    _msgTitle.text = _curMessageModel.title;
                    _content.text = _curMessageModel.content;
                    _time.text = _curMessageModel.add_time;
                    if(_curMessageModel.type == 0){
                        _type.text = @"测";
                        _type.backgroundColor = UIColorFromRGB(0x4a9af2);
                    } else if(_curMessageModel.type == 1){
                        _type.text = @"需";
                        _type.backgroundColor = UIColorFromRGB(0xf24b4b);
                    } else if(_curMessageModel.type == 3){
                        _type.text = @"官";
                        _type.backgroundColor = UIColorFromRGB(0x0e56a8);
                    }

                }
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
