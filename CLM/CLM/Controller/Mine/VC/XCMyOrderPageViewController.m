//
//  XCMyOrderPageViewController.m
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMyOrderPageViewController.h"
#import "XCMyOrderViewController.h"
#import "XCMyOrderHelpViewController.h"

@interface XCMyOrderPageViewController ()
{
    UIViewController *_curVC;
    NSMutableArray *_vcArray;
    UISegmentedControl *_segMent;
}

@end

@implementation XCMyOrderPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    [self customBackButton];
    [self customNavigationBarItemWithImageName:nil title:@"帮助" isLeft:NO];
    
    XCMyOrderViewController * vc1=[[XCMyOrderViewController alloc]init];
    [vc1.view setFrame:CGRectMake(0, 0, self.view.width, Screen_Height)];
    vc1.edgesForExtendedLayout=UIRectEdgeNone;
    vc1.extendedLayoutIncludesOpaqueBars=NO;
    vc1.automaticallyAdjustsScrollViewInsets=NO;
     [self addChildViewController:vc1];
    
    XCMyOrderViewController * vc2=[[XCMyOrderViewController alloc]init];
    [vc2.view setFrame:CGRectMake(0, 0, self.view.width, Screen_Height)];
    vc2.type = 1;
    vc2.edgesForExtendedLayout=UIRectEdgeNone;
    vc2.extendedLayoutIncludesOpaqueBars=NO;
    vc2.automaticallyAdjustsScrollViewInsets=NO;
    [self addChildViewController:vc2];
    
    _vcArray = [NSMutableArray new];
    [_vcArray addObjectsFromArray:@[vc1,vc2]];
    _curVC = _vcArray[_curSelected];
    [self.view addSubview:_curVC.view];
    [_curVC didMoveToParentViewController:self];
    
    _segMent = [[UISegmentedControl alloc] initWithItems:@[@"我的测试订单",@"我的测试需求"]];
    _segMent.frame = CGRectMake(0, 0, 150, 30);
    //    _segMent.tintColor = getHexColor(@"#1e59c5");
    _segMent.layer.masksToBounds = YES;
    _segMent.layer.cornerRadius = 15;
    [_segMent setTitleTextAttributes:@{
                                       UITextAttributeTextColor: getHexColor(@"#a2a2a2"),
                                       UITextAttributeFont: [UIFont systemFontOfSize:13],
                                       UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)] }
                            forState:UIControlStateNormal];
    
    [_segMent setTitleTextAttributes:@{
                                       UITextAttributeTextColor: [UIColor whiteColor],
                                       UITextAttributeFont: [UIFont systemFontOfSize:13],
                                       UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)]}
                            forState:UIControlStateSelected];
    [_segMent setBackgroundImage:[UIImage imageWithColor:getHexColor(@"#f5f5f5")] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [_segMent setBackgroundImage:[UIImage imageWithColor:getHexColor(@"#1e59c5")]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    _segMent.apportionsSegmentWidthsByContent = NO;
    
    [_segMent addTarget:self action:@selector(seclectedVC) forControlEvents:UIControlEventValueChanged];
    _segMent.selectedSegmentIndex = _curSelected;
    self.navigationItem.titleView = _segMent;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    XCMyOrderHelpViewController *vc = [[XCMyOrderHelpViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)seclectedVC{
    _curSelected = _segMent.selectedSegmentIndex;
    
    [self replaceController:_curVC newController:_vcArray[_curSelected]];
 }

//  切换各个标签内容
- (void)replaceController:(UIViewController *)oldController newController:(UIViewController *)newController
{
    [self addChildViewController:newController];
    [self transitionFromViewController:oldController toViewController:newController duration:0 options:UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionTransitionNone animations:nil completion:^(BOOL finished) {
        [newController didMoveToParentViewController:self];
        [oldController willMoveToParentViewController:nil];
        [oldController removeFromParentViewController];
        
        [newController.view setFrame:CGRectMake(0, 0, Screen_Width, Screen_Height-64)];
  
        _curVC = newController;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
