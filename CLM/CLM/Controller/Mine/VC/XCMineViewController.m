//
//  XCMineViewController.m
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMineViewController.h"
#import "XCLoginViewController.h"
#import "XCMineListTableViewCell.h"
#import "XCMineListModel.h"
#import "HFStretchableTableHeaderView.h"
#import "XCMineListHeaderView.h"
#import "XCUserModel.h"
#import "XCAboutUsViewController.h"
#import "XCFeedBackViewController.h"
#import "XCInviteFriendsViewController.h"
#import "XCMyInfoViewController.h"
#import "ChatViewController.h"
#import "ConversationListController.h"
#import "XCMyQuestionViewController.h"
#import "XCMyOrderViewController.h"
#import "XCSupplyViewController.h"
#import "XCJiGouViewController.h"
#import "XCMyMessagePageViewController.h"
#import "XCMyOrderPageViewController.h"
#import "XCCardsViewController.h"
#import "XCDevicesViewController.h"

@interface XCMineViewController (){
    XCMineListHeaderView *header;
    NSString *tel;
}
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic, strong) HFStretchableTableHeaderView* stretchableTableHeaderView;

@end

@implementation XCMineViewController

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
    self.dataArray = [NSMutableArray new];
    [self getMindList];
    
    [self defaultConfig:self.tableView];
    header = [[[NSBundle mainBundle] loadNibNamed:@"XCMineListHeaderView" owner:self options:nil] lastObject];
    
    WEAKSELF;
    [header setDidClickBlock:^(NSInteger type) {
        if(type == 0 || type == 1){
            [weakSelf goMineInfo:type];
        } else if(type == 2){
            XCMyMessagePageViewController *vc = [[XCMyMessagePageViewController alloc] init];
             vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
         }
    }];
    // Do any additional setup after loading the view from its nib.
    _stretchableTableHeaderView = [HFStretchableTableHeaderView new];
    [_stretchableTableHeaderView stretchHeaderForTableView:self.tableView withView:header];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    [self SetUserInfo];
    
    XCUserManager *manager =[XCUserManager sharedInstance];
    if(manager.isLogin){
        [self requestUserInfo];
    } else {
        [self refreshUI];
    }
    if(!tel){
        [self requestTel];
    }
}

#pragma mark - 请求数据
- (void)requestUserInfo{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetUserInfo" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                XCUserManager *manager =[XCUserManager sharedInstance];
                manager.userModel = [XCUserModel mj_objectWithKeyValues:jsonResponse];
                [manager save];
                
                [self refreshUI];
                
            } else {
                showHint(jsonResponse[@"msg"]);
                XCUserManager *manager = [XCUserManager sharedInstance];
                [manager clear];
                [self refreshUI];
                
                [manager presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                    if(isSuccess){
                        [self requestUserInfo];
                    }
                }];
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

#pragma mark - 请求数据
- (void)requestTel{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetKF" forKey:@"action"];
    [params setValue:@"0" forKey:@"type"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                tel = jsonResponse[@"tel"];
                
                [self refreshUI];
                
            } else {
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)refreshUI{
    XCUserManager *manager =[XCUserManager sharedInstance];
    header.model = manager.userModel;
    for(int i = 0; i < _dataArray.count ;i ++){
        NSArray *tempArray = _dataArray[i];
        for(XCMineListModel *model in tempArray){
            if([model.title isEqualToString:@"退出"]){
                if([[XCUserManager sharedInstance] isLogin]){
                    model.isHidden = 0;
                } else {
                    model.isHidden = 1;
                }
            }
            else if([model.title isEqualToString:@"联系客服"]){
                model.content = tel;
            }else if([model.title isEqualToString:@"我的问答"]){
                NSInteger ct = (manager.userModel.new_myanswer + manager.userModel.new_myaccepted + manager.userModel.new_myfabu);
                if(ct > 0){
                    model.content = [NSString stringWithFormat:@"%ld",ct];
                } else {
                    model.content = @"";
                }
            }
        }
    }
    [self.tableView reloadData];
}

#pragma mark - HFStretchableTableHeaderView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_stretchableTableHeaderView scrollViewDidScroll:scrollView];
}

- (void)viewDidLayoutSubviews
{
    [_stretchableTableHeaderView resizeView];
}

#pragma mark - 获取数据
//获取当前界面数据
- (void)getMindList{
    NSData *data = [NSData dataWithContentsOfFile:[NSString mainBundlePath:@"MineList.json"]];
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
    for(int i = 0; i < jsonArray.count ;i ++){
        [self.dataArray addObject:[XCMineListModel mj_objectArrayWithKeyValuesArray:jsonArray[i]]];
    }
    
}

- (void)rightBarButtonAction:(UIButton *)btn{
    XCLoginViewController *vc = [[XCLoginViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_dataArray[section] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    XCMineListModel *model = _dataArray[indexPath.section][indexPath.row];
    if(model.isHidden == 0){
        return 45;
    }
    return 0;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"XCMineListTableViewCell";
    XCMineListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    if([cell.model.title isEqualToString:@"联系客服"]){
        cell.content.backgroundColor = UIColorFromRGB(0xf2f2f2);
//        cell.content.textColor = [UIColor whiteColor];
    }else if([cell.model.title isEqualToString:@"我的问答"]){
        cell.content.backgroundColor = UIColorFromRGB(0xff0000);
        cell.content.textColor = [UIColor whiteColor];
    }
    cell.model = _dataArray[indexPath.section][indexPath.row];

    return cell;
}

#pragma mark UITableViewDelegate 列表点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XCMineListModel *model = _dataArray[indexPath.section][indexPath.row];
    if([model.title isEqualToString:@"我发布过的机构"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCJiGouViewController *vc = [[XCJiGouViewController alloc] init];
            vc.vcType = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
       
    }else if([model.title isEqualToString:@"我发布过的项目"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
            vc.vcType = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
      
    }else if([model.title isEqualToString:@"我发布过的需求"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCDemandViewController *vc = [[XCDemandViewController alloc] init];
            vc.vcType = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];

        }];
    }else if([model.title isEqualToString:@"我的订单"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCMyOrderPageViewController *vc = [[XCMyOrderPageViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
       
    }else if([model.title isEqualToString:@"我申请过的专家"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
            vc.vcType = 1;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
       
    }else if([model.title isEqualToString:@"我的问答"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCMyQuestionViewController *vc = [[XCMyQuestionViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        
    }else if([model.title isEqualToString:@"我发布的帖子"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCCardsViewController *vc = [[XCCardsViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            vc.vcType = 1;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        
    } else if([model.title isEqualToString:@"我发布的求购/出售信息"]){
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            XCDevicesViewController *vc = [[XCDevicesViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            vc.vcType = 1;
            [vc customBackButton];
            [self.navigationController pushViewController:vc animated:YES];
        }];
  
    } else if([model.title isEqualToString:@"关于我们"]){
        XCAboutUsViewController *vc = [[XCAboutUsViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    } else if([model.title isEqualToString:@"我要反馈"]){
         XCFeedBackViewController *vc = [[XCFeedBackViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
     }else if([model.title isEqualToString:@"邀请好友"]){
        XCInviteFriendsViewController *vc = [[XCInviteFriendsViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    } else if([model.title isEqualToString:@"联系客服"]){
        if(model.content){
            openTel(model.content);
        }
    }  else if([model.title isEqualToString:@"退出"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"是否退出当前用户？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
                XCUserManager *manager = [XCUserManager sharedInstance];
                [manager exitSuccess:^{
                    showHint(@"退出登录成功");
                    [self refreshUI];
                 } failure:^(NSError *error) {
                     showHint(@"退出登录失败");
                }];
            }
        }];
        [alert show];
    }
}

#pragma mark - 重写----设置标题和标注的高度
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 1){
        return 12;
    }
    return 0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    return v;
}

- (void)goMineInfo:(NSInteger)type{
    if(type == 0){//需要回调进入个人信息
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                XCMyInfoViewController *vc = [[XCMyInfoViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }}];
    } else {
        if([[XCUserManager sharedInstance] isLogin]){
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                if(isSuccess){
                    XCMyInfoViewController *vc = [[XCMyInfoViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                }}];
        } else {
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                if(isSuccess){
                    
                }
            }];
        }
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
