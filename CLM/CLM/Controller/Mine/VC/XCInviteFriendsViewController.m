//
//  XCInviteFriendsViewController.m
//  CLM
//
//  Created by cong on 17/1/6.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCInviteFriendsViewController.h"

@interface XCInviteFriendsViewController ()

@end

@implementation XCInviteFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"邀请好友";
    [self customBackButton];
    [self customShareButton];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",AFSERVER_DATA,@"/my/apage_app.aspx?id=2"]]];
    [self.webView loadRequest:req];
    // Do any additional setup after loading the view from its nib.
}
- (void)rightBarButtonAction:(UIButton *)btn{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    [params setValue:@(8) forKey:@"type"];
    [params setValue:@(2) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
