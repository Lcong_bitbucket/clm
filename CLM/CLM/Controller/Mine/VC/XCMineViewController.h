//
//  XCMineViewController.h
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCMineViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
