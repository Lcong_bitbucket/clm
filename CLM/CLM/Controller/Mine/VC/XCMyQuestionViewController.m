//
//  XCMyQuestionViewController.m
//  CLM
//
//  Created by cong on 17/1/20.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMyQuestionViewController.h"
#import "NYSegmentedControl.h"
#import "XCQuestionsViewController.h"
#import "XCSegMentView.h"

@interface XCMyQuestionViewController ()
{
    UIViewController *_curVC;
    NSMutableArray *_vcArray;
    XCSegMentView *segment;
}
@end

@implementation XCMyQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    [self customNavigationBarItemWithImageName:nil title:@"  " isLeft:NO ];
    _vcArray = [NSMutableArray new];
   
    XCUserModel *userModel = [XCUserManager sharedInstance].userModel;
    segment = [[XCSegMentView alloc] initWithTitles:@[@"我发布的",@"我回答的",@"被采纳的"] badges:@[@(userModel.new_myfabu),@(userModel.new_myanswer),@(userModel.new_myaccepted)]];
    self.navigationItem.titleView = segment;
    WEAKSELF;
    [segment setDidClickBlock:^(NSInteger index) {
        [weakSelf replaceController:_curVC newController:_vcArray[index]];

    }];
    

    for(int i = 0; i < 3; i ++){
        XCQuestionsViewController *vc = [[XCQuestionsViewController alloc] init];
        vc.vcType = i+1;
        vc.view.frame = CGRectMake(0, 0, Screen_Width, Screen_Height - 64);
         vc.hidesBottomBarWhenPushed = YES;
        vc.edgesForExtendedLayout=UIRectEdgeNone;
        vc.extendedLayoutIncludesOpaqueBars=NO;
        vc.automaticallyAdjustsScrollViewInsets=NO;
        [self addChildViewController:vc];
        [_vcArray addObject:vc];
    }
    
    _curVC = _vcArray[0];
    self.view.frame = CGRectMake(0, 0, Screen_Width, Screen_Height - 64);
    [self.view addSubview:_curVC.view];
    [_curVC didMoveToParentViewController:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMsg) name:ReloadUserModelNotification object:nil];
    [self reloadMsg];

    // Do any additional setup after loading the view from its nib.
}

- (void)reloadMsg{
    XCUserModel *userModel = [XCUserManager sharedInstance].userModel;
    segment.badges = @[@(userModel.new_myfabu),@(userModel.new_myanswer),@(userModel.new_myaccepted)];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[XCUserManager sharedInstance] reloadInfoSuccess:^{
        
    } failure:^(NSError *error) {
        
    }];

}

- (void)switcher:(NYSegmentedControl *)seg{
    [self replaceController:_curVC newController:_vcArray[seg.selectedSegmentIndex]];
}

//  切换各个标签内容
- (void)replaceController:(UIViewController *)oldController newController:(UIViewController *)newController
{
    [self addChildViewController:newController];
    [self transitionFromViewController:oldController toViewController:newController duration:0 options:UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionTransitionNone animations:nil completion:^(BOOL finished) {
        [newController didMoveToParentViewController:self];
        [oldController willMoveToParentViewController:nil];
        [oldController removeFromParentViewController];
        _curVC = newController;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
