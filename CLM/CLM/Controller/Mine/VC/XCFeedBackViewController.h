//
//  XCFeedBackViewController.h
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"

@interface XCFeedBackViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *content;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
 @property (weak, nonatomic) IBOutlet UITextField *PhoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
- (IBAction)submit:(id)sender;

@end
