//
//  XCFeedBackViewController.m
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCFeedBackViewController.h"

@interface XCFeedBackViewController ()

@end

@implementation XCFeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"意见反馈";
    _content.placeholder = @"意见描述";
    XCUserModel *userModel = [[XCUserManager sharedInstance] userModel];
    _nameTF.text = userModel.nick_name;
    _PhoneTF.text = userModel.mobile;
    _emailTF.text = userModel.email;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(id)sender {
    if(_content.text.length ==0 ){
        showHint(@"请填写意见描述");
        [XCTools shakeAnimationForView:_content];
        return;
    }else if(_nameTF.text.length ==0 ){
        showHint(@"请填写联系人姓名");
        [XCTools shakeAnimationForView:_nameTF];
        return;
    }else if(_PhoneTF.text.length ==0 ){
        showHint(@"请填写联系方式");
        [XCTools shakeAnimationForView:_PhoneTF];
        return;
    }else {
        [self commitFeedBack];
    }
}

- (void)commitFeedBack{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_content.text forKey:@"content"];
    [params setValue:_nameTF.text forKey:@"contact"];
    [params setValue:_PhoneTF.text forKey:@"mobile"];
    [params setValue:_emailTF.text forKey:@"email"];
    [params setValue:@"feedback" forKey:@"action"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
@end
