//
//  XCMyInfoViewController.m
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMyInfoViewController.h"

@interface XCMyInfoViewController ()<TZImagePickerControllerDelegate>
{
    NSString *_avatarStr;
}
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableArray *photos;

@end

@implementation XCMyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    _assets = [NSMutableArray new];
    _photos = [NSMutableArray new];

    self.title = @"个人信息";
    WEAKSELF;
    [_avatarBG whenTouchedUp:^{
        [weakSelf pushImagePickerController];
    }];
   
    [self refreshUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    __block typeof(UIImageView *) a = _avatar;
    [_avatar whenTouchedUp:^{
        ZLPhotoPickerBrowserViewController *browserVc = [[ZLPhotoPickerBrowserViewController alloc] init];
        [browserVc showHeadPortrait:a];
    }];
}

- (void)pushImagePickerController {
   
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    
    
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.isSelectOriginalPhoto = YES;
    
//    imagePickerVc.selectedAssets = self.assets; // 目前已经选中的图片数组
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
    // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
    // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];
    
    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingGif = NO;
    
    // 4. 照片排列按修改时间升序
    imagePickerVc.sortAscendingByModificationDate = YES;
    
    // imagePickerVc.minImagesCount = 3;
    // imagePickerVc.alwaysEnableDoneBtn = YES;
    
    // imagePickerVc.minPhotoWidthSelectable = 3000;
    // imagePickerVc.minPhotoHeightSelectable = 2000;
    
    /// 5. Single selection mode, valid when maxImagesCount = 1
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.needCircleCrop = NO;
    imagePickerVc.circleCropRadius = Screen_Width/2;
    /*
     [imagePickerVc setCropViewSettingBlock:^(UIView *cropView) {
     cropView.layer.borderColor = [UIColor redColor].CGColor;
     cropView.layer.borderWidth = 2.0;
     }];*/
    
    //imagePickerVc.allowPreview = NO;
#pragma mark - 到这里为止
    
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}
#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    self.assets = assets.mutableCopy;
    self.photos = photos.mutableCopy;
    if(self.assets.count > 0){
        
        for(int i = 0; i < self.assets.count ; i++){
            XCBaseCommand *command = [XCBaseCommand new];
            command.curView = self.view;
          
            command.imgs = @[self.photos[i]];
            [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
                if(error){
                    
                } else {
                    if([[jsonResponse objectForKey:@"status"] intValue]==1)
                    {
                        _avatarStr = [jsonResponse objectForKey:@"path"];
                        showHint(jsonResponse[@"desc"]);
                        [self reloadScrollView];
                    }  else {
                        showHint(jsonResponse[@"desc"]);
                    }
                }
            }];
            [[XCHttpClient sharedInstance] upload:command];
        }
    } else {
        
    }
    
}


//#pragma mark - 选择图片
//- (void)photoSelectet{
//    ZLPhotoPickerViewController *pickerVc = [[ZLPhotoPickerViewController alloc] init];
//    // MaxCount, Default = 9
//    pickerVc.maxCount = 1;
//    // Jump AssetsVc
//    pickerVc.status = PickerViewShowStatusCameraRoll;
//    // Filter: PickerPhotoStatusAllVideoAndPhotos, PickerPhotoStatusVideos, PickerPhotoStatusPhotos.
//    pickerVc.photoStatus = PickerPhotoStatusPhotos;
//    // Recoder Select Assets
//    pickerVc.selectPickers = self.assets;
//    // Desc Show Photos, And Suppor Camera
//    pickerVc.topShowPhotoPicker = YES;
//    pickerVc.isShowCamera = YES;
//    // CallBack
//    WEAKSELF;
//    pickerVc.callBack = ^(NSArray<ZLPhotoAssets *> *status){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            weakSelf.assets = status.mutableCopy;
//            if(weakSelf.assets.count > 0){
//               
//                for(int i = 0; i < self.assets.count ; i++){
//                    
//                    XCBaseCommand *command = [XCBaseCommand new];
//                    command.curView = self.view;
//                    command.imgs = [@[[self.assets[i] thumbImage]] mutableCopy];
//                    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
//                        if(error){
//                            
//                        } else {
//                            if([[jsonResponse objectForKey:@"status"] intValue]==1)
//                            {
//                                _avatarStr = [jsonResponse objectForKey:@"path"];
//                                showHint(jsonResponse[@"desc"]);
//                                 [weakSelf reloadScrollView];
//                            }  else {
//                                showHint(jsonResponse[@"desc"]);
//                            }
//                        }
//                    }];
//                    [[XCHttpClient sharedInstance] upload:command];
//                }
//            } else {
//
//            }
//            
//        });
//    };
//    [pickerVc showPickerVc:self];
//}

- (void)reloadScrollView{
    XCLog(@"%@",ImgUrl(_avatarStr));
    [_avatar setImageURLStr:ImgUrl(_avatarStr) placeholder:Default_Avatar_Image_1];
 }

- (void)refreshUI{
    XCUserModel *userModel = [[XCUserManager sharedInstance] userModel];
    [_avatar setImageURLStr:ImgUrl(userModel.avatar) placeholder:Default_Avatar_Image_1];
    _avatarStr = userModel.avatar;
    _nameTF.text = userModel.nick_name;
    _zhiweiTF.text = userModel.zhiwei;
    _companyTF.text = userModel.company_name;
    _phoneTF.text = userModel.mobile;
    _emailTF.text = userModel.email;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)save:(id)sender {
    [self modifyInfo];
}

- (void)modifyInfo{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_avatarStr forKey:@"avatar"];
    [params setValue:_nameTF.text forKey:@"nick_name"];
    [params setValue:_companyTF.text forKey:@"company"];
    [params setValue:_zhiweiTF.text forKey:@"zhiwei"];
    [params setValue:_emailTF.text forKey:@"email"];
    [params setValue:@"user_info_edit" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                 showHint(jsonResponse[@"msg"]);
                if(self.isRegist){
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    [self goBack];
                }
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
@end
