//
//  XCMessageDetailViewController.h
//  CLM
//
//  Created by cong on 16/12/22.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"

@interface XCMessageDetailViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *msgTitle;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (nonatomic,assign) NSInteger id;
@end
