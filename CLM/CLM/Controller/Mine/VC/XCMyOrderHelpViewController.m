//
//  XCMyOrderHelpViewController.m
//  CLM
//
//  Created by cong on 17/3/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMyOrderHelpViewController.h"

@interface XCMyOrderHelpViewController ()

@end

@implementation XCMyOrderHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"订单流程";
    [self customBackButton];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",AFSERVER_DATA,@"/xuqiu_help.aspx"]]];
    [self.webView loadRequest:req];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
