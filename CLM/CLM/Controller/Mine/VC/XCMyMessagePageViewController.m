//
//  XCMyMessagePageViewController.m
//  CLM
//
//  Created by cong on 17/3/7.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMyMessagePageViewController.h"
#import "ConversationListController.h"
#import "XCMessageViewController.h"
#import "WZLBadgeImport.h"

@interface XCMyMessagePageViewController ()
{
    UIViewController *_curVC;
    NSMutableArray *_vcArray;
    
    UIView *_bgView;
    UISegmentedControl *_segMent;
    
    UIView *_badge1;
    UIView *_badge2;

}
@property (nonatomic,assign) NSInteger curSelected;

@end

@implementation XCMyMessagePageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的消息";
    [self customBackButton];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    ConversationListController * vc1=[[ConversationListController alloc]init];
    [vc1.view setFrame:CGRectMake(0, 0, self.view.width, Screen_Height)];
    vc1.edgesForExtendedLayout=UIRectEdgeNone;
    vc1.extendedLayoutIncludesOpaqueBars=NO;
    vc1.automaticallyAdjustsScrollViewInsets=NO;
    [ChatDemoHelper shareHelper].conversationListVC = vc1;
    [self addChildViewController:vc1];
    
    XCMessageViewController * vc2=[[XCMessageViewController alloc]init];
    [vc2.view setFrame:CGRectMake(0, 0, self.view.width, Screen_Height)];
    vc2.edgesForExtendedLayout=UIRectEdgeNone;
    vc2.extendedLayoutIncludesOpaqueBars=NO;
    vc2.automaticallyAdjustsScrollViewInsets=NO;
    [self addChildViewController:vc2];
    
    _vcArray = [NSMutableArray new];
    [_vcArray addObjectsFromArray:@[vc1,vc2]];
    _curVC = _vcArray[_curSelected];
    [self.view addSubview:_curVC.view];
    [_curVC didMoveToParentViewController:self];
    
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    
    _segMent = [[UISegmentedControl alloc] initWithItems:@[@"聊天消息",@"系统消息"]];
    _segMent.frame = CGRectMake(0, 0, 150, 30);
//    _segMent.tintColor = getHexColor(@"#1e59c5");
    _segMent.layer.masksToBounds = YES;
    _segMent.layer.cornerRadius = 15;
    [_segMent setTitleTextAttributes:@{
                                       UITextAttributeTextColor: getHexColor(@"#a2a2a2"),
                                       UITextAttributeFont: [UIFont systemFontOfSize:13],
                                       UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)] }
                            forState:UIControlStateNormal];
    
    [_segMent setTitleTextAttributes:@{
                                       UITextAttributeTextColor: [UIColor whiteColor],
                                       UITextAttributeFont: [UIFont systemFontOfSize:13],
                                       UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)]}
                            forState:UIControlStateSelected];
    [_segMent setBackgroundImage:[UIImage imageWithColor:getHexColor(@"#f5f5f5")] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    
    [_segMent setBackgroundImage:[UIImage imageWithColor:getHexColor(@"#1e59c5")]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    _segMent.apportionsSegmentWidthsByContent = NO;
    
    
    [_segMent addTarget:self action:@selector(seclectedVC) forControlEvents:UIControlEventValueChanged];
    _segMent.selectedSegmentIndex = _curSelected;
    [_bgView addSubview:_segMent];
    
    _badge1 = [[UIView alloc] initWithFrame:CGRectMake(50, 2, 15, 15)];
    _badge1.badgeCenterOffset = CGPointMake(0, 0);
     [_bgView addSubview:_badge1];
    
    _badge2 = [[UIView alloc] initWithFrame:CGRectMake(125, 2, 15, 15)];
    _badge2.badgeCenterOffset = CGPointMake(0, 0);
     [_bgView addSubview:_badge2];
    
    self.navigationItem.titleView = _bgView;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:ReloadUserModelNotification object:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupUnreadMessageCount];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    if (unreadCount > 0) {
        [_badge1 showBadgeWithStyle:WBadgeStyleNumber value:unreadCount animationType:WBadgeAnimTypeNone];
    }else{
        [_badge1 clearBadge];
        
    }
    
    
    XCUserManager *um = [XCUserManager sharedInstance];
    XCUserModel *user = um.userModel;
    
    if(user && user.new_message > 0){
         [_badge2 showBadgeWithStyle:WBadgeStyleNumber value:user.new_message animationType:WBadgeAnimTypeNone];
    } else {
        [_badge2 clearBadge];

    }
}

- (void)seclectedVC{
    _curSelected = _segMent.selectedSegmentIndex;
  
    [self replaceController:_curVC newController:_vcArray[_curSelected]];
    
}

//  切换各个标签内容
- (void)replaceController:(UIViewController *)oldController newController:(UIViewController *)newController
{
    [self addChildViewController:newController];
    [self transitionFromViewController:oldController toViewController:newController duration:0 options:UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionTransitionNone animations:nil completion:^(BOOL finished) {
         [newController didMoveToParentViewController:self];
        [oldController willMoveToParentViewController:nil];
        [oldController removeFromParentViewController];
        [newController.view setFrame:CGRectMake(0, 0,Screen_Width, Screen_Height-64)];

        _curVC = newController;
     }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
