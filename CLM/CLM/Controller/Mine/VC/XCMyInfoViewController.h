//
//  XCMyInfoViewController.h
//  CLM
//
//  Created by cong on 16/12/15.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCMyInfoViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIView *avatarBG;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *zhiweiTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (nonatomic,assign) BOOL isRegist;
- (IBAction)save:(id)sender;

@end
