//
//  XCMyOrderViewController.m
//  CLM
//
//  Created by cong on 17/3/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMyOrderViewController.h"
#import "XCAcceptOrderTableViewCell.h"
#import "XCPublishOrderTableViewCell.h"
#import "XCOrderModel.h"
#import "XCOrderUserModel.h"
#import "XCDemandsDetailViewController.h"
#import "XCSupplysDetailViewController.h"

@interface XCMyOrderViewController ()

@property (nonatomic,strong) NSMutableArray *dataArray;
@end

@implementation XCMyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.dataArray = [NSMutableArray new];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    [self beginRefreshing];
    // Do any additional setup after loading the view from its nib.
}

- (void)refresh{
    [self request:self.curPage];
}

- (void)request:(NSInteger)pageIndex{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
 
    [params setValue:@(pageIndex) forKey:@"pageindex"];
    [params setValue:@(15) forKey:@"pagesize"];
    if(self.type == 0) {
        [params setValue:@"GetMyOrder" forKey:@"action"];
    } else if(self.type == 1) {
        [params setValue:@"GetMyDemand" forKey:@"action"];
    }
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];

        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(pageIndex == 1){
                    [_dataArray removeAllObjects];
                }
                
                [_dataArray addObjectsFromArray:[XCOrderModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                
                if(pageIndex * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
                [self stopRefresh:NO];

            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.type == 0){
        NSString *identifier = @"XCAcceptOrderTableViewCell";
        XCAcceptOrderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _dataArray[indexPath.row];
        WEAKSELF;
        [cell setDidClickBlock:^(NSInteger type, XCOrderModel *model) {
            if(type == 0){
                [weakSelf goChatVC:[NSString stringWithFormat:@"%ld",model.user_id]];
            } else if(type == 1){
                [weakSelf cancelOrder:model.order_id];
            }
        }];
         return cell;
    } else if(self.type == 1){
        NSString *identifier = @"XCPublishOrderTableViewCell";
        XCPublishOrderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _dataArray[indexPath.row];
        WEAKSELF;
        [cell setDidClickBlock:^(NSInteger type, XCOrderModel *order, XCOrderUserModel *user) {
            if(type == 0){
                [weakSelf confirmComplete:user];

            } else if(type == 1){
                [weakSelf goChatVC:[NSString stringWithFormat:@"%ld",user.user_id]];

            } else if(type == 2){
                [weakSelf cancelOrder:user.id];
            }
        }];
        return cell;
    }
    return nil;
}

- (void)goChatVC:(NSString *)username{
    WEAKSELF;
    [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
        ChatViewController* vc = [[ChatViewController alloc] initWithConversationChatter:username conversationType:EMConversationTypeChat];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    }];
}

- (void)confirmComplete:(XCOrderUserModel *)user{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"ConfirmComplete" forKey:@"action"];
    [params setValue:@(user.id) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1) {
                 self.curPage = 1;
                [self refresh];
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)cancelOrder:(NSInteger )orderId{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"CancelOrder" forKey:@"action"];
    [params setValue:@(orderId) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1) {
                self.curPage = 1;
                [self refresh];
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

#pragma mark UITableViewDelegate 列表点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(self.type == 0){
        XCSupplysDetailViewController *vc = [[XCSupplysDetailViewController alloc] init];
        vc.id = [_dataArray[indexPath.row] id];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    } else if(self.type == 1){
        XCDemandsDetailViewController *vc = [[XCDemandsDetailViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        vc.id = [_dataArray[indexPath.row] id];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
