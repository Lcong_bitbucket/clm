//
//  XCMessageViewController.m
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMessageViewController.h"
#import "XCMyMessageTableViewCell.h"
#import "XCMessageDetailViewController.h"

@interface XCMessageViewController ()
@property (nonatomic,strong) NSMutableArray *dataArray;
@end

@implementation XCMessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    self.title = @"我的消息";
    _dataArray = [NSMutableArray new];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    [self beginRefreshing];
     // Do any additional setup after loading the view from its nib.
}

- (void)refresh{
    [self request:self.curPage];
}

- (void)request:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:@"GetMyMessage" forKey:@"action"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1){
                    [_dataArray removeAllObjects];
                }
                [_dataArray addObjectsFromArray:[XCMessageModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                [self stopRefresh:NO];
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"XCMyMessageTableViewCell";
    XCMyMessageTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    cell.model = _dataArray[indexPath.row];
    return cell;
}

#pragma mark UITableViewDelegate 列表点击
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self beginRefreshing];
    XCUserModel *model = _dataArray[indexPath.row];
    XCMessageDetailViewController *vc = [[XCMessageDetailViewController alloc] init];
    vc.id = [model id];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
