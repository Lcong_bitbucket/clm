//
//  XCSegMentView.h
//  CLM
//
//  Created by cong on 17/1/20.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCSegMentView : UIView{
    UIButton *_curBtn;
}
@property (nonatomic,strong) NSArray *btns;
@property (nonatomic,strong) NSArray *titles;
@property (nonatomic,strong) NSArray *badges;
@property (nonatomic,assign) NSInteger selectedIndex;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger selectedIndex);
- (id)initWithTitles:(NSArray *)titles badges:(NSArray *)badges;
@end
