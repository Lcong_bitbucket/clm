//
//  XCPublishOrderTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCOrderModel.h"
#import "XCOrderAcceptUserTableViewCell.h"

@interface XCPublishOrderTableViewCell : UITableViewCell{
 }
@property (weak, nonatomic) IBOutlet UIView *nightModeView;
- (IBAction)cancelClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *title;
 
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewH;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger type,XCOrderModel *order,XCOrderUserModel *user);

@property (nonatomic,strong) XCOrderModel *model;
@end
