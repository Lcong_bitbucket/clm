//
//  XCPublishOrderTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCPublishOrderTableViewCell.h"
#import "XCOrderuserSectionView.h"

@implementation XCPublishOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _tableView.estimatedRowHeight = 44.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
 
    // Initialization code
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _model.jigou_list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"XCOrderAcceptUserTableViewCell";
    XCOrderAcceptUserTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
     cell.model = _model.jigou_list[indexPath.row];
    [cell setDidClickBlock:^(NSInteger type,XCOrderUserModel *user) {
        if(self.didClickBlock){
            self.didClickBlock(type,_model,user);
        }
    }];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
 
}

#pragma mark - 重写----设置标题和标注的高度
-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    if(_model.jigou_list.count > 0){
        return 30;
    }
    return 0;
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section {
    XCOrderuserSectionView *v = [[[NSBundle mainBundle] loadNibNamed:@"XCOrderuserSectionView" owner:self options:nil] lastObject];
    return v;
}
 

- (void)setModel:(XCOrderModel *)model{
    _model = model;
    
    NSInteger order_status;
    if(_model.type == 0){
        if(_model.jigou_list.count > 0)
            order_status = [_model.jigou_list[0] order_status];
    } else {
        order_status = _model.demand_status;
    }
    
    _nightModeView.hidden = YES;

    
    switch (order_status) {
        case -1:{
            _orderStatus.textColor = UIColorFromRGB(0x999999);
            _orderStatus.text = @"交易关闭";
            
            _nightModeView.hidden = NO;
            if(_model.type == 0){
                _cancelBtn.hidden = YES;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
        }
            break;
        case 0:{
            if(_model.type == 0){
                _cancelBtn.hidden = NO;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0xff0000);
             _orderStatus.text = @"对接中";
        }
            break;
        case 1:{
            if(_model.type == 0){
                _cancelBtn.hidden = NO;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
            _orderStatus.text = @"选择机构并付款";
        }
            break;
        case 2:{
            if(_model.type == 0){
                _cancelBtn.hidden = YES;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
             _orderStatus.text = @"机构确认接单";
        }
            break;
        case 3:{
            if(_model.type == 0){
                _cancelBtn.hidden = YES;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
             _orderStatus.text = @"测试中";
        }
            break;
        case 4:{
            if(_model.type == 0){
                _cancelBtn.hidden = YES;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
             _orderStatus.text = @"上传结果";
        }
            break;
        case 5:{
            if(_model.type == 0){
                _cancelBtn.hidden = YES;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0x00ff00);
             _orderStatus.text = @"已完成";
        }
            break;
        case 6:{
            if(_model.type == 0){
                _cancelBtn.hidden = YES;
            } else if(_model.type == 1){
                _cancelBtn.hidden = YES;
            }
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
             _orderStatus.text = @"评价";
        }
            break;
            
        default:
            break;
    }
    
    _price.text = _model.price;
    _title.text = _model.title;
     
    [_tableView reloadData];
    _tableViewH.constant = _model.jigou_list.count * 99 + (_model.jigou_list.count > 0 ? 30 :0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)cancelClick:(id)sender {
  
    if(_model.jigou_list.count > 0){
        if(self.didClickBlock){
            self.didClickBlock(2,_model,[_model.jigou_list lastObject]);
        }
    }
   
}
@end
