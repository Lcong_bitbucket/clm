//
//  XCMyMessageTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCMessageModel.h"

@interface XCMyMessageTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *type;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIView *isRead;

@property (nonatomic,strong) XCMessageModel *model;
@end
