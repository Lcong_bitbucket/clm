//
//  XCOrderAcceptUserTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/9.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCOrderAcceptUserTableViewCell.h"
#import "XCMyOrderViewController.h"

@implementation XCOrderAcceptUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setModel:(XCOrderUserModel *)model{
    _model = model;
    [_avatar setImageURLStr:ImgUrl(_model.avatar) placeholder:Default_Avatar_Image_1];
    _nickName.text = _model.nick_name;
    _company.text = _model.company_name;
    _price.text = _model.pre_price;
    _nightModeView.hidden = YES;

    switch (_model.order_status) {
        case -1: {//交易关闭
          
            _nightModeView.hidden = NO;
            
            _btn1.hidden = YES;
            _btn1Width.constant = 0;
         }
            break;
        case 0: {//未对接
           
            _btn1.hidden = NO;
            _btn1Width.constant = 100;
        }
            break;
        case 1: {//选择机构并付款
           
            _btn1.hidden = NO;
            _btn1Width.constant = 100;

         }
            break;
        case 2: {//机构确认接单
            
            _btn1.hidden = YES;
            _btn1Width.constant = 0;

         }
            break;
        case 3: {//测试中
           
            _btn1.hidden = YES;
            _btn1Width.constant = 0;

         }
            break;
        case 4: {//上传结果
           
            _btn1.hidden = YES;
            _btn1Width.constant = 0;

         }
            break;
        case 5: {//需求方确认测试完成
            
            _btn1.hidden = YES;
            _btn1Width.constant = 0;

         }
            break;
        case 6: {//评价
           
            _btn1.hidden = YES;
            _btn1Width.constant = 0;

         }
            break;
        default:
            break;
    }
    
}

- (IBAction)click1:(id)sender {
    if(self.didClickBlock){
        self.didClickBlock(0,_model);
    }
}

- (IBAction)click2:(id)sender {
    if(self.didClickBlock){
        self.didClickBlock(1,_model);
    }
}
 
- (IBAction)cancelClick:(id)sender {
   
}
@end
