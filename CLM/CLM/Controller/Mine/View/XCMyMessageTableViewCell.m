//
//  XCMyMessageTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMyMessageTableViewCell.h"

@implementation XCMyMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCMessageModel *)model{
    _model = model;
    _title.text = _model.title;
    _content.text = _model.content;
    _time.text = _model.add_time;
    _isRead.hidden = _model.is_read;
    if(_model.type == 0){
        _type.text = @"测";
        _type.backgroundColor = UIColorFromRGB(0x4a9af2);
    } else if(_model.type == 1){
        _type.text = @"需";
        _type.backgroundColor = UIColorFromRGB(0xf24b4b);
    } else if(_model.type == 3){
        _type.text = @"官";
        _type.backgroundColor = UIColorFromRGB(0x0e56a8);
    }
}

@end
