//
//  XCAcceptOrderTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCAcceptOrderTableViewCell.h"
#import "ChatViewController.h"

@implementation XCAcceptOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCOrderModel *)model{
    _model = model;
    _nightModeView.hidden = YES;

    switch (_model.order_status) {
        case -1:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x999999);
            _orderStatus.text = @"交易关闭";
            _nightModeView.hidden = NO;
          }
            break;
        case 0:{
            _cancelBtn.hidden = NO;
            _orderStatus.textColor = UIColorFromRGB(0xff0000);
            _orderStatus.text = @"对接中";
        }
            break;
        case 1:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
            _orderStatus.text = @"选择机构并付款";
         }
            break;
        case 2:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
            _orderStatus.text = @"机构确认接单";
         }
            break;
        case 3:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
            _orderStatus.text = @"测试中";
         }
            break;
        case 4:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
            _orderStatus.text = @"上传结果";
        }
            break;
        case 5:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x00ff00);
            _orderStatus.text = @"已完成";
        }
            break;
        case 6:{
            _cancelBtn.hidden = YES;
            _orderStatus.textColor = UIColorFromRGB(0x1E59C5);
            _orderStatus.text = @"评价";
            
        }
            break;
            
        default:
            break;
    }
    
    _price.text = _model.price;
    _pre_price.text = _model.pre_price;
    _title.text = _model.title;
    [_avatar setImageURLStr:ImgUrl(_model.avatar) placeholder:Default_Avatar_Image_1];
    _nickName.text = _model.nick_name;
    _company.text = _model.company_name;
    
}

- (IBAction)zixunClick:(id)sender {
    if(self.didClickBlock){
        self.didClickBlock(0,_model);
    }
}

- (IBAction)cancelClick:(id)sender {
    if(self.didClickBlock){
        self.didClickBlock(1,_model);
    }
}

 
@end
