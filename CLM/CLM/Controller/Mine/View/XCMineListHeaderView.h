//
//  XCMineListHeaderView.h
//  CLM
//
//  Created by cong on 16/12/9.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCUserModel.h"

@interface XCMineListHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;
@property (weak, nonatomic) IBOutlet UIView *infoBG;
@property (weak, nonatomic) IBOutlet UIView *myMsgBG;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *myMessageLabel;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger type);//0 头像 1 label
@property (nonatomic,strong) XCUserModel *model;
@end
