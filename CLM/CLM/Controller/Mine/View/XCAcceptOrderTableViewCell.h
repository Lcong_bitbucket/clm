//
//  XCAcceptOrderTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCOrderModel.h"

@interface XCAcceptOrderTableViewCell : UITableViewCell 
@property (weak, nonatomic) IBOutlet UIView *nightModeView;
@property (weak, nonatomic) IBOutlet UILabel *orderStatus;
@property (weak, nonatomic) IBOutlet UILabel *pre_price;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *company;
- (IBAction)zixunClick:(id)sender;
- (IBAction)cancelClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (nonatomic,strong) XCOrderModel *model;
@property (nonatomic,copy) void (^didClickBlock)(NSInteger type,XCOrderModel *order);

@end
