//
//  XCOrderAcceptUserTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/9.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCOrderUserModel.h"

@interface XCOrderAcceptUserTableViewCell : UITableViewCell{
}
@property (weak, nonatomic) IBOutlet UIView *nightModeView;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nickName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btn1Width;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
- (IBAction)click1:(id)sender;
- (IBAction)click2:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
- (IBAction)cancelClick:(id)sender;

@property (nonatomic,copy) void (^didClickBlock)(NSInteger type,XCOrderUserModel *user);
 
@property (nonatomic,strong) XCOrderUserModel *model;
@end
