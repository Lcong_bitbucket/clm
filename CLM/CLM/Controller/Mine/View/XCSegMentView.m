//
//  XCSegMentView.m
//  CLM
//
//  Created by cong on 17/1/20.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCSegMentView.h"
#import "WZLBadgeImport.h"

@implementation XCSegMentView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithTitles:(NSMutableArray *)titles badges:(NSMutableArray *)badges{
    self = [super init];
    if (self) {
        _titles = titles;
        _badges = badges;
        _selectedIndex = 0;
        [self createUI];
    }
    return self;
}

- (void)createUI{
    CGFloat totalW = 0;
    CGFloat totalH = 34;

    for(int i = 0; i < _titles.count ; i++){
        NSString *s = _titles[i];
        CGFloat sW = GetStringSize(s, 14, nil, CGSizeMake(Screen_Width, CGFLOAT_MAX)).width + 8;
        
        UIButton *btn = [[UIButton alloc] init];
        btn.frame = CGRectMake(totalW + 2, 2, sW, totalH-4);
        totalW += sW;

        [btn setTitle:s forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
        [btn setTitleColor:UIColorFromRGB(0x1e59c5) forState:UIControlStateSelected];
        btn.badgeCenterOffset = CGPointMake(-8,8);
        btn.backgroundColor = [UIColor clearColor];
        [btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 10 + i;
        
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = btn.height/2;
        [self addSubview:btn];

    }
    totalW += 4;
    _curBtn = [self viewWithTag:10+_selectedIndex];
    _curBtn.selected = YES;
    _curBtn.backgroundColor = UIColorFromRGB(0xffffff);
    
    [self refreshBadge];

    self.frame = CGRectMake(0, 0, totalW, totalH);
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.height/2;
    self.backgroundColor = UIColorFromRGB(0x1e59c5);
    
}

- (void)click:(UIButton *)btn{
    _curBtn.selected = NO;
    _curBtn.backgroundColor = [UIColor clearColor];
    
    _curBtn = btn;
    
    _curBtn.selected = YES;
    _curBtn.backgroundColor = [UIColor whiteColor];
    
    _selectedIndex = _curBtn.tag - 10;
    if(self.didClickBlock){
        self.didClickBlock(_selectedIndex);
    }
}

- (void)refreshBadge{
    for(int i = 0; i < _badges.count; i++){
        NSString *s = _badges[i];
        UIButton *btn = [self viewWithTag:10+i];
        if(s && [s integerValue] > 0){
            [btn showBadgeWithStyle:WBadgeStyleRedDot value:[s integerValue] animationType:WBadgeAnimTypeBreathe];
        } else {
            [btn clearBadge];
        }
    }
}

- (void)setBadges:(NSArray *)badges{
    _badges = badges;
    [self refreshBadge];
}

@end
