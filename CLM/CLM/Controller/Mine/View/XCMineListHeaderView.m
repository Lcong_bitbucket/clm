//
//  XCMineListHeaderView.m
//  CLM
//
//  Created by cong on 16/12/9.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCMineListHeaderView.h"

@implementation XCMineListHeaderView

- (void)awakeFromNib{
    [super awakeFromNib];
    _myMessageLabel.hidden = YES;

    _myMessageLabel.backgroundColor = UIColorFromRGB(0xf24b4b);
    _myMessageLabel.textColor = [UIColor whiteColor];
    _myMessageLabel.textInsets = UIEdgeInsetsMake(2, 4, 2, 4);
    [_avatar whenTouchedUp:^{
        if(self.didClickBlock){
            self.didClickBlock(0);
        }
    }];
    
    [_infoBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(1);
        }
    }];
    
    
    [_myMsgBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(2);
        }
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:ReloadUserModelNotification object:nil];

}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setModel:(XCUserModel *)model{
    _model = model;
    
    _label1.text = _model.nick_name;
    
    if(![[XCUserManager sharedInstance] isLogin]){
        _label2.text = @"立即登录";
    } else {
        _label2.text = _model.zhiwei;
    }
    _label3.text = _model.company_name;
    [_avatar setImageURLStr:ImgUrl(_model.avatar) placeholder:Default_Avatar_Image_1];
    [self setupUnreadMessageCount];
 
}

// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    XCUserManager *um = [XCUserManager sharedInstance];
    XCUserModel *user = um.userModel;
    
    if(user)
        unreadCount += user.new_message;
    
 
        if (unreadCount > 0) {
            _myMessageLabel.text = [NSString stringWithFormat:@"%ld",(long)unreadCount];
            _myMessageLabel.hidden = NO;
        }else{
            _myMessageLabel.text = @"";
            _myMessageLabel.hidden = YES;
        }
 }

@end
