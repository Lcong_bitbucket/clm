//
//  XCOrderModel.h
//  CLM
//
//  Created by cong on 17/3/9.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCOrderModel : XCBaseModel

@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *pre_price;
@property (nonatomic,assign) NSInteger order_status;
@property (nonatomic,assign) NSInteger demand_status;
@property (nonatomic,assign) NSInteger type;
@property (nonatomic,assign) NSInteger has_done;
@property (nonatomic,assign) NSInteger order_id;



@property (nonatomic,copy) NSString *user_name;
@property (nonatomic,copy) NSString *nick_name;
@property (nonatomic,assign) NSInteger user_id;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *company_name;

@property (nonatomic,strong) NSMutableArray *jigou_list;
@end
