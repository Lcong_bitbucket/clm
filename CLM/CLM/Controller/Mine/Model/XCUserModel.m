//
//  XCUserModel.m
//  CLM
//
//  Created by cong on 16/12/8.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCUserModel.h"

@implementation XCUserModel

MJExtensionCodingImplementation

- (NSString *)nick_name{
    if(_nick_name.length == 0){
        _nick_name = _user_name;
    }
    return _nick_name;
}

@end
