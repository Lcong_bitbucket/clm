//
//  XCUserManager.h
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"
#import "XCUserManager.h"
#import "XCUserModel.h"

@interface XCUserManager : XCBaseModel

+ (XCUserManager*)sharedInstance;
@property (nonatomic,strong) XCUserModel *userModel;
- (void)save;
- (BOOL)presentloginVcIfNotSign:(UIViewController*)pVc success:(void (^)(BOOL isSuccess))success;
@property (nonatomic,assign) BOOL isLogin;
@property (nonatomic,strong) NSArray *cookies;

//清楚本地用户信息
- (void)clear;
//刷新用户信息
- (void)reloadInfoSuccess:(void (^)())successBlock
                  failure:(void (^)(NSError *error))failureBlock;

//登录本地服务器
- (void)loginUsername:(NSString *)username
                      password:(NSString *)password
                       success:(void (^)())successBlock
                       failure:(void (^)(NSError *error))failureBlock;

//退出本地服务器
- (void)exitSuccess:(void (^)())successBlock
            failure:(void (^)(NSError *error))failureBlock;

//登录环信
- (void)hxLoginUsername:(NSString *)hx_username
             password:(NSString *)hx_password
              success:(void (^)())successBlock
              failure:(void (^)(NSError *error))failureBlock;

//退出环信
- (void)hxExitSuccess:(void (^)())successBlock
            failure:(void (^)(NSError *error))failureBlock;

//根据环信获取用户信息
- (XCUserModel *)getUserModelWithHXUsername:(NSString *)username;
@end
