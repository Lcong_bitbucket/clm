//
//  XCMineListModel.h
//  CLM
//
//  Created by cong on 16/12/8.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCMineListModel : XCBaseModel
@property (nonatomic,copy) NSString *icon;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *rightIcon;
@property (nonatomic,assign) BOOL isHidden;
@end
