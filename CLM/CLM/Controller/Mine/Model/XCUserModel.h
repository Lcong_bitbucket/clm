//
//  XCUserModel.h
//  CLM
//
//  Created by cong on 16/12/8.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCUserModel : XCBaseModel

@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *user_name;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *nick_name;
@property (nonatomic,copy) NSString *company_name;
@property (nonatomic,copy) NSString *zhiwei;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *email;
@property (nonatomic,assign) NSInteger new_message;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,assign) NSInteger new_myanswer;//我回答的问题
@property (nonatomic,assign) NSInteger new_myaccepted;//被采纳的回答数量
@property (nonatomic,assign) NSInteger new_myfabu;//我发布的问题

@property (nonatomic,copy) NSString *easemob_username;
@property (nonatomic,copy) NSString *easemob_pass;


@end
