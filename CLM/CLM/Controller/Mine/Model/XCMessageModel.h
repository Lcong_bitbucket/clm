//
//  XCMessageModel.h
//  CLM
//
//  Created by cong on 16/12/16.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCMessageModel : XCBaseModel
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,assign) NSInteger article_id;
@property (nonatomic,copy) NSString *company_add;
@property (nonatomic,copy) NSString *company_name;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,assign) NSInteger from_user_id;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,assign) NSInteger is_lock;
@property (nonatomic,assign) NSInteger is_read;
@property (nonatomic,copy) NSString *read_time;
@property (nonatomic,copy) NSString *reply_content;
@property (nonatomic,copy) NSString *reply_time;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,assign) NSInteger to_user_id;
@property (nonatomic,assign) NSInteger type;//0 测 1 需 3官
@property (nonatomic,copy) NSString *user_email;
@property (nonatomic,copy) NSString *user_name;
@property (nonatomic,copy) NSString *user_qq;
@property (nonatomic,copy) NSString *user_tel;

@end
