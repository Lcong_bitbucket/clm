//
//  XCUserManager.m
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCUserManager.h"
#import "XCLoginViewController.h"
#import "ChatDemoHelper.h"
#import "JPUSHService.h"
//#import "UserProfileManager.h"

@implementation XCUserManager
static XCUserManager *manager = nil;

MJExtensionCodingImplementation
//哪些不归档
//+ (NSArray *)mj_ignoredCodingPropertyNames
//{
//    return @[@"didReloadInfoBlock"];
//}

+ (XCUserManager*)sharedInstance {
    static dispatch_once_t shareManagerPredicate;
    dispatch_once(&shareManagerPredicate, ^{
        NSString *file = [NSString getPathForDocuments:@"XCUserManager.data"];
        manager = [NSKeyedUnarchiver unarchiveObjectWithFile:file];
        if(!manager){
            XCLog(@"XCUserManager 解档 不存在");
            manager =[[XCUserManager alloc] init];
            manager.userModel = [[XCUserModel alloc] init];
        } else {
            XCLog(@"XCUserManager 解档  %@",manager);
        }
    });
    return manager;
}

- (void)setUserModel:(XCUserModel *)userModel{
    _userModel = userModel;
}

- (void)save{
    NSString *file = [NSString getPathForDocuments:@"XCUserManager.data"];
    // 归档
   BOOL isSuccess = [NSKeyedArchiver archiveRootObject:manager toFile:file];
   XCLog(@"归档 %d",isSuccess);
    [[NSNotificationCenter defaultCenter] postNotificationName:ReloadUserModelNotification object:nil];
 
}

- (BOOL)isLogin{
    if(self.userModel && self.userModel.id != 0){
        return YES;
    } else
        return NO;
}

- (void)loginUsername:(NSString *)username
             password:(NSString *)password
              success:(void (^)())successBlock
              failure:(void (^)(NSError *error))failureBlock{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:username forKey:@"user_login_name"];
    [params setValue:password forKey:@"user_login_pwd"];
    [params setValue:@"user_login" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = app_delegate().window;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            if(failureBlock){
                failureBlock(error);
            }
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                self.userModel.id = [jsonResponse[@"user_id"] integerValue];
                self.cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
                [self save];
                
                self.userModel.easemob_username = jsonResponse[@"user_id"];
                self.userModel.easemob_pass = jsonResponse[@"easemob_pass"];
                
                [self hxLoginUsername:self.userModel.easemob_username password:self.userModel.easemob_pass success:^{
                    if(successBlock){
                        successBlock();
                    }
                } failure:^(NSError *error) {
                    if(failureBlock){
                        failureBlock(error);
                    }
                }];
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)hxLoginUsername:(NSString *)hx_username
             password:(NSString *)hx_password
              success:(void (^)())successBlock
              failure:(void (^)(NSError *error))failureBlock
{
    if(hx_username == nil || hx_password==nil){
        if(failureBlock){
            failureBlock(nil);
        }
        return;
    }
    
    showHUDCustomView(app_delegate().window,nil,@"loading1",0);

    //异步登陆账号
    [[EMClient sharedClient] asyncLoginWithUsername:hx_username password:hx_password success:^{
        hidHUDCustomView(app_delegate().window);

        XCLog(@"环信登录成功");
        //设置是否自动登录
        [[EMClient sharedClient].options setIsAutoLogin:YES];
        [[EMClient sharedClient] dataMigrationTo3];
        
        [[ChatDemoHelper shareHelper] asyncGroupFromServer];
        [[ChatDemoHelper shareHelper] asyncConversationFromDB];
        [[ChatDemoHelper shareHelper] asyncPushOptions];
   
        if(successBlock){
            successBlock();
        }
        //注册用户的JPush别名和标签
        [JPUSHService setTags:[NSSet setWithObject:@"ios_yonghu"] alias:hx_username fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
            XCLog(@"*****设置推送*****i\n ResCode = %d \n tags = %@ \n alias = %@",iResCode,iTags,iAlias);
            
            if(iResCode == 0){
                
            } else {
                showHint(@"推送设置失败！");
            }
        }];
    } failure:^(EMError *aError) {
        hidHUDCustomView(app_delegate().window);

        [self exitSuccess:^{
            if(failureBlock){
                failureBlock(nil);
            }
        } failure:^(NSError *error) {
            if(failureBlock){
                failureBlock(nil);
            }
        }];
     }];
}

//退出本地服务器
- (void)exitSuccess:(void (^)())successBlock
            failure:(void (^)(NSError *error))failureBlock{
    
    [self hxExitSuccess:^{
        if(successBlock){
            [self clear];
             successBlock();
        }
    } failure:^(NSError *error) {
        if(failureBlock){
            failureBlock(nil);
        }
    }];
}

//退出环信
- (void)hxExitSuccess:(void (^)())successBlock
              failure:(void (^)(NSError *error))failureBlock{
    [[EMClient sharedClient] asyncLogout:YES success:^{
        if(successBlock){
            successBlock();
        }
        XCLog(@"环信退出成功");
        [JPUSHService setTags:[NSSet setWithObject:@"ios_youke"] alias:@""  fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {
            XCLog(@"*****设置推送*****i\n ResCode = %d \n tags = %@ \n alias = %@",iResCode,iTags,iAlias);
            if(iResCode == 0 ){
                
            } else {
                showHint(@"推送设置失败！");
            }
        }];
    } failure:^(EMError *aError) {
        if(failureBlock){
            failureBlock(nil );
        }
    }];

    
}

-(BOOL)presentloginVcIfNotSign:(UIViewController*)pVc success:(void (^)(BOOL isSuccess))success
{
    if(pVc == nil ){
        pVc = app_delegate().window.rootViewController;
    }
    if (!self.isLogin) {
        XCLoginViewController *lgVc = [[XCLoginViewController alloc] init];
        UINavigationController *loginNav =[[UINavigationController alloc] initWithRootViewController:lgVc];
        [lgVc setSuccess:^(BOOL isSuccess) {
            if(success){
                success(isSuccess);
            }
        }];
        [pVc presentViewController:loginNav animated:YES completion:^{
            XCLog(@"present login vc");
        }];
        return NO;
    }else {
        if(success){
            success(YES);
        }
        return YES;
    }
}

#pragma mark - 清除本地用户信息
- (void)clear{
     NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookieArray = [NSArray arrayWithArray:[cookieJar cookies]];
    for (NSHTTPCookie *obj in cookieArray) {
        [cookieJar deleteCookie:obj];
    }
    manager.userModel = nil ;
    manager.cookies = nil;
    manager.userModel = [[XCUserModel alloc] init];
    [self save];
}

#pragma mark - 刷新信息
- (void)reloadInfoSuccess:(void (^)())successBlock
                  failure:(void (^)(NSError *error))failureBlock{
    if(self.isLogin){
        NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
        [params setValue:@"GetUserInfo" forKey:@"action"];
        
        XCBaseCommand *command = [XCBaseCommand new];
        command.api = @"/tools/submit_ajax.ashx";
        command.params = params;
        
        [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
            if(error){
                if(failureBlock){
                    failureBlock(error);
                }
            } else {
                if([[jsonResponse objectForKey:@"status"] intValue]==1)
                {
                    self.userModel = [XCUserModel mj_objectWithKeyValues:jsonResponse];
                     [self save];
                    if(successBlock){
                        successBlock();
                    }
                } else {
                     [self clear];
                    if(failureBlock){
                        failureBlock(nil);
                    }
                }
              }
        }];
        [[XCHttpClient sharedInstance] request:command];
    }
}


//根据环信获取用户信息
- (XCUserModel *)getUserModelWithHXUsername:(NSString *)username{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetInfoByUid" forKey:@"action"];
    [params setValue:username forKey:@"id"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    id object = [command.cache objectForKey:command.cacheKey];
    if(object){
        return [XCUserModel mj_objectWithKeyValues:object];
    } else {
        [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
            
        }];
        [[XCHttpClient sharedInstance] request:command];
        return nil;
    }
   
}
@end
