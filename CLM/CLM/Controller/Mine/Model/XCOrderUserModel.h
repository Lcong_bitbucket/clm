//
//  XCOrderUserModel.h
//  CLM
//
//  Created by cong on 17/3/9.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCOrderUserModel : XCBaseModel

@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *pre_price;
@property (nonatomic,assign) NSInteger order_status;
@property (nonatomic,copy) NSString *nick_name;
@property (nonatomic,assign) NSInteger user_id;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *company_name;
@end
