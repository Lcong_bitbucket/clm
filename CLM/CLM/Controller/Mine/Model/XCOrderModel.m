//
//  XCOrderModel.m
//  CLM
//
//  Created by cong on 17/3/9.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCOrderModel.h"

@implementation XCOrderModel
+ (NSDictionary *)mj_objectClassInArray{
    return @{
             @"jigou_list" : @"XCOrderUserModel",
             };
}

- (NSString *)price{
    if(_price.length == 0){
        return @"面议";
    }
    return _price;
}

- (NSString *)pre_price{
    if(_pre_price.length == 0){
        return @"面议";
    }
    return _pre_price;
}
@end
