//
//  XCSupplyTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCSupplyModel.h"

@interface XCSupplyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *ce_time;
@property (weak, nonatomic) IBOutlet UILabel *ce_price;
@property (weak, nonatomic) IBOutlet UILabel *remark;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *company;
@property (weak, nonatomic) IBOutlet UIScrollView *renzhengScroll;
@property (nonatomic,copy) void (^didClickBlock)(NSDictionary *dic);
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rzH;
@property (weak, nonatomic) IBOutlet UILabel *getOrderNum;

@property (nonatomic,strong) XCSupplyModel *model;
@end
