//
//  XCSupplyTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCSupplyTableViewCell.h"

@implementation XCSupplyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
 }

- (void)setModel:(XCSupplyModel *)model{
    _model = model;
    UIImage *img = Default_Loading_Image_1;
    [img stretchableImageWithLeftCapWidth:img.size.width/2 topCapHeight:img.size.height/2];
    
    _company.text = _model.company;
    _address.text = [NSString stringWithFormat:@"地址:%@",_model.ce_address];
    [_icon setImageURLStr:ImgUrl(_model.img_url) placeholder:img];
    _title.text = _model.title;
    _ce_time.text = [NSString stringWithFormat:@"时间:%@",_model.ce_time];
    _ce_price.text = [NSString stringWithFormat:@"%@",_model.price];
    _remark.text = @"";
    
    if(_model.order_count == 0){
        _getOrderNum.hidden = YES;
    } else {
        _getOrderNum.hidden = NO;
        _getOrderNum.text = [NSString stringWithFormat:@"%ld人接单",(long)_model.order_count];
    }
    
//    _getOrderNum.text = [NSString stringWithFormat:@"%ld人接单",(long)_model.order_count];
    
    [self.renzhengScroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat w = 26;
    CGFloat h = 26;
    NSMutableArray *list = _model.renzheng_list;
    if(list.count == 0){
        _rzH.constant = 0;
    } else {
        _rzH.constant = 26;
    }
    for(int i = 0; i < list.count; i++){
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [iv setImageURLStr:ImgUrl(list[i][@"img_url"]) placeholder:Default_Loading_Image_1];
        [iv whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(list[i]);
            }
        }];
        [self.renzhengScroll addSubview:iv];
    }
    self.renzhengScroll.contentSize = CGSizeMake(list.count * (w +10) +10, 0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
