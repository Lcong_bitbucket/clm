//
//  XCSupplyDetailTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCSupplyModel.h"

@interface XCSupplyDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *ce_time;
@property (weak, nonatomic) IBOutlet UILabel *ce_price;
@property (weak, nonatomic) IBOutlet UILabel *remark;
@property (weak, nonatomic) IBOutlet UILabel *renzheng;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *need;
@property (weak, nonatomic) IBOutlet UILabel *anli;
@property (nonatomic,strong) XCSupplyModel *model;
@end
