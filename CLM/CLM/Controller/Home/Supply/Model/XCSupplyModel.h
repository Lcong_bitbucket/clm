//
//  XCSupplyModel.h
//  CLM
//
//  Created by cong on 16/12/13.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCSupplyModel : XCBaseModel
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,copy) NSString *ce_address;
@property (nonatomic,copy) NSString *ce_time;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *remark;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *special_demand;
@property (nonatomic,copy) NSString *anli;
@property (nonatomic,copy) NSString *renzheng;
@property (nonatomic,copy) NSString *category;
@property (nonatomic,strong) NSMutableArray *renzheng_list;
@property (nonatomic,assign) NSInteger jigou_id;
@property (nonatomic,copy) NSString *company;
@property (nonatomic,assign) NSInteger order_count;//接单人数
@property (nonatomic,assign) NSInteger user_id;//发布者ID

@end
