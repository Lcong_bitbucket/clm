//
//  XCMarkModel.h
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCMarkModel : XCBaseModel

@property (nonatomic,assign) NSInteger id;
//@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *img_url;
@property (nonatomic,assign) BOOL isSelected;
@end
