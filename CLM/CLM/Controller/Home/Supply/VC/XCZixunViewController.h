//
//  XCZixunViewController.h
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"
#import "XCSupplyModel.h"
#import "XCDemandModel.h"

@interface XCZixunViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *zsTitle;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *content;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *zhiweiTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
- (IBAction)submit:(id)sender;
//@property (nonatomic,strong) XCSupplyModel *supplyModel;
//@property (nonatomic,strong) XCDemandModel *demandModel;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,strong) NSString *zixunTitle;
@property (nonatomic,assign) NSInteger type;//0 项目／需求咨询  1机构咨询 2预约专家
@end
