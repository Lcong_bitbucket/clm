//
//  XCZixunViewController.m
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCZixunViewController.h"

@interface XCZixunViewController ()

@end

@implementation XCZixunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!self.title){
        self.title = @"咨询";
        if(self.type == 1){
            self.title = @"咨询机构";
        }
        if(self.type == 2){
            self.title = @"预约专家";
        }
    }
   
    [self customBackButton];
    _zsTitle.text = self.zixunTitle;
    
    _content.placeholder = @"请填写备注";
    XCUserModel *userModel = [[XCUserManager sharedInstance] userModel];
    _nameTF.text = userModel.nick_name;
    _phoneTF.text = userModel.mobile;
    _emailTF.text = userModel.email;
    _zhiweiTF.text = userModel.zhiwei;
    _companyTF.text = userModel.company_name;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(id)sender {
    if(_content.text.length ==0 ){
        showHint(@"请填写咨询内容");
        [XCTools shakeAnimationForView:_content];
        return;
    }else if(_nameTF.text.length ==0 ){
        showHint(@"请填写联系人姓名");
        [XCTools shakeAnimationForView:_nameTF];
        return;
    }else if(_phoneTF.text.length ==0 ){
        showHint(@"请填写联系方式");
        [XCTools shakeAnimationForView:_phoneTF];
        return;
    }else {
//        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
//            if(isSuccess){
                [self requestZixun];
//            }
//        }];
    }
}

- (void)requestZixun{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_content.text forKey:@"content"];
    [params setValue:_nameTF.text forKey:@"contact"];
    [params setValue:_phoneTF.text forKey:@"mobile"];
    [params setValue:_emailTF.text forKey:@"email"];
    [params setValue:_zhiweiTF.text forKey:@"zhiwei"];
    [params setValue:_companyTF.text forKey:@"company_name"];
    [params setValue:@(self.id) forKey:@"id"];
   
    if(self.type == 0){
        [params setValue:@"Zixun" forKey:@"action"];
    } else if(self.type == 1){
        [params setValue:@"ZixunJigou" forKey:@"action"];
    } else if(self.type == 2){
        [params setValue:@"YueZhuanjia" forKey:@"action"];
    }
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
@end
