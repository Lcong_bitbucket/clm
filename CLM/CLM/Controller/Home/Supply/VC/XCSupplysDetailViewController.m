//
//  XCSupplysDetailViewController.m
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCSupplysDetailViewController.h"

@interface XCSupplysDetailViewController ()

@end

@implementation XCSupplysDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customNavigationBarItems:@[@{@"imageName":@"icon_share"}] isLeft:NO];
    
    NSArray *imgarr=@[@"icon_share_fx.png",@"icon_share_search"];
    NSArray * titlearr=@[@"编辑",@"删除"];
    [self setPopver:imgarr titleArr:titlearr];
    
    [self customBackButton];
    self.title = @"项目详情";
    if(self.id){
         NSString *urlString = nil;
        if([[XCUserManager sharedInstance] isLogin]){
            urlString = [NSString stringWithFormat:@"%@/app/xiangmu_detail.aspx?id=%ld&uid=%ld&version=%@",AFSERVER_DATA,(long)self.id,(long)[XCUserManager sharedInstance].userModel.id,curVersion];
        } else {
            urlString = [NSString stringWithFormat:@"%@/app/xiangmu_detail.aspx?id=%ld&version=%@",AFSERVER_DATA,(long)self.id,curVersion];
        }
        NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [self.webView loadRequest:req];
        [self requestDetail];

    }
    [self.bridge registerHandler:@"handleClick" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSLog(@"response: %@", data);
        if([data[@"jumptype"] integerValue] == 17){
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                NSString *urlString = self.webView.URL.absoluteString;
                
                NSString *urlstr = [NSString stringWithFormat:@"%@&uid=%ld",urlString,(long)[XCUserManager sharedInstance].userModel.id];
                
                NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlstr]];
                [self.webView loadRequest:req];
                
            }];
        } else {
            goADPage(data, self);
        }
        responseCallback(@"成功");
    }];
    [self getAttach:self.id];

    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.webView reload];
}

- (void)requestDetail{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetDetail" forKey:@"action"];
    [params setValue:@(self.id) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self customNavigationBarItems:@[@{@"imageName":@"icon_share"}] isLeft:NO];
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                NSArray *ds = jsonResponse[@"ds"];
                if(ds.count > 0 && [[ds lastObject][@"user_id"] integerValue] == [XCUserManager sharedInstance].userModel.id){
                    [self customNavigationBarItems:@[@{@"imageName":@"icon_share"},@{@"imageName":@"icon_Create"}] isLeft:NO];
                } else {
                    [self customNavigationBarItems:@[@{@"imageName":@"icon_share"}] isLeft:NO];
                    
                }
                
            }else {
                [self customNavigationBarItems:@[@{@"imageName":@"icon_share"}] isLeft:NO];
                
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}


- (void)rightBarButtonAction:(UIButton *)btn{
    
    if(btn.tag == 100){
        NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
        [params setValue:@"GetShareLink" forKey:@"action"];
        [params setValue:@(3) forKey:@"type"];
        [params setValue:@(self.id) forKey:@"id"];
        
        XCBaseCommand *command = [XCBaseCommand new];
        command.api = @"/tools/submit_ajax.ashx";
        command.curView = self.view;
        command.params = params;
        
        [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
            if(error){
                
            } else {
                if([[jsonResponse objectForKey:@"status"] intValue]==1)
                {
                    [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
                }else {
                    showHint([jsonResponse objectForKey:@"desc"]);
                }
            }
        }];
        [[XCHttpClient sharedInstance] request:command];
  
    } else if(btn.tag == 101){
        [self hiddenRightControl];
    }
}

- (void)BtnClicked:(UIButton *)btn{
    if(btn.tag == 200){
        [self modify];
    } else if(btn.tag == 201){
        [self deleteDemand];
    }
}
- (void)deleteDemand{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"delete_demand" forKey:@"action"];
    [params setValue:@(self.id) forKey:@"id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)modify{
    XCPublishSupplyViewController *vc = [[XCPublishSupplyViewController alloc] init];
    vc.id = self.id;
    [vc setDidPublishBlock:^{
        [self.webView reload];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
