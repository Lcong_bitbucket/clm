//
//  XCPublishSupplyViewController.m
//  CLM
//
//  Created by cong on 16/12/21.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCPublishSupplyViewController.h"
#import "XCSupplyModel.h"
#import "STPickerArea.h"
#import "STPickerDate.h"
#import "STPickerSingle.h"
#import "XCMarkModel.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "XCJiGouModel.h"

@interface XCPublishSupplyViewController ()<STPickerAreaDelegate, STPickerSingleDelegate, STPickerDateDelegate,TZImagePickerControllerDelegate>
{
    XCSupplyModel *_curModel;
    
    NSMutableArray *_menu1Array;
    NSMutableArray *_menu1Value;
    
    NSMutableArray *_menu2Array;
    
    NSMutableArray *_jigouArray;
    NSMutableArray *_jigouValue;

   __block XCJiGouModel *_curJiGouModel;
}
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableArray *photos;
@property (nonatomic , strong) NSMutableArray *imgUrls;
@end

@implementation XCPublishSupplyViewController
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area
{
    NSString *text = [NSString stringWithFormat:@"%@ %@ %@", province, city, area];
    self.addressTF.text = text;
}

- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle selectedIndex:(NSInteger)row
{
    if(pickerSingle.tag == 111){
        NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
        self.fenleiTF.text = text;
    } else if(pickerSingle.tag == 112){
        NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
        if(row == 0){
            self.jigouL.text = @"";
            _curJiGouModel = nil;
        } else {
            self.jigouL.text = text;
            _curJiGouModel = _jigouArray[row-1];
        }
    }
}

- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSString *text = [NSString stringWithFormat:@"%zd年%zd月%zd日", year, month, day];
    self.timeL.text = text;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    _assets = [NSMutableArray new];
    _photos = [NSMutableArray new];
     _imgUrls = [NSMutableArray new];
    
    _menu1Array = [NSMutableArray new];
    _menu1Value = [NSMutableArray new];

    _menu2Array = [NSMutableArray new];
    
    _jigouArray = [NSMutableArray new];
    _jigouValue = [NSMutableArray new];

    self.title = @"发布测试项目";
 
    
    [_timeBG whenTapped:^{
        [self.view endEditing:YES];

        STPickerDate *pickerDate = [[STPickerDate alloc]init];
        [pickerDate setDelegate:self];
        [pickerDate show];
    }];
    
    UIView *addressCover = [[UIView alloc] init];
    [addressCover whenTouchedUp:^{
        [self.view endEditing:YES];
        
        STPickerArea *pickerArea = [[STPickerArea alloc]init];
        [pickerArea setDelegate:self];
        [pickerArea setContentMode:STPickerContentModeBottom];
        [pickerArea show];
    }];
    [self.addressBG addSubview:addressCover];
    [addressCover mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.addressBG.mas_left);
        make.top.mas_equalTo(self.addressBG.mas_top);
        make.bottom.mas_equalTo(self.addressBG.mas_bottom);
        make.right.mas_equalTo(self.addressBG.mas_right);
    }];
    
    UIView *fenLeiCover = [[UIView alloc] init];
    [fenLeiCover whenTouchedUp:^{
        [self.view endEditing:YES];
        
        if(_menu1Value.count > 0){
            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
            pickerSingle.tag = 111;
            pickerSingle.widthPickerComponent = 200;
            [pickerSingle setArrayData:_menu1Value];
            [pickerSingle setTitle:@"请选择测试类别"];
            [pickerSingle setTitleUnit:@""];
            [pickerSingle setContentMode:STPickerContentModeBottom];
            [pickerSingle setDelegate:self];
            [pickerSingle show];
        } else {
            [self requestCategory];
        }
    }];
    [self.fenleiBG addSubview:fenLeiCover];
    [fenLeiCover mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.fenleiBG.mas_left);
        make.top.mas_equalTo(self.fenleiBG.mas_top);
        make.bottom.mas_equalTo(self.fenleiBG.mas_bottom);
        make.right.mas_equalTo(self.fenleiBG.mas_right);
    }];
    
  
 //    [_jigouBG whenTapped:^{
//        [self.view endEditing:YES];
//
//        if(_jigouValue.count > 0){
//            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
//            pickerSingle.tag = 112;
//            pickerSingle.widthPickerComponent = 300;
//            [pickerSingle setArrayData:_jigouValue];
//            [pickerSingle setTitle:@"请选择所属机构"];
//            [pickerSingle setTitleUnit:@""];
//            [pickerSingle setContentMode:STPickerContentModeBottom];
//            [pickerSingle setDelegate:self];
//            [pickerSingle show];
//        } else {
//            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
//                if(isSuccess){
//                    [self requestJiGou];
//                }
//            }];
//        }
//    }];
    
    
    [self requestJiGou];
    [self requestCategory];
    [self requestMark:2];
    
    if(self.id){
        [self requestDetail];
    }
    
    [self customNavigationBarItemWithImageName:@"icon_fbsb" title:nil isLeft:NO];
    // Do any additional setup after loading the view from its nib.
}
- (void)refreshUI{
    if(_curModel){
        _titleTF.text = _curModel.title;
        _timeTF.text = _curModel.ce_time;
        _priceTF.text = _curModel.price;
        _addressTF.text = _curModel.ce_address;
        _special_demand.text = _curModel.special_demand;
         _fenleiTF.text = _curModel.category;
        [_imgUrls addObject:_curModel.img_url];
        [self reloadScrollView];
        _jigouTF.text = _curModel.company;
 
    }
    
    CGFloat w = 36,h=36;
    for(int i = 0; i < _menu2Array.count; i++){
        XCMarkModel *model = _menu2Array[i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [btn setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:ImgUrl([model img_url])] placeholderImage:Default_Loading_Image_1];
        btn.tag = 100+i;
        [btn setImage:imageNamed(@"tick") forState:UIControlStateSelected];
        NSArray *renzhengList = nil;
        if(_curModel.renzheng.length>0){
            renzhengList = [_curModel.renzheng componentsSeparatedByString:@","];
        }
        for(NSString*str in renzhengList){
            if([str integerValue] == model.id){
                model.isSelected = YES;
            }
        }
        btn.selected = model.isSelected;
        [btn whenTapped:^{
            btn.selected = !model.isSelected;
            model.isSelected = !model.isSelected;
        }];
        [self.renzhengScroll addSubview:btn];
    }
    self.renzhengScroll.contentSize = CGSizeMake(_menu2Array.count * (w +10) +10, 0);
}

- (void)requestJiGou{
    if(![[XCUserManager sharedInstance] isLogin])
        return;
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetJigouList" forKey:@"action"];
    [params setValue:@([[[XCUserManager sharedInstance] userModel] id]) forKey:@"user_id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_jigouArray removeAllObjects];
                [_jigouValue removeAllObjects];
                [_jigouArray addObjectsFromArray:[XCJiGouModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [_jigouValue addObject:@"无"];
                for(int i = 0; i < _jigouArray.count; i++){
                    [_jigouValue addObject:[_jigouArray[i] name]];
                }
                [self refreshUI];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}


- (void)requestCategory{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetCategory" forKey:@"action"];
    [params setValue:@"13" forKey:@"parent"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_menu1Array removeAllObjects];
                [_menu1Value removeAllObjects];
                [_menu1Array addObjectsFromArray:jsonResponse[@"ds"]];
                for(int i = 0; i < _menu1Array.count; i++){
                    [_menu1Value addObject:_menu1Array[i][@"title"]];
                }
                [self refreshUI];

            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestMark:(NSInteger)type{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(type) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(type == 2){
                    [_menu2Array removeAllObjects];
                    [_menu2Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                    [self refreshUI];
                 }

            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

#pragma mark - 请求数据
- (void)requestTel{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetKF" forKey:@"action"];
    [params setValue:@"2" forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                openTel(jsonResponse[@"tel"]);
                
            } else {
                
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)rightBarButtonAction:(UIButton *)btn{
    [self requestTel];
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"GetDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _curModel = [XCSupplyModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    [self refreshUI];
                 
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

//#pragma mark - GYZCityPickerDelegate
//- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
//{
//    _addressL.text = city.cityName;
//    [chooseCityController.navigationController popViewControllerAnimated:YES];
//}
//
//- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
//{
//    [chooseCityController.navigationController popViewControllerAnimated:YES];
//
//}
//


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)fabu:(id)sender {
  
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                [self request];
               }
        }];
 }

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_titleTF.text forKey:@"title"];
    [params setValue:_fenleiTF.text forKey:@"category"];
    
    NSMutableString *renzheng = [NSMutableString new];
    for(NSInteger i = 0; i < _menu2Array.count;i ++){
        XCMarkModel *model = _menu2Array[i];
        if(model.isSelected){
            if(i == _menu2Array.count - 1){
                [renzheng appendFormat:@"%ld",(long)model.id];
            } else {
                [renzheng appendFormat:@"%ld,",(long)model.id];
            }
        }
    }
    if(renzheng.length > 0 ){
         [params setValue:renzheng forKey:@"renzheng"];
    }
    
    [params setValue:_priceTF.text forKey:@"price"];
    [params setValue:_timeTF.text forKey:@"ce_time"];
    [params setValue:_addressTF.text forKey:@"ce_address"];
    [params setValue:_special_demand.text forKey:@"special_demand"];
     [params setValue:_anli.text forKey:@"anli"];
    if(_imgUrls.count > 0){
        [params setValue:_imgUrls[0] forKey:@"img_url"];
    }
//    if(_curJiGouModel){
//        [params setValue:@(_curJiGouModel.id) forKey:@"jigou_id"];
//    }
    [params setValue:_jigouTF.text forKey:@"company"];
    if(self.id){
        [params setValue:@(self.id) forKey:@"id"];
    }
    
    [params setValue:@(0) forKey:@"type"];

    [params setValue:@"EditDevice" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                if(self.didPublishBlock){
                    self.didPublishBlock();
                }
                [self goBack];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (IBAction)picClick:(id)sender {
    [self pushImagePickerController];
}
- (void)pushImagePickerController {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    
    
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.isSelectOriginalPhoto = YES;
    
    //    imagePickerVc.selectedAssets = self.assets; // 目前已经选中的图片数组
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
    // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
    // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];
    
    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingGif = NO;
    
    // 4. 照片排列按修改时间升序
    imagePickerVc.sortAscendingByModificationDate = YES;
    
    // imagePickerVc.minImagesCount = 3;
    // imagePickerVc.alwaysEnableDoneBtn = YES;
    
    // imagePickerVc.minPhotoWidthSelectable = 3000;
    // imagePickerVc.minPhotoHeightSelectable = 2000;
    
    /// 5. Single selection mode, valid when maxImagesCount = 1
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.needCircleCrop = NO;
    imagePickerVc.circleCropRadius = Screen_Width/2;
    /*
     [imagePickerVc setCropViewSettingBlock:^(UIView *cropView) {
     cropView.layer.borderColor = [UIColor redColor].CGColor;
     cropView.layer.borderWidth = 2.0;
     }];*/
    
    //imagePickerVc.allowPreview = NO;
#pragma mark - 到这里为止
    
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}
#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    self.assets = assets.mutableCopy;
    self.photos = photos.mutableCopy;
    if(self.assets.count > 0){
        [_imgUrls removeAllObjects];
        for(int i = 0; i < self.assets.count ; i ++){
            [_imgUrls addObject:@""];
        }
        for(int i = 0; i < self.assets.count ; i++){
            XCBaseCommand *command = [XCBaseCommand new];
            command.curView = self.view;
            
            command.imgs = @[self.photos[i]];
            [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
                if(error){
                    
                } else {
                    if([[jsonResponse objectForKey:@"status"] intValue]==1)
                    {
                        NSString *logPath=[jsonResponse objectForKey:@"path"];
                        [_imgUrls replaceObjectAtIndex:i withObject:logPath];
                        showHint(jsonResponse[@"desc"]);
                        [self reloadScrollView];
                    }  else {
                        showHint(jsonResponse[@"desc"]);
                    }
                }
            }];
            [[XCHttpClient sharedInstance] upload:command];
        }
    } else {
        
    }
    
}

//#pragma mark - 选择图片
//- (void)photoSelectet{
//    ZLPhotoPickerViewController *pickerVc = [[ZLPhotoPickerViewController alloc] init];
//    // MaxCount, Default = 9
//    pickerVc.maxCount = 1;
//    // Jump AssetsVc
//    pickerVc.status = PickerViewShowStatusCameraRoll;
//    // Filter: PickerPhotoStatusAllVideoAndPhotos, PickerPhotoStatusVideos, PickerPhotoStatusPhotos.
//    pickerVc.photoStatus = PickerPhotoStatusPhotos;
//    // Recoder Select Assets
//    pickerVc.selectPickers = self.assets;
//    // Desc Show Photos, And Suppor Camera
//    pickerVc.topShowPhotoPicker = YES;
//    pickerVc.isShowCamera = YES;
//    // CallBack
//    pickerVc.callBack = ^(NSArray<ZLPhotoAssets *> *status){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.assets = status.mutableCopy;
//            if(self.assets.count > 0){
//                [_imgUrls removeAllObjects];
//                for(int i = 0; i < self.assets.count ; i ++){
//                    [_imgUrls addObject:@""];
//                }
//                
//                for(int i = 0; i < self.assets.count ; i++){
//                    
//                    XCBaseCommand *command = [XCBaseCommand new];
//                    command.curView = self.view;
//                    command.imgs = [@[[self.assets[i] thumbImage]] mutableCopy];
//                    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
//                        if(error){
//                            
//                        } else {
//                            if([[jsonResponse objectForKey:@"status"] intValue]==1)
//                            {
//                                NSString *logPath=[jsonResponse objectForKey:@"path"];
//                                [_imgUrls replaceObjectAtIndex:i withObject:logPath];
//                                [self reloadScrollView];
//                            }  else {
//                                showHint(jsonResponse[@"desc"]);
//                            }
//                        }
//                    }];
//                    [[XCHttpClient sharedInstance] upload:command];
//                }
//            } else {
//                
//            }
//            
//        });
//    };
//    [pickerVc showPickerVc:self];
//}

- (void)reloadScrollView{
    if(_imgUrls.count > 0){
        [_pic_btn setImageWithURLString:ImgUrl(_imgUrls[0]) forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"icon_file"]];
    } else {
        [_pic_btn setImage:[UIImage imageNamed:@"icon_file"] forState:UIControlStateNormal];
    }
}

@end
