//
//  XCZixunViewController.h
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"
#import "XCSupplyModel.h"
#import "XCDemandModel.h"

@interface XCApplyOrderViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UILabel *zsTitle;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *content;
@property (weak, nonatomic) IBOutlet UILabel *pricePrompt;
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *zhiweiTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *priceTF;

- (IBAction)submit:(id)sender;

@property (nonatomic,assign) NSInteger type;//0项目 1需求
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,strong) NSString *zixunTitle;
 @end
