//
//  XCPublishSupplyViewController.h
//  CLM
//
//  Created by cong on 16/12/21.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"

@interface XCPublishSupplyViewController : XCBaseViewController

@property (weak, nonatomic) IBOutlet UIView *addressBG;
@property (weak, nonatomic) IBOutlet UITextField *titleTF;

@property (weak, nonatomic) IBOutlet UIScrollView *renzhengScroll;
@property (weak, nonatomic) IBOutlet UIView *fenleiBG;
@property (weak, nonatomic) IBOutlet UITextField *fenleiTF;

@property (weak, nonatomic) IBOutlet UIView *jigouBG;
@property (weak, nonatomic) IBOutlet UITextField *jigouTF;
@property (weak, nonatomic) IBOutlet UILabel *jigouL;
@property (weak, nonatomic) IBOutlet UITextField *priceTF;
@property (weak, nonatomic) IBOutlet UIView *timeBG;
@property (weak, nonatomic) IBOutlet UITextField *timeTF;
@property (weak, nonatomic) IBOutlet UILabel *timeL;
@property (weak, nonatomic) IBOutlet UITextField *addressTF;

@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *special_demand;

@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *anli;
@property (weak, nonatomic) IBOutlet UIButton *pic_btn;
- (IBAction)picClick:(id)sender;
 
- (IBAction)fabu:(id)sender;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) void (^didPublishBlock)();
@end
