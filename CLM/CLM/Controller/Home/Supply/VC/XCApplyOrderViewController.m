//
//  XCZixunViewController.m
//  CLM
//
//  Created by cong on 16/12/19.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCApplyOrderViewController.h"

@interface XCApplyOrderViewController ()

@end

@implementation XCApplyOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.type == 0){
        self.title = @"我要下单";
        _pricePrompt.text = @"预算";
    } else if(self.type == 1){
        self.title = @"我要接单";
        _pricePrompt.text = @"报价";
    }
    
    [self customBackButton];
    _zsTitle.text = self.zixunTitle;
    
    _content.placeholder = @"请填写备注";
    XCUserModel *userModel = [[XCUserManager sharedInstance] userModel];
    _nameTF.text = userModel.nick_name;
    _phoneTF.text = userModel.mobile;
    _emailTF.text = userModel.email;
    _zhiweiTF.text = userModel.zhiwei;
    _companyTF.text = userModel.company_name;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)submit:(id)sender {
 
    [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
        if(isSuccess){
            [self requestZixun];
        }
    }];
}

- (void)requestZixun{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_content.text forKey:@"content"];
    [params setValue:_nameTF.text forKey:@"contact"];
    [params setValue:_phoneTF.text forKey:@"mobile"];
    [params setValue:_emailTF.text forKey:@"email"];
    [params setValue:_zhiweiTF.text forKey:@"zhiwei"];
    [params setValue:_companyTF.text forKey:@"company_name"];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"DemandOrder" forKey:@"action"];
    [params setValue:_priceTF.text forKey:@"pre_price"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                [self.navigationController popViewControllerAnimated:YES];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
@end
