//
//  XCPublishDeviceViewController.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"
@interface XCPublishDeviceViewController : XCBaseViewController
@property (nonatomic,assign) NSInteger id;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIButton *saleBtn;
- (IBAction)categoryClick:(id)sender;
- (IBAction)newDegreeClick:(id)sender;


@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UITextField *producerTF;
@property (weak, nonatomic) IBOutlet UITextField *xinghaoTF;
@property (weak, nonatomic) IBOutlet UITextField *unitTF;
@property (weak, nonatomic) IBOutlet UITextField *priceTF;

@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *contentTX;

@property (weak, nonatomic) IBOutlet UIButton *firstBtn;
@property (weak, nonatomic) IBOutlet UIButton *secBtn;
- (IBAction)fabuClick:(id)sender;

@property (nonatomic,copy) void (^didPublishBlock)();
@end
