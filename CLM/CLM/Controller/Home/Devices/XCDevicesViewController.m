//
//  XCDevicesViewController.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCDevicesViewController.h"
#import "XCSearchView.h"
#import "XCDeviceTableViewCell.h"
#import "CycleTableViewCell.h"
#import "XCPublishDeviceViewController.h"
#import "XCDeviceDetailViewController.h"
#import "ItemTableViewCell.h"
@interface XCDevicesViewController ()
{
    NSString *_keyword;
    NSInteger category_id;
 
}
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSMutableArray *adArray;
@property (nonatomic,strong) NSMutableArray *func1Array;

@end

@implementation XCDevicesViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    
    _dataArray = [NSMutableArray new];
    _adArray = [NSMutableArray new];
    
    _func1Array = [NSMutableArray new];
    NSArray *tempfunc1 = @[
                           @{@"title":@"求购",@"img_url":@"icon_buy"},
                           @{@"title":@"出售",@"img_url":@"icon_sale"},
                           @{@"title":@"发布",@"img_url":@"icon_release"}
                           ];
    [_func1Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:tempfunc1]];
 
    if(self.vcType == 0){
        XCSearchView *searchView = [[XCSearchView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 80, 30)];
        searchView.searchTF.placeholder = @"查询您感兴趣的关键字...";
        [searchView setDidSearchBlock:^(NSString *text) {
            _keyword = text;
            self.curPage = 1;
            [self refresh];
        }];
        self.navigationItem.titleView = searchView;
    } else {
        self.title = @"我发布的求购/出售信息";
    }
    [self beginRefreshing];
    
//    [self customShareButton];

    [self customNavigationBarItemWithImageName:nil title:@"发布" isLeft:NO];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)rightBarButtonAction:(UIButton *)btn{
    XCPublishDeviceViewController *vc = [[XCPublishDeviceViewController alloc] init];
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 数据请求
- (void)refresh{
    if(self.curPage == 1 && self.vcType == 0)
        [self requestAD];
    [self request:self.curPage];
}

- (void)request:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:_keyword forKey:@"keyword"];
    [params setValue:@"GetCeshijiaList" forKey:@"action"];
    if(category_id){
        [params setValue:@(category_id) forKey:@"category_id"];
     }
    if(self.vcType == 1){
        [params setValue:@([[[XCUserManager sharedInstance] userModel] id]) forKey:@"user_id"];
    }
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1){
                    [_dataArray removeAllObjects];
                }
                [_dataArray addObjectsFromArray:[XCDeviceModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                [self stopRefresh:NO];
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestAD{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"9" forKey:@"place"];
    [params setValue:@"GetAD" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_adArray removeAllObjects];
                [_adArray addObjectsFromArray:jsonResponse[@"ds"]];
                [_tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}
#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if(self.adArray.count > 0)
            return 1;
        return 0;
    } else if(section == 1){
        if(self.vcType == 0){
            return 1;
        }
        else if(self.vcType == 1){
            return 0;
        }
    } else {
        return _dataArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0){
        
        NSString *identifier = @"CycleTableViewCell";
        CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        if(self.adArray.count > 0){
            cell.heightC.constant = 120 * Screen_Width / 375;
            cell.contentView.hidden = NO;
        } else {
            cell.contentView.hidden = YES;
            cell.heightC.constant = 0;
        }
        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic,self);
        }];
        cell.cycles = _adArray;
        return cell;
        
    }else if(indexPath.section == 1){
        NSString *identifier = @"ItemTableViewCell";
        ItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        WEAKSELF;
        [cell setDidClickBlock:^(XCMarkModel *model) {
            if([model.title isEqualToString:@"求购"]){
                category_id = 8;
                [self beginRefreshing];
            } else if([model.title isEqualToString:@"出售"]){
                category_id = 9;
                [self beginRefreshing];
            } else if([model.title isEqualToString:@"发布"]){
                XCPublishDeviceViewController *vc = [[XCPublishDeviceViewController alloc] init];
                vc.hidesBottomBarWhenPushed = YES;
                [vc setDidPublishBlock:^{
                    [self beginRefreshing];
                }];
                [weakSelf.navigationController pushViewController:vc animated:YES];
                
            }
        }];
        cell.col = 3;
        cell.array = self.func1Array;
        return cell;
        
    } else if(indexPath.section == 2){
        NSString *identifier = @"XCDeviceTableViewCell";
        XCDeviceTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _dataArray[indexPath.row];
        return cell;
    }
    return nil;
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 添加一个删除按钮
    if(self.vcType == 1){
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewRowAction *deleteRoWAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {//title可自已定义
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"删除后不可恢复,确定删除？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
                [self delete:indexPath];
            }
        }];
        [alert show];
        
    }];//此处是iOS8.0以后苹果最新推出的api，UITableViewRowAction，Style是划出的标签颜色等状态的定义，这里也可自行定义
    
    UITableViewRowAction *editRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self modify:indexPath];
    }];
    editRowAction.backgroundColor = [UIColor colorWithRed:0 green:124/255.0 blue:223/255.0 alpha:1];//可以定义RowAction的颜色
    return @[deleteRoWAction, editRowAction];//最后返回这俩个RowAction 的数组
    
}

- (void)delete:(NSIndexPath *)indexPath{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"delete_article" forKey:@"action"];
    [params setValue:@([_dataArray[indexPath.row] id]) forKey:@"id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_dataArray removeObjectAtIndex:indexPath.row];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)modify:(NSIndexPath *)indexPath{
    XCPublishDeviceViewController *vc = [[XCPublishDeviceViewController alloc] init];
    vc.id = [_dataArray[indexPath.row] id];
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    XCDeviceDetailViewController *vc = [[XCDeviceDetailViewController alloc] init];
    vc.id = [_dataArray[indexPath.row] id];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
