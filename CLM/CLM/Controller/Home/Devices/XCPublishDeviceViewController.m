//
//  XCPublishDeviceViewController.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCPublishDeviceViewController.h"
#import "XCDeviceModel.h"

@interface XCPublishDeviceViewController (){
    UIButton *_categoryBtn;
    UIButton *_newDegreeBtn;
    XCDeviceModel *_model;
}

@end

@implementation XCPublishDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发布求购/出售信息";
    [self customBackButton];
    _categoryBtn = _buyBtn;
    _categoryBtn.selected = YES;
    
    _newDegreeBtn = _firstBtn;
    _newDegreeBtn.selected = YES;
    
    _contentTX.placeholder = @"简要描述您的设备信息";
    
    if(self.id){
        [self requestDetail];
    }
    // Do any additional setup after loading the view from its nib.
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(self.id) forKey:@"id"];
    [params setValue:@"GetNewsDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _model = [XCDeviceModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    [self refreshUI];
                }
             } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)refreshUI{

    if(_model.category_id == 9 || _model.category_id == 10){
        _categoryBtn.selected = NO;
        _categoryBtn = (UIButton *)[self.view viewWithTag:_model.category_id];
        _categoryBtn.selected = YES;
    }
    
    if([_model.degree isEqualToString:@"全新"]){
        _newDegreeBtn.selected = NO;
        _newDegreeBtn = _firstBtn;
        _newDegreeBtn.selected = YES;
    } else if([_model.degree isEqualToString:@"二手"]){
        _newDegreeBtn.selected = NO;
        _newDegreeBtn = _secBtn;
        _newDegreeBtn.selected = YES;
    }
    
    _titleTF.text = _model.title;
    _producerTF.text = _model.producer;
    _xinghaoTF.text = _model.xinghao;
    _unitTF.text = _model.unit;
    _priceTF.text = _model.price;
    _contentTX.text = _model.content;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)fabuClick:(id)sender {
    

        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                [self request];
            }
        }];
}

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];

    [params setValue:_titleTF.text forKey:@"title"];
    [params setValue:@(_categoryBtn.tag) forKey:@"category_id"];
    [params setValue:_producerTF.text forKey:@"producer"];
    [params setValue:_xinghaoTF.text forKey:@"xinghao"];
    [params setValue:_unitTF.text forKey:@"unit"];
    [params setValue:_priceTF.text forKey:@"price"];
    [params setValue:(_newDegreeBtn.tag == 20 )?@"全新":@"二手" forKey:@"new_degree"];
    [params setValue:_contentTX.text forKey:@"content"];
    
//    NSMutableString *img_url = [[NSMutableString alloc] init];
//    for(int i = 0; i < _imgUrls.count ; i ++){
//        if(i == _imgUrls.count -1 ){
//            [img_url appendString:[NSString stringWithFormat:@"%@",_imgUrls[i]]];
//        } else {
//            [img_url appendString:[NSString stringWithFormat:@"%@,",_imgUrls[i]]];
//        }
//    }
    
    if(self.id){
        [params setValue:@(self.id) forKey:@"id"];
    }
    
//    [params setValue:img_url forKey:@"img_url"];
    
    [params setValue:@"EditCeshijia" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1) {
                showHint(jsonResponse[@"msg"]);
                if(self.didPublishBlock){
                    self.didPublishBlock();
                }
                [self goBack];
            } else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}


- (IBAction)categoryClick:(id)sender {
    _categoryBtn.selected = NO;
    _categoryBtn = (UIButton *)sender;
    _categoryBtn.selected = YES;
    
}

- (IBAction)newDegreeClick:(id)sender {
    _newDegreeBtn.selected = NO;
    _newDegreeBtn = (UIButton *)sender;
    _newDegreeBtn.selected = YES;
}
@end
