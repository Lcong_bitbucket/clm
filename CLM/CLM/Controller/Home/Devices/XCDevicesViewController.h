//
//  XCDevicesViewController.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCDevicesViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,assign) NSInteger vcType; //0 普通列表 1 我发布的

@end
