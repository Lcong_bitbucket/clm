//
//  XCDeviceModel.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCDeviceModel.h"

@implementation XCDeviceModel
- (NSString *)price{
    if(_price.length == 0){
        return @"面议";
    }
    return _price;
}

+ (NSDictionary *)mj_replacedKeyFromPropertyName
{
    return @{@"degree" : @"new_degree",
             };
}

@end
