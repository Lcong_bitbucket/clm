//
//  XCDeviceModel.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCDeviceModel : XCBaseModel
@property (nonatomic,copy) NSString *add_time;
@property (nonatomic,assign) NSInteger category_id;
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *degree;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *producer;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *xinghao;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *unit;
@property (nonatomic,copy) NSString *zhaiyao;


@end
