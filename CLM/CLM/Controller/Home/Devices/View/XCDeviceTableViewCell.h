//
//  XCDeviceTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCDeviceModel.h"

@interface XCDeviceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *gsLabel;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *xinghao;

@property (nonatomic,strong) XCDeviceModel *model;
@end
