//
//  XCDeviceTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/27.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCDeviceTableViewCell.h"

@implementation XCDeviceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCDeviceModel *)model{
    _model = model;
    if(_model.category_id == 9){
        _gsLabel.text = @"售";
        _gsLabel.backgroundColor = UIColorFromRGB(0x5978f3);
    } else if(_model.category_id == 10){
        _gsLabel.text = @"购";
        _gsLabel.backgroundColor = UIColorFromRGB(0xf7b115);
    }
    _title.text = _model.title;
    _price.text = _model.price;
    _content.text = _model.zhaiyao;
    _xinghao.text = [NSString stringWithFormat:@"厂商/型号:%@",_model.xinghao];
}

@end
