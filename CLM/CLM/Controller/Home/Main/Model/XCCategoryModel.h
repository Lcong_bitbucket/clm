//
//  XCCategoryModel.h
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCCategoryModel : XCBaseModel
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *img_url;
@property (nonatomic,assign) NSInteger id;

@end
