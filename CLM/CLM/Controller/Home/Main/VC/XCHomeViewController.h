//
//  XCHomeViewController.h
//  CLM
//
//  Created by cong on 16/12/29.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCTableViewController.h"

@interface XCHomeViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
 
@end
