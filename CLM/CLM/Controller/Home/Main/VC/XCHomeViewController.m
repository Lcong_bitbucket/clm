//
//  XCHomeViewController.m
//  CLM
//
//  Created by cong on 16/12/29.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCHomeViewController.h"
#import "CycleTableViewCell.h"
#import "XCMarkModel.h"
#import "ItemTableViewCell.h"
#import "BottomTableViewCell.h"
#import "HotSectionView.h"
#import "XCZhuanjiaModel.h"
#import "XCZhuanjiaScrollTableViewCell.h"
#import "XCNewJiGouTableViewCell.h"
#import "AD1TableViewCell.h"
#import "XCNewsTableViewCell.h"
#import "XCHomeHeader.h"
#import "GYZChooseCityController.h"
#import "JCCollectionViewTagFlowLayout.h"
#import "ZCLMarkTableViewCell.h"
#import "XCJiGouViewController.h"
#import "XCZhuanjiaViewController.h"
#import "XCSupplyViewController.h"
#import "XCDemandViewController.h"
#import "XCNewsViewController.h"
#import "XCPublishDemandViewController.h"
#import "XCLocationManager.h"
#import "XCPublishSupplyViewController.h"
#import "XCZhuanJiaDetailViewController.h"
#import "XCJiGouDetailViewController.h"
#import "XCNewsDetailViewController.h"
#import "XCMyMessagePageViewController.h"
#import "XCZixunMainViewController.h"
#import "XCMoreTableViewCell.h"
#import "XCMarqueesTableViewCell.h"

 
@interface XCHomeViewController ()<GYZChooseCityDelegate>
@property (nonatomic,strong) XCHomeHeader *header;

@property (nonatomic,strong) NSMutableArray *ad1Array;
@property (nonatomic,strong) NSMutableArray *ad2Array;

@property (nonatomic,strong) NSMutableArray *func1Array;
@property (nonatomic,strong) NSMutableArray *func2Array;

@property (nonatomic,strong) NSMutableArray *categoryArray;
@property (nonatomic,strong) NSMutableArray *zhuanjiaArray;
@property (nonatomic,strong) NSMutableArray *jigouArray;
@property (nonatomic,strong) NSMutableArray *markArray;
@property (nonatomic,strong) NSMutableArray *newsArray;
@property (nonatomic,strong) NSMutableArray *hotNewsArray;

@property (nonatomic, strong) JCCollectionViewTagFlowLayout *layout;

@end

@implementation XCHomeViewController
#pragma mark - GYZCityPickerDelegate
- (void) cityPickerController:(GYZChooseCityController *)chooseCityController didSelectCity:(GYZCity *)city
{
    [_header.addressBtn setTitle:city.cityName forState:UIControlStateNormal];
    [_header.addressBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];

}

- (void) cityPickerControllerDidCancel:(GYZChooseCityController *)chooseCityController
{
    
}

- (void)viewDidLoad {
    self.hiddenNavBarWhenPush = YES;
    [super viewDidLoad];
    CGRect frame=CGRectMake(0, 0, 0, CGFLOAT_MIN);
    self.tableView.tableHeaderView = [[UIView alloc]initWithFrame:frame];
    
    self.layout = [[JCCollectionViewTagFlowLayout alloc] init];

    _header = [[[NSBundle mainBundle] loadNibNamed:@"XCHomeHeader" owner:self options:nil]lastObject];
    
    WEAKSELF;
    [_header setDidClickBlock:^(NSInteger type) {
        if(type == 0){
            GYZChooseCityController *cityPickerVC = [[GYZChooseCityController alloc] init];
            [cityPickerVC setDelegate:weakSelf];
            cityPickerVC.hotCitys = @[@"100010000", @"200010000", @"300210000", @"600010000", @"300110000"];
            cityPickerVC.hidesBottomBarWhenPushed = YES;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:cityPickerVC];
            [weakSelf presentViewController:nav animated:YES completion:nil];
        } else if(type == 1){
            XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:vc animated:YES];
            
        } else if(type == 2){
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:weakSelf success:^(BOOL isSuccess) {
                if(isSuccess){
                    XCMyMessagePageViewController *vc = [[XCMyMessagePageViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }
            }];
        }
    }];

    [self.view addSubview:_header];
    [_header mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(64);
    }];
    
    _ad1Array = [NSMutableArray new];
    _ad2Array = [NSMutableArray new];
    _zhuanjiaArray = [NSMutableArray new];
    _jigouArray = [NSMutableArray new];
    _markArray = [NSMutableArray new];
    _newsArray = [NSMutableArray new];
    _func1Array = [NSMutableArray new];
    _hotNewsArray = [NSMutableArray new];
    
    NSArray *tempfunc1 = @[
                           @{@"title":@"预约测试",@"img_url":@"icon_yycs"},
                           @{@"title":@"我要测试",@"img_url":@"icon_wycs"},
                           @{@"title":@"专家咨询",@"img_url":@"icon_zjzx"}
                            ];
    [_func1Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:tempfunc1]];
    
    _func2Array = [NSMutableArray new];
    NSArray *tempfunc2 = @[
                           @{@"title":@"发现需求",@"img_url":@"icon_fxxu"},
                           @{@"title":@"提供测试",@"img_url":@"icon_tgcs"},
                           @{@"title":@"专家入驻",@"img_url":@"icon_zjrz"}
                           ];
    [_func2Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:tempfunc2]];
    
    _categoryArray = [NSMutableArray new];

    [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:NO tableView:self.tableView];
    [self refresh];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[XCUserManager sharedInstance] reloadInfoSuccess:^{
        
    } failure:^(NSError *error) {
        
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [super scrollViewDidScroll:scrollView];
    CGFloat y = 0;
 
    y = self.tableView.contentOffset.y / 100;//self.view.mj_h;
    XCLog(@"yy = %lf",y);
  
    if(y < 1){
      _header.hidden = NO;
    }
    else {
        _header.hidden = YES;
    }
}

- (void)refresh{
    [self requestAD:3];
    [self requestAD:4];
    [self requestFucn2];
    [self requestZhuanjia];
    [self requestJigou];
    [self requestMark];
    [self requestNews:self.curPage];
    [self requestHotNews];

}

- (void)requestAD:(NSInteger)place{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetAD" forKey:@"action"];
    [params setValue:@(place) forKey:@"place"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(place == 3){
                    [_ad1Array removeAllObjects];
                    [_ad1Array addObjectsFromArray:jsonResponse[@"ds"]];
                } else if(place == 4){
                    [_ad2Array removeAllObjects];
                    [_ad2Array addObjectsFromArray:jsonResponse[@"ds"]];
                }
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestFucn2{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetCategory" forKey:@"action"];
    [params setValue:@(1) forKey:@"is_hot"];
    [params setValue:@(13) forKey:@"parent"];

    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_categoryArray removeAllObjects];
                [_categoryArray addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestZhuanjia{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetZhuanjiaList" forKey:@"action"];
    [params setValue:@(1) forKey:@"is_hot"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_zhuanjiaArray removeAllObjects];
                [_zhuanjiaArray addObjectsFromArray:[XCZhuanjiaModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestJigou{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetJigouList" forKey:@"action"];
    [params setValue:@(1) forKey:@"is_hot"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_jigouArray removeAllObjects];
                [_jigouArray addObjectsFromArray:[XCJiGouModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestMark{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(1) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_markArray removeAllObjects];
                [_markArray addObjectsFromArray:jsonResponse[@"ds"]];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestNews:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"5" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:@"GetNewsList" forKey:@"action"];
    [params setValue:@"2" forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1){
                    [_newsArray removeAllObjects];
                }
                [_newsArray addObjectsFromArray:[XCNewsModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
                [self.tableView reloadData];
            }else {
                [self stopRefresh:NO];
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}
- (void)requestHotNews{
    
    NSMutableDictionary *params = [NSMutableDictionary new];

    [params setValue:@"GetNewsList" forKey:@"action"];
    [params setValue:@"1" forKey:@"type"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_hotNewsArray removeAllObjects];
                [_hotNewsArray addObjectsFromArray:[XCNewsModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
            }else {
                 showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 1;
    }else if(section == 1){
        return 1;
    } else if(section == 2){
        return 1;
    } else if(section == 3){
        return 1;
    } else if(section == 4){
        return 1;
    } else if(section == 5){
        return 1;
    } else if(section == 6){
        return _jigouArray.count+1;
    } else if(section == 7){
        if(_ad2Array.count > 0)
        return 1;
    } else if(section == 8){
        return 1;
    } else if(section == 9){
        return _newsArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0){
        
        NSString *identifier = @"CycleTableViewCell";
        CycleTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.heightC.constant = 180 * Screen_Width / 375;
        cell.cycles = _ad1Array;
        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic,self);
        }];
        return cell;
        
    } else if(indexPath.section == 1){
        NSString *identifier = @"ItemTableViewCell";
        ItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        WEAKSELF;
        [cell setDidClickBlock:^(XCMarkModel *model) {
            [weakSelf goPage:model];
        }];
        cell.col = 3;
        cell.array = self.func1Array;
        return cell;
        
    } else if(indexPath.section == 2){
        NSString *identifier = @"ItemTableViewCell";
        ItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        WEAKSELF;
        [cell setDidClickBlock:^(XCMarkModel *model) {
            [weakSelf goPage:model];
        }];
        cell.col = 3;
        cell.array = self.func2Array;
        return cell;
        
    }else if(indexPath.section == 3){
        NSString *identifier = @"XCMarqueesTableViewCell";
        XCMarqueesTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        WEAKSELF;
        cell.dataArray = self.hotNewsArray;
        [cell setDidClickBlock:^(XCNewsModel *m) {
            XCNewsDetailViewController *vc = [[XCNewsDetailViewController alloc] init];
            vc.id = m.id;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];

        }];
        return cell;
        
    } else if(indexPath.section == 4){
        if(indexPath.row == 0){
            NSString *identifier = @"ItemTableViewCell";
            ItemTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            [cell setDidClickBlock:^(XCMarkModel *model) {
                XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
                vc.category = model.title;
                vc.hidesBottomBarWhenPushed = YES;
                [vc customBackButton];
                [self.navigationController pushViewController:vc animated:YES];
            }];
            cell.height = 85;
            cell.col = 4;
            cell.array = self.categoryArray;
            return cell;
        } else {
            
            NSString *identifier = @"BottomTableViewCell";
            BottomTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            [cell setDidClickBlock:^(NSInteger type) {
                if(type == 0){
                    XCPublishDemandViewController *vc = [[XCPublishDemandViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    
                } else if(type == 1){
                    XCPublishSupplyViewController *vc = [[XCPublishSupplyViewController alloc] init];
                    vc.hidesBottomBarWhenPushed = YES;
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
            }];
            return cell;
        }
    } else if(indexPath.section == 5){
        NSString *identifier = @"XCZhuanjiaScrollTableViewCell";
        XCZhuanjiaScrollTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.array = _zhuanjiaArray;
        [cell setDidClickBlock:^(XCZhuanjiaModel *model) {
            XCZhuanJiaDetailViewController *vc = [[XCZhuanJiaDetailViewController alloc] init];
            vc.id = model.id;
            vc.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:vc animated:YES];
        }];
        return cell;
    } else if(indexPath.section == 6){
        if(indexPath.row < _jigouArray.count){
            NSString *identifier = @"XCNewJiGouTableViewCell";
            XCNewJiGouTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
            cell.model = _jigouArray[indexPath.row];
            [cell setDidClickBlock:^(NSInteger supplyID) {
                XCSupplysDetailViewController *vc = [[XCSupplysDetailViewController alloc] init];
                vc.id = supplyID;
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            
            return cell;
        } else {
            NSString *identifier = @"XCMoreTableViewCell";
            XCMoreTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
             [cell setDidClickBlock:^(NSInteger supplyID) {
                 XCJiGouViewController *vc = [[XCJiGouViewController alloc] init];
                 vc.hidesBottomBarWhenPushed = YES;
                 [vc customBackButton];
                 [self.navigationController pushViewController:vc animated:YES];
            }];
            
            return cell;
        }
       
    } else if(indexPath.section == 7){
        
        NSString *identifier = @"AD1TableViewCell";
        AD1TableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        if(self.ad2Array.count > 0){
            cell.galleryH.constant = 25 + (Screen_Width - 25)*125/750 ;
            cell.contentView.hidden = NO;
        } else {
            cell.galleryH.constant = 0 ;
            cell.contentView.hidden = YES;
        }
        cell.cycles = _ad2Array;
        [cell setCycleDetailBlock:^(NSDictionary *dic) {
            goADPage(dic,self);
        }];
        return cell;
    }else if(indexPath.section == 8){
        NSString *identifier = @"ZCLMarkTableViewCell";
        ZCLMarkTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        [cell setDidClickBlock:^(NSDictionary *dic) {
            XCDemandViewController *vc = [[XCDemandViewController alloc] init];
            vc.keyword = dic[@"title"];
            vc.hidesBottomBarWhenPushed = YES;
            [vc customBackButton];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        cell.tags = self.markArray;
        NSMutableArray *tags = [NSMutableArray new];
        for(NSDictionary *dic in self.self.markArray){
            [tags addObject:dic[@"title"]];
        }
        CGFloat h = [self.layout calculateContentHeight:tags];
        cell.markH.constant = h;
        return cell;

    } else if(indexPath.section == 9){
        NSString *identifier = @"XCNewsTableViewCell";
        XCNewsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell)
        {
            [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
            cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        }
        cell.model = _newsArray[indexPath.row];
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 6){
        XCJiGouDetailViewController *vc = [[XCJiGouDetailViewController alloc] init];
        vc.id = [_jigouArray[indexPath.row] id];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if(indexPath.section == 9){
        XCNewsDetailViewController *vc = [[XCNewsDetailViewController alloc] init];
        vc.id = [_newsArray[indexPath.row] id];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (void)goPage:(XCMarkModel *)model{
    if([model.title isEqualToString:@"发现需求"]){
        XCDemandViewController *vc = [[XCDemandViewController alloc] init];
        [vc customBackButton];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if([model.title isEqualToString:@"提供测试"]){
        XCPublishSupplyViewController *vc = [[XCPublishSupplyViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if([model.title isEqualToString:@"预约测试"]){
        XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];

    } else if([model.title isEqualToString:@"我要测试"]){
        XCPublishDemandViewController *vc = [[XCPublishDemandViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else if([model.title isEqualToString:@"专家入驻"]){
        XCZhuanjiaApplyViewController *vc = [[XCZhuanjiaApplyViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];

    } else if([model.title isEqualToString:@"专家咨询"]){
        XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
        vc.hidesBottomBarWhenPushed = YES;
        [vc customBackButton];
        [self.navigationController pushViewController:vc animated:YES];
        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 1){
        return 40;
    }
    if(section == 2){
        return 50;
    }
    if(section == 4){
        return 50;
    }
    if(section == 5){
        return 50;
    }
    if(section == 6){
        return 50;
    }
     if(section == 8){
        return 50;
    }
    if(section == 9){
        return 50;
    }
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 1){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"我要测试/咨询";
        sectionView.more.hidden = YES;
        return sectionView;
    }else if(section == 2){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"测试机构/专家";
        sectionView.more.hidden = YES;
        return sectionView;
    }else if(section == 4){
            HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
            sectionView.title.text = @"热门测试";
            sectionView.more.text = @"更多";
            [sectionView whenTapped:^{
                XCSupplyViewController *vc = [[XCSupplyViewController alloc] init];
                [vc customBackButton];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
            }];
            return sectionView;
    } else if(section == 5){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"推荐专家";
        sectionView.more.text = @"更多";
        [sectionView whenTapped:^{
            XCZhuanjiaViewController *vc = [[XCZhuanjiaViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [vc customBackButton];
             [self.navigationController pushViewController:vc animated:YES];
            
        }];
        return sectionView;
    } else if(section == 6){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"热门测试机构";
        sectionView.more.text = @"更多";
        [sectionView whenTapped:^{
            XCJiGouViewController *vc = [[XCJiGouViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [vc customBackButton];
             [self.navigationController pushViewController:vc animated:YES];
        }];
        return sectionView;
    }else if(section == 8){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"热门需求标签";
        sectionView.more.text = @"更多";
        [sectionView whenTouchedUp:^{
            XCDemandViewController *vc = [[XCDemandViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [vc customBackButton];
             [self.navigationController pushViewController:vc animated:YES];
            
        }];
        return sectionView;
    }else if(section == 9){
        HotSectionView *sectionView = [[[NSBundle mainBundle] loadNibNamed:@"HotSectionView" owner:self options:nil]lastObject];
        sectionView.title.text = @"最新资讯";
        sectionView.more.text = @"更多";
        [sectionView whenTouchedUp:^{
            XCNewsViewController *vc = [[XCNewsViewController alloc] init];
            vc.hidesBottomBarWhenPushed = YES;
            [vc customBackButton];
            [self.navigationController pushViewController:vc animated:YES];
            
        }];
        return sectionView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section {
 
    return CGFLOAT_MIN;
    
}

-(UIView*)tableView:(UITableView*)tableView viewForFooterInSection:(NSInteger)section {
  
    return nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
