//
//  XCZhuanjiaScrollTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCZhuanjiaModel.h"
@interface XCZhuanjiaScrollTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property (nonatomic,strong) NSMutableArray *array;
@property (nonatomic,copy) void (^didClickBlock)(XCZhuanjiaModel *model);
@end
