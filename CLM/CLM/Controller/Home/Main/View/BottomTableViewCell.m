//
//  BottomTableViewCell.m
//  xcwl
//
//  Created by cong on 16/10/30.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "BottomTableViewCell.h"

@implementation BottomTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_leftView whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(0);
        }
    }];
    
    [_rightView whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(1);
        }
    }];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

 
@end
