//
//  XCHomeHeader.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCHomeHeader : UIView
@property (weak, nonatomic) IBOutlet UIButton *addressBtn;
@property (weak, nonatomic) IBOutlet UIView *searchBG;
@property (weak, nonatomic) IBOutlet UIView *msgBG;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *msgBadge;
@property (nonatomic,copy)void (^didClickBlock)(NSInteger type);
@end
