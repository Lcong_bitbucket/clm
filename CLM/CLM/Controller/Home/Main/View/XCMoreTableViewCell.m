//
//  XCMoreTableViewCell.m
//  CLM
//
//  Created by cong on 17/5/4.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMoreTableViewCell.h"

@implementation XCMoreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self whenTouchedUp:^{
        if(self.didClickBlock){
            self.didClickBlock();
        }
    }];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
