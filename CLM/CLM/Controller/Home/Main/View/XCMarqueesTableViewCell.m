//
//  XCMarqueesTableViewCell.m
//  CLM
//
//  Created by cong on 17/5/5.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCMarqueesTableViewCell.h"
#import "XCNewsModel.h"

@implementation XCMarqueesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
    _headLine = [[SXHeadLine alloc]initWithFrame:CGRectMake(90, 0, Screen_Width - 98, 40)];
    _headLine.textColor = getHexColor(@"#333333");

    [self.contentView addSubview:_headLine];
 
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    NSMutableArray *tempArray = [NSMutableArray new];
    for(NSInteger i = 0; i < _dataArray.count ; i ++){
        [tempArray addObject:[_dataArray[i] title]];
    }
    
    [_headLine stop];
    _headLine.messageArray = tempArray;
    [_headLine setScrollDuration:0.5 stayDuration:3];
    WEAKSELF;
    [_headLine setTapBlock:^(NSInteger index) {
        if (weakSelf.didClickBlock){
            weakSelf.didClickBlock(dataArray[index]);
        }
    }];
     [_headLine start];
 }

@end
