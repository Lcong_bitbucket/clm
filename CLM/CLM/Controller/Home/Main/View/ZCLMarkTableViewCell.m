//
//  FriendMarkTableViewCell.m
//  NewMaterials
//
//  Created by cong on 16/10/27.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "ZCLMarkTableViewCell.h"

@implementation ZCLMarkTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_mark setBackgroundColor:UIColorFromRGB(0xffffff)];
    
    self.mark.tagTextColor = UIColorFromRGB(0x4c4c4c);
//    self.mark.tagCornerRadius = 3.0f;
    self.mark.tagBackgroundColor =  UIColorFromRGB(0xffffff);
    self.mark.tagStrokeColor = UIColorFromRGB(0xd1d1d1);
    [self.mark setCompletionBlockWithSelected:^(NSInteger index) {
        if(self.didClickBlock){
            self.didClickBlock(self.tags[index]);
        }
    }];
 }


- (void)setTags:(NSArray *)tags{
    _tags = tags;
    NSMutableArray *titles = [NSMutableArray new];
    for(int i = 0; i < _tags.count ; i++){
        [titles addObject:[NSString stringWithFormat:@"%@",_tags[i][@"title"]]];
    }
  
    [self setupData:titles];
}

- (void)setupData:(NSMutableArray *)titles
{
    self.mark.tags = titles;
    [self.mark.collectionView reloadData];
}

- (void)handleBtn:(UIButton *)btn
{
    NSLog(@"%@", btn.titleLabel.text);
    if(self.didClickBlock){
        self.didClickBlock(self.tags[btn.tag - 100]);
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
