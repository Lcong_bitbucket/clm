//
//  XCHomeHeader.m
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCHomeHeader.h"
#import "XCLocationManager.h"
@implementation XCHomeHeader


- (void)awakeFromNib{
    [super awakeFromNib];
    [_addressBtn whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(0);
        }
    }];
    [_searchBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(1);
        }
    }];
    [_msgBG whenTapped:^{
        if(self.didClickBlock){
            self.didClickBlock(2);
        }
    }];
    
    XCLocationManager *m = [XCLocationManager sharedInstance] ;
    [m setDidUpdateLocatin:^(NSDictionary *d) {
        [_addressBtn setTitle:d[@"city"] forState:UIControlStateNormal];
        [_addressBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];
    }];
    [m locationStart];
    [_addressBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleRight imageTitleSpace:5];
    
    [self setupUnreadMessageCount];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUnreadMessageCount) name:ReloadUserModelNotification object:nil];
 
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    
    XCUserManager *um = [XCUserManager sharedInstance];
    XCUserModel *user = um.userModel;
    if(user)
        unreadCount += user.new_message;
  
    if (unreadCount > 0) {
            _msgBadge.hidden = NO;
            _msgBadge.text = [NSString stringWithFormat:@"%ld",(long)unreadCount];
    }else{
            _msgBadge.hidden = YES;
            _msgBadge.text=@"";
    }
 }



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
