//
//  XCMoreTableViewCell.h
//  CLM
//
//  Created by cong on 17/5/4.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCMoreTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moreLabel;
@property (nonatomic,copy) void (^didClickBlock)();
@end
