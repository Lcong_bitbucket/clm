//
//  CycleTableViewCell.m
//  NewMaterials
//
//  Created by cong on 16/10/14.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "CycleTableViewCell.h"

@implementation CycleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setCycles:(NSArray *)cycles{
    _cycles = cycles;
    if (_cycles.count>0) {
        self.gallery.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
        self.gallery.pageControlStyle = SDCycleScrollViewPageContolStyleAnimated;
        self.gallery.placeholderImage = Default_Loading_Image_1;
        self.gallery.delegate = self;
        self.gallery.autoScrollTimeInterval = 3;
        
        if(_cycles.count <= 1){
            self.gallery.infiniteLoop = NO;
            self.gallery.autoScroll = NO;
        } else {
            self.gallery.infiniteLoop = YES;
            self.gallery.autoScroll = YES;
        }
        
        NSMutableArray *titles = [NSMutableArray new];
        NSMutableArray *imgs = [NSMutableArray new];
        for ( int i = 0; i < _cycles.count ; i++){
            [titles addObject:_cycles[i][@"title"]];
            [imgs addObject:ImgUrl(_cycles[i][@"img_url"])];
        }
        self.gallery.imageURLStringsGroup = imgs;
//         self.gallery.titlesGroup = titles;
    }
}


- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (self.cycleDetailBlock){
        self.cycleDetailBlock(_cycles[index]);
    }
}

@end
