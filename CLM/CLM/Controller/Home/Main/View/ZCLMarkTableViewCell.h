//
//  FriendMarkTableViewCell.h
//  NewMaterials
//
//  Created by cong on 16/10/27.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JCTagListView.h"

@interface ZCLMarkTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet JCTagListView *mark;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *markH;
@property (nonatomic,strong) NSArray *tags;
@property (nonatomic,copy) void (^didClickBlock)(NSDictionary *dic);
@end
