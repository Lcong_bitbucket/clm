//
//  XCZhuanjiaScrollTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCZhuanjiaScrollTableViewCell.h"

@implementation XCZhuanjiaScrollTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
   
    // Configure the view for the selected state
}

- (void)setArray:(NSMutableArray *)array{
    _array = array;
    [self.scroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat w = 165;
    CGFloat h = 80;
    for(int i = 0; i < _array.count; i++){
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [iv setImageURLStr:ImgUrl([_array[i] img_banner]) placeholder:Default_Loading_Image_1];
        [iv whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(_array[i]);
            }
        }];
        [self.scroll addSubview:iv];
    }
    self.scroll.contentSize = CGSizeMake(_array.count * (w +10) +10, 0);
}

@end
