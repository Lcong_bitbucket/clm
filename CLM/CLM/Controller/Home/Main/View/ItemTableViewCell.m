//
//  ItemTableViewCell.m
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "ItemTableViewCell.h"
#import "ItemView.h"

@implementation ItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _height = 95.0;
    // Initialization code
}

- (void)setArray:(NSMutableArray *)array{
    _array = array;
 
    CGFloat w = ceilf(Screen_Width/_col);
    
    [bg removeFromSuperview];
    bg = [[UIView alloc] init];
    [self.contentView addSubview:bg];
    [bg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.height.mas_equalTo(self.height*ceilf(_array.count/1.0/_col));
    }];
    
    for(int i = 0; i < _array.count; i++){
        ItemView *itemView = [[ItemView alloc] init];
        itemView.frame = CGRectMake((i%_col)*w, self.height*(i/_col), w, self.height);
        itemView.model = _array[i];
        [bg addSubview:itemView];
        [itemView whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(_array[i]);
            }
        }];
    }
   
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
