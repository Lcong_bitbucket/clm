//
//  HotSectionView.h
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotSectionView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *more;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *top;

@end
