//
//  XCMarqueesTableViewCell.h
//  CLM
//
//  Created by cong on 17/5/5.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SXHeadLine.h"
#import "XCNewsModel.h"

@interface XCMarqueesTableViewCell : UITableViewCell  
@property (strong, nonatomic) SXHeadLine *headLine;
@property (nonatomic,strong) NSMutableArray *dataArray;
@property (nonatomic,copy) void (^didClickBlock)(XCNewsModel *model);
@end
