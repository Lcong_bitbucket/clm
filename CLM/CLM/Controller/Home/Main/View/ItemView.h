//
//  ItemView.h
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCMarkModel.h"
@interface ItemView : UIView
@property (nonatomic,strong) UIView *bgView;
@property (strong, nonatomic) UIImageView *icon;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UIView *rLine;
@property (strong, nonatomic) UIView *bLine;
@property (nonatomic,strong) XCMarkModel *model;

 @end
