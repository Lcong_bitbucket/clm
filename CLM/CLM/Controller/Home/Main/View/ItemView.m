//
//  ItemView.m
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import "ItemView.h"

@implementation ItemView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)init
{
    self = [super init];
    if (self) {
        _bgView = [[UIView alloc] init];
        [self addSubview:_bgView];
        
        _icon = [[UIImageView alloc] init];
        
        _title = [[UILabel alloc] init];
        _title.font = [UIFont systemFontOfSize:14];
        _title.textColor = UIColorFromRGB(0x2f3746);
        _title.minimumScaleFactor = 0.1;
        _title.adjustsFontSizeToFitWidth = YES;
        [_title sizeToFit];
        _title.textAlignment = NSTextAlignmentCenter;
        
        _rLine = [[UIView alloc] init];
        _rLine.backgroundColor = UIColorFromRGB(0xf5f5f5);
        
        _bLine = [[UIView alloc] init];
        _bLine.backgroundColor = UIColorFromRGB(0xf5f5f5);

        [self addSubview:_icon];
        [self addSubview:_title];
        [self addSubview:_rLine];
        [self addSubview:_bLine];
    }
    return self;
}

- (void)setModel:(XCMarkModel *)model{
    _model = model;
    
    if(_model.id){
         [_icon sd_setImageWithURL:[NSURL URLWithString:ImgUrl(_model.img_url)] placeholderImage:Default_Loading_Image_1 completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if(!error){
                _icon.image = image;
                CGFloat w = image.size.width/2;
                CGFloat h = image.size.height/2 ;//* 90 / image.size.width;
                _icon.frame = CGRectMake((self.width - w)/2, 10+(50-h)/2, w, h);
            } else {
                _icon.frame = CGRectMake((self.width - 90)/2, 10, 90, 50);
            }
         }];
         _title.text = _model.title;
         _title.frame = CGRectMake(5, self.height - 28, self.width-10, 20);

    } else {
        UIImage *image = [UIImage imageNamed:_model.img_url];
 
        _icon.frame = CGRectMake((self.width - image.size.width)/2, 10, image.size.width, image.size.height);
        
        _icon.image = [UIImage imageNamed:_model.img_url];
        _title.text = _model.title;
        _title.frame = CGRectMake(5, self.height - 28, self.width-10, 20);

    }
    
    _rLine.frame = CGRectMake(self.width - 0.5, 0, 0.5, self.height);
    _bLine.frame = CGRectMake(0, self.height - 0.5, self.width, 0.5);
}

@end
