//
//  BottomTableViewCell.h
//  xcwl
//
//  Created by cong on 16/10/30.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *rightView;


@property (weak, nonatomic) IBOutlet UIView *midLine;
 
@property (nonatomic,copy) void (^didClickBlock)(NSInteger type);
@end
