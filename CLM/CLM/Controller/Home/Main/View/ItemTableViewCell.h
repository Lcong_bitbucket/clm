//
//  ItemTableViewCell.h
//  NewMaterials
//
//  Created by cong on 16/10/17.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCMarkModel.h"

@interface ItemTableViewCell : UITableViewCell{
    UIView *bg;
}
@property (nonatomic,assign) CGFloat height;
@property (nonatomic,assign) NSInteger col;
@property (nonatomic,strong) NSMutableArray *array;
@property (nonatomic,copy) void (^didClickBlock)(XCMarkModel *model);
@end
