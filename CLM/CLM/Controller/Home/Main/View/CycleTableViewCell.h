//
//  CycleTableViewCell.h
//  NewMaterials
//
//  Created by cong on 16/10/14.
//  Copyright © 2016年 Karl0n. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDCycleScrollView.h"
@interface CycleTableViewCell : UITableViewCell<SDCycleScrollViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightC;
@property (weak, nonatomic) IBOutlet SDCycleScrollView *gallery;
@property (nonatomic,strong) NSArray *cycles;
@property (nonatomic,copy) void (^cycleDetailBlock)(NSDictionary *dic);
@end
