//
//  XCPublishJiGouViewController.h
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "BRPlaceholderTextView.h"

@interface XCPublishJiGouViewController : XCBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *titleTF;
@property (weak, nonatomic) IBOutlet UIView *fenleiBG;
@property (weak, nonatomic) IBOutlet UILabel *fenleiL;

@property (weak, nonatomic) IBOutlet UIScrollView *renzhengScroll;
@property (weak, nonatomic) IBOutlet UIView *addressBG;
@property (weak, nonatomic) IBOutlet UILabel *addressL;
@property (weak, nonatomic) IBOutlet BRPlaceholderTextView *des;
@property (weak, nonatomic) IBOutlet UIButton *picBtn;
- (IBAction)picClick:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *contact;
@property (weak, nonatomic) IBOutlet UITextField *zhiwei;
@property (weak, nonatomic) IBOutlet UITextField *company;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UIButton *fabu;
- (IBAction)fabuClick:(id)sender;
@property (nonatomic,copy) void (^didPublishBlock)();

@end
