//
//  XCJiGouViewController.m
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCJiGouViewController.h"
#import "XCNewJiGouTableViewCell.h"
#import "XCSearchView.h"
//#import "JSDropDownMenu.h"
#import "XCPublishJiGouViewController.h"
#import "XCJiGouDetailViewController.h"
#import "DOPDropDownMenu.h"


@interface XCJiGouViewController ()<DOPDropDownMenuDataSource,DOPDropDownMenuDelegate>{
    NSMutableArray *_menu1Array;
    NSMutableArray *_menu1Value;
    NSInteger _currentMenu1Index;
    NSString *_category;
    
    NSMutableArray *_menu2Array;
    NSMutableArray *_menu2Value;
     NSString *_diqu;
    
    NSMutableArray *_menu3Array;
    NSMutableArray *_menu3Value;
    NSInteger _currentMenu3Index;
    NSInteger _sort;
}
@property (nonatomic,strong) NSString *keyword;
@property (nonatomic, strong) DOPDropDownMenu *menu;
@property (nonatomic,strong) NSMutableArray *jigouArray;
@end

@implementation XCJiGouViewController
- (void)initMenu{
    // 添加下拉菜单
    _menu = [[DOPDropDownMenu alloc] initWithOrigin:CGPointMake(0, 0) andHeight:44];
    _menu.indicatorColor = [UIColor colorWithRed:175.0f/255.0f green:175.0f/255.0f blue:175.0f/255.0f alpha:1.0];
    _menu.separatorColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0];
    _menu.textColor = [UIColor colorWithRed:83.f/255.0f green:83.f/255.0f blue:83.f/255.0f alpha:1.0f];
    _menu.dataSource = self;
    _menu.delegate = self;
    
    [self.view addSubview:_menu];
    [_menu selectDefalutIndexPath];
    
}

- (NSInteger)numberOfColumnsInMenu:(DOPDropDownMenu *)menu
{
    return 3;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfRowsInColumn:(NSInteger)column
{
    if (column == 0) {
        return _menu1Value.count;
    }else if (column == 1){
        return _menu2Value.count;
    }else if (column == 2){
        return _menu3Value.count;
    }
    return 0;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if (indexPath.column == 0) {
        return _menu1Value[indexPath.row];
    } else if (indexPath.column == 1){
        return [[_menu2Value[indexPath.row] allKeys] lastObject];
    }else if (indexPath.column == 2){
        return _menu3Value[indexPath.row];
    }
    return nil;
}

// new datasource

- (NSString *)menu:(DOPDropDownMenu *)menu imageNameForRowAtIndexPath:(DOPIndexPath *)indexPath
{
     return nil;
}

- (NSString *)menu:(DOPDropDownMenu *)menu imageNameForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
    return nil;
}

// new datasource

- (NSString *)menu:(DOPDropDownMenu *)menu detailTextForRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
    return nil;
}

- (NSString *)menu:(DOPDropDownMenu *)menu detailTextForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    return nil;
}

- (NSInteger)menu:(DOPDropDownMenu *)menu numberOfItemsInRow:(NSInteger)row column:(NSInteger)column
{
    if(column == 1){
        return [[[_menu2Value[row] allValues] lastObject] count];
    }
    return 0;
}

- (NSString *)menu:(DOPDropDownMenu *)menu titleForItemsInRowAtIndexPath:(DOPIndexPath *)indexPath
{
    if(indexPath.column == 1){
        if(indexPath.item == 0){
            return [[_menu2Value[indexPath.row] allValues] lastObject][indexPath.item];

        }
        return [[_menu2Value[indexPath.row] allValues] lastObject][indexPath.item];
    }
    return nil;
}

- (void)menu:(DOPDropDownMenu *)menu didSelectRowAtIndexPath:(DOPIndexPath *)indexPath
{
    
    if(indexPath.column == 0){
        _currentMenu1Index = indexPath.row;
        _category = _menu1Value[_currentMenu1Index];
        if(indexPath.row == 0){
            _category = @"";
        }
        [self beginRefreshing];

    } else if(indexPath.column == 1){
        if(indexPath.row == 0){
            _diqu = @"";
            [self beginRefreshing];

        } else {
            if(indexPath.item >= 0){
               
                 _diqu = [[_menu2Value[indexPath.row] allValues] lastObject][indexPath.item];
            
                [self beginRefreshing];

            }
        }

    } else {
        _currentMenu3Index = indexPath.row;
        _sort = [_menu3Array[_currentMenu3Index][@"id"] integerValue];
        [self beginRefreshing];

    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
     [self defaultConfig:self.tableView];
    [self openMJRefreshHeader:YES Footer:YES tableView:self.tableView];
    
    _jigouArray = [NSMutableArray new];
    
    if(self.vcType == 0){
        _menu1Array = [NSMutableArray new];
        _menu2Array = [NSMutableArray new];
        _menu3Array = [NSMutableArray new];

        _menu1Value = [NSMutableArray new];
        _menu2Value = [NSMutableArray new];
        _menu3Value = [NSMutableArray new];

        XCSearchView *searchView = [[XCSearchView alloc] initWithFrame:CGRectMake(0, 0, Screen_Width - 80, 30)];
        [searchView setDidSearchBlock:^(NSString *text) {
            _keyword = text;
            self.curPage = 1;
            [self refresh];
        }];
        self.navigationItem.titleView = searchView;
        if(_menu1Array.count == 0){
            [self requestCategory];
        }
  
        if(_menu2Array.count == 0){
            [self requestMenu2];
        }
        if(_menu3Array.count == 0){
            [self requestMenu3];
        }
    } else {
        self.title = @"我发布的机构";
        self.tbTop.constant = 0;
        [self beginRefreshing];
        
    }
    
    UIButton *fabuBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width - 65, Screen_Height - 200, 55, 55)];
    [fabuBtn setTitle:@"发布\n机构" forState:UIControlStateNormal];
    fabuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    fabuBtn.titleLabel.numberOfLines = 2;
    [fabuBtn setBackgroundImage:[UIImage imageNamed:@"bt_circular"] forState:UIControlStateNormal];
    [fabuBtn addTarget:self action:@selector(fabu) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:fabuBtn];

    
    // Do any additional setup after loading the view from its nib.
}

- (void)fabu{
    XCPublishJiGouViewController *vc = [[XCPublishJiGouViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)refresh{
    [self requestJigou:self.curPage];
    
}

- (void)requestCategory{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetCategory" forKey:@"action"];
    [params setValue:@"17" forKey:@"parent"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_menu1Array removeAllObjects];
                [_menu1Value removeAllObjects];
                [_menu1Array addObject:@{@"title":@"类别"}];
                
                [_menu1Array addObjectsFromArray:jsonResponse[@"ds"]];
                for(int i = 0; i < _menu1Array.count; i++){
                    [_menu1Value addObject:_menu1Array[i][@"title"]];
                }
                if(_menu1Value.count > 0 && _menu2Value.count > 0 && _menu3Value.count >0){
                    [self initMenu];
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestMenu2{
    [_menu2Array removeAllObjects];
    [_menu2Value removeAllObjects];
    [_menu2Array addObject:@{@"城市":@[]}];
    [_menu2Value addObject:@{@"城市":@[]}];

    NSArray *arrayRoot = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"]];
    XCLog(@"pat = %@",[[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"]);
        for(int i = 0; i < arrayRoot.count; i++){
            NSString *key = arrayRoot[i][@"state"];
            NSMutableArray *cityArray = [NSMutableArray new];
            [_menu2Array addObject:@{key:cityArray}];
            [_menu2Value addObject:@{key:cityArray}];

            [cityArray addObject:key];
             for(int j = 0; j < [arrayRoot[i][@"cities"] count]; j++){
                  [cityArray addObject:arrayRoot[i][@"cities"][j][@"city"]];
            }
        }
    
        if(_menu1Value.count > 0 && _menu2Value.count > 0 && _menu3Value.count >0){
            [self initMenu];
        }
 }

- (void)requestMenu3{
    [_menu3Array removeAllObjects];
    [_menu3Value removeAllObjects];
    [_menu3Array addObject:@{@"title":@"筛选",@"id":@"0"}];
    [_menu3Array addObjectsFromArray:@[@{@"title":@"订单量",@"id":@"1"},@{@"title":@"资质",@"id":@"2"},@{@"title":@"热门度",@"id":@"3"}]];
    for(int i = 0; i < _menu3Array.count; i++){
        [_menu3Value addObject:_menu3Array[i][@"title"]];
    }
    if(_menu1Value.count > 0 && _menu2Value.count > 0 && _menu3Value.count >0){
        [self initMenu];
    }
    

}


- (void)requestMark:(NSInteger)type{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(type) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(type == 2){
                    [_menu2Array removeAllObjects];
                    [_menu2Value removeAllObjects];
                    [_menu2Array addObject:@{@"title":@"认证资质"}];
                    [_menu2Array addObjectsFromArray:jsonResponse[@"ds"]];
                    for(int i = 0; i < _menu2Array.count; i++){
                        [_menu2Value addObject:_menu2Array[i][@"title"]];
                    }
                }
                if(_menu2Value.count > 0 && _menu1Value.count > 0){
                    [self initMenu];
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestJigou:(NSInteger)page{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetJigouList" forKey:@"action"];
    [params setValue:@"10" forKey:@"pagesize"];
    [params setValue:@(page) forKey:@"pageindex"];
    [params setValue:_category forKey:@"category"];
    [params setValue:_diqu forKey:@"diqu"];
    [params setValue:@(_sort) forKey:@"sort"];
    [params setValue:_keyword forKey:@"keyword"];
    if(self.vcType == 1){
        [params setValue:@([[[XCUserManager sharedInstance] userModel] id]) forKey:@"user_id"];
    }
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            [self stopRefresh:NO];
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(page == 1)
                    [_jigouArray removeAllObjects];
                [_jigouArray addObjectsFromArray:[XCJiGouModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                [self.tableView reloadData];
                if(page * 10 >= [jsonResponse[@"total"] integerValue]){
                    [self stopRefresh:YES];
                } else {
                    [self stopRefresh:NO];
                }
            }else {
                [self stopRefresh:NO];

                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _jigouArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"XCNewJiGouTableViewCell";
    XCNewJiGouTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    cell.model = _jigouArray[indexPath.row];
    [cell setDidClickBlock:^(NSInteger supplyID) {
        XCSupplysDetailViewController *vc = [[XCSupplysDetailViewController alloc] init];
        vc.id = supplyID;
        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }];
//    [cell setDidClickBlock:^(NSDictionary *dic) {
//        XCLog(@"jigou = %@",dic);
//    }];
//    [cell setDidClickBtn:^(NSInteger type , XCJiGouModel *m) {
//        if(type == 0){
//            XCZixunViewController *vc = [[XCZixunViewController alloc] init];
//            vc.zixunTitle = m.name;
//            vc.id = m.id;
//            vc.type = 1;
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
//            
//        } else if(type == 1){
//            openTel(m.tel);
//        } else if(type == 2){
//            XCJiGouDetailViewController *vc = [[XCJiGouDetailViewController alloc] init];
//            vc.id = m.id;
//            vc.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//    }];
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    XCJiGouDetailViewController *vc = [[XCJiGouDetailViewController alloc] init];
    vc.id = [_jigouArray[indexPath.row] id];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 添加一个删除按钮
    if(self.vcType == 1){
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewRowAction *deleteRoWAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {//title可自已定义
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"删除后不可恢复,确定删除？" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        [alert uxy_handlerClickedButton:^(UIAlertView *alertView, NSInteger btnIndex) {
            if(btnIndex == 1){
                [self delete:indexPath];
            }
        }];
        [alert show];
        
    }];//此处是iOS8.0以后苹果最新推出的api，UITableViewRowAction，Style是划出的标签颜色等状态的定义，这里也可自行定义
    
    UITableViewRowAction *editRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self modify:indexPath];
    }];
    editRowAction.backgroundColor = [UIColor colorWithRed:0 green:124/255.0 blue:223/255.0 alpha:1];//可以定义RowAction的颜色
    return @[deleteRoWAction, editRowAction];//最后返回这俩个RowAction 的数组
    
}

- (void)delete:(NSIndexPath *)indexPath{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"delete_jigou" forKey:@"action"];
    [params setValue:@([_jigouArray[indexPath.row] id]) forKey:@"id"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_jigouArray removeObjectAtIndex:indexPath.row];
                [self.tableView reloadData];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)modify:(NSIndexPath *)indexPath{
    XCPublishJiGouViewController *vc = [[XCPublishJiGouViewController alloc] init];
//    vc.id = [_jigouArray[indexPath.row] id];
    [vc setDidPublishBlock:^{
        [self beginRefreshing];
    }];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
