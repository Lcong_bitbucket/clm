//
//  XCPublishJiGouViewController.m
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCPublishJiGouViewController.h"
#import "XCSupplyModel.h"
#import "STPickerArea.h"
#import "STPickerDate.h"
#import "STPickerSingle.h"
#import "XCMarkModel.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "XCJiGouModel.h"

@interface XCPublishJiGouViewController ()<STPickerAreaDelegate, STPickerSingleDelegate, STPickerDateDelegate,TZImagePickerControllerDelegate>{
    NSMutableArray *_menu2Array;
    
    NSMutableArray *_menu1Array;
    NSMutableArray *_menu1Value;
    
    NSString *_province;
    NSString *_diqu;
    
    XCJiGouModel *_curModel;
    
}
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableArray *photos;
@property (nonatomic , strong) NSMutableArray *imgUrls;
@end

@implementation XCPublishJiGouViewController
- (void)pickerArea:(STPickerArea *)pickerArea province:(NSString *)province city:(NSString *)city area:(NSString *)area
{
    NSString *text = [NSString stringWithFormat:@"%@ %@ %@", province, city, area];
    _province = province;
    _diqu = [NSString stringWithFormat:@"%@ %@",city, area];
    self.addressL.text = text;
}

- (void)pickerSingle:(STPickerSingle *)pickerSingle selectedTitle:(NSString *)selectedTitle selectedIndex:(NSInteger)row
{
    NSString *text = [NSString stringWithFormat:@"%@", selectedTitle];
    self.fenleiL.text = text;
}


- (void)pickerDate:(STPickerDate *)pickerDate year:(NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSString *text = [NSString stringWithFormat:@"%zd年%zd月%zd日", year, month, day];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customBackButton];
    _assets = [NSMutableArray new];
    _photos = [NSMutableArray new];
    _imgUrls = [NSMutableArray new];
    
    _menu1Array = [NSMutableArray new];
    _menu1Value = [NSMutableArray new];
    
    _menu2Array = [NSMutableArray new];
    
    self.title = @"发布测试机构";
    [_addressBG whenTapped:^{
        [self.view endEditing:YES];

        STPickerArea *pickerArea = [[STPickerArea alloc]init];
        [pickerArea setDelegate:self];
        [pickerArea setContentMode:STPickerContentModeBottom];
        [pickerArea show];
    }];
    
    [_fenleiBG whenTapped:^{
        [self.view endEditing:YES];

        if(_menu1Value.count > 0){
            STPickerSingle *pickerSingle = [[STPickerSingle alloc]init];
            pickerSingle.widthPickerComponent = 200;
            [pickerSingle setArrayData:_menu1Value];
            [pickerSingle setTitle:@"请选择测试类别"];
            [pickerSingle setTitleUnit:@""];
            [pickerSingle setContentMode:STPickerContentModeBottom];
            [pickerSingle setDelegate:self];
            [pickerSingle show];
        } else {
            [self requestCategory];
        }
        
    }];
    
    [self requestCategory];
    [self requestMark:2];
     
    XCUserModel *userModel = [[XCUserManager sharedInstance] userModel];
    _contact.text = userModel.nick_name;
    _phone.text = userModel.mobile;
    _zhiwei.text = userModel.zhiwei;
    _company.text = userModel.company_name;
    _email.text = userModel.email;
  
     [self requestDetail];
     
    // Do any additional setup after loading the view from its nib.
}

- (void)refreshUI{
    if(_curModel){
        _titleTF.text = _curModel.name;
        _province = _curModel.province;
        _diqu = _curModel.diqu;
        _addressL.text = [NSString stringWithFormat:@"%@%@",_curModel.province,_curModel.diqu];
        _fenleiL.text = _curModel.category;
        _des.text = _curModel.content;
        [_imgUrls addObject:_curModel.img_url];
        [self reloadScrollView];
    }
    CGFloat w = 36,h=36;
    for(int i = 0; i < _menu2Array.count; i++){
        XCMarkModel *model = _menu2Array[i];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [btn setBackgroundImageForState:UIControlStateNormal withURL:[NSURL URLWithString:ImgUrl([model img_url])] placeholderImage:Default_Loading_Image_1];
        btn.tag = 100+i;
        [btn setImage:imageNamed(@"tick") forState:UIControlStateSelected];
        NSArray *renzhengList = nil;
        if(_curModel.renzheng.length>0){
            renzhengList = [_curModel.renzheng componentsSeparatedByString:@","];
        }
        for(NSString*str in renzhengList){
            if([str integerValue] == model.id){
                model.isSelected = YES;
            }
        }
        btn.selected = model.isSelected;
        [btn whenTapped:^{
            btn.selected = !model.isSelected;
            model.isSelected = !model.isSelected;
        }];
        [self.renzhengScroll addSubview:btn];
    }
    self.renzhengScroll.contentSize = CGSizeMake(_menu2Array.count * (w +10) +10, 0);
    
}


- (void)requestCategory{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetCategory" forKey:@"action"];
    [params setValue:@"17" forKey:@"parent"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [_menu1Array removeAllObjects];
                [_menu1Value removeAllObjects];
                [_menu1Array addObjectsFromArray:jsonResponse[@"ds"]];
                for(int i = 0; i < _menu1Array.count; i++){
                    [_menu1Value addObject:_menu1Array[i][@"title"]];
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)requestMark:(NSInteger)type{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@"GetLabels" forKey:@"action"];
    [params setValue:@(type) forKey:@"type"];
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if(type == 2){
                    [_menu2Array removeAllObjects];
                    [_menu2Array addObjectsFromArray:[XCMarkModel mj_objectArrayWithKeyValuesArray:jsonResponse[@"ds"]]];
                    [self refreshUI];
                    
                    
                }
                
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)requestDetail{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
     [params setValue:@"GetJigouDetail" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                if([jsonResponse[@"ds"] count] > 0){
                    _curModel = [XCJiGouModel mj_objectWithKeyValues:[jsonResponse[@"ds"] lastObject]];
                    [self refreshUI];
                }
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)picClick:(id)sender {
    [self pushImagePickerController];
}
- (IBAction)fabuClick:(id)sender {
    if(_titleTF.text.length == 0){
        showHint(@"请输入公司名称");
        [XCTools shakeAnimationForView:_titleTF];

    }else if(_fenleiL.text.length == 0){
        showHint(@"请选择测试类别");
        [XCTools shakeAnimationForView:_fenleiBG];

    }  else if(_addressL.text.length == 0){
        showHint(@"请输入公司所在地区");
        [XCTools shakeAnimationForView:_addressBG];

    } else if(_des.text.length == 0){
        showHint(@"请输入公司简介");
        [XCTools shakeAnimationForView:_des];

    }  else if(_contact.text.length == 0){
        showHint(@"请输入联系人姓名");
        [XCTools shakeAnimationForView:_contact];

    } else if(_phone.text.length == 0){
        showHint(@"请输入联系方式");
        [XCTools shakeAnimationForView:_phone];

    }else {
        [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
            if(isSuccess){
                [self request];
            }
        }];
    }
}

- (void)request{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:_titleTF.text forKey:@"name"];
    [params setValue:_fenleiL.text forKey:@"category"];
    
//    NSMutableString *renzheng = [NSMutableString new];
//    for(XCMarkModel *model in _menu2Array){
//        if(model.isSelected){
//            [renzheng appendFormat:@",%ld",(long)model.id];
//        }
//    }
//    if(renzheng.length > 0 ){
//        [renzheng appendString:@","];
//        [params setValue:renzheng forKey:@"renzheng"];
//    }
 
    [params setValue:_province forKey:@"province"];
    [params setValue:_diqu forKey:@"diqu"];

    [params setValue:_des.text forKey:@"content"];
    if(_imgUrls.count > 0)
        [params setValue:_imgUrls[0] forKey:@"img_url"];
    
     [params setValue:@(_curModel.id) forKey:@"id"];
    
    [params setValue:_contact.text forKey:@"contact"];
    [params setValue:_phone.text forKey:@"mobile"];
    [params setValue:_email.text forKey:@"email"];
    [params setValue:_zhiwei.text forKey:@"zhiwei"];
    [params setValue:_company.text forKey:@"company_name"];
    
    [params setValue:@"EditJigou" forKey:@"action"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    command.curView = self.view;
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                showHint(jsonResponse[@"msg"]);
                if(self.didPublishBlock){
                    self.didPublishBlock();
                }
                [self goBack];
            }else {
                showHint(jsonResponse[@"msg"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}
- (void)pushImagePickerController {
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 columnNumber:4 delegate:self pushPhotoPickerVc:YES];
    
    
#pragma mark - 四类个性化设置，这些参数都可以不传，此时会走默认设置
    imagePickerVc.isSelectOriginalPhoto = YES;
    
    //    imagePickerVc.selectedAssets = self.assets; // 目前已经选中的图片数组
    imagePickerVc.allowTakePicture = YES; // 在内部显示拍照按钮
    
    // 2. Set the appearance
    // 2. 在这里设置imagePickerVc的外观
    // imagePickerVc.navigationBar.barTintColor = [UIColor greenColor];
    // imagePickerVc.oKButtonTitleColorDisabled = [UIColor lightGrayColor];
    // imagePickerVc.oKButtonTitleColorNormal = [UIColor greenColor];
    
    // 3. Set allow picking video & photo & originalPhoto or not
    // 3. 设置是否可以选择视频/图片/原图
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.allowPickingImage = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.allowPickingGif = NO;
    
    // 4. 照片排列按修改时间升序
    imagePickerVc.sortAscendingByModificationDate = YES;
    
    // imagePickerVc.minImagesCount = 3;
    // imagePickerVc.alwaysEnableDoneBtn = YES;
    
    // imagePickerVc.minPhotoWidthSelectable = 3000;
    // imagePickerVc.minPhotoHeightSelectable = 2000;
    
    /// 5. Single selection mode, valid when maxImagesCount = 1
    /// 5. 单选模式,maxImagesCount为1时才生效
    imagePickerVc.showSelectBtn = NO;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.needCircleCrop = NO;
    imagePickerVc.circleCropRadius = Screen_Width/2;
    /*
     [imagePickerVc setCropViewSettingBlock:^(UIView *cropView) {
     cropView.layer.borderColor = [UIColor redColor].CGColor;
     cropView.layer.borderWidth = 2.0;
     }];*/
    
    //imagePickerVc.allowPreview = NO;
#pragma mark - 到这里为止
    
    // You can get the photos by block, the same as by delegate.
    // 你可以通过block或者代理，来得到用户选择的照片.
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        
    }];
    
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}
#pragma mark - TZImagePickerControllerDelegate

/// User click cancel button
/// 用户点击了取消
- (void)tz_imagePickerControllerDidCancel:(TZImagePickerController *)picker {
    // NSLog(@"cancel");
}

// The picker should dismiss itself; when it dismissed these handle will be called.
// If isOriginalPhoto is YES, user picked the original photo.
// You can get original photo with asset, by the method [[TZImageManager manager] getOriginalPhotoWithAsset:completion:].
// The UIImage Object in photos default width is 828px, you can set it by photoWidth property.
// 这个照片选择器会自己dismiss，当选择器dismiss的时候，会执行下面的代理方法
// 如果isSelectOriginalPhoto为YES，表明用户选择了原图
// 你可以通过一个asset获得原图，通过这个方法：[[TZImageManager manager] getOriginalPhotoWithAsset:completion:]
// photos数组里的UIImage对象，默认是828像素宽，你可以通过设置photoWidth属性的值来改变它
- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto {
    self.assets = assets.mutableCopy;
    self.photos = photos.mutableCopy;
    if(self.assets.count > 0){
        [_imgUrls removeAllObjects];
        for(int i = 0; i < self.assets.count ; i ++){
            [_imgUrls addObject:@""];
        }
        for(int i = 0; i < self.assets.count ; i++){
            XCBaseCommand *command = [XCBaseCommand new];
            command.curView = self.view;
            
            command.imgs = @[self.photos[i]];
            [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
                if(error){
                    
                } else {
                    if([[jsonResponse objectForKey:@"status"] intValue]==1)
                    {
                        NSString *logPath=[jsonResponse objectForKey:@"path"];
                        [_imgUrls replaceObjectAtIndex:i withObject:logPath];
                        showHint(jsonResponse[@"desc"]);
                        [self reloadScrollView];
                    }  else {
                        showHint(jsonResponse[@"desc"]);
                    }
                }
            }];
            [[XCHttpClient sharedInstance] upload:command];
        }
    } else {
        
    }
    
}

- (void)reloadScrollView{
    if(_imgUrls.count > 0){
        [_picBtn setImageWithURLString:ImgUrl(_imgUrls[0]) forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"icon_file"]];
    } else {
        [_picBtn setImage:[UIImage imageNamed:@"icon_file"] forState:UIControlStateNormal];
    }
}

@end
