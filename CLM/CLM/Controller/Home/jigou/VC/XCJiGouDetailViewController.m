//
//  XCJiGouDetailViewController.m
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCJiGouDetailViewController.h"

@interface XCJiGouDetailViewController ()

@end

@implementation XCJiGouDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customShareButton];
    
    self.title = @"机构详情";
    if(self.id){
        NSString *urlString = nil;
        if([[XCUserManager sharedInstance] isLogin]){
            urlString = [NSString stringWithFormat:@"%@/app/jigou_detail.aspx?id=%ld&uid=%ld&version=%@",AFSERVER_DATA,(long)self.id,(long)[XCUserManager sharedInstance].userModel.id,curVersion];
        } else {
            urlString = [NSString stringWithFormat:@"%@/app/jigou_detail.aspx?id=%ld&version=%@",AFSERVER_DATA,(long)self.id,curVersion];
        }
        NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        
        [self.webView loadRequest:req];
    }
    
    [self.bridge registerHandler:@"handleClick" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSLog(@"response: %@", data);
        if([data[@"jumptype"] integerValue] == 17){
            [[XCUserManager sharedInstance] presentloginVcIfNotSign:self success:^(BOOL isSuccess) {
                NSString *urlString = self.webView.URL.absoluteString;
                
                NSString *urlstr = [NSString stringWithFormat:@"%@&uid=%ld",urlString,(long)[XCUserManager sharedInstance].userModel.id];
                
                NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlstr]];
                [self.webView loadRequest:req];
                
            }];
        } else {
            goADPage(data, self);
        }
        responseCallback(@"成功");
    }];
    [self getAttach:self.id];

    // Do any additional setup after loading the view from its nib.
}
- (void)rightBarButtonAction:(UIButton *)btn{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetShareLink" forKey:@"action"];
    [params setValue:@(7) forKey:@"type"];
    [params setValue:@(self.id) forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.curView = self.view;
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
        if(error){
            
        } else {
            if([[jsonResponse objectForKey:@"status"] intValue]==1)
            {
                [[ShareManager shareManeger] shareDic:jsonResponse vc:self];
            }else {
                showHint([jsonResponse objectForKey:@"desc"]);
            }
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
