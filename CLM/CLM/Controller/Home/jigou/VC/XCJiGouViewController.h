//
//  XCJiGouViewController.h
//  CLM
//
//  Created by cong on 17/1/3.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCTableViewController.h"
#import "XCJiGouModel.h"

@interface XCJiGouViewController : XCTableViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbTop;
@property (nonatomic,assign) NSInteger vcType; //0 普通列表 1 我发布的
@end
