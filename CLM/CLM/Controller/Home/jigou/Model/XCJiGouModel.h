//
//  XCJiGouModel.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseModel.h"

@interface XCJiGouModel : XCBaseModel
@property (nonatomic,assign) NSInteger id;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *img_url;
@property (nonatomic,copy) NSString *zhaiyao;
@property (nonatomic,strong) NSMutableArray *renzheng_list;
@property (nonatomic,copy) NSString *renzheng;
@property (nonatomic,copy) NSString *category;
@property (nonatomic,copy) NSString *province;
@property (nonatomic,copy) NSString *diqu;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *tel;
@property (nonatomic,assign) NSInteger xiangmu_count;
@property (nonatomic,strong) NSMutableArray *xiangmu_list;

@end
