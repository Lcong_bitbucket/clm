//
//  XCJigouSupplyTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCJigouSupplyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (nonatomic,strong) NSDictionary *dic;
@end
