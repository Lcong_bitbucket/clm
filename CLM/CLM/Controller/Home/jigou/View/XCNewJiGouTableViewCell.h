//
//  XCNewJiGouTableViewCell.h
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCJiGouModel.h"
@interface XCNewJiGouTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *pic;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *total;
@property (weak, nonatomic) IBOutlet UILabel *des;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewH;
@property (nonatomic, strong) CALayer *bottomLine;

@property (nonatomic,strong) XCJiGouModel *model;

@property (nonatomic,copy) void (^didClickBlock)(NSInteger id);

@end
