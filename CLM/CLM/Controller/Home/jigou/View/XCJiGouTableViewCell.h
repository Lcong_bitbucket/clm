//
//  XCJiGouTableViewCell.h
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XCJiGouModel.h"
@interface XCJiGouTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *zixunBtn;
- (IBAction)zixunClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *telBtn;
- (IBAction)telClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *moreBtn;
- (IBAction)moreClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *renzhengScroll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rzH;
@property (weak, nonatomic) IBOutlet UILabel *zhaiyao;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (nonatomic,copy) void (^didClickBlock)(NSDictionary *dic);
@property (nonatomic,copy) void(^didClickBtn)(NSInteger type,XCJiGouModel *model);
@property (nonatomic,strong) XCJiGouModel *model;
@end
