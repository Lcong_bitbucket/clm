//
//  XCNewJiGouTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/8.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCNewJiGouTableViewCell.h"
#import "XCJigouSupplyTableViewCell.h"

@implementation XCNewJiGouTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _tableView.estimatedRowHeight = 44.0f;
    _tableView.rowHeight = UITableViewAutomaticDimension;
 
    _bottomLine = [CALayer layer];
    _bottomLine.backgroundColor = UIColorFromRGB(0xe8e8e8).CGColor;
    [self.layer addSublayer:_bottomLine];

    // Initialization code
}


- (void)layoutSubviews{
    _bottomLine.frame = CGRectMake(0, self.height-1, Screen_Width, 1/[UIScreen mainScreen].scale) ;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - tableview
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _model.xiangmu_list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"XCJigouSupplyTableViewCell";
    XCJigouSupplyTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
        cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    }
    cell.dic = _model.xiangmu_list[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if(self.didClickBlock){
        self.didClickBlock([_model.xiangmu_list[indexPath.row][@"id"] integerValue]);
    }
}

- (void)setModel:(XCJiGouModel *)model{
    _model = model;
    _des.text = _model.zhaiyao;
    [_pic setImageURLStr:ImgUrl(_model.img_url) placeholder:Default_Loading_Image_1];
    _title.text = _model.name;
    _total.text = [NSString stringWithFormat:@"测试项目(共%ld项)",(long)_model.xiangmu_count];
    [_tableView reloadData];

//    CGFloat totalH = 0;
//    for(NSDictionary *dic in _model.xiangmu_list){
////       CGSize size = GetStringSize(dic[@"title"], 14, nil, CGSizeMake(Screen_Width - 110 - 16 - 10, 100));
//        totalH += 18.5 + 16;
//    }
    _tableViewH.constant = _model.xiangmu_list.count *(18.5 + 16) ;
    [_tableView reloadData];

}
@end
