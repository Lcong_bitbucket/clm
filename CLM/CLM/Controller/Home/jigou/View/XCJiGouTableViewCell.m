//
//  XCJiGouTableViewCell.m
//  CLM
//
//  Created by cong on 16/12/31.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCJiGouTableViewCell.h"

@implementation XCJiGouTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [_zixunBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:8];
    [_telBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:8];
    [_moreBtn layoutButtonWithEdgeInsetsStyle:MKButtonEdgeInsetsStyleLeft imageTitleSpace:8];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(XCJiGouModel *)model{
    _model = model;
    [_icon setImageURLStr:ImgUrl(_model.img_url) placeholder:Default_Loading_Image_1];
    _title.text = _model.name;
    _zhaiyao.text = _model.category;
    _time.text = @"";
    _address.text = [NSString stringWithFormat:@"%@ %@",_model.province,_model.diqu];
     
    [self.renzhengScroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGFloat w = 26;
    CGFloat h = 26;
    NSMutableArray *list = _model.renzheng_list;
    if(list.count == 0){
        _rzH.constant = 0;
    } else {
        _rzH.constant = 26;
    }
    for(int i = 0; i < list.count; i++){
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(10 + i*(w+10), 0, w, h)];
        [iv setImageURLStr:ImgUrl(list[i][@"img_url"]) placeholder:Default_Loading_Image_1];
        [iv whenTapped:^{
            if(self.didClickBlock){
                self.didClickBlock(list[i]);
            }
        }];
        [self.renzhengScroll addSubview:iv];
    }
    self.renzhengScroll.contentSize = CGSizeMake(list.count * (w +10) +10, 0);
    
}

- (IBAction)zixunClick:(id)sender {
    if(self.didClickBtn){
        self.didClickBtn(0,_model);
    }
}

- (IBAction)telClick:(id)sender {
    if(self.didClickBtn){
        self.didClickBtn(1,_model);
    }
}

- (IBAction)moreClick:(id)sender {
    if(self.didClickBtn){
        self.didClickBtn(2,_model);
    }
}
@end
