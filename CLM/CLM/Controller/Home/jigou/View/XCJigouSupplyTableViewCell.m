//
//  XCJigouSupplyTableViewCell.m
//  CLM
//
//  Created by cong on 17/3/13.
//  Copyright © 2017年 聪. All rights reserved.
//

#import "XCJigouSupplyTableViewCell.h"

@implementation XCJigouSupplyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDic:(NSDictionary *)dic{
    _title.text = [NSString stringWithFormat:@"∙ %@", dic[@"title"]];
    _time.text = dic[@"ce_time"];
    _price.text = dic[@"price"];
}

@end
