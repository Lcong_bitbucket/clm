/************************************************************
 *  * Hyphenate CONFIDENTIAL
 * __________________
 * Copyright (C) 2016 Hyphenate Inc. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of Hyphenate Inc.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Hyphenate Inc.
 */

#import "ChatViewController.h"
#import "ChatPromptTableViewCell.h"
#import "EMChatViewCell.h"

#import "ChatGroupDetailViewController.h"
#import "ChatroomDetailViewController.h"
//#import "UserProfileViewController.h"
//#import "UserProfileManager.h"
#import "CustomMessageCell.h"
#import "ContactListSelectViewController.h"
#import "ChatDemoHelper.h"
#import "EMChooseViewController.h"
#import "ContactSelectionViewController.h"

#import "XCSupplysDetailViewController.h"
#import "XCDemandsDetailViewController.h"

@interface ChatViewController ()<UIAlertViewDelegate,EMClientDelegate, EMChooseViewDelegate>
{
    UIMenuItem *_copyMenuItem;
    UIMenuItem *_deleteMenuItem;
    UIMenuItem *_transpondMenuItem;
}

@property (nonatomic) BOOL isPlayingAudio;

@property (nonatomic) NSMutableDictionary *emotionDic;
@property (nonatomic, copy) EaseSelectAtTargetCallback selectedCallback;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.estimatedRowHeight = 44;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.showRefreshHeader = YES;
    self.delegate = self;
    self.dataSource = self;
    [self customBackButton];
    
    [self getUserModelWithHXUsername:self.conversation.conversationId];
    
    [self.chatBarMoreView removeItematIndex:4];
    [self.chatBarMoreView removeItematIndex:3];
    
    [[EaseBaseMessageCell appearance] setSendBubbleBackgroundImage:[[UIImage imageNamed:@"chat_sender_bg"] stretchableImageWithLeftCapWidth:5 topCapHeight:35]];
    [[EaseBaseMessageCell appearance] setRecvBubbleBackgroundImage:[[UIImage imageNamed:@"chat_receiver_bg"] stretchableImageWithLeftCapWidth:35 topCapHeight:35]];
    
 //    [self _setupBarButtonItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteAllMessages:) name:KNOTIFICATIONNAME_DELETEALLMESSAGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitGroup) name:@"ExitGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertCallMessage:) name:@"insertCallMessage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCallNotification:) name:@"callOutWithChatter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleCallNotification:) name:@"callControllerClose" object:nil];
    
    if(self.promptInfo){
        [self addPrompt:self.promptInfo];
    }
}

//根据环信获取用户信息
- (void)getUserModelWithHXUsername:(NSString *)username{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]init];
    [params setValue:@"GetInfoByUid" forKey:@"action"];
    [params setValue:username forKey:@"id"];
    
    XCBaseCommand *command = [XCBaseCommand new];
    command.api = @"/tools/submit_ajax.ashx";
    command.params = params;
    id object = [command.cache objectForKey:command.cacheKey];
    if(object){
        XCUserModel *user = [XCUserModel mj_objectWithKeyValues:object];
        self.title = user.nick_name;
        
    } else {
        [command setResponseBlock:^(NSDictionary *jsonResponse, NSError *error) {
            XCUserModel *user = [XCUserModel mj_objectWithKeyValues:jsonResponse];
            self.title = user.nick_name;
        }];
        [[XCHttpClient sharedInstance] request:command];
     }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    if (self.conversation.type == EMConversationTypeChatRoom)
    {
        //退出聊天室，删除会话
        if (self.isJoinedChatroom) {
            NSString *chatter = [self.conversation.conversationId copy];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                EMError *error = nil;
                [[EMClient sharedClient].roomManager leaveChatroom:chatter error:&error];
                if (error !=nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Leave chatroom '%@' failed [%@]", chatter, error.errorDescription] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alertView show];
                    });
                }
            });
        }
        else {
            [[EMClient sharedClient].chatManager deleteConversation:self.conversation.conversationId isDeleteMessages:YES completion:nil];
        }
    }
    
    [[EMClient sharedClient] removeDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.conversation.type == EMConversationTypeGroupChat) {
        NSDictionary *ext = self.conversation.ext;
        if ([[ext objectForKey:@"subject"] length])
        {
            self.title = [ext objectForKey:@"subject"];
        }
        
        if (ext && ext[kHaveUnreadAtMessage] != nil)
        {
            NSMutableDictionary *newExt = [ext mutableCopy];
            [newExt removeObjectForKey:kHaveUnreadAtMessage];
            self.conversation.ext = newExt;
        }
    }
}

#pragma mark - setup subviews

- (void)_setupBarButtonItem
{
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    backButton.accessibilityIdentifier = @"back";
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.navigationItem setLeftBarButtonItem:backItem];
    
    //单聊
    if (self.conversation.type == EMConversationTypeChat) {
        UIButton *clearButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        clearButton.accessibilityIdentifier = @"clear_message";
        [clearButton setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        [clearButton addTarget:self action:@selector(deleteAllMessages:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:clearButton];
    }
    else{//群聊
        UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        detailButton.accessibilityIdentifier = @"detail";
        [detailButton setImage:[UIImage imageNamed:@"group_detail"] forState:UIControlStateNormal];
        [detailButton addTarget:self action:@selector(showGroupDetailAction) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:detailButton];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.cancelButtonIndex != buttonIndex) {
        self.messageTimeIntervalTag = -1;
        [self.conversation deleteAllMessages:nil];
        [self.dataArray removeAllObjects];
        [self.messsagesSource removeAllObjects];
        
        [self.tableView reloadData];
    }
}

- (UITableViewCell *)messageViewController:(UITableView *)tableView cellForMessageModel:(id<IMessageModel>)model
{
    if (model.bodyType == EMMessageBodyTypeText || model.bodyType == EMMessageBodyTypeVoice) {
        if(model.message.ext[@"promptCell"]){
             NSString *identifier = @"ChatPromptTableViewCell";
            ChatPromptTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell)
            {
                [tableView registerNib:[UINib nibWithNibName:identifier bundle:nil] forCellReuseIdentifier:identifier];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            }
             cell.dic = model.message.ext[@"promptCell"];
            WEAKSELF;
            [cell setSendPromptBlock:^(NSDictionary *dic) {
                [weakSelf sendPrompt:dic];
            }];
            return cell;
        } else if(model.message.ext[@"prompt"]){
            NSString *cellIdentifier = [EMChatViewCell cellIdentifierForMessageModel:model];
            EMChatViewCell *cell = (EMChatViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [[EMChatViewCell alloc] initWithMessageModel:model reuseIdentifier:cellIdentifier];
                cell.backgroundColor = [UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.model = model;
            [cell setDidSelectAvatarBlock:^(id<IMessageModel>m) {
                
            }];
            WEAKSELF;
            [cell setDidSelectedBubbleBlock:^(id<IMessageModel>m) {
                if([m.message.ext[@"prompt"][@"channel"] integerValue] == 0){
                    XCSupplysDetailViewController *vc = [[XCSupplysDetailViewController alloc] init];
                    vc.id = [m.message.ext[@"prompt"][@"id"] integerValue];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                } else if([m.message.ext[@"prompt"][@"channel"] integerValue] == 1){
                    XCDemandsDetailViewController *vc = [[XCDemandsDetailViewController alloc] init];
                    vc.id = [m.message.ext[@"prompt"][@"id"] integerValue];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                } else if([m.message.ext[@"prompt"][@"channel"] integerValue] == 2) {
                    XCJiGouDetailViewController *vc = [[XCJiGouDetailViewController alloc] init];
                    vc.id = [m.message.ext[@"prompt"][@"id"] integerValue];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                }
            }];
            return cell;
        } else {
            NSString *CellIdentifier = [CustomMessageCell cellIdentifierWithModel:model];
            //发送cell
            CustomMessageCell *sendCell = (CustomMessageCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            // Configure the cell...
            if (sendCell == nil) {
                sendCell = [[CustomMessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier model:model];
                sendCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            sendCell.model = model;
            return sendCell;
        }
        
    }
    return nil;
}

- (CGFloat)messageViewController:(EaseMessageViewController *)viewController
           heightForMessageModel:(id<IMessageModel>)messageModel
                   withCellWidth:(CGFloat)cellWidth
{
     if (messageModel.bodyType == EMMessageBodyTypeText) {
        if(messageModel.message.ext[@"promptCell"])
            return UITableViewAutomaticDimension;
        else if(messageModel.message.ext[@"prompt"]){
            CGFloat  h = 140;
            return h;
        }
        return [CustomMessageCell cellHeightWithModel:messageModel];
    }
    return 0.f;
}

#pragma mark - EaseMessageViewControllerDelegate

- (BOOL)messageViewController:(EaseMessageViewController *)viewController
   canLongPressRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)messageViewController:(EaseMessageViewController *)viewController
   didLongPressRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.dataArray objectAtIndex:indexPath.row];
    if (![object isKindOfClass:[NSString class]]) {
        EaseMessageCell *cell = (EaseMessageCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        [cell becomeFirstResponder];
        self.menuIndexPath = indexPath;
        [self showMenuViewController:cell.bubbleView andIndexPath:indexPath messageType:cell.model.bodyType];
    }
    return YES;
}

- (void)messageViewController:(EaseMessageViewController *)viewController
   didSelectAvatarMessageModel:(id<IMessageModel>)messageModel
{
//    UserProfileViewController *userprofile = [[UserProfileViewController alloc] initWithUsername:messageModel.message.from];
//    [self.navigationController pushViewController:userprofile animated:YES];
}

- (void)messageViewController:(EaseMessageViewController *)viewController
               selectAtTarget:(EaseSelectAtTargetCallback)selectedCallback
{
    _selectedCallback = selectedCallback;
    EMGroup *chatGroup = nil;
    NSArray *groupArray = [[EMClient sharedClient].groupManager getJoinedGroups];
    for (EMGroup *group in groupArray) {
        if ([group.groupId isEqualToString:self.conversation.conversationId]) {
            chatGroup = group;
            break;
        }
    }
    
    if (chatGroup == nil) {
        chatGroup = [EMGroup groupWithId:self.conversation.conversationId];
    }
    
    if (chatGroup) {
        if (!chatGroup.occupants) {
            __weak ChatViewController* weakSelf = self;
            [self showHudInView:self.view hint:@"Fetching group members..."];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                EMError *error = nil;
                EMGroup *group = [[EMClient sharedClient].groupManager fetchGroupInfo:chatGroup.groupId includeMembersList:YES error:&error];
                dispatch_async(dispatch_get_main_queue(), ^{
                    __strong ChatViewController *strongSelf = weakSelf;
                    if (strongSelf) {
                        [strongSelf hideHud];
                        if (error) {
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[NSString stringWithFormat:@"Fetching group members failed [%@]", error.errorDescription] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                            [alertView show];
                        }
                        else {
                            NSMutableArray *members = [group.occupants mutableCopy];
                            NSString *loginUser = [EMClient sharedClient].currentUsername;
                            if (loginUser) {
                                [members removeObject:loginUser];
                            }
                            if (![members count]) {
                                if (strongSelf.selectedCallback) {
                                    strongSelf.selectedCallback(nil);
                                }
                                return;
                            }
                            ContactSelectionViewController *selectController = [[ContactSelectionViewController alloc] initWithContacts:members];
                            selectController.mulChoice = NO;
                            selectController.delegate = self;
                            [self.navigationController pushViewController:selectController animated:YES];
                        }
                    }
                });
            });
        }
        else {
            NSMutableArray *members = [chatGroup.occupants mutableCopy];
            NSString *loginUser = [EMClient sharedClient].currentUsername;
            if (loginUser) {
                [members removeObject:loginUser];
            }
            if (![members count]) {
                if (_selectedCallback) {
                    _selectedCallback(nil);
                }
                return;
            }
            ContactSelectionViewController *selectController = [[ContactSelectionViewController alloc] initWithContacts:members];
            selectController.mulChoice = NO;
            selectController.delegate = self;
            [self.navigationController pushViewController:selectController animated:YES];
        }
    }
}

#pragma mark - EaseMessageViewControllerDataSource

- (id<IMessageModel>)messageViewController:(EaseMessageViewController *)viewController
                           modelForMessage:(EMMessage *)message
{

    XCLog(@"ext = %@",message.ext);
    id<IMessageModel> model = nil;
    model = [[EaseMessageModel alloc] initWithMessage:message];
    if(model.isSender){
//        XCUserModel *user = [XCUserManager sharedInstance].userModel;
//        NSMutableDictionary *ext = [[NSMutableDictionary alloc] initWithDictionary:message.ext];
//        [ext setValue:ImgUrl(user.avatar)forKey:@"avatar"];
//        [ext setValue:user.nick_name forKey:@"nick_name"];
//        
//        model.message.ext = ext;
        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];
        model.avatarURLPath = message.ext[@"avatar"];
        model.nickname = message.ext[@"nick_name"];
        model.failImageName = @"imageDownloadFail";
    } else {
        
        model.avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];
         model.avatarURLPath = message.ext[@"avatar"];
        model.nickname = message.ext[@"nick_name"];
        model.failImageName = @"imageDownloadFail";
    }
  
    return model;
}

- (NSArray*)emotionFormessageViewController:(EaseMessageViewController *)viewController
{
    NSMutableArray *emotions = [NSMutableArray array];
    for (NSString *name in [EaseEmoji allEmoji]) {
        EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:name emotionThumbnail:name emotionOriginal:name emotionOriginalURL:@"" emotionType:EMEmotionDefault];
        [emotions addObject:emotion];
    }
    EaseEmotion *temp = [emotions objectAtIndex:0];
    EaseEmotionManager *managerDefault = [[EaseEmotionManager alloc] initWithType:EMEmotionDefault emotionRow:3 emotionCol:7 emotions:emotions tagImage:[UIImage imageNamed:temp.emotionId]];
    
    NSMutableArray *emotionGifs = [NSMutableArray array];
    _emotionDic = [NSMutableDictionary dictionary];
    NSArray *names = @[@"icon_002",@"icon_007",@"icon_010",@"icon_012",@"icon_013",@"icon_018",@"icon_019",@"icon_020",@"icon_021",@"icon_022",@"icon_024",@"icon_027",@"icon_029",@"icon_030",@"icon_035",@"icon_040"];
    int index = 0;
    for (NSString *name in names) {
        index++;
        EaseEmotion *emotion = [[EaseEmotion alloc] initWithName:[NSString stringWithFormat:@"[示例%d]",index] emotionId:[NSString stringWithFormat:@"em%d",(1000 + index)] emotionThumbnail:[NSString stringWithFormat:@"%@_cover",name] emotionOriginal:[NSString stringWithFormat:@"%@.gif",name] emotionOriginalURL:@"" emotionType:EMEmotionGif];
        [emotionGifs addObject:emotion];
        [_emotionDic setObject:emotion forKey:[NSString stringWithFormat:@"em%d",(1000 + index)]];
    }
    EaseEmotionManager *managerGif= [[EaseEmotionManager alloc] initWithType:EMEmotionGif emotionRow:2 emotionCol:4 emotions:emotionGifs tagImage:[UIImage imageNamed:@"icon_002_cover"]];
    
    return @[managerDefault,managerGif];
}

- (BOOL)isEmotionMessageFormessageViewController:(EaseMessageViewController *)viewController
                                    messageModel:(id<IMessageModel>)messageModel
{
    BOOL flag = NO;
    if ([messageModel.message.ext objectForKey:MESSAGE_ATTR_IS_BIG_EXPRESSION]) {
        return YES;
    }
    return flag;
}

- (EaseEmotion*)emotionURLFormessageViewController:(EaseMessageViewController *)viewController
                                      messageModel:(id<IMessageModel>)messageModel
{
    NSString *emotionId = [messageModel.message.ext objectForKey:MESSAGE_ATTR_EXPRESSION_ID];
    EaseEmotion *emotion = [_emotionDic objectForKey:emotionId];
    if (emotion == nil) {
        emotion = [[EaseEmotion alloc] initWithName:@"" emotionId:emotionId emotionThumbnail:@"" emotionOriginal:@"" emotionOriginalURL:@"" emotionType:EMEmotionGif];
    }
    return emotion;
}

- (NSDictionary*)emotionExtFormessageViewController:(EaseMessageViewController *)viewController
                                        easeEmotion:(EaseEmotion*)easeEmotion
{
//    return @{MESSAGE_ATTR_EXPRESSION_ID:easeEmotion.emotionId,MESSAGE_ATTR_IS_BIG_EXPRESSION:@(YES)};
    
        return @{EASEUI_EMOTION_DEFAULT_EXT:easeEmotion.emotionOriginal,MESSAGE_ATTR_IS_BIG_EXPRESSION:@(YES)};
}

- (void)messageViewControllerMarkAllMessagesAsRead:(EaseMessageViewController *)viewController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setupUnreadMessageCount" object:nil];
}

#pragma mark - 昵称 头像
#pragma mark - 发送文本消息，对消息进行扩展
//- (void)sendTextMessage:(NSString *)textMessage{
//    NSLog(@"发送文本消息");
// 
//    XCUserModel *user = [XCUserManager sharedInstance].userModel;
//    NSMutableDictionary *ext = [NSMutableDictionary new];
//    [ext setValue:ImgUrl(user.avatar) forKey:@"avatar"];
//    [ext setValue:user.nick_name forKey:@"nick_name"];
//    
//    EMMessage *message = [EaseSDKHelper sendTextMessage:textMessage to:self.conversation.conversationId messageType:self.conversation.type messageExt:ext];
//
//    [self sendTextMessage:textMessage withExt:ext];
//
//}

//- (void)sendTextMessage:(NSString *)text withExt:(NSDictionary*)ext
//{
//    EMMessage *message = [EaseSDKHelper sendTextMessage:text
//                                                     to:self.conversation.conversationId
//                                            messageType:self.conversation.type
//                                             messageExt:ext];
//    [super _sendMessage:message];
//}

#pragma mark - 发送图片消息

//- (void)sendImageMessage:(UIImage *)image{
//    NSLog(@"发送图片消息");
//    XCUserModel *user = [XCUserManager sharedInstance].userModel;
//    NSMutableDictionary *ext = [NSMutableDictionary new];
//    [ext setValue:ImgUrl(user.avatar) forKey:@"avatar"];
//    [ext setValue:user.nick_name forKey:@"nick_name"];
//    
//     EMMessage *message = [EaseSDKHelper sendImageMessageWithImage:image to:self.conversation.conversationId messageType:self.conversation.type messageExt:ext];
//    
//    [super _sendMessage:message];
//
// }

#pragma mark - 发送位置消息
//- (void)sendLocationMessageLatitude:(double)latitude
//                          longitude:(double)longitude
//                         andAddress:(NSString *)address{
//    NSLog(@"发送位置消息");
//    XCUserModel *user = [XCUserManager sharedInstance].userModel;
//    NSMutableDictionary *ext = [NSMutableDictionary new];
//    [ext setValue:ImgUrl(user.avatar) forKey:@"avatar"];
//    [ext setValue:user.nick_name forKey:@"nick_name"];
//
//    
//    EMMessage *message = [EaseSDKHelper sendLocationMessageWithLatitude:latitude
//                                                              longitude:longitude
//                                                                address:address
//                                                                     to:self.conversation.conversationId
//                                                            messageType:self.conversation.type
//                                                              messageExt:ext];
//    
//    [super _sendMessage:message];
//    
//}

#pragma mark - 发送语音消息
//- (void)sendVoiceMessageWithLocalPath:(NSString *)localPath
//                             duration:(NSInteger)duration{
//    NSLog(@"发送语音消息");
//    XCUserModel *user = [XCUserManager sharedInstance].userModel;
//    NSMutableDictionary *ext = [NSMutableDictionary new];
//    [ext setValue:ImgUrl(user.avatar) forKey:@"avatar"];
//    [ext setValue:user.nick_name forKey:@"nick_name"];
//  
//    EMMessage *message = [EaseSDKHelper sendVoiceMessageWithLocalPath:localPath duration:duration to:self.conversation.conversationId messageType:self.conversation.type messageExt:ext];
//    [super _sendMessage:message];
//
//}

#pragma mark - 发送视频消息
//- (void)sendVideoMessageWithURL:(NSURL *)url{
//    NSLog(@"发送视频消息");
//    XCUserModel *user = [XCUserManager sharedInstance].userModel;
//    NSMutableDictionary *ext = [NSMutableDictionary new];
//    [ext setValue:ImgUrl(user.avatar) forKey:@"avatar"];
//    [ext setValue:user.nick_name forKey:@"nick_name"];
//
//    
//    EMMessage *message = [EaseSDKHelper sendVideoMessageWithURL:url
//                                                             to:self.conversation.conversationId
//                                                    messageType:self.conversation.type
//                                                      messageExt:ext ];
//    
//    [super _sendMessage:message];
//
//}


#pragma mark - EaseMob

#pragma mark - EMClientDelegate

- (void)userAccountDidLoginFromOtherDevice
{
    if ([self.imagePicker.mediaTypes count] > 0 && [[self.imagePicker.mediaTypes objectAtIndex:0] isEqualToString:(NSString *)kUTTypeMovie]) {
        [self.imagePicker stopVideoCapture];
    }
}

- (void)userAccountDidRemoveFromServer
{
    if ([self.imagePicker.mediaTypes count] > 0 && [[self.imagePicker.mediaTypes objectAtIndex:0] isEqualToString:(NSString *)kUTTypeMovie]) {
        [self.imagePicker stopVideoCapture];
    }
}

- (void)userDidForbidByServer
{
    if ([self.imagePicker.mediaTypes count] > 0 && [[self.imagePicker.mediaTypes objectAtIndex:0] isEqualToString:(NSString *)kUTTypeMovie]) {
        [self.imagePicker stopVideoCapture];
    }
}

#pragma mark - action

- (void)backAction
{
    [[EMClient sharedClient].chatManager removeDelegate:self];
    [[EMClient sharedClient].roomManager removeDelegate:self];
    [[ChatDemoHelper shareHelper] setChatVC:nil];
    
    if (self.deleteConversationIfNull) {
        //判断当前会话是否为空，若符合则删除该会话
        EMMessage *message = [self.conversation latestMessage];
        if (message == nil) {
            [[EMClient sharedClient].chatManager deleteConversation:self.conversation.conversationId isDeleteMessages:NO completion:nil];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showGroupDetailAction
{
    [self.view endEditing:YES];
    if (self.conversation.type == EMConversationTypeGroupChat) {
        ChatGroupDetailViewController *detailController = [[ChatGroupDetailViewController alloc] initWithGroupId:self.conversation.conversationId];
        [self.navigationController pushViewController:detailController animated:YES];
    }
    else if (self.conversation.type == EMConversationTypeChatRoom)
    {
        ChatroomDetailViewController *detailController = [[ChatroomDetailViewController alloc] initWithChatroomId:self.conversation.conversationId];
        [self.navigationController pushViewController:detailController animated:YES];
    }
}

- (void)deleteAllMessages:(id)sender
{
    if (self.dataArray.count == 0) {
        [self showHint:NSLocalizedString(@"message.noMessage", @"no messages")];
        return;
    }
    
    if ([sender isKindOfClass:[NSNotification class]]) {
        NSString *groupId = (NSString *)[(NSNotification *)sender object];
        BOOL isDelete = [groupId isEqualToString:self.conversation.conversationId];
        if (self.conversation.type != EMConversationTypeChat && isDelete) {
            self.messageTimeIntervalTag = -1;
            [self.conversation deleteAllMessages:nil];
            [self.messsagesSource removeAllObjects];
            [self.dataArray removeAllObjects];
            
            [self.tableView reloadData];
            [self showHint:NSLocalizedString(@"message.noMessage", @"no messages")];
        }
    }
    else if ([sender isKindOfClass:[UIButton class]]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"sureToDelete", @"please make sure to delete") delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
        [alertView show];
    }
}

- (void)transpondMenuAction:(id)sender
{
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.dataArray objectAtIndex:self.menuIndexPath.row];
        ContactListSelectViewController *listViewController = [[ContactListSelectViewController alloc] initWithNibName:nil bundle:nil];
        listViewController.messageModel = model;
        [listViewController tableViewDidTriggerHeaderRefresh];
        [self.navigationController pushViewController:listViewController animated:YES];
    }
    self.menuIndexPath = nil;
}

- (void)copyMenuAction:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.dataArray objectAtIndex:self.menuIndexPath.row];
        pasteboard.string = model.text;
    }
    
    self.menuIndexPath = nil;
}

- (void)deleteMenuAction:(id)sender
{
    if (self.menuIndexPath && self.menuIndexPath.row > 0) {
        id<IMessageModel> model = [self.dataArray objectAtIndex:self.menuIndexPath.row];
        NSMutableIndexSet *indexs = [NSMutableIndexSet indexSetWithIndex:self.menuIndexPath.row];
        NSMutableArray *indexPaths = [NSMutableArray arrayWithObjects:self.menuIndexPath, nil];
        
        [self.conversation deleteMessageWithId:model.message.messageId error:nil];
        [self.messsagesSource removeObject:model.message];
        
        if (self.menuIndexPath.row - 1 >= 0) {
            id nextMessage = nil;
            id prevMessage = [self.dataArray objectAtIndex:(self.menuIndexPath.row - 1)];
            if (self.menuIndexPath.row + 1 < [self.dataArray count]) {
                nextMessage = [self.dataArray objectAtIndex:(self.menuIndexPath.row + 1)];
            }
            if ((!nextMessage || [nextMessage isKindOfClass:[NSString class]]) && [prevMessage isKindOfClass:[NSString class]]) {
                [indexs addIndex:self.menuIndexPath.row - 1];
                [indexPaths addObject:[NSIndexPath indexPathForRow:(self.menuIndexPath.row - 1) inSection:0]];
            }
        }
        
        [self.dataArray removeObjectsAtIndexes:indexs];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
        if ([self.dataArray count] == 0) {
            self.messageTimeIntervalTag = -1;
        }
    }
    
    self.menuIndexPath = nil;
}

#pragma mark - notification
- (void)exitGroup
{
    [self.navigationController popToViewController:self animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)insertCallMessage:(NSNotification *)notification
{
    id object = notification.object;
    if (object) {
        EMMessage *message = (EMMessage *)object;
        [self addMessageToDataSource:message progress:nil];
        [[EMClient sharedClient].chatManager importMessages:@[message] completion:nil];
    }
}

- (void)handleCallNotification:(NSNotification *)notification
{
    id object = notification.object;
    if ([object isKindOfClass:[NSDictionary class]]) {
        //开始call
        self.isViewDidAppear = NO;
    } else {
        //结束call
        self.isViewDidAppear = YES;
    }
}

#pragma mark - private

- (void)showMenuViewController:(UIView *)showInView
                   andIndexPath:(NSIndexPath *)indexPath
                    messageType:(EMMessageBodyType)messageType
{
    if (self.menuController == nil) {
        self.menuController = [UIMenuController sharedMenuController];
    }
    
    if (_deleteMenuItem == nil) {
        _deleteMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"delete", @"Delete") action:@selector(deleteMenuAction:)];
    }
    
    if (_copyMenuItem == nil) {
        _copyMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"copy", @"Copy") action:@selector(copyMenuAction:)];
    }
    
    if (_transpondMenuItem == nil) {
        _transpondMenuItem = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"transpond", @"Transpond") action:@selector(transpondMenuAction:)];
    }
    
    if (messageType == EMMessageBodyTypeText) {
        [self.menuController setMenuItems:@[_copyMenuItem, _deleteMenuItem,_transpondMenuItem]];
    } else if (messageType == EMMessageBodyTypeImage){
        [self.menuController setMenuItems:@[_deleteMenuItem,_transpondMenuItem]];
    } else {
        [self.menuController setMenuItems:@[_deleteMenuItem]];
    }
    [self.menuController setTargetRect:showInView.frame inView:showInView.superview];
    [self.menuController setMenuVisible:YES animated:YES];
}


#pragma mark - EMChooseViewDelegate

- (BOOL)viewController:(EMChooseViewController *)viewController didFinishSelectedSources:(NSArray *)selectedSources
{
    if ([selectedSources count]) {
        EaseAtTarget *target = [[EaseAtTarget alloc] init];
        target.userId = selectedSources.firstObject;
//        UserProfileEntity *profileEntity = [[UserProfileManager sharedInstance] getUserProfileByUsername:target.userId];
//        if (profileEntity) {
//            target.nickname = profileEntity.nickname == nil ? profileEntity.username : profileEntity.nickname;
//        }
        XCUserModel *user = [[XCUserManager sharedInstance] getUserModelWithHXUsername:target.userId];
        if(user){
            target.nickname = user.nick_name;
        }
        if (_selectedCallback) {
            _selectedCallback(target);
        }
    }
    else {
        if (_selectedCallback) {
            _selectedCallback(nil);
        }
    }
    return YES;
}

- (void)viewControllerDidSelectBack:(EMChooseViewController *)viewController
{
    if (_selectedCallback) {
        _selectedCallback(nil);
    }
}

#pragma mark - 自定义消息
//- (EMMessage *)getPrompt{
//    
//    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:@""];
//    NSString *from = [[EMClient sharedClient] currentUsername];
//    //生成Message
//    EMMessage *message = [[EMMessage alloc] initWithConversationID:self.conversation.conversationId from:from to:self.conversation.conversationId body:body ext:@{@"prompt":self.promptInfo}];
//    message.chatType = EMChatTypeChat;
//    if (self.conversation.type == EMConversationTypeChat) {
//        message.chatType = EMConversationTypeChat;
//    } else if (self.conversation.type == EMConversationTypeGroupChat) {
//        message.chatType = EMConversationTypeGroupChat;
//    }
//    return message;
//}

//发送提示信息
- (void)sendPrompt:(NSDictionary *)dic{
//    EMMessage *message = [self getSendPrompt:dic];
//    [[EMClient sharedClient].chatManager asyncSendMessage:message progress:^(int progress) {
//        
//    } completion:^(EMMessage *message, EMError *error) {
//        
//    }];
//    [self addMessageToDataSource:message progress:nil];
//    [[EMClient sharedClient].chatManager updateMessage:message];
    if(dic != nil){
        EMMessage *message = [EaseSDKHelper sendTextMessage:@"" to:self.conversation.conversationId messageType:self.conversation.type messageExt:@{@"prompt":dic}];
        
        [super _sendMessage:message];
    } else {
        showHint(@"商品信息不存在");
    }
 
}

//- (EMMessage *)getSendPrompt:(NSDictionary *)dic{
//    
//    EMTextMessageBody *body = [[EMTextMessageBody alloc] initWithText:@""];
//    NSString *from = [[EMClient sharedClient] currentUsername];
//    //生成Message
//    EMMessage *message = [[EMMessage alloc] initWithConversationID:self.conversation.conversationId from:from to:self.conversation.conversationId body:body ext:dic[@"prompt"]];
//    
//    if (self.conversation.type == EMConversationTypeChat) {
//        message.chatType = EMConversationTypeChat;
//    } else if (self.conversation.type == EMConversationTypeGroupChat) {
//        message.chatType = EMConversationTypeGroupChat;
//    }
//    message.ext = dic[@"prompt"];
//    return message;
//}

//添加提示消息
- (void)addPrompt:(NSDictionary *)dic{
    EMMessage *message = [EaseSDKHelper sendTextMessage:@"" to:self.conversation.conversationId messageType:self.conversation.type messageExt:@{@"promptCell":dic}];
    [self addMessageToDataSource:message progress:nil];
}

@end
