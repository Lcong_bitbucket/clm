//
//  EMChatCustomBubbleView.h
//  CustomerSystem-ios
//
//  Created by dhc on 15/3/30.
//  Copyright (c) 2015年 easemob. All rights reserved.
//

#import "EaseBubbleView.h"

@interface EMProduceBubbleView : EaseBubbleView
{
    UIView *_bgView;
    UIImageView *_proIconView;

    UILabel *_proNameLabel;
//    UILabel *_comNameLabel;
    UILabel *_desLabel;
    UILabel *_proPriceLabel;
}
@property (nonatomic,strong) NSDictionary *dic;
- (void)updateProMargin:(UIEdgeInsets)margin;

@end
