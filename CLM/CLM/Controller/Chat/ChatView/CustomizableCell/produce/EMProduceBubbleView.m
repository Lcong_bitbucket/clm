//
//  EMChatCustomBubbleView.m
//  CustomerSystem-ios
//
//  Created by dhc on 15/3/30.
//  Copyright (c) 2015年 easemob. All rights reserved.
//

#import "EMProduceBubbleView.h"

@implementation EMProduceBubbleView

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundColor = [UIColor redColor];
        [self setupProBubbleView];
    }
    return self;
}


#pragma mark - setter
-(void)setDic:(NSDictionary *)dic{
    _dic = dic;
    [_proIconView setImageURLStr:dic[@"img_url"] placeholder:nil];
    _proNameLabel.text = dic[@"title"];
    _desLabel.text = dic[@"desc"];
    _proPriceLabel.text = dic[@"price"];
    self.backgroundImageView.hidden = NO;
}

#pragma mark - public

-(void)bubbleViewPressed:(id)sender
{
    
}

- (void)updateProMargin:(UIEdgeInsets)margin{
    if (_margin.top == margin.top && _margin.bottom == margin.bottom && _margin.left == margin.left && _margin.right == margin.right) {
        return;
    }
    _margin = margin;
    
    [self removeConstraints:self.marginConstraints];
    [self setupProContriants];
}

- (void)setupProBubbleView{
    _bgView = [[UIView alloc] init];
    _bgView.backgroundColor = [UIColor blueColor];
    [self addSubview:_bgView];
    
    _proIconView = [[UIImageView alloc] init];
    _proIconView.backgroundColor = [UIColor redColor];
    [_bgView addSubview:_proIconView];
    
    _proNameLabel = [[UILabel alloc] init];
    _proNameLabel.font = [UIFont systemFontOfSize:12.0];
    [_bgView addSubview:_proNameLabel];
    
    _proPriceLabel = [[UILabel alloc] init];
    _proPriceLabel.font = [UIFont systemFontOfSize:13.0];
    _proPriceLabel.textColor = [UIColor redColor];
    [_bgView addSubview:_proPriceLabel];
    
    _desLabel = [[UILabel alloc] init];
    _desLabel.numberOfLines = 2;
    _desLabel.font = [UIFont systemFontOfSize:13.0];
    [_bgView addSubview:_desLabel];
    
    [self setupProContriants];
}

- (void)setupProContriants{
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.backgroundImageView.mas_top).with.offset(self.margin.top);
        make.left.mas_equalTo(self.backgroundImageView.mas_left).with.offset(self.margin.left);
        make.right.mas_equalTo(self.backgroundImageView.mas_right).with.offset(self.margin.right);
        make.bottom.mas_equalTo(self.backgroundImageView.mas_bottom).with.offset(self.margin.bottom);
    }];
    
    [_proIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(8);
        make.width.mas_equalTo(60);
        make.height.mas_equalTo(60);
    }];
    
    [_proNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_proIconView.mas_top);
        make.left.mas_equalTo(_proIconView.mas_right).with.offset(8);
        make.right.mas_greaterThanOrEqualTo(8);
    }];
    
    [_proPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_proNameLabel.mas_bottom).with.offset(8);
        make.left.mas_equalTo(_proIconView.mas_right ).with.offset(8);
        make.right.mas_greaterThanOrEqualTo(8);
    }];
    
    [_desLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_proPriceLabel.mas_bottom).with.offset(8);
        make.left.mas_equalTo(_proIconView.mas_right).with.offset(8);
        make.right.mas_greaterThanOrEqualTo(8);
        make.bottom.mas_greaterThanOrEqualTo(8);
    }];
}


@end
