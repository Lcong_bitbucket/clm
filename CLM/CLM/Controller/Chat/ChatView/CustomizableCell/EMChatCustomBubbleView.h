//
//  EMChatCustomBubbleView.h
//  CustomerSystem-ios
//
//  Created by dhc on 15/3/30.
//  Copyright (c) 2015年 easemob. All rights reserved.
//

#import "EMChatBaseBubbleView.h"

@interface EMChatCustomBubbleView : EMChatBaseBubbleView
{
    UILabel *_topLabel;
    UILabel *_titleLabel;
    UILabel *_descLabel;
    UIImageView *_cimageView;
    UILabel *_priceLabel;
}


@end
