//
//  ChatPromptTableViewCell.h
//  xcwl
//
//  Created by 聪 on 16/5/4.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatPromptTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconW;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconL;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
- (IBAction)sendEvetn:(id)sender;
@property (nonatomic,copy) void (^sendPromptBlock)(NSDictionary *dic);
@property (nonatomic,copy) void (^didSelectedBlock)(NSDictionary *dic);
+ (CGFloat)cellHeightWithModel:(id<IMessageModel>)model;
@property (nonatomic,strong) NSDictionary *dic;
@end
