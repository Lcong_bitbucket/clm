//
//  ChatPromptTableViewCell.m
//  xcwl
//
//  Created by 聪 on 16/5/4.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "ChatPromptTableViewCell.h"

@implementation ChatPromptTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self whenTapped:^{
        if(self.didSelectedBlock){
            self.didSelectedBlock(self.dic);
        }
    }];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDic:(NSDictionary *)dic{
    _dic = dic;
  
    
     switch ([_dic[@"channel"] integerValue]) {
        case 0:
        {
            _title.text = _dic[@"title"];
            _content.text = _dic[@"content"];
            _price.text = [NSString stringWithFormat:@"%@",_dic[@"price"]];
            NSArray *imgs = [_dic[@"img_url"] componentsSeparatedByString:@","];
            if([[imgs objectAtIndex:0] length] == 0){
                _iconW.constant = 0;
                _iconL.constant = 0;
            } else {
                _iconW.constant = 80;
                _iconL.constant = 8;
                [_icon setImageURLStr:[imgs objectAtIndex:0] placeholder:nil];
            }
             [_sendBtn setTitle:@"发送项目信息" forState:UIControlStateNormal];
        }
            break;
        case 1:
        {
            _title.text = _dic[@"title"];
            _content.text = _dic[@"content"];
            _price.text = [NSString stringWithFormat:@"%@",_dic[@"price"]];
            NSArray *imgs = [_dic[@"img_url"] componentsSeparatedByString:@","];
            if([[imgs objectAtIndex:0] length] == 0){
                _iconW.constant = 0;
                _iconL.constant = 0;
            } else {
                _iconW.constant = 80;
                _iconL.constant = 8;
                [_icon setImageURLStr:[imgs objectAtIndex:0] placeholder:nil];
            }
             [_sendBtn setTitle:@"发送需求信息" forState:UIControlStateNormal];
        }
            break;
        case 2:
        {
            _title.text = _dic[@"title"];
            _content.text = _dic[@"content"];
            _price.text = [NSString stringWithFormat:@"%@",_dic[@"price"]];
            NSArray *imgs = [_dic[@"img_url"] componentsSeparatedByString:@","];
            if([[imgs objectAtIndex:0] length] == 0){
                _iconW.constant = 0;
                _iconL.constant = 0;
            } else {
                _iconW.constant = 80;
                _iconL.constant = 8;
                [_icon setImageURLStr:[imgs objectAtIndex:0] placeholder:nil];
            }
            [_sendBtn setTitle:@"发送机构信息" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    
}

//+ (CGFloat)cellHeightWithModel:(id<IMessageModel>)model
//{
//     return 130;
//}

- (IBAction)sendEvetn:(id)sender {
    if(self.sendPromptBlock){
        self.sendPromptBlock(self.dic);
    }
}

@end
