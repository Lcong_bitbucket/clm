//
//  EMChatCustomBubbleView.m
//  CustomerSystem-ios
//
//  Created by dhc on 15/3/30.
//  Copyright (c) 2015年 easemob. All rights reserved.
//

#import "EMChatCustomBubbleView.h"

#define kImageWidth 70
#define kImageHeight 70
#define kTitleHeight 20

@implementation EMChatCustomBubbleView


- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        _topLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
        _topLabel.font = [UIFont systemFontOfSize:12.0];
        _topLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_topLabel];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_topLabel.frame) + 5, 100, 0)];
        _titleLabel.font = [UIFont systemFontOfSize:14.0];
        _titleLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_titleLabel];
        
        _cimageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_titleLabel.frame) + 5, kImageWidth, kImageHeight)];
        [self addSubview:_cimageView];
        
        _descLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_titleLabel.frame) + 5, 120, 35)];
        _descLabel.numberOfLines = 2;
        _descLabel.textColor = [UIColor grayColor];
        _descLabel.font = [UIFont systemFontOfSize:13.0];
        _descLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:_descLabel];
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_descLabel.frame), 120, 15)];
        _priceLabel.font = [UIFont systemFontOfSize:13.0];
        _priceLabel.backgroundColor = [UIColor clearColor];
        _priceLabel.textColor = [UIColor redColor];
        [self addSubview:_priceLabel];
    }
    return self;
}

-(CGSize)sizeThatFits:(CGSize)size
{
    CGFloat width = 3 * BUBBLE_VIEW_PADDING + kImageWidth + 120 + 30;
    CGFloat height = 2 * BUBBLE_VIEW_PADDING + kImageHeight + 20;
    
    return CGSizeMake(width, height);
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    if([self.model.message.ext objectForKey:@"prompt"]){
        NSDictionary *dic = [self.model.message.ext objectForKey:@"prompt"];
        NSArray *imgs = [dic[@"img_url"] componentsSeparatedByString:@","];
        if([[imgs objectAtIndex:0] length] == 0){
            _topLabel.frame = CGRectMake(10, 5, self.frame.size.width - 20, 20);
            _cimageView.frame = CGRectMake(10, CGRectGetMaxY(_topLabel.frame) + 5, 0, 0);
            _titleLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame), CGRectGetMaxY(_topLabel.frame) , 150, 20);
            _descLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) , CGRectGetMaxY(_titleLabel.frame) , 150, 40);
            _priceLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame), CGRectGetMaxY(_descLabel.frame) , 150, 15);
        } else {
            _topLabel.frame = CGRectMake(10, 5, self.frame.size.width - 20, 20);
            _cimageView.frame = CGRectMake(10, CGRectGetMaxY(_topLabel.frame) + 5, kImageWidth, kImageHeight);
            _titleLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_topLabel.frame) , 150, 20);
            _descLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_titleLabel.frame) , 150, 40);
            _priceLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_descLabel.frame) , 150, 15);
        }
    }
//    else if([self.model.message.ext objectForKey:@"need"]){
//        NSDictionary *dic = [self.model.message.ext objectForKey:@"need"];
//        NSArray *imgs = [dic[@"desc_pics"] componentsSeparatedByString:@","];
//        if([[imgs objectAtIndex:0] length] == 0){
//            _topLabel.frame = CGRectMake(10, 5, self.frame.size.width - 20, 20);
//            _cimageView.frame = CGRectMake(10, CGRectGetMaxY(_topLabel.frame) + 5, 0, 0);
//            _titleLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame), CGRectGetMaxY(_topLabel.frame) , self.frame.size.width - 20, 20);
//            _descLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame), CGRectGetMaxY(_titleLabel.frame) , self.frame.size.width - 20, 40);
//            _priceLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame), CGRectGetMaxY(_descLabel.frame) , self.frame.size.width - 20, 15);
//        } else {
//            _topLabel.frame = CGRectMake(10, 5, self.frame.size.width - 20, 20);
//            _cimageView.frame = CGRectMake(10, CGRectGetMaxY(_topLabel.frame) + 5, kImageWidth, kImageHeight);
//            _titleLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_topLabel.frame) , self.frame.size.width - 20 - 70, 20);
//            _descLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_titleLabel.frame) , self.frame.size.width - 20 - 70, 40);
//            _priceLabel.frame = CGRectMake(CGRectGetMaxX(_cimageView.frame) + 5, CGRectGetMaxY(_descLabel.frame) , self.frame.size.width - 20 - 70, 15);
//        }
//    }
}

#pragma mark - setter

- (void)setModel:(id<IMessageModel>)model{
    [super setModel:model];
    NSDictionary *prompt = model.message.ext[@"prompt"];
    switch ([prompt[@"channel"] integerValue]) {
        case 0:
        {
            _topLabel.text = @"项目信息:";
            _titleLabel.text = [prompt objectForKey:@"title"];
            _descLabel.text = [prompt objectForKey:@"content"];
            _priceLabel.text = [NSString stringWithFormat:@"%@",prompt[@"price"] ];
            
            NSArray *imgs = [prompt[@"img_url"] componentsSeparatedByString:@","];
            if([[imgs objectAtIndex:0] length] == 0){
                
            } else {
                [_cimageView setImageURLStr:imgs[0] placeholder:nil];
            }
        }
            break;
        case 1:
        {
            _topLabel.text = @"需求信息:";
            _titleLabel.text = [prompt objectForKey:@"title"];
            _descLabel.text = [prompt objectForKey:@"content"];
            _priceLabel.text = [NSString stringWithFormat:@"%@",prompt[@"price"] ];
            
            NSArray *imgs = [prompt[@"img_url"] componentsSeparatedByString:@","];
            if([[imgs objectAtIndex:0] length] == 0){
                
            } else {
                [_cimageView setImageURLStr:imgs[0] placeholder:nil];
            }
        }
            break;
        case 2:
        {
            _topLabel.text = @"机构信息:";
            _titleLabel.text = [prompt objectForKey:@"title"];
            _descLabel.text = [prompt objectForKey:@"content"];
            _priceLabel.text = [NSString stringWithFormat:@"%@",prompt[@"price"] ];
            
            NSArray *imgs = [prompt[@"img_url"] componentsSeparatedByString:@","];
            if([[imgs objectAtIndex:0] length] == 0){
                
            } else {
                [_cimageView setImageURLStr:imgs[0] placeholder:nil];
            }
        }
            break;
        default:
            break;
    }
   

}





@end
