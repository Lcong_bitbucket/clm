//
//  XCWKWebViewController.m
//  xcwl
//
//  Created by 聪 on 16/7/14.
//  Copyright © 2016年 聪. All rights reserved.
//


#import "XCWKWebViewController.h"
#import "AttachListViewController.h"

#import "TUSafariActivity.h"
#import "ARChromeActivity.h"

static void *KINWebBrowserContext = &KINWebBrowserContext;

@interface XCWKWebViewController () <UIAlertViewDelegate,WKNavigationDelegate,UIScrollViewDelegate>
{
    NSMutableArray *imageArray;
}

@end

@implementation XCWKWebViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isShowIMG = NO;
    self.isShowHtmlTitle = YES;
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.allowsInlineMediaPlayback = YES;
    config.mediaPlaybackRequiresUserAction = NO;
    self.webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    
    [self customBackButton];
    if(self.webView) {
        [self.webView setFrame:CGRectMake(0, 0, Screen_Width, Screen_Height - 64)];
        [self.webView setNavigationDelegate:self];
        [self.webView setUIDelegate:self];
        [self.webView setMultipleTouchEnabled:YES];
        self.webView.scrollView.delegate = self;
        [self.webView.scrollView setAlwaysBounceVertical:YES];
        [self.view addSubview:self.webView];
        
        self.attachBtn = [[UIButton alloc] initWithFrame:CGRectMake(Screen_Width , Screen_Height , 39, 39)];
        [self.attachBtn setImage:imageNamed(@"download") forState:UIControlStateNormal];
        self.attachBtn.hidden = YES;
        [self.attachBtn addTarget:self action:@selector(goAttachVC:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.attachBtn];
        
        _attachArray = [NSMutableArray new];
        
        [self.webView addObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress)) options:0 context:KINWebBrowserContext];
    }
    
    if (self.bridge) { return;}
    [WebViewJavascriptBridge enableLogging];
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:self.webView];
    [self.bridge setWebViewDelegate:self];
  
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    [self.progressView setTrackTintColor:[UIColor colorWithWhite:1.0f alpha:0.0f]];
    [self.progressView setFrame:CGRectMake(0, 0, Screen_Width, self.progressView.frame.size.height)];
     [self.webView addSubview:self.progressView];

}

- (void)goBack{
    if([self.webView canGoBack]){
        [self.webView goBack];
    } else {
        [super goBack];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
 
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
 
}

#pragma mark - Public Interface

- (void)loadRequest:(NSURLRequest *)request {
    if(self.webView) {
        [self.webView loadRequest:request];
    }
}

- (void)loadURL:(NSURL *)URL {
    [self loadRequest:[NSURLRequest requestWithURL:URL]];
}

- (void)loadURLString:(NSString *)URLString {
    NSURL *URL = [NSURL URLWithString:URLString];
    [self loadURL:URL];
}

- (void)loadHTMLString:(NSString *)HTMLString {
    if(self.webView) {
        [self.webView loadHTMLString:HTMLString baseURL:nil];
    }
}

- (NSMutableArray *)attachArray{
    if(!_attachArray){
        _attachArray = [NSMutableArray new];
    }
    return _attachArray;
}

#pragma mark - Attach
- (void)getAttach:(NSInteger)article_id{
    XCBaseCommand *command = [XCBaseCommand new];
    command.api =  @"/tools/submit_ajax.ashx?action=GetAttach";
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setValue:@(article_id) forKey:@"id"];
    command.params = params;
    
    [command setResponseBlock:^(NSDictionary *responseObject, NSError *error) {
        if(error){
            
        } else {
            if ([[responseObject objectForKey:@"status"] intValue]==1) {
                [self.attachArray removeAllObjects];
                [self.attachArray addObjectsFromArray:responseObject[@"ds"]];
                if(self.attachArray.count > 0){
                    [UIView animateWithDuration:0.8 delay:1 usingSpringWithDamping:0.5 initialSpringVelocity:5 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
                        self.attachBtn.hidden = NO;
                        self.attachBtn.frame = CGRectMake(self.webView.width - 50, self.webView.height - 100 , 39, 39);
                        
                    } completion:nil];
                }
            } else
            {
                showHint([responseObject objectForKey:@"desc"]);
            }
            
        }
    }];
    [[XCHttpClient sharedInstance] request:command];
}

- (void)goAttachVC:(UIButton*)btn{
    AttachListViewController *vc = [[AttachListViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    vc.dataArray = self.attachArray;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
  
//    [UIView animateWithDuration:.8 animations:^{
        self.attachBtn.hidden = YES;
        self.attachBtn.frame = CGRectMake(Screen_Width, Screen_Height, 39, 39);
//    }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(self.attachArray.count != 0 && !decelerate){
        [UIView animateWithDuration:0.8 delay:1 usingSpringWithDamping:0.5 initialSpringVelocity:5 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            self.attachBtn.hidden = NO;
            self.attachBtn.frame = CGRectMake(self.webView.width - 50, self.webView.height - 100 , 39, 39);
         } completion:nil];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(self.attachArray.count != 0){
        [UIView animateWithDuration:0.8 delay:1 usingSpringWithDamping:0.5 initialSpringVelocity:5 options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            self.attachBtn.hidden = NO;
            self.attachBtn.frame = CGRectMake(self.webView.width - 50, self.webView.height - 100 , 39, 39);
        } completion:nil];
     
    }
}

#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
  
    XCLog(@"start load web");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if(webView == self.webView) {
        if(self.isShowIMG)
            imageArray = setDefaultJSEvent(webView);
    }
    if(self.isShowHtmlTitle){
        [webView evaluateJavaScript:@"document.title" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            self.title = result;
        }];
    }
   
    XCLog(@"finish load web");

}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
      withError:(NSError *)error {
    if(webView == self.webView) {
        [webView loadHTMLString:@"加载内容失败,请重试..." baseURL:nil];
    }
    XCLog(@"fail load web");
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation
      withError:(NSError *)error {
  
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
   
        NSURL *URL = navigationAction.request.URL;
        NSString *requestString = [[URL absoluteString] stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
        XCLog(@"requestring = %@",requestString);
    
        if(![self checkCustomUrl:requestString]){
            self.NotFirstLoad = YES;
            
            if ([URL.scheme isEqualToString:@"xcshowimg"]) {
                NSString* path = [URL.absoluteString substringFromIndex:[@"xcshowimg:" length]];
                path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSInteger index = 0;
                for(int i = 0; i < imageArray.count;i++){
                    if([path isEqualToString:imageArray[i]]){
                        index = i;
                        break;
                    }
                }
                openImageWithZLPhotoLib(index,imageArray);
                decisionHandler(WKNavigationActionPolicyCancel);
                
            } else if ([[URL scheme] isEqualToString:@"tel"]) {
                NSString *resourceSpecifier = [URL resourceSpecifier];
                NSString *callPhone = [NSString stringWithFormat:@"telprompt://%@", resourceSpecifier];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone]];
                 decisionHandler(WKNavigationActionPolicyCancel);
            } else {
                    if(!navigationAction.targetFrame) {
                        [self loadURL:URL];
                        decisionHandler(WKNavigationActionPolicyCancel);
                    } else {
                        decisionHandler(WKNavigationActionPolicyAllow);
                    }
            }

        } else  {
            decisionHandler(WKNavigationActionPolicyCancel);
        }
}

- (BOOL)checkCustomUrl:(NSString *)urlString {
    return NO;
}

#pragma mark - WKUIDelegate

- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}


#pragma mark - Estimated Progress KVO (WKWebView)

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))] && object == self.webView) {
        [self.progressView setAlpha:1.0f];
        BOOL animated = self.webView.estimatedProgress > self.progressView.progress;
        [self.progressView setProgress:self.webView.estimatedProgress animated:animated];
        XCLog(@"progress = %lf",self.webView.estimatedProgress);
        // Once complete, fade out UIProgressView
        if(self.webView.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.3f delay:0.3f options:UIViewAnimationOptionCurveEaseOut animations:^{
                [self.progressView setAlpha:0.0f];
            } completion:^(BOOL finished) {
                [self.progressView setProgress:0.0f animated:NO];
            }];
        }
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - Dealloc

- (void)dealloc {
    self.webView.scrollView.delegate = nil;
    [self.webView setNavigationDelegate:nil];
    [self.webView setUIDelegate:nil];
    if ([self isViewLoaded]) {
        [self.webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    }
}


@end

