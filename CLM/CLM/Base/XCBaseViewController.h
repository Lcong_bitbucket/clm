//
//  XCBaseViewController.h
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XCBaseViewController : UIViewController
{
    
}
@property (nonatomic,assign) BOOL hiddenNavBarWhenPush;//隐藏导航栏
@property (nonatomic,assign) BOOL clearNavBar;//透明导航栏
@property (nonatomic,strong) UIColor *navColor;//设置导航栏颜色
- (void)customShareButton;
- (void)customBackButton;
- (void)customNavigationBarItemWithImageName:(NSString*)imgName title:(NSString *)title isLeft:(bool)isLeft;
- (void)customNavigationBarItems:(NSArray *)items isLeft:(bool)isLeft ;
- (void)rightBarButtonAction:(UIButton*)btn;
- (void)leftBarButtonAction:(UIButton *)btn;
- (void)goBack;
- (void)setPopver:(NSArray *)imgarr titleArr:(NSArray *)titlearr;
- (void)hiddenRightControl;//隐藏/显示下拉选项
-(void)BtnClicked:(UIButton*)btn;
@end
