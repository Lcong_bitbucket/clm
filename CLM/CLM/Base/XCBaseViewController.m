//
//  XCBaseViewController.m
//  CLM
//
//  Created by cong on 16/11/28.
//  Copyright © 2016年 聪. All rights reserved.
//

#import "XCBaseViewController.h"
#import "UINavigationBar+Awesome.h"

@interface XCBaseViewController ()
@property(nonatomic,strong)UIControl * rightControl;
@property(nonatomic,strong)UIImageView * btnimageview;
@end

@implementation XCBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
    self.navColor = [UIColor whiteColor];// UIColorFromRGB(0x0e56a8);
    [self titleColor:[UIColor blackColor]];
    self.edgesForExtendedLayout=UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars=NO;
    self.automaticallyAdjustsScrollViewInsets=NO;
    
    // Do any additional setup after loading the view.
}

- (void)titleColor:(UIColor *)color{

    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor: color,
                                                                    UITextAttributeFont : [UIFont boldSystemFontOfSize:18]};
}
 
- (void)viewWillAppear:(BOOL)animated{
    if (self.hiddenNavBarWhenPush){
        [self.navigationController setNavigationBarHidden:YES animated:animated];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }else{
        [self.navigationController setNavigationBarHidden:NO animated:animated];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.barTintColor = self.navColor;
    
    if(self.clearNavBar){
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        self.navigationController.navigationBar.translucent = YES;
        self.edgesForExtendedLayout=UIRectEdgeTop;
        [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0]];
        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    }
  
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(self.clearNavBar){
        self.edgesForExtendedLayout=UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar lt_reset];
    }
    
}

- (void)customBackButton {
    [self customNavigationBarItemWithImageName:@"back-arrow" title:nil isLeft:YES];
}
- (void)customShareButton{
    [self customNavigationBarItemWithImageName:@"icon_share" title:nil isLeft:NO];
}
//自定义样式的导航栏item-- 用图片
- (void)customNavigationBarItemWithImageName:(NSString*)imgName title:(NSString *)title isLeft:(bool)isLeft {
    
    CGFloat btnW = 0;
    CGFloat btnH = 0;
    UIImage *im = [UIImage imageNamed:imgName];
    if (imgName){
        btnW = im.size.width;
        btnH = im.size.height;
        btnW = btnW > 44 ? btnW :44;
        btnH = btnH > 44 ? btnH :44;
    }
    if(title){
        CGSize titleSize = GetStringSize(title, 15, nil, CGSizeMake(100, 30));
        btnW = titleSize.width;
        btnH = 40;
    }
    
    UIButton* jumpButton= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnW, btnH)];//微调
    [jumpButton setTitle:title forState:UIControlStateNormal];
    [jumpButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    [jumpButton setTitleColor:UIColorFromRGB(0x7e8597) forState:UIControlStateNormal];
    [jumpButton.titleLabel setTextAlignment:2];
    [jumpButton setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    if (!isLeft) {
        [jumpButton addTarget:self action:@selector(rightBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [jumpButton addTarget:self action:@selector(leftBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    UIBarButtonItem* actionItem= [[UIBarButtonItem alloc] initWithCustomView:jumpButton];
    if (isLeft) {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -15;//这个数值可以根据情况自由变化
         self.navigationItem.leftBarButtonItems = @[negativeSpacer, actionItem];
    }
    else {
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = 0;//这个数值可以根据情况自由变化
        
        self.navigationItem.rightBarButtonItems = @[actionItem];
    }
}

- (void)customNavigationBarItems:(NSArray *)items isLeft:(bool)isLeft {
    
    NSMutableArray *barBtnItems = [NSMutableArray new];
  
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -15;//这个数值可以根据情况自由变化
    [barBtnItems addObject:negativeSpacer];
   
    
    for(NSInteger i = 0; i < items.count; i++){
      NSString * imgName = items[i][@"imageName"];
        NSString *title = items[i][@"title"];
        CGFloat btnW = 0;
        CGFloat btnH = 0;
        UIImage *im = [UIImage imageNamed:imgName];
        if (imgName){
            btnW = im.size.width;
            btnH = im.size.height;
            btnW = btnW > 44 ? btnW :44;
            btnH = btnH > 44 ? btnH :44;
        }
        if(title){
            CGSize titleSize = GetStringSize(title, 15, nil, CGSizeMake(100, 30));
            btnW = titleSize.width;
            btnH = 40;
        }
        
        UIButton* jumpButton= [[UIButton alloc] initWithFrame:CGRectMake(0, 0, btnW, btnH)];//微调
        jumpButton.tag = 100 + i;
        [jumpButton setTitle:title forState:UIControlStateNormal];
        [jumpButton.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
        [jumpButton setTitleColor:UIColorFromRGB(0x7e8597) forState:UIControlStateNormal];
        [jumpButton.titleLabel setTextAlignment:2];
        [jumpButton setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
        if (!isLeft) {
            [jumpButton addTarget:self action:@selector(rightBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            [jumpButton addTarget:self action:@selector(leftBarButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        UIBarButtonItem* actionItem= [[UIBarButtonItem alloc] initWithCustomView:jumpButton];
        [barBtnItems addObject:actionItem];
       
    }
    
    if (isLeft) {
        self.navigationItem.leftBarButtonItems = barBtnItems;

    }else {
        self.navigationItem.rightBarButtonItems = barBtnItems;

    }
    
}


//导航栏右键响应函数，重写此函数，响应点击事件
- (void)rightBarButtonAction:(UIButton *)btn {
    XCLog(@"need to implement this methor");
}

- (void)leftBarButtonAction:(UIButton *)btn{
    [self goBack];
}

//返回键响应函数，重写此函数，实现返回前执行一系列操作
- (void)goBack {
    
    if([self.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark  - 下拉选项
- (void)setPopver:(NSArray *)imgarr titleArr:(NSArray *)titlearr{
    [self.rightControl removeFromSuperview];
    
    self.rightControl = [[UIControl alloc] initWithFrame:self.view.bounds];
    [self.rightControl addTarget:self action:@selector(hiddenRightControl) forControlEvents:UIControlEventTouchDown];
    self.rightControl.hidden = YES;
    [self.view addSubview:self.rightControl];
    
    self.btnimageview=[[UIImageView alloc]init];
    self.btnimageview.userInteractionEnabled=YES;
    self.btnimageview.frame=CGRectMake(Screen_Width-130,0,120,titlearr.count*45 + 5);
    self.btnimageview.image=[[UIImage imageNamed:@"bg_edit"] stretchableImageWithLeftCapWidth:self.btnimageview.mj_h/2 topCapHeight:self.btnimageview.mj_w/2];
    [self.rightControl addSubview:self.btnimageview];
    
    for(int i=0;i<titlearr.count;i++)
    {
        UIButton * btn=[[UIButton alloc] initWithFrame:CGRectMake(0,5 + i*45,120,45)];
        [btn setImage:[UIImage imageNamed:imgarr[i]] forState:UIControlStateNormal];
        [btn setTitle:titlearr[i] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
        [btn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
 
        btn.tag=200+i;
        [btn addTarget:self action:@selector(BtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if(i!=titlearr.count - 1){
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, btn.frame.size.height - 1, btn.frame.size.width, 1)];
            line.backgroundColor = UIColorFromRGB(0x333333);
            [btn addSubview:line];
        }
         
        [self.btnimageview addSubview:btn];
    }
}

- (void)hiddenRightControl{
    [self.view bringSubviewToFront:self.rightControl];
    self.rightControl.hidden=!self.rightControl.hidden;
}

-(void)BtnClicked:(UIButton*)btn
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
