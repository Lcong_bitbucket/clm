//
//  XCWKWebViewController.h
//  xcwl
//
//  Created by 聪 on 16/7/14.
//  Copyright © 2016年 聪. All rights reserved.
//



#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "XCBaseViewController.h"
#import "WebViewJavascriptBridge.h"

@class XCWKWebViewController;

@interface XCWKWebViewController : XCBaseViewController <WKNavigationDelegate, WKUIDelegate>

@property WebViewJavascriptBridge * bridge;

@property (nonatomic,assign) BOOL isShowIMG;
@property (nonatomic,assign) BOOL isShowHtmlTitle;
@property (nonatomic,assign) BOOL NotFirstLoad;
@property (nonatomic,strong) NSMutableArray *attachArray;

// The main and only UIProgressView
@property (nonatomic, strong) UIProgressView *progressView;

@property (nonatomic,strong) UIButton *attachBtn;
// The web views
// Depending on the version of iOS, one of these will be set
@property (nonatomic, strong) WKWebView *webView;
//@property (nonatomic, strong) UIWebView *uiWebView;

- (id)initWithConfiguration:(WKWebViewConfiguration *)configuration NS_AVAILABLE_IOS(8_0);


- (void)getAttach:(NSInteger)article_id;
#pragma mark - Static Initializers



// Load a NSURLURLRequest to web view
// Can be called any time after initialization
//- (void)loadRequest:(NSURLRequest *)request;
//
//// Load a NSURL to web view
//// Can be called any time after initialization
//- (void)loadURL:(NSURL *)URL;
//
//// Loads a URL as NSString to web view
//// Can be called any time after initialization
//- (void)loadURLString:(NSString *)URLString;
//
//
//// Loads an string containing HTML to web view
//// Can be called any time after initialization
//- (void)loadHTMLString:(NSString *)HTMLString;

@end


