//
//  XCTableViewController.m
//  VKTemplateForiPhone
//
//  Created by 聪 on 16/4/5.
//  Copyright © 2016年 Vescky. All rights reserved.
//

#import "XCTableViewController.h"
#import "MJChiBaoZiFooter.h"
#import "MJChiBaoZiHeader.h"
#import <AudioToolbox/AudioToolbox.h>

@interface XCTableViewController ()<UIScrollViewDelegate>{
    UIButton *_goTopBtn;
    CGFloat _lastOffset;
    CGFloat _startConstant;
}

@end

@implementation XCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (void)showGoTopBtn{
    return;
    if(!_goTopBtn){
        _goTopBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.superTableView.mj_x + self.superTableView.mj_w - 60, self.superTableView.mj_y + self.superTableView.mj_h - 60, 50, 50)];
        _goTopBtn.layer.masksToBounds = YES;
        _goTopBtn.layer.borderColor = UIColorFromRGB(0xcdcdcd).CGColor;
        _goTopBtn.layer.borderWidth = 1;
        _goTopBtn.layer.cornerRadius = 25;

        [_goTopBtn setImage:imageNamed(@"icon_goTop") forState:UIControlStateNormal];
        [_goTopBtn addTarget:self action:@selector(goTopEvent:) forControlEvents:UIControlEventTouchUpInside];
        _goTopBtn.hidden = YES;
        [self.view addSubview:_goTopBtn];
    }
    if(_goTopBtn.hidden == YES){
        [UIView animateWithDuration:1.0 animations:^{
            _goTopBtn.hidden = NO;
        }];
    }
}

- (void)hiddeGoTopBtn{
    if(_goTopBtn.hidden == NO){
        [UIView animateWithDuration:1.0 animations:^{
            _goTopBtn.hidden = YES;
        }];
    }
}

- (void)goTopEvent:(UIButton *)btn{

    [self.superTableView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //    if(scrollView.contentOffset.y > )
//    XCLog(@"y = %lf",self.superTableView.contentOffset.y);
    
    
//    if(self.scrollHiddenNav && !self.clearNavBar)
        [self didScroll:scrollView];
}


- (void)didScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    
    //是否隐藏头部多余部分
    if(!self.clearNavBar && self.scrollHiddenNav){
        
        if(_lastOffset - offsetY < 0 ){
            [self setNavigationBarTransformProgress:64];
        }
        
        if(_lastOffset - offsetY > 10 || offsetY <= 60){
            [self setNavigationBarTransformProgress:0];
            self.navigationController.navigationBar.backIndicatorImage = [UIImage new];
        }
    }
    
    //是否显示返回顶部按钮
    if(offsetY > self.superTableView.mj_h ){
        [self showGoTopBtn];

//        if(_lastOffset - offsetY < 0 ){
//            [self hiddeGoTopBtn];
//        }
//        
//        if(_lastOffset - offsetY > 10 ){
//           [self showGoTopBtn];
//        }
    } else {
        [self hiddeGoTopBtn];
    }
    //滚动即调用
    [self.view endEditing:YES];
   
//    XCLog(@"off == %lf",_lastOffset- offsetY);
    _lastOffset = offsetY;
}

- (void)setNavigationBarTransformProgress:(CGFloat)progress
{
    if(progress == 0){
        self.isHiddenNav = NO;
    } else {
        self.isHiddenNav = YES;
    }
    [self hiddenTop];
}

- (void)hiddenTop{
    
}

- (void)defaultConfig:(UITableView *)tableView{
    self.superTableView = tableView;
    self.superTableView.tableFooterView = [UIView new];
    self.superTableView.delegate = self;
    self.superTableView.estimatedRowHeight = 44.0f;
    self.superTableView.rowHeight = UITableViewAutomaticDimension;
    self.superTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)openMJRefreshHeader:(BOOL)header Footer:(BOOL)footer tableView:(UITableView *)tableView{
    self.superTableView = tableView;
    self.curPage = 1;
    self.superTableView.mj_header = nil;
    self.superTableView.mj_footer = nil;
    if(header){
        self.superTableView.mj_header = [MJChiBaoZiHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    }
    if(footer){
         self.superTableView.mj_footer = [MJChiBaoZiFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    }

}

#pragma mark - function
- (void)beginRefreshing{
    [self.superTableView.mj_header beginRefreshing];
}

- (void)loadNewData{
    self.curPage = 1;
    [self refresh];
}

- (void)loadMoreData{
    self.curPage ++;
    [self refresh];
}

- (void)refresh{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)stopRefresh:(BOOL)isLast{
    if(isLast){
        [self.superTableView.mj_header endRefreshing];
        [self.superTableView.mj_footer endRefreshingWithNoMoreData];
    } else{
        [self.superTableView.mj_footer endRefreshing];
        [self.superTableView.mj_header endRefreshing];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
