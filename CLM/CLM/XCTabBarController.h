//
//  XCTabBarController.h
//  xcwl
//
//  Created by 聪 on 16/4/14.
//  Copyright © 2016年 聪. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MYBlurIntroductionView.h"
@interface XCTabBarController : UITabBarController<MYIntroductionDelegate>
- (void)setupUnreadMessageCount;
@end
